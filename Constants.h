//
//  Constants.h
//  Museum
//
//  Created by Anju Singh on 12/8/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#ifndef Museum_Constants_h
#define Museum_Constants_h
//#define //NSLog //

//#define //NSLog //R: 252 G: 191 B: 96//R: 85 G: 96 B: 169//R: 252 G: 191 B: 96

#define kOragneColor [UIColor colorWithRed:(85.0/255.0) green:(96.0/255.0) blue:(169.0/255.0) alpha:1.0]

//#define kOragneColor [UIColor colorWithRed:(0.0/255.0) green:(172.0/255.0) blue:(220.0/255.0) alpha:1.0]
//#define kOragneColor [UIColor colorWithRed:3.0/255.0 green:157.0/255.0 blue:220.0/255.0 alpha:1.0];
#define kTextColor [UIColor darkGrayColor];
#define kHeaderTextColor [UIColor darkTextColor];


//rgb(239,109,45)

#define kBtnSelectionColor [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];;
#define kBtnUnselectedColor [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];;
//#define kBottomBtnColor [UIColor colorWithRed:(0.0/255.0) green:(172.0/255.0) blue:(220.0/255.0) alpha:1.0];
#define KTextColor [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0];
#define kgrayColor [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];

#define kgray1Color [UIColor colorWithRed:150.0/255.0 green:149.0/255.0 blue:149.0/255.0 alpha:1.0];
#define kBlueColor [UIColor colorWithRed:42.0/255.0 green:55.0/255.0 blue:68.0/255.0 alpha:1.0];
#define kgray2Color [UIColor colorWithRed:116.0/255.0 green:111.0/255.0 blue:117.0/255.0 alpha:1.0];
//#define kViewBackgroundColor [UIColor colorWithRed:232.0/255.0 green:231.0/255.0 blue:244.0/255.0 alpha:1.0];

//#define kgrayColor [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:249.0/255.0 alpha:1.0];


#define kContentViewColor  [UIColor colorWithRed:211.0/255.0 green:209.0/255.0 blue:233.0/255.0 alpha:1.0];
#define kgray3Color [UIColor colorWithRed:238.0/255.0 green:237.0/255.0 blue:246.0/255.0 alpha:1.0];
#define kHeaderbluecolor  [UIColor colorWithRed:51.0/255.0 green:61.0/255.0 blue:152.0/255.0 alpha:1.0];


//#define FontAleoSIZE    15
//#define FontAleo(s)     [UIFont fontWithName:@"Aleo-Regular" size:s]
//#define FontAleoBold(s)     [UIFont fontWithName:@"Aleo-Bold" size:s]

#define FontAleoSIZE    15
#define FontAleo(s)     [UIFont fontWithName:@"Oswald-Regular" size:s]
#define FontAleoBold(s)     [UIFont fontWithName:@"OpenSans-Bold" size:s]
#define FontAleoMedium(s)     [UIFont fontWithName:@"Raleway-Medium" size:s]

#define FontKhmerSIZE    10.0
#define FontKhmer(s)     [UIFont fontWithName:@"OpenSans" size:s]
//#define NSLog //

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


//#define kBaseUrl @"http://14.141.136.170:8888/projects/Teacherapp/"
#define kBaseUrl @"http://54.164.146.171/teacherapp.com/"

//#define kBaseUrl @"http://14.141.136.170:8888/projects/Teacherapp_lang/";

//#define kBaseUrl @"http://192.168.125.114/projects/teacherapp/"
//http://192.168.125.114/projects/teacherapp/index.php/Teachers/login
#define ACCEPTABLE_CHARACTERS @"+0123456789"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IPAD_PRO ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1366)



#if defined(IS_IPAD)
#define FontKhmerIPADSIZE  [UIFont fontWithName:@"KhmerUI" size:25]
#else
#define FontKhmerIPADSIZE  [UIFont fontWithName:@"KhmerUI" size:25]


#endif



#endif
