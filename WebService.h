//
//  WebService.h
//  Putnam Teacher's App
//
//  Created by Inficare on 5/31/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "AppDelegate.h"
@interface WebService : NSObject<NSURLSessionDelegate>
+(WebService *) sharedInstance;
-(void)SignUpApi:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void)ApiOfgetMethod:(NSString *)urlString andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void) fetchDataWithCompletionBlock:(NSString *)urlString param:(NSMutableDictionary *)param andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void) fetchCategoryDataWithCompletionBlock:(NSString *)urlString param:(NSMutableString *)param strDate:(NSString *)Date andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void) SubmitSubCategoryDataWithCompletionBlock:(NSString *)urlString param:(NSMutableString *)param date:(NSString *)strDate dateOnly:(NSString *)strDateOnly MainCategoryId:(NSMutableString *)strId andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void)RemoveSubCategoryDataWithCompletionBlock:(NSString *)urlString param:(NSString *)param strDateForRemoveItinerary:(NSString *)strDateForRemoveItinerary trip_id:(NSString *)Trip_id andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void)GradeApiDataWithCompletionBlock:(NSString *)urlString param:(NSString *)param andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
-(void) SubmitSubCategoryDataWithCompletionBlock:(NSString *)urlString param:(NSMutableString *)param date:(NSString *)strDate dateOnly:(NSString *)strDateOnly MainCategoryId:(NSMutableString *)strId Grade:(NSString *)strGrade TripId:(NSString *)Trip_Id andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;


@end
