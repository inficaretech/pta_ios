//
//  WebService.m
//  Putnam Teacher's App
//
//  Created by Inficare on 5/31/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "WebService.h"
#import "Constants.h"
#import "StartingPageViewController.h"
#import "CommonMethods.h"


@implementation WebService

+(WebService *)sharedInstance
{
    static WebService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WebService alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

-(void)SignUpApi:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *__jsonData;
    NSString *__jsonString;
    //  __jsonData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    ;
    
    if([NSJSONSerialization isValidJSONObject:dict])
    {
        __jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        __jsonString = [[NSString alloc]initWithData:__jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 300.0;
    defaultConfigObject.timeoutIntervalForResource = 600.0;
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    request.HTTPShouldHandleCookies = YES;
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookieAcceptPolicy currentPolicy = [cookieJar cookieAcceptPolicy];
    [cookieJar setCookieAcceptPolicy: NSHTTPCookieAcceptPolicyAlways];
    
    [cookieJar setCookieAcceptPolicy: currentPolicy];
    
    [request setHTTPBody:__jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[__jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [[delegateFreeSession dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response,
                                                                           NSError *error)
      {
          NSError *jsonParsingError = nil;
          NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
          
          if ([[jsonArray valueForKey:@"error"] isEqualToString:@"Session expired"] || [[jsonArray valueForKey:@"error"] isEqualToString:@"session expired"]) {
              [self SuccessfullyLogout];
              UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
              StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
              UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
              AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
              appdel.window.rootViewController = obj;
              
              
          }
          
          
          
          completionBlock (jsonArray, error);
          
          
      }
      
      ]resume];
    
    
}



-(void) fetchDataWithCompletionBlock:(NSString *)urlString param:(NSMutableArray *)param andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    
    NSError *error;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 300.0;
    defaultConfigObject.timeoutIntervalForResource = 600.0;
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
   // NSMutableData *body;
   // body = [NSMutableData data];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    
    
    NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
    NSString *accesstoken = [defualts valueForKey:@"access_token"];
    NSString *teacherId = [defualts valueForKey:@"id"];
    [dict setValue:accesstoken forKey:@"access_token"];
    [dict setValue:teacherId forKey:@"teacher_id"];
    [dict setValue:param forKey:@"mapping_id"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    [[delegateFreeSession dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response,
                                                                           NSError *error)
      {
          NSError *jsonParsingError = nil;
          id jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
          
          NSLog(@"print jsonarray %@",jsonArray);
          if ([[jsonArray valueForKey:@"error"] isEqualToString:@"Session expired"] || [[jsonArray valueForKey:@"error"] isEqualToString:@"session expired"]) {
              [self SuccessfullyLogout];
              UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
              StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
              UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
              AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
              appdel.window.rootViewController = obj;
              
              
          }
          
          
          completionBlock (jsonArray, error);
      }
      ]resume];
    
    
    
    
}

-(void) fetchCategoryDataWithCompletionBlock:(NSString *)urlString param:(NSMutableString *)param andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    
    
    NSError *error;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 300.0;
    defaultConfigObject.timeoutIntervalForResource = 600.0;
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSMutableData *body;
    body = [NSMutableData data];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    
    
    NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
    NSString *accesstoken = [defualts valueForKey:@"access_token"];
    
    [dict setValue:accesstoken forKey:@"access_token"];
    
    [dict setValue:param forKey:@"id"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    [[delegateFreeSession dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response,
                                                                           NSError *error)
      {
          NSError *jsonParsingError = nil;
          id jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
          
          NSLog(@"print jsonarray %@",jsonArray);
          if ([[jsonArray valueForKey:@"error"] isEqualToString:@"Session expired"] || [[jsonArray valueForKey:@"error"] isEqualToString:@"session expired"]) {
              [self SuccessfullyLogout];
              UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
              StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
              UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
              AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
              appdel.window.rootViewController = obj;
              
              
          }
          
          
          completionBlock (jsonArray, error);
      }
      ]resume];
}

-(void)ApiOfgetMethod:(NSString *)urlString andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    
    NSLog(@"main thread? ANS - %@",[NSThread isMainThread]? @"YES":@"NO");
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    
    [[delegateFreeSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response,
                                                NSError *error)
      {
          NSLog(@"Got response %@ with error %@.\n", response, error);
          NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          completionBlock (dataArray, error);
          if ([[dataArray valueForKey:@"error"] isEqualToString:@"Session expired"] || [[dataArray valueForKey:@"error"] isEqualToString:@"session expired"]) {
              [self SuccessfullyLogout];
              UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
              StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
              UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
              AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
              appdel.window.rootViewController = obj;
              
              
          }
          
      }
      ]resume];
    
}
-(void)SuccessfullyLogout{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:@"isVerified"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[CommonMethods sharedInstance] AlertAction:@"Session expired! Please login again"];
    
}



@end
