//
//  CustomCellWallOfFameTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 5/30/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellWallOfFameTableViewCell : UITableViewCell

{
    
}
@property(nonatomic,strong)IBOutlet UILabel *lblAlphabet;
@property(nonatomic,strong)IBOutlet UILabel *lblDetail;
@property(nonatomic,strong)IBOutlet UILabel *lblLine;
@end
