//
//  ShowDetailTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 7/19/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UIView *viewLine;
@property(weak,nonatomic)IBOutlet UILabel *labelDescription;
@end
