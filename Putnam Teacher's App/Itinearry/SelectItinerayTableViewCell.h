//
//  SelectItinerayTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 6/14/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectItinerayTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,weak)IBOutlet UIImageView *imageIconItinerary;
@property(nonatomic,weak)IBOutlet UIImageView *imageTick;
@property(nonatomic,weak)IBOutlet UILabel *labelItineraryName;
@end
