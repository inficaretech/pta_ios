//
//  ShowDetailCategoryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 7/5/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "ShowDetailCategoryViewController.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "PanormaViewController.h"
#import "Constants.h"
#import "ShowDetailTableViewCell.h"
//#import "GlobalVariables.h"

@interface ShowDetailCategoryViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tableViewShowDetailOfSubcategory;
    NSInteger indexPathSection;
    BOOL isClicked;
    NSMutableArray *arrForSubCategories;
    AppDelegate *appDelegate ;
    NSMutableString *strSelectedSubCategory;
    NSString *filePath;
    NSMutableString *newmutablestring;
    BOOL isAdded;
    int selectedButtonTag;
    
}
@end

@implementation ShowDetailCategoryViewController
@synthesize arrResponse,strCategoryName;
- (void)viewDidLoad {
    [super viewDidLoad];
    newmutablestring = [[NSMutableString alloc] init];
    [self UiForNavigationBar];
    
    arrForSubCategories = [[NSMutableArray alloc]init];
    if(arrResponse.count == 0)
    {
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        [self.view addSubview:label];
        label.text = @"No Itinerary found";
        label.textColor = KTextColor;
        label.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
        label.textAlignment = NSTextAlignmentCenter;
    }
    else
    {
        arrForSubCategories = [[[arrResponse objectAtIndex:0]objectAtIndex:0] valueForKey:@"sub_categories"];
        indexPathSection=200;
        tableViewShowDetailOfSubcategory = [[UITableView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        tableViewShowDetailOfSubcategory.separatorColor = [UIColor clearColor];
        tableViewShowDetailOfSubcategory.delegate=self;
        tableViewShowDetailOfSubcategory.dataSource=self;
        [self.view addSubview:tableViewShowDetailOfSubcategory];
        tableViewShowDetailOfSubcategory.separatorColor = [UIColor clearColor];
        strSelectedSubCategory = [[NSMutableString alloc]init];
    }
    
    
}

-(void)UiForNavigationBar
{
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =strCategoryName;
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

-(void)BackButtonPressed
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"print %@", strSelectedSubCategory);
    NSMutableString   *_strSelectedSubCategory=  [NSMutableString string];
    _strSelectedSubCategory =  [defaults valueForKey:@"strId"];
    
    [newmutablestring appendString:[NSString stringWithFormat:@",%@,%@",_strSelectedSubCategory,strSelectedSubCategory]];
    
    
    [defaults setObject:newmutablestring forKey:@"strId"];
    
    
    
    
    [defaults removeObjectForKey:@"strSelectedSubCategory"];
    [defaults setObject:strSelectedSubCategory forKey:@"strSelectedSubCategory"];
    [defaults synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrForSubCategories.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableViewShowDetailOfSubcategory.frame.size.width-15, 60)];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0,tableViewShowDetailOfSubcategory.frame.size.width , 15)];
    label.backgroundColor = [UIColor whiteColor];
    [viewHeader addSubview:label];
    UIButton *btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPlus.frame = CGRectMake(15.0, 15.0, 25, viewHeader.frame.size.height-15);
    btnPlus.backgroundColor =[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    UIView* shadowView = [[UIView alloc] initWithFrame: viewHeader.bounds];
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowRadius = 5.0;
    shadowView.layer.shadowOffset = CGSizeMake(3.0, 3.0);
    shadowView.layer.shadowOpacity = 1.0;
    [viewHeader addSubview: shadowView];
    
    if (!isClicked) {
        [btnPlus setTitle:@"+" forState:UIControlStateNormal];
        btnPlus.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
    }
    else
    {     if(indexPathSection==section)
        {
            [btnPlus setTitle:@"-" forState:UIControlStateNormal];
            btnPlus.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:30];
        }
        else{
            [btnPlus setTitle:@"+" forState:UIControlStateNormal];
            btnPlus.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        }
    }
    
    btnPlus.tag=section;
    btnPlus.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnPlus addTarget:self action:@selector(OpenDetailsForTryThis:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btnPlus];
    UIButton *btnSection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSection.frame = CGRectMake(40.0, 15.0, viewHeader.frame.size.width-40, viewHeader.frame.size.height-15);
    btnSection.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [btnSection setTitle:[[arrForSubCategories valueForKey:@"title"]objectAtIndex:section]  forState:UIControlStateNormal];
    btnSection.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnSection.tag=section;
    [btnSection addTarget:self action:@selector(OpenDetailsForTryThis:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btnSection];
    return viewHeader;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (isClicked) {
        if (indexPathSection==section) {
            return 1;
        }
        return 0;
    }
    else
        return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"CellIdentifier";
//    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    }
    
    
    static NSString *CellIdentifier = @"Cell";
    
    ShowDetailTableViewCell *cell = (ShowDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIView *vwLine;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShowDetailTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        vwLine=[[UIView alloc]init];
        vwLine.backgroundColor = [UIColor clearColor];
        vwLine.layer.borderColor=[UIColor lightGrayColor].CGColor;
        vwLine.layer.borderWidth=1;
        [cell.contentView addSubview:vwLine];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
     UILabel *label = [[UILabel alloc]init];
  
    CGSize size=[self getSizeForText: [[arrForSubCategories valueForKey:@"description"]objectAtIndex:indexPath.section] maxWidth:tableViewShowDetailOfSubcategory.frame.size.width-60  font:@"Roboto-Regular" fontSize:14];
    
    cell.labelDescription.hidden=NO;
    cell.viewLine.hidden=NO;
    NSString *strUrl = [[arrForSubCategories  objectAtIndex:indexPath.section] valueForKey:@"image"];
    if (strUrl.length ==0) {
        cell.labelDescription.frame = CGRectMake(15, 10.0, [[UIScreen mainScreen] bounds].size.width-40, size.height);
        vwLine.frame=CGRectMake(15.0, 10.0, [[UIScreen mainScreen] bounds].size.width-30,  cell.labelDescription.frame.size.height+90);
    }
    
    else
    {
    UIImageView *image = [[UIImageView alloc]init];
    
    
    image.frame = CGRectMake(15.0, 0.0, [[UIScreen mainScreen] bounds].size.width-30, 70);
    [cell.contentView addSubview:image];
    image.userInteractionEnabled=YES;
    NSURL *url = [NSURL URLWithString:[[arrForSubCategories  objectAtIndex:indexPath.section] valueForKey:@"image"] ];
    
    [image sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    image.contentMode = UIViewContentModeScaleToFill;
    
    UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PanormaImagesShow:)];
    singleTap.numberOfTapsRequired=1;
    [image addGestureRecognizer:singleTap];
    label.frame = CGRectMake(20, 80.0, [[UIScreen mainScreen] bounds].size.width-40, size.height);
    vwLine.frame=CGRectMake(15.0, 80.0, [[UIScreen mainScreen] bounds].size.width-30,  label.frame.size.height+70);
    
    }
    
    
    
    [cell.contentView addSubview:label];
    NSString *strDescription = [[arrForSubCategories valueForKey:@"description"]objectAtIndex:indexPath.section];
    strDescription = [strDescription stringByAppendingString:@"\n\nTime = "];
    strDescription = [strDescription stringByAppendingString:[[arrForSubCategories valueForKey:@"time_duration"]objectAtIndex:indexPath.section]];
    label.text = nil;
    cell.labelDescription.text = strDescription;
    cell.labelDescription.numberOfLines=0;
    cell.labelDescription.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    [cell.labelDescription sizeToFit];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UIButton *btnAddToItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddToItinerary.frame = CGRectMake(vwLine.frame.size.width/2-60, vwLine.frame.size.height-50, 120.0, 40.0);
    [vwLine addSubview:btnAddToItinerary];
    btnAddToItinerary.backgroundColor = [UIColor grayColor];
    btnAddToItinerary.layer.shadowColor = [UIColor grayColor].CGColor;
    btnAddToItinerary.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnAddToItinerary.layer.shadowOpacity = 2.0;
    btnAddToItinerary.layer.shadowRadius = 3.0;
    

    NSString *strSubCategoryId = [[arrForSubCategories valueForKey:@"sub_category_id"]objectAtIndex:indexPath.section];
    btnAddToItinerary.tag = [(strSubCategoryId) integerValue];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableString *str = [defaults valueForKey:@"strId"];
    
    if ([str containsString:strSubCategoryId]) {
        
        [btnAddToItinerary setTitle:@"Added to itinerary" forState:UIControlStateNormal];
        btnAddToItinerary.userInteractionEnabled=NO;
    }
    else if([[[arrForSubCategories valueForKey:@"already_saved"]objectAtIndex:indexPath.row] isEqualToString:@"1" ])
    {
        [btnAddToItinerary setTitle:@"Already added" forState:UIControlStateNormal];
        btnAddToItinerary.userInteractionEnabled=NO;
        
    }
    
    else{
        if ([strSelectedSubCategory containsString:strSubCategoryId]) {
            [btnAddToItinerary setTitle:@"Added to itinerary" forState:UIControlStateNormal];
            btnAddToItinerary.userInteractionEnabled=NO;
        }
        else
        {
        [btnAddToItinerary setTitle:@"Add to itinerary" forState:UIControlStateNormal];
        }
    }
    btnAddToItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    
    [btnAddToItinerary addTarget:self action:@selector(BtnAddToItineraryClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}
#pragma mark- PanormaImagesShow
-(void)PanormaImagesShow:(UITapGestureRecognizer *)sender{
    UIImageView *imGvw=(UIImageView *)(id)sender.view;
    UIImage *img=imGvw.image;
    NSData *pngData = UIImagePNGRepresentation(img);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    filePath = [documentsPath stringByAppendingPathComponent:@"image.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PanormaViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PanormaViewController"];
    MyItineraryViewController.workingImage=filePath;
    MyItineraryViewController.strNavTitil=strCategoryName;
    [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
    
    
}



-(void)BtnAddToItineraryClicked:(id)sender
{
    UIButton *btn = (id)sender;
    [btn setTitle:@"Added to Itinerary" forState:UIControlStateNormal];
    btn.userInteractionEnabled= NO;
    selectedButtonTag = (int)[sender tag];
    isAdded = YES;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AddItinerary"];
    
    NSString *strId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    
    
    if(strSelectedSubCategory.length > 0)
    {
        [strSelectedSubCategory appendString:@","];
    }
    
    [strSelectedSubCategory appendString:strId];
    
    
    // isBtnPressed=YES;
    
}

-(void)OpenDetailsForTryThis:(id)sender
{
    
    UIButton *btn=(id)sender;
    
    if (btn.tag==indexPathSection) {
        if (!isClicked) {
            isClicked=YES;
            indexPathSection = btn.tag;
            //    NSLog(@"%ld",(long)indexPathSection);
            [tableViewShowDetailOfSubcategory reloadData];
            
        }
        else
        {
            isClicked=NO;
            [tableViewShowDetailOfSubcategory reloadData];
        }
        
    }else{
        isClicked=YES;
        indexPathSection = btn.tag;
        // NSLog(@"%ld",(long)indexPathSection);
        [tableViewShowDetailOfSubcategory reloadData];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize size=[self getSizeForText: [[arrForSubCategories valueForKey:@"description"]objectAtIndex:indexPath.section] maxWidth:tableViewShowDetailOfSubcategory.frame.size.width-60  font:@"Roboto-Regular" fontSize:14];
    NSString *strUrl = [[arrForSubCategories  objectAtIndex:indexPath.row] valueForKey:@"image"];
    if (strUrl.length ==0) {
    if (IS_IPHONE_5) {
        return size.height+110;
    }
    else if(IS_IPAD)
    {
        return size.height+130;
    }
    else
    {
        return size.height+90;
    }
    }
    else{
        if (IS_IPHONE_5) {
            return size.height+150;
        }
        else if(IS_IPAD)
        {
            return size.height+190;
        }
        else
        {
            return size.height+150;
        }

    }
}
#pragma mark - getSizeForString
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end