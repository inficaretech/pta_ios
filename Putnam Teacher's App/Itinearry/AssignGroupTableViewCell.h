//
//  AssignGroupTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 6/7/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignGroupTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,weak)IBOutlet UILabel *labelName;
@property(nonatomic,weak)IBOutlet UIButton *btnAssignGroup;
@property(nonatomic,weak)IBOutlet UIImageView *imageViewDropDownIcon;
@end
