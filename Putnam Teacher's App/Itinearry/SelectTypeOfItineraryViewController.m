//
//  SelectTypeOfItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 7/13/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "SelectTypeOfItineraryViewController.h"
#import "Constants.h"
#import "MyItineraryViewController.h"
#import "CurrentItineraryViewController.h"
#import "CommonMethods.h"
#import "WebService.h"

@interface SelectTypeOfItineraryViewController ()
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
}
@end

@implementation SelectTypeOfItineraryViewController
@synthesize arrForCLassLevel;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self UiForNavigationBar];
    [self ApiForCheckOfSavedPlans];
}
-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}


-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)CreateUi
{
    NSAttributedString *str =  [[NSAttributedString alloc] initWithString:@"Please allow time for a museum staff or volunteer to provide a brief meet & greet for your group when you arrive. This can take anywhere from 5-15 minutes once your group is seated and ready to listen. Be prepared to provide the exact count of students and adults in your group for billing purposes."];
    
    CGSize size = [self getSizeForText:str maxWidth:[[UIScreen mainScreen] bounds].size.width-40 font:@"Roboto-regular" fontSize:15];
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(10, 74,[[UIScreen mainScreen] bounds].size.width-20 ,size.height+90 );
    // [self.view addSubview:view];
    view.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    UILabel *labelDescription = [[UILabel alloc]init];
    labelDescription.frame = CGRectMake(20.0, 74, [[UIScreen mainScreen] bounds].size.width-40, size.height);
    [self.view addSubview:labelDescription];
    
    labelDescription.attributedText = str;    //@"Please allow time for a museum staff or volunteer to provide a brief meet & greet for your group when you arrive. This can take anywhere from 5-15 minutes once your group is seated and ready to listen. Be prepared to provide the exact count of students and adults in your group for billing purposes.";
    labelDescription.font = [UIFont fontWithName:@"Roboto-regular" size:15];
    
    
    labelDescription.textColor = [UIColor darkGrayColor];
    labelDescription.numberOfLines = 0;
    [labelDescription sizeToFit];
    
    btnBuildItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBuildItinerary.frame = CGRectMake(30.0, [[UIScreen mainScreen] bounds].size.height/2+50, [[UIScreen mainScreen] bounds].size.width-60, 40.0);
    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
    [self.view addSubview:btnBuildItinerary];
    btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    [btnBuildItinerary setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnBuildItinerary.backgroundColor = kBtnUnselectedColor;
    [btnBuildItinerary addTarget:self action:@selector(btnBuildItineraryClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([[arrForCLassLevel valueForKey:@"itinerary"] isEqualToString:@"1"]) {
        btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCurrentItinerary.frame = CGRectMake(30.0, [[UIScreen mainScreen] bounds].size.height/2-20,[[UIScreen mainScreen] bounds].size.width-60, 40.0);
        [btnCurrentItinerary setTitle:@"Saved Itinerary" forState:UIControlStateNormal];
        [self.view addSubview:btnCurrentItinerary];
        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        [btnCurrentItinerary setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCurrentItinerary.backgroundColor = kBtnUnselectedColor;
        
        [btnCurrentItinerary addTarget:self action:@selector(btnCurrentItineraryClicked) forControlEvents:UIControlEventTouchUpInside];
        btnCurrentItinerary.layer.shadowColor = [UIColor grayColor].CGColor;
        btnCurrentItinerary.layer.shadowOffset = CGSizeMake(0, 1.0);
        btnCurrentItinerary.layer.shadowOpacity = 2.0;
        btnCurrentItinerary.layer.shadowRadius = 4.0;
        
        
    }
    
    btnBuildItinerary.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBuildItinerary.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnBuildItinerary.layer.shadowOpacity = 2.0;
    btnBuildItinerary.layer.shadowRadius = 4.0;
    
    
    
}


-(void)btnBuildItineraryClicked
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MyItineraryViewController"];
    
    [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
}

-(void)btnCurrentItineraryClicked
{
    //CurrentItineraryViewController.h
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CurrentItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
    
    [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
    
    
}


-(void)ApiForCheckOfSavedPlans
{
    [btnBuildItinerary removeFromSuperview];
    [btnCurrentItinerary removeFromSuperview];
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_grades?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                
                arrForCLassLevel = [returArray mutableCopy];
                [self CreateUi];
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    
    
}



- (CGSize)getSizeForText:(NSAttributedString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    
    
    
    NSAttributedString *attributedText = text;
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.frame.size.width, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];//you need to specify the some width, height will be calculated
    CGSize requiredSize = rect.size;
    
    return requiredSize;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end