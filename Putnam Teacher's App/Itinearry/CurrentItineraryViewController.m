//
//  CurrentItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/29/16.
//  Copyright Ãƒâ€šÃ‚Â© 2016 inficare. All rights reserved.
//

#import "CurrentItineraryViewController.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "AddToItineraryTableViewCell.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "RPFloatingPlaceholderTextField.h"

@interface CurrentItineraryViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *arrResponse;
    UIView *viewDetail;
    NSMutableArray *arrTag;
    NSMutableArray *arrCurrentItinerary;
    NSUInteger index;
    BOOL isClicked;
    NSInteger indexPathSection;
    UIButton *btnPlus;
    UIButton *btnSection;
    int openCellIndex;
    int openSectionIndex;
    BOOL isBtnPressed;
    UITableView *tableViewCurrentItinerary ;
    UIDatePicker *objPickerView;
    //UIDatePicker *timePickerVIew;
    UIToolbar *toolBar;
    UIToolbar *_toolBar;
    UIBarButtonItem *barButtonDone;
    UIBarButtonItem *_barButtonDone;
    NSString *strDate;
    UIButton *btnMonth;
    UIButton *btnDay;
    UIButton *btnYear;
    UIButton *btnMinute;
    UIButton *btnHour;
    NSMutableArray   *_arrCurrentitinerary;
    UIView *viewtoAnimate;
    NSMutableString *strSelectedSubcategories;
    NSMutableArray *arrRecommendedItinerary;
    NSString *strRecommendedTitle;
    NSMutableArray *arrSubCategories;
    int subcategory;
    int category;
    BOOL isPressed;
    BOOL isPressedForOtherSection;
    NSInteger sectionClickedForTripCHange;
    BOOL showAlert;
    UIView *viewForDateChange;
    BOOL isDateSet;
    BOOL isTimeSet;
    NSString *strTime;
    BOOL firstTime;
    UIDatePicker *_objPickerView;
    UIButton *btnDateSelection;
    UIButton *btnTimeSelection;
    NSString *strHour;
    NSString *strMin;
    NSString *strGradeForAddition;
    UIView *viewSendToPutnam;
    UIScrollView *scrlView;
    UIDatePicker *datePicker;
    int textFieldTag;
    NSInteger intNeedLunchroom;
    NSInteger intConsessionStand;
    NSInteger intMuseumStore;
    NSInteger intPermissionPhoto;
    int btnSendTag;
    
    UITextField *tfFieldtripName;
    
}
@property(nonatomic,strong) NSMutableArray *arrRecommendedItinerary;
@end

@implementation CurrentItineraryViewController
@synthesize delegate,arrRecommendedItinerary;
- (void)viewDidLoad {
    [super viewDidLoad];
    firstTime = YES;
    sectionClickedForTripCHange = -1;
    [self UiForNavigationBar];
    [self ApiForCurrentItinerary];
    strSelectedSubcategories = [[NSMutableString alloc]init];
    intConsessionStand = 0;
    intNeedLunchroom = 1;
    intPermissionPhoto = 1;
    intMuseumStore = 1;
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UI for NavigationBar

-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}
#pragma mark - Action on back button of Navigation Bar

-(void)BackButtonPressed
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

#pragma mark - Ui for View

-(void)CreateUI
{
    if (_arrCurrentitinerary.count==0) {
        NSLog(@"notFound");
        NSArray *viewsToRemove = [self.view subviews];
        for (UIView *v in viewsToRemove) {
            NSLog(@"View %@",v);
         [v removeFromSuperview];
        }
     //   [tableViewCurrentItinerary setHidden:true];
       // [viewtoAnimate removeFromSuperview];
     //  [ tableViewCurrentItinerary removeFromSuperview];
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(0.0,0.0 , [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-55);
        [self.view addSubview:label];
        label.text = @"No Itinerary found for this Grade, Please build the Itinerary";
        label.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 2;
        label.textColor = [UIColor lightGrayColor];
        [viewtoAnimate removeFromSuperview];
        
    }
    else
    {
         tableViewCurrentItinerary = [[UITableView alloc] init];
        
        
        viewtoAnimate = [[UIView alloc]init];
        viewtoAnimate.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:viewtoAnimate];
      
        tableViewCurrentItinerary.frame = CGRectMake(0.0, 65.0, self.view.frame.size.width, self.view.frame.size.height-65);
        [viewtoAnimate addSubview:tableViewCurrentItinerary];
        tableViewCurrentItinerary.delegate = self;
        tableViewCurrentItinerary.dataSource=self;
        tableViewCurrentItinerary.separatorColor= [UIColor clearColor];
        
        
    }
    openCellIndex = -1;
    openSectionIndex = -1;
    
}

#pragma mark - Method For Border

-(void)BorderforButton:(UIButton *)button
{
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1.0;
    button.layer.cornerRadius = 2;
}


#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (arrRecommendedItinerary.count == 0) {
        return _arrCurrentitinerary.count;
    }
    else
    {
        return _arrCurrentitinerary.count+1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section < [_arrCurrentitinerary count])
    {
        return [[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"current_itenery_category"] count] + [[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"current_itenery_subcategory"] count];
    }
    else
        return arrRecommendedItinerary.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *CellIdentifier = @"Cell";
    
    AddToItineraryTableViewCell *cell = (AddToItineraryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblTime;
    UIView *vwLine;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddToItineraryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        vwLine=[[UIView alloc]init];
        lblTime=[[UILabel alloc]init];
        lblTime.textColor=[UIColor darkGrayColor];
        [vwLine addSubview:lblTime];
        vwLine.layer.borderColor=[UIColor lightGrayColor].CGColor;
        vwLine.layer.borderWidth=1;
        [cell.contentView addSubview:vwLine];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    [cell.btnSubCategory setTag:indexPath.section];
    [cell.btnSubCategory  setAccessibilityValue:[NSString stringWithFormat:@"%d", (int)indexPath.row]];
    [cell.btnSubCategory  addTarget:self action:@selector(expandRow:) forControlEvents:UIControlEventTouchUpInside];
    
    [[cell.btnSubCategory layer] setBorderWidth:1.0f];
    [[cell.btnSubCategory layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    cell.btnSubCategory.backgroundColor = [UIColor clearColor];
    cell.btnSubCategory.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    [cell.btnSubCategory setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1] forState:UIControlStateNormal];
    cell.labelPlus.textColor =[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    cell.labelPlus.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
    if (indexPath.section == _arrCurrentitinerary.count) {
        [cell.btnSubCategory setTitle:[[arrRecommendedItinerary  objectAtIndex:indexPath.row] valueForKey:@"category"] forState:UIControlStateNormal];
        NSMutableArray *arr = [[arrRecommendedItinerary objectAtIndex:indexPath.row] valueForKey:@"subcategories"];
        if (arr.count==0) {
            [cell.labelPlus removeFromSuperview];
            
        }
        
    }
    else
    {
        NSArray *subcategoryArr = [[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"];
        NSArray *categoryArr = [[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_category"] ;
        
        if([subcategoryArr count] > indexPath.row){
            NSString *titlestr = [[[subcategoryArr  objectAtIndex:indexPath.row] valueForKey:@"subcategory"] valueForKey:@"title"];
            
            [cell.btnSubCategory setTitle:titlestr forState:UIControlStateNormal];
        }
        else if([categoryArr count] > 0){
            NSString *titlestr1 = [[[categoryArr  objectAtIndex:indexPath.row-subcategoryArr.count] valueForKey:@"category"] valueForKey:@"title"];
            
            [cell.btnSubCategory setTitle:titlestr1 forState:UIControlStateNormal];
            [cell.labelPlus removeFromSuperview];
        }
        
    }
    
    if(indexPath.row == openCellIndex && indexPath.section == openSectionIndex)
    {
        
        [cell.labelDescription setNumberOfLines:0];
        if (indexPath.section == _arrCurrentitinerary.count) {
            arrSubCategories = [[NSMutableArray alloc]init];
            arrSubCategories = [[arrRecommendedItinerary  objectAtIndex:indexPath.row] valueForKey:@"subcategories"];
            if (arrSubCategories.count>0) {
                for (int k=0; k<arrSubCategories.count; k++) {
                    UIButton *lbl=[UIButton buttonWithType:UIButtonTypeCustom];
                    lbl.frame=CGRectMake(20, 50+50*k,  [[UIScreen mainScreen] bounds].size.width-40, 40);
                    cell.labelPlus.font = [UIFont fontWithName:@"Roboto-Regular" size:30];
                    cell.labelPlus.text = @"-";
                    lbl.layer.borderColor=[UIColor lightGrayColor].CGColor;
                    lbl.layer.borderWidth=1;
                    [lbl setTitle:[[arrSubCategories objectAtIndex:k] valueForKey:@"title"] forState:UIControlStateNormal];
                    lbl.titleLabel.textAlignment=NSTextAlignmentCenter;
                    [lbl setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    [cell.contentView addSubview:lbl];
                    lbl.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular"  size:13];
                    lbl.tag = k;
                    [lbl addTarget:self action:@selector(SaveButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
            
            else
            {
                
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to add this category to the itinerary." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                alert.tag = indexPath.row;
                alert.tag =10001;
                [alert show];
                //[self AddMainCategoryToItinerary];
            }
        }
        else{
            int rowNo = (int)[[[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"] count];
            if(indexPath.row < rowNo){
                NSString *descstr = [[[[[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"]  objectAtIndex:indexPath.row] valueForKey:@"subcategory"] valueForKey:@"description"];
                NSAttributedString *_descstr = [[NSAttributedString alloc] initWithString:descstr];
                CGFloat hieght1 =[[CommonMethods sharedInstance]textViewHeightForAttributedText:_descstr andWidth:tableView.frame.size.width];
                
                CGRect initialFrame = CGRectMake(20.0, 50.0, [[UIScreen mainScreen] bounds].size.width-60, hieght1);
                UIEdgeInsets contentInsets = UIEdgeInsetsMake(5, 10, 0, 0);
                CGRect paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets);
                
                cell.labelDescription.frame = paddedFrame;
                cell.labelDescription.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
                cell.labelPlus.font = [UIFont fontWithName:@"Roboto-Regular" size:30];
                cell.labelPlus.text = @"-";
                
                cell.btnSubCategory.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:14];
                cell.labelDescription.attributedText=_descstr;
                [cell.labelDescription sizeToFit];
                if (IS_IPAD) {
                    vwLine.frame=CGRectMake(20.0, 47.0, [[UIScreen mainScreen] bounds].size.width-40, cell.labelDescription.frame.size.height+150);
                    
                }else{
                    vwLine.frame=CGRectMake(20.0, 47.0, [[UIScreen mainScreen] bounds].size.width-40,  cell.labelDescription.frame.size.height+150);
                }
                
                NSString *strTime1=[@"Time - " stringByAppendingString:[[[[[_arrCurrentitinerary  objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"] valueForKey:@"subcategory"] valueForKey:@"time_duration"] objectAtIndex:indexPath.row]];
                lblTime.frame=CGRectMake(20.0, vwLine.frame.size.height-120, 200.0, 20.0) ;
                lblTime.text=strTime1;
                lblTime.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
                UIButton *removeFromItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
                [removeFromItinerary removeFromSuperview];
                removeFromItinerary.frame = CGRectMake((vwLine.frame.size.width/2)-75.0,vwLine.frame.size.height-70, 150.0, 40.0);
                [vwLine addSubview:removeFromItinerary];
                removeFromItinerary.backgroundColor = [UIColor grayColor];
                [removeFromItinerary setTitle:@"Remove from itinerary" forState:UIControlStateNormal];
                [removeFromItinerary addTarget:self action:@selector(BtnRemoveFromItineraryClicked:) forControlEvents:UIControlEventTouchUpInside];
                removeFromItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
                removeFromItinerary.tag = indexPath.row;
                removeFromItinerary.layer.shadowColor = [UIColor grayColor].CGColor;
                removeFromItinerary.layer.shadowOffset = CGSizeMake(0, 1.0);
                removeFromItinerary.layer.shadowOpacity = 2.0;
                removeFromItinerary.layer.shadowRadius = 3.0;
                [cell.labelDescription sizeToFit];
                removeFromItinerary.hidden=NO;
                cell.labelDescription.hidden = NO;
                [removeFromItinerary setShowsTouchWhenHighlighted:TRUE];
                
            }
            else if(showAlert){
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to remove it from itinerary." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                alert.tag = 10002;
                
                [alert show];
            }
            
        }
    }
    //Are you sure you want to remove it from itinerary.
    else
    {
        cell.labelDescription.hidden = YES;
        cell.labelDescription.text = @"";
        cell.labelPlus.text = @"+";
        cell.labelPlus.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        
        
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == openCellIndex && indexPath.section == openSectionIndex)
    {
        
        if (openSectionIndex==_arrCurrentitinerary.count) {
            NSMutableArray *arr = [[arrRecommendedItinerary objectAtIndex:indexPath.row]valueForKey:@"subcategories"];
            return (arr.count+1)*50;
        }
        else
        {
            NSString *descstr = nil;
            NSArray *subcategoryArr = [[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"];
            int rowNo = (int)[[[_arrCurrentitinerary objectAtIndex:indexPath.section] valueForKey:@"current_itenery_subcategory"] count];
            if(indexPath.row < rowNo){
                descstr = [[[subcategoryArr  objectAtIndex:indexPath.row] valueForKey:@"subcategory"] valueForKey:@"description"];
            }
            else return 50;
            
            NSAttributedString *_descstr = [[NSAttributedString alloc] initWithString:descstr];
            
            
            CGFloat size =[[CommonMethods sharedInstance]textViewHeightForAttributedText:_descstr andWidth:tableView.frame.size.width];
            
            if (IS_IPHONE_6) {
                return size+350;
            }
            else
                return size+370.0;
        }
    }
    else
        return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //    if (section == 0) {
    //        return 115;
    //    }
    //    else
    //        return 70;
    if (section == _arrCurrentitinerary.count){
        return 70;
    }
    else if(section == 0){
        return 200;
    }
    else{
        return 155;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc]init];
    
    
    if (section == _arrCurrentitinerary.count) {
        header.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 60);
        header.backgroundColor = [UIColor whiteColor];
        UILabel *lblBackground = [[UILabel alloc]init];
        lblBackground.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 60);
        lblBackground.backgroundColor = [UIColor clearColor];
        [header addSubview:lblBackground];
        UILabel *labelMySavedPlans = [[UILabel alloc]init];
        labelMySavedPlans.frame = CGRectMake( 20.0, 5.0, [[UIScreen mainScreen] bounds].size.width - 40, 60.0);
        [lblBackground addSubview:labelMySavedPlans];
        
        labelMySavedPlans.font = [UIFont fontWithName:@"Roboto-Bold" size:16];
        labelMySavedPlans.textAlignment = NSTextAlignmentCenter;
        
        UILabel *line =[[UILabel alloc]init];
        line.frame = CGRectMake(20.0, 45.0, [[UIScreen mainScreen] bounds].size.width-40, 1.0);
        
        line.backgroundColor = [UIColor colorWithRed:228/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        //        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //        [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
        //        NSDate *date1= [formatter dateFromString:strRecommendedTitle];
        //        NSCalendar* cal = [NSCalendar currentCalendar];
        //
        //        NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:date1];
        //        NSInteger Day=  [comp weekday];
        //        NSString *strMonthFromDate = [strRecommendedTitle substringWithRange:NSMakeRange(0, 2)];
        //        NSInteger Month = [(strMonthFromDate) integerValue];
        //        NSString *strDay = [self DayName:Day];
        //        NSString *strMonth = [self MonthName:Month];
        //
        //        NSString *strDateForPrinting = [strRecommendedTitle substringWithRange:NSMakeRange(3, 2)];
        //        NSString *strYear = [strRecommendedTitle substringWithRange:NSMakeRange(6, 4)];
        //
        //        strDay = [strDay stringByAppendingString:strMonth];
        //        strDay = [strDay stringByAppendingString:strDateForPrinting];
        //        strDay =[strDay stringByAppendingString:@", "];
        //        strDay =[strDay stringByAppendingString:strYear];
        //
        
        
        UIButton *labelDate = [UIButton buttonWithType:UIButtonTypeCustom];
        labelDate.frame = CGRectMake(20.0, 45.0, [[UIScreen mainScreen] bounds].size.width-40,  45.0);
        //[lblBackground addSubview:labelDate];
        labelDate.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        labelDate.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        labelDate.titleLabel.textAlignment = NSTextAlignmentCenter;
        labelDate.titleLabel.textColor = [UIColor whiteColor];
        
        
        //[lblBackground addSubview:line];
        labelMySavedPlans.text =@"Major Categories";
        labelMySavedPlans.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        //labelDate.titleLabel.text =strDay;
        labelMySavedPlans.textColor = [UIColor whiteColor];
        //  [labelDate setTitle:strDay forState:UIControlStateNormal];
        
        
    }
    else if(section == 0)
    {
        
        
        header.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 120);
        header.backgroundColor = [UIColor whiteColor];
        UILabel *lblBackground = [[UILabel alloc]init];
        lblBackground.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 120);
        lblBackground.backgroundColor = [UIColor clearColor];
        [header addSubview:lblBackground];
        lblBackground.userInteractionEnabled = YES;
        UILabel *labelMySavedPlans = [[UILabel alloc]init];
        labelMySavedPlans.frame = CGRectMake( 20.0, 0.0, [[UIScreen mainScreen] bounds].size.width - 40, 30.0);
        [lblBackground addSubview:labelMySavedPlans];
        labelMySavedPlans.font = [UIFont fontWithName:@"Roboto-Bold" size:16];
        labelMySavedPlans.textAlignment = NSTextAlignmentCenter;
        
        UILabel *line =[[UILabel alloc]init];
        line.frame = CGRectMake(20.0, 35.0, [[UIScreen mainScreen] bounds].size.width-40, 1.0);
        
        line.backgroundColor = [UIColor colorWithRed:228/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        
        UIButton *labelDate = [UIButton buttonWithType:UIButtonTypeCustom];
        labelDate.frame = CGRectMake(20.0, 45.0, [[UIScreen mainScreen] bounds].size.width-80,  30.0);
        [lblBackground addSubview:labelDate];
        labelDate.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        labelDate.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
        labelDate.titleLabel.textAlignment = UIControlContentHorizontalAlignmentCenter;
        labelDate.titleLabel.textColor = [UIColor whiteColor];
        
        UIButton *buttonEdit = [[UIButton alloc]init];
        buttonEdit.frame = CGRectMake(labelDate.frame.size.width+20, 45.0,40 , 30.0);
        [header addSubview:buttonEdit];
        [buttonEdit setImage:[UIImage imageNamed:@"edit_icon3"] forState:UIControlStateNormal];
        [buttonEdit addTarget:self action:@selector(ChangeDate:) forControlEvents:UIControlEventTouchUpInside];
        //  [buttonEdit setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
        buttonEdit.tag=section;
        
        UIButton *btnGradeName = [UIButton buttonWithType:UIButtonTypeCustom];
        btnGradeName.frame = CGRectMake(20.0, 75.0, [[UIScreen mainScreen] bounds].size.width-40,  30.0);
        [lblBackground addSubview:btnGradeName];
        btnGradeName.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        btnGradeName.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        btnGradeName.titleLabel.textAlignment = UIControlContentHorizontalAlignmentCenter;
        btnGradeName.titleLabel.textColor = [UIColor whiteColor];
        
        
        UIImageView *imageTick = [[UIImageView alloc]init];
        imageTick.frame = CGRectMake(10.0, 5.0, 20.0, 20.0);
        [labelDate addSubview:imageTick];
        imageTick.image = [UIImage imageNamed:@"tick_mark_white"];
        imageTick.contentMode = UIViewContentModeScaleAspectFit;
        
        
        NSString *strDate1 = [[_arrCurrentitinerary objectAtIndex:section ] valueForKey:@"date"];
        if([strDate1 isEqualToString: @"(null)"])
        {
            [labelDate setTitle:@"N/A" forState:UIControlStateNormal];
        }
        else
        {
            strDate1 = [strDate1 stringByReplacingOccurrencesOfString:@"\u00a0" withString:@" "];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
            NSDate *date1= [formatter dateFromString:strDate1];
            NSCalendar* cal = [NSCalendar currentCalendar];
            
            NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:date1];
            NSInteger Day=  [comp weekday];
            NSString *strMonthFromDate = [strDate1 substringWithRange:NSMakeRange(0, 2)];
            NSInteger Month = [(strMonthFromDate) integerValue];
            NSString *strDay = [self DayName:Day];
            NSString *strMonth = [self MonthName:Month];
            
            NSString *strDateForPrinting = [strDate1 substringWithRange:NSMakeRange(3, 2)];
            NSString *strYear = [strDate1 substringWithRange:NSMakeRange(6, 4)];
            
            strDay = [strDay stringByAppendingString:strMonth];
            strDay = [strDay stringByAppendingString:strDateForPrinting];
            strDay =[strDay stringByAppendingString:@", "];
            strDay =[strDay stringByAppendingString:strYear];
            [labelDate setTitle:strDay forState:UIControlStateNormal];
        }
        [lblBackground addSubview:line];
        labelMySavedPlans.text =@"My Saved Plans";
        
        
        
        int strFieldtripSelected = [[[_arrCurrentitinerary valueForKey:@"fieldtrip_selected"]objectAtIndex:section] intValue];
        if ((firstTime && strFieldtripSelected == 1) || sectionClickedForTripCHange == 0)
        {
            labelDate.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
            btnGradeName.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
            [buttonEdit setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
            labelDate.userInteractionEnabled = NO;
            btnGradeName.userInteractionEnabled = NO;
            imageTick.hidden = NO;
        }
        else
        {
            labelDate.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
            btnGradeName.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
            [buttonEdit setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1]];
            imageTick.hidden = YES;
            labelDate.userInteractionEnabled = YES;
            btnGradeName.userInteractionEnabled = YES;
        }
        
        
        NSString *strFieldTripName;
        if ([[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"fieldtrip"] isEqual:(id)[NSNull null]]) {
            
            strFieldTripName = @"N/A ";
        }
        else
        {
            strFieldTripName = [[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"fieldtrip"];
        }
        
        // strFieldTripName = [strFieldTripName stringByAppendingString:[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"fieldtrip"]];
        strFieldTripName = [strFieldTripName stringByAppendingString:@"  |  "];
        
        NSString *str =[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"grade" ];
        
        str = [self GradeLevelName:str];
        strFieldTripName = [strFieldTripName stringByAppendingString:str];
        [btnGradeName setTitle:strFieldTripName forState:UIControlStateNormal];
        
        [btnGradeName addTarget:self action:@selector(CallApiForAddingItinerary:) forControlEvents:UIControlEventTouchUpInside];
        [labelDate addTarget:self action:@selector(CallApiForAddingItinerary:) forControlEvents:UIControlEventTouchUpInside];
        labelDate.tag = section;
        btnGradeName.tag = section;
        
        UIButton *btnSendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnSendButton setFrame:CGRectMake(20, 115, [[UIScreen mainScreen] bounds].size.width-40, 35)];
        [btnSendButton setBackgroundColor:labelDate.backgroundColor];
        if ([[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"request_done"] isEqualToString:@"1"]) {
            [btnSendButton setTitle:@"Itinerary sent" forState:UIControlStateNormal];
            btnSendButton.userInteractionEnabled = NO;
        }
        else
        {
            [btnSendButton setTitle:@"Send to Putnam Museum" forState:UIControlStateNormal];
        }
        [btnSendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // [btnSendButton setTag:section];
        [btnSendButton addTarget:self action:@selector(btnSendToPutnamMuseumClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnSendButton.tag = 2016;
        [header addSubview:btnSendButton];
        UIButton *buttonCloneTheItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonCloneTheItinerary setFrame:CGRectMake(20, 160, [[UIScreen mainScreen] bounds].size.width-40, 35)];
        [buttonCloneTheItinerary setBackgroundColor:labelDate.backgroundColor];
        [buttonCloneTheItinerary setTitle:@"Clone the Itinerary" forState:UIControlStateNormal];
        buttonCloneTheItinerary.tag = section;
        [header addSubview:buttonCloneTheItinerary];
        [buttonCloneTheItinerary addTarget:self action:@selector(chooseDateForCloneFieldtrip:) forControlEvents:UIControlEventTouchUpInside];
        if ((firstTime && strFieldtripSelected == 1) || sectionClickedForTripCHange == 0)
        {
            
        }
        
    }
    else
    {
        
        
        header.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 70);
        header.backgroundColor = [UIColor whiteColor];
        UILabel *lblBackground = [[UILabel alloc]init];
        lblBackground.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 70);
        lblBackground.backgroundColor = [UIColor clearColor];
        [header addSubview:lblBackground];
        lblBackground.userInteractionEnabled = YES;
        
        UIButton *labelDate = [UIButton buttonWithType:UIButtonTypeCustom];
        labelDate.frame = CGRectMake(20.0, 5.0, [[UIScreen mainScreen] bounds].size.width-80,  30.0);
        [lblBackground addSubview:labelDate];
        // [labelDate setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        
        labelDate.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
        labelDate.titleLabel.textAlignment = UIControlContentHorizontalAlignmentCenter;
        labelDate.titleLabel.textColor = [UIColor whiteColor];
        
        UIButton *btnGradeName = [UIButton buttonWithType:UIButtonTypeCustom];
        btnGradeName.frame = CGRectMake(20.0, 35.0, [[UIScreen mainScreen] bounds].size.width-40,  30.0);
        [lblBackground addSubview:btnGradeName];
        
        btnGradeName.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        btnGradeName.titleLabel.textAlignment = UIControlContentHorizontalAlignmentCenter;
        btnGradeName.titleLabel.textColor = [UIColor whiteColor];
        
        UIButton *buttonEdit = [[UIButton alloc]init];
        buttonEdit.frame = CGRectMake(labelDate.frame.size.width+20, 5.0,40 , 30.0);
        [header addSubview:buttonEdit];
        [buttonEdit setImage:[UIImage imageNamed:@"edit_icon3"] forState:UIControlStateNormal];
        [buttonEdit addTarget:self action:@selector(ChangeDate:) forControlEvents:UIControlEventTouchUpInside];
        // [buttonEdit setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
        buttonEdit.tag=section;
        
        UIImageView *imageTick = [[UIImageView alloc]init];
        imageTick.frame = CGRectMake(10.0, 5.0, 20.0, 20.0);
        [labelDate addSubview:imageTick];
        imageTick.image = [UIImage imageNamed:@"tick_mark_white"];
        imageTick.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *strDate1 = [[_arrCurrentitinerary objectAtIndex:section ] valueForKey:@"date"];
        
        if([strDate1 isEqualToString:@"(null)"])
        {
            [labelDate setTitle:@"N/A" forState:UIControlStateNormal];
        }
        else
        {
            strDate1 = [strDate1 stringByReplacingOccurrencesOfString:@"\u00a0" withString:@" "];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
            NSDate *date1= [formatter dateFromString:strDate1];
            NSCalendar* cal = [NSCalendar currentCalendar];
            
            NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:date1];
            NSInteger Day=  [comp weekday];
            NSString *strMonthFromDate = [strDate1 substringWithRange:NSMakeRange(0, 2)];
            NSInteger Month = [(strMonthFromDate) integerValue];
            NSString *strDay = [self DayName:Day];
            NSString *strMonth = [self MonthName:Month];
            
            NSString *strDateForPrinting = [strDate1 substringWithRange:NSMakeRange(3, 2)];
            NSString *strYear = [strDate1 substringWithRange:NSMakeRange(6, 4)];
            
            strDay = [strDay stringByAppendingString:strMonth];
            strDay = [strDay stringByAppendingString:strDateForPrinting];
            strDay =[strDay stringByAppendingString:@", "];
            strDay =[strDay stringByAppendingString:strYear];
            
            [labelDate setTitle:strDay forState:UIControlStateNormal];
        }
        int strFieldtripSelected = [[[_arrCurrentitinerary valueForKey:@"fieldtrip_selected"]objectAtIndex:section] intValue];
        NSLog(@"strFieldtripSelected : %d", strFieldtripSelected);
        //        if (sectionClickedForTripCHange==section && isPressedForOtherSection) {
        //            labelDate.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        //            btnGradeName.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
        //        }
        //        else
        //if (strFieldtripSelected ==1)
        if ((firstTime && strFieldtripSelected == 1) || sectionClickedForTripCHange == section)
        {
            labelDate.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
            btnGradeName.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
            [buttonEdit setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
            imageTick.hidden = NO;
            labelDate.userInteractionEnabled = NO;
            btnGradeName.userInteractionEnabled = NO;
        }
        else
        {
            labelDate.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
            btnGradeName.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
            [buttonEdit setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1]];
            imageTick.hidden = YES;
            labelDate.userInteractionEnabled = YES;
            btnGradeName.userInteractionEnabled = YES;
        }
        
        NSString *strFieldTripName;
        if ([[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"fieldtrip"] isEqual:(id)[NSNull null]]) {
            
            strFieldTripName = @"N/A ";
        }
        else
        {
            strFieldTripName = [[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"fieldtrip"];
        }
        strFieldTripName = [strFieldTripName stringByAppendingString:@"  |  "];
        NSString *str =[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"grade" ];
        
        str = [self GradeLevelName:str];
        strFieldTripName = [strFieldTripName stringByAppendingString:str];
        [btnGradeName setTitle:strFieldTripName forState:UIControlStateNormal];
        
        [btnGradeName addTarget:self action:@selector(CallApiForAddingItinerary:) forControlEvents:UIControlEventTouchUpInside];
        [labelDate addTarget:self action:@selector(CallApiForAddingItinerary:) forControlEvents:UIControlEventTouchUpInside];
        labelDate.tag = section;
        btnGradeName.tag = section;
        
        UIButton *btnSendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnSendButton setFrame:CGRectMake(20, 75, [[UIScreen mainScreen] bounds].size.width-40, 35)];
        [btnSendButton setBackgroundColor:labelDate.backgroundColor];
        if ([[[_arrCurrentitinerary objectAtIndex:section] valueForKey:@"request_done"] isEqualToString:@"1"]) {
            [btnSendButton setTitle:@"Itinerary sent" forState:UIControlStateNormal];
            btnSendButton.userInteractionEnabled = NO;
        }
        else
        {
            [btnSendButton setTitle:@"Send to Putnam Museum" forState:UIControlStateNormal];
        }
        
        [btnSendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // [btnSendButton setTag:section];
        [btnSendButton addTarget:self action:@selector(btnSendToPutnamMuseumClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnSendButton.tag = section;
        [header addSubview:btnSendButton];
        
        UIButton *buttonCloneTheItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonCloneTheItinerary setFrame:CGRectMake(20, 120, [[UIScreen mainScreen] bounds].size.width-40, 35)];
        [buttonCloneTheItinerary setBackgroundColor:labelDate.backgroundColor];
          buttonCloneTheItinerary.tag = section;
        [buttonCloneTheItinerary addTarget:self action:@selector(chooseDateForCloneFieldtrip:) forControlEvents:UIControlEventTouchUpInside];
        [buttonCloneTheItinerary setTitle:@"Clone the Itinerary" forState:UIControlStateNormal];
        [header addSubview:buttonCloneTheItinerary];
        
        
    }
    return header;
}

#pragma mark - Button Action of view in Section

//-(void)ButtonClicked:(id)sender
//{
//    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure to change the Trip" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//
//
//    UIButton *btn = (UIButton *)(id)sender;
//      alert.tag =btn.tag;
//    [alert show];
//}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    showAlert = NO;
    if (alertView.tag==10001) {
        
        
        if (buttonIndex == 0) {
            
        }
        if (buttonIndex==1) {
            [self AddMainCategoryToItinerary];
        }
    }
    
    
    else if (alertView.tag==10002)
    {
        if (buttonIndex == 0)
        {
            
        }
        if (buttonIndex == 1)
        {
            
            
            
            
            NSString *strCategoryId;
            NSArray *subcategoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_subcategory"];
            NSArray *categoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"] ;
            if (subcategoryArr.count > 0 &&  categoryArr.count >0) {
                
                
                strCategoryId  = [[[[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"]valueForKey:@"id"]objectAtIndex:openCellIndex-subcategoryArr.count];
                
            }else if (subcategoryArr.count == 0 && categoryArr.count >0){
                
                strCategoryId  = [[[[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"]valueForKey:@"id"]objectAtIndex:openCellIndex];
            }else if (subcategoryArr.count >0 && categoryArr.count == 0){
                
                
                
            }else if(subcategoryArr.count == 0 && categoryArr.count == 0)
                
            {
                
            }
            
            
            NSString *strDateForRemoveItinerary = [[ _arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"date"];
               NSString *Strfieldtrip_id = [[ _arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"fieldtrip_id"];
            //fieldtrip_id
            strDateForRemoveItinerary = [strDateForRemoveItinerary substringWithRange:NSMakeRange(0,10)];
            
            
            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
            if (isinternet) {
                
                [[WebService sharedInstance]RemoveSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/remove_itinerary",kBaseUrl] param:strCategoryId strDateForRemoveItinerary:strDateForRemoveItinerary trip_id:Strfieldtrip_id  andcompletionhandler:^(NSArray *returArray, NSError *error) {
                    if (!error) {
                        NSLog(@"returArray %@",returArray);
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        [tableViewCurrentItinerary removeFromSuperview];
                        
                        [self ApiForCurrentItinerary];
                        firstTime= YES;
                        
                    }
                    
                    
                    
                    else
                    {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        self.view.userInteractionEnabled = YES;
                    }
                }];
                
                
            }
            
            
            
            
        }
    }
    
    else
    {
        
        if (buttonIndex== 0) {
            
        }
        else
        {
            // [self CallApiForAddingItinerary:alertView.tag];
        }
        
        
        //        if (buttonIndex == 0)
        //        {
        //
        //        }
        //        if (buttonIndex == 1)
        //        {
        //
        //
        //
        //
        //            NSString *strCategoryId;
        //            NSArray *subcategoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_subcategory"];
        //            NSArray *categoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"] ;
        //            if (subcategoryArr.count > 0 &&  categoryArr.count >0) {
        //
        //
        //                strCategoryId  = [[[[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"]valueForKey:@"id"]objectAtIndex:openCellIndex-subcategoryArr.count];
        //
        //            }else if (subcategoryArr.count == 0 && categoryArr.count >0){
        //
        //                strCategoryId  = [[[[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"]valueForKey:@"id"]objectAtIndex:openCellIndex];
        //            }else if (subcategoryArr.count >0 && categoryArr.count == 0){
        //
        //
        //
        //            }else if(subcategoryArr.count == 0 && categoryArr.count == 0)
        //
        //            {
        //
        //            }
        //
        //
        //            NSString *strDateForRemoveItinerary = [[ _arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"date"];
        //            strDateForRemoveItinerary = [strDateForRemoveItinerary substringWithRange:NSMakeRange(0,10)];
        //
        //
        //            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        //            if (isinternet) {
        //
        //                [[WebService sharedInstance]RemoveSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/remove_itinerary",kBaseUrl] param:strCategoryId strDateForRemoveItinerary:strDateForRemoveItinerary andcompletionhandler:^(NSArray *returArray, NSError *error) {
        //                    if (!error) {
        //                        NSLog(@"returArray %@",returArray);
        //                        [[CommonMethods sharedInstance] removeSpinner:self.view];
        //                        [tableViewCurrentItinerary removeFromSuperview];
        //
        //                        [self ApiForCurrentItinerary];
        //                        firstTime= YES;
        //
        //                    }
        //
        //
        //
        //                    else
        //                    {
        //                        [[CommonMethods sharedInstance] removeSpinner:self.view];
        //                        self.view.userInteractionEnabled = YES;
        //                    }
        //                }];
        //
        //
        //            }
        //
        //
        //
        //
        //        }
    }
}


#pragma mark - Method for Converting Grade Number To String

-(NSString *)GradeLevelName:(NSString *)Grade
{
    NSString *str;
    
    if ([Grade isEqualToString:@"1"]) {
        
        str = @"Pre - k";
    }
    else if ([Grade isEqualToString:@"2"])
    {
        str = @"K - 2";
    }
    else if ([Grade isEqualToString:@"3"])
    {
        str = @"3rd - 5th";
    }
    else if ([Grade isEqualToString:@"4"])
    {
        str = @"6th - 8th";
    }
    else if ([Grade isEqualToString:@"5"])
    {
        str = @"High School +";
    }
    return str;
}

#pragma mark - Method for getting the Day Names

-(NSString *)DayName:(NSInteger )Day
{
    NSString *strDay;
    switch (Day) {
        case 1:
        {
            strDay = @"        Sunday, ";
        }
            break;
        case 2:
        {
            strDay = @"        Monday, ";
        }
            break;
        case 3:
        {
            strDay = @"       Tuseday, ";
        }
            break;
        case 4:
        {
            strDay = @"       Wednesday, ";
        }
            break;
        case 5:
        {
            strDay = @"       Thursday, ";
        }
            break;
        case 6:
        {
            strDay =@"        Friday, ";
        }
            break;
        case 7:
        {
            strDay = @"       Saturday, ";
        }
            break;
        default:
            break;
    }
    return strDay;
}

#pragma mark - Method for COnvering Month Number TO month name

-(NSString *)MonthName:(NSInteger )Month
{
    NSString *strMonth;
    switch (Month) {
        case 1:
        {
            strMonth = @"Jan ";
        }
            break;
        case 2:
        {
            strMonth = @"Feb ";
        }
            break;
        case 3:
        {
            strMonth = @"Mar ";
        }
            break;
        case 4:
        {
            strMonth = @"Apr ";
        }
            break;
        case 5:
        {
            strMonth = @"May ";
        }
            break;
        case 6:
        {
            strMonth = @"Jun ";
        }
            break;
        case 7:
        {
            strMonth = @"Jul ";
        }
            break;
        case 8:
        {
            strMonth = @"Aug ";
        }
            break;
        case 9:
        {
            strMonth = @"Sept ";
        }
            break;
        case 10:
        {
            strMonth = @"Oct ";
        }
            break;
        case 11:
        {
            strMonth = @"Nov ";
        }
            break;
        case 12:
        {
            strMonth = @"Dec ";
        }
            break;
            
        default:
            break;
    }
    return strMonth;
}

#pragma mark - Click of Button Clone the Itinerary
-(void)ButtonCloneItineraryPressed:(id)sender{
    UIView *viewForClone = [[UIView alloc]initWithFrame:CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height*2, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 64)];
    //[self.view addSubview:viewForClone];
    viewForClone.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.7];
    
    //    UIDatePicker *picker = [[UIDatePicker alloc]init];
    //    picker.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height - 150, [[UIScreen mainScreen]  bounds].size.width, 150.0);
    //    [viewForClone addSubview:picker];
    
    UIDatePicker *PickerView = [[UIDatePicker alloc]init];
    PickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-214,[[UIScreen mainScreen] bounds].size.width , 150);
    [viewForClone addSubview:PickerView];
    
    PickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [PickerView setDatePickerMode:UIDatePickerModeDateAndTime];
    PickerView.minimumDate=[NSDate date];
    [PickerView addTarget:self action:@selector(DateSelected:) forControlEvents:UIControlEventValueChanged];
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         viewForClone.frame = CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 64);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    [self.view addSubview:viewForClone];
    
    
}


#pragma mark -  Click Action of button Remove From Itinerary

-(void)BtnRemoveFromItineraryClicked:(id )sender{
    //  [self openDetails:sender.tag];
    
    UIButton *btn = (UIButton *)sender;
    //NSString *strId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    //  NSString *strCellIndex = [NSString stringWithFormat:@"%ld",(long)openCellIndex];
    
    NSString *strDateForRemoveItinerary = [[ _arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"date"];
    
     NSString *strTripId = [[ _arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"fieldtrip_id"];
    
    strDateForRemoveItinerary = [strDateForRemoveItinerary substringWithRange:NSMakeRange(0,10)];
    NSString *strSubCategoryId = [[[[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_subcategory"] objectAtIndex:btn.tag] valueForKey:@"id"];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
         [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]RemoveSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/remove_itinerary",kBaseUrl] param:strSubCategoryId strDateForRemoveItinerary:strDateForRemoveItinerary trip_id:strTripId  andcompletionhandler:^(NSArray *returArray, NSError *error) {
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [tableViewCurrentItinerary removeFromSuperview];
                
                [self ApiForCurrentItinerary];
                firstTime = YES;
                
            }
            
            
            
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
            }
        }];
        
        
    }
    
    
    
}

#pragma mark -  Click Method Of button Add to itinerary

-(void)BtnAddToItineraryClicked:(UITapGestureRecognizer *)sender
{
    UIButton *btn = (id)sender;
    NSLog(@"%ld", (long)btn.tag);
    [btn setTitle:@"Added to Itinerary" forState:UIControlStateNormal];
    
    NSString *strId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    if(strSelectedSubcategories.length == 1)
    {
        [strSelectedSubcategories appendString:@","];
    }
    [strSelectedSubcategories appendString:strId];
    isBtnPressed=YES;
    
}

#pragma mark -  Method for clcicking the row of any section

- (void) expandRow:(UIButton*) sender{
    if(openCellIndex == [[sender accessibilityValue] intValue] && openSectionIndex == [sender tag]){
        openCellIndex = -1;
        openSectionIndex = -1;
        [tableViewCurrentItinerary reloadData];
        //        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
        //        [tableViewCurrentItinerary beginUpdates];
        //        [tableViewCurrentItinerary reloadRowsAtIndexPaths:@[tempIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        //        [tableViewCurrentItinerary endUpdates];
        //
        
        //[tableAddToItinerary reloadData];
    }
    else{
        openCellIndex = [[sender accessibilityValue] intValue];
        openSectionIndex = (int)[sender tag];
        showAlert = YES;
        //
        //        NSArray *subcategoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_subcategory"];
        //        NSArray *categoryArr = [[_arrCurrentitinerary objectAtIndex:openSectionIndex] valueForKey:@"current_itenery_category"] ;
        //        subcategory = (int)subcategoryArr.count;
        //        category = (int )categoryArr.count;
        //
        //        if (subcategoryArr.count > 0 &&  categoryArr.count >0) {
        //
        //            //[tableViewCurrentItinerary reloadData];
        //
        //
        //        }else if (subcategoryArr.count == 0 && categoryArr.count >0){
        //
        //
        //        }else if (subcategoryArr.count >0 && categoryArr.count == 0){
        //
        //            [tableViewCurrentItinerary reloadData];
        //        }
        //        else{
        [tableViewCurrentItinerary reloadData];
        //    }
        
        
        
        //        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
        //        [tableViewCurrentItinerary beginUpdates];
        //        [tableViewCurrentItinerary reloadRowsAtIndexPaths:@[tempIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        //        [tableViewCurrentItinerary endUpdates];
    }
}


#pragma mark -  Method for getting the Heght of the Text

- (CGSize)getSizeForText:(NSAttributedString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    
    
    
    NSAttributedString *attributedText = text;
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.frame.size.width, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];//you need to specify the some width, height will be calculated
    CGSize requiredSize = rect.size;
    
    return requiredSize;
}

#pragma mark - Method for Calling Api for Current Itinerary

-(void)ApiForCurrentItinerary
{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [viewForDateChange removeFromSuperview];
        // self.view.userInteractionEnabled= yO;
        
        // NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Grades/student_grades?grades=@%&access_token=fLDZ4FpiNIHljQeXkoxV";
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accesstoken = [defaults valueForKey:@"access_token"];
        NSString *teacherId = [defaults valueForKey:@"id"];
        // NSString *grade= [defaults valueForKey:@"ClassId"];
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        //   http://192.168.125.52/projects/Teacherapp/index.php/Itineraries/current_itinerary?access_token=gPWjZAD84bmzNl3YLvkx&teacher_id=10&grade=2
        
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Itineraries/current_itinerary?teacher_id=%@&access_token=%@",kBaseUrl,teacherId,accesstoken] andcompletionhandler:^(NSArray *returArray, NSError *error) {
             self.view.userInteractionEnabled = YES;
            if (!error) {
                NSLog(@"returArrayResult %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                strGradeForAddition = [[NSString alloc]init];
                strGradeForAddition = [returArray valueForKey:@"grade"];
                arrResponse = [[NSMutableArray alloc]init];
                _arrCurrentitinerary = [[NSMutableArray alloc] init];
                _arrCurrentitinerary =  [returArray valueForKey:@"result"];
                
                 NSLog(@"returArray,count %lu",(unsigned long)_arrCurrentitinerary.count);
                if (!(_arrCurrentitinerary.count==0)) {
                    arrRecommendedItinerary = [[NSMutableArray alloc] init];
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    arr = [returArray valueForKey:@"majorall"];
                    for(int i = 0; i<arr.count ;i++)
                    {
                        NSArray *arrSub = [[arr valueForKey:@"subcategories"]objectAtIndex:i];
                        if([[[arr valueForKey:@"subcat_exist"]objectAtIndex:i ] isEqualToString:@"1"] && arrSub.count==0 )
                        {
                            
                            
                        }else{
                            [arrRecommendedItinerary addObject:[arr objectAtIndex:i]];
                        }
                        
                        
                        
                        
                    }
                    strRecommendedTitle = [returArray valueForKey:@"majordate"];
                    NSString *strGrade = [[_arrCurrentitinerary objectAtIndex:0]valueForKey:@"grade"];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:strGrade forKey:@"strFieldTripGrade"];
                   
                     [self CreateUI];
                }else{
                     NSLog(@"Result %@",returArray);
                    [tableViewCurrentItinerary reloadData];
                     [self CreateUI];
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
            }
        }];
        
    }
    
}

#pragma mark -  Method for Removing the Object from any Mutable array

- (NSArray *)arrayByRemovingObject:(id)object
{
    if (!object) {
        return arrRecommendedItinerary;
    }
    
    NSUInteger indexOfObject = [arrRecommendedItinerary indexOfObject:object];
    NSArray *firstSubArray = [arrRecommendedItinerary subarrayWithRange:NSMakeRange(0, indexOfObject)];
    NSArray *secondSubArray = [arrRecommendedItinerary subarrayWithRange:NSMakeRange(indexOfObject + 1, arrRecommendedItinerary.count - indexOfObject - 1)];
    NSArray *newArray = [firstSubArray arrayByAddingObjectsFromArray:secondSubArray];
    
    return newArray;
}

#pragma mark -  Method for clicking the Section which is for Calling Api of MAjor Categories

-(void)CallApiForAddingItinerary:(id)sender
{
    firstTime = NO;
    UIButton *btn = (UIButton *)(id)sender;
    [btn setSelected:YES];
    sectionClickedForTripCHange = btn.tag;
    if (btn.tag == 0) {
        isPressed= YES;
    }
    else
    {
        isPressedForOtherSection = YES;
    }
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accesstoken = [defaults valueForKey:@"access_token"];
        NSString *teacherId = [defaults valueForKey:@"id"];
        NSString *grade= [NSString stringWithString:[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"grade"]];//[defaults valueForKey:@"ClassId"];
        NSString *strDate2 = [[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"date"];
        NSString *strDate3 = [strDate2 substringWithRange:NSMakeRange(0, 10)];
        NSString *strFleildTripId = [NSString stringWithString:[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip_id"]];
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        NSString * unescapedQuery = [NSString stringWithFormat:@"?access_token=%@&grade=%@&teacher_id=%@&itinerary_date=%@&trip_id=%@&itinerary_datetime=%@",accesstoken,grade,teacherId,strDate3,strFleildTripId,strDate2] ;
        NSString * escapedQuery = [unescapedQuery stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSString * urlString = [[NSString alloc] initWithFormat:@"%@index.php/Itineraries/get_majorcategories%@",kBaseUrl, escapedQuery];
        
        [[WebService sharedInstance]ApiOfgetMethod:urlString andcompletionhandler:^(NSArray *returArray, NSError *error) {
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                strGradeForAddition = [[NSString alloc]init];
                strGradeForAddition = [returArray valueForKey:@"grade"];
                
                arrRecommendedItinerary = [[NSMutableArray alloc] init];
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                arr = [returArray valueForKey:@"result"];
                for(int i = 0; i<arr.count ;i++)
                {
                    NSArray *arrSub = [[arr valueForKey:@"subcategories"]objectAtIndex:i];
                    if([[[arr valueForKey:@"subcat_exist"]objectAtIndex:i ] isEqualToString:@"1"] && arrSub.count==0 )
                    {
                        
                    }else{
                        [arrRecommendedItinerary addObject:[arr objectAtIndex:i]];
                    }
                    
                }
                
                strRecommendedTitle = [returArray valueForKey:@"datetime"];
                [self CreateUI];
                
                NSString *strFlieldTripName =[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip"];
                NSString *strFleildTripId =[NSString stringWithString:[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip_id"]];
                NSString *strFieldTripDate = [[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"date"];
                NSString *strFieldTripGrade = [NSString stringWithString:[[_arrCurrentitinerary objectAtIndex:btn.tag]valueForKey:@"grade"]];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:strFlieldTripName forKey:@"FieldTripName"];
                [defaults setObject:strFleildTripId forKey:@"FieldTripId"];
                [defaults setObject:strFieldTripDate forKey:@"FieldTripDate"];
                [defaults removeObjectForKey:@"strFieldTripGrade"];
                [defaults setObject:strFieldTripGrade forKey:@"strFieldTripGrade"];
                isPressed =YES;
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
            }
        }];
    }
    
}


#pragma mark -  ViewFor EditTheFieldtrip
-(void)ChangeDate:(id)sender
{
    
//    
//    NSDate *currentTime = [NSDate date];
//    
//    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat:@"HH:mm"];
//    
//    NSString *hourCountString = [timeFormatter stringFromDate:currentTime];
//    
//    NSArray* foo = [hourCountString componentsSeparatedByString: @":"];
//   strHour = [foo objectAtIndex: 0];
//    strMin = [foo objectAtIndex: 1];
//    
    
    
    UIButton *btn = (UIButton *)(id)sender;
    
    NSString *strDateToPrint = [[_arrCurrentitinerary objectAtIndex:btn.tag ] valueForKey:@"date"];
    
   
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];
    NSDate *date = [dateFormatter dateFromString:strDateToPrint];
     [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    strDate = [dateFormatter stringFromDate:date];
    
    
    NSString *strMonth = [strDateToPrint substringWithRange:NSMakeRange(0,2)];
    NSString *strDay = [strDateToPrint substringWithRange:NSMakeRange(3, 2)];
    NSString *strYear = [strDateToPrint substringWithRange:NSMakeRange(6, 4)];
    strHour = [strDateToPrint substringWithRange:NSMakeRange(11, 2)];
    strMin = [strDateToPrint substringWithRange:NSMakeRange(14, 2)];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    viewForDateChange= [[UIView alloc] initWithFrame:screenRect];
    viewForDateChange.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(30,180, [[UIScreen mainScreen]bounds].size.width-60, 240.0)];
    vwForAccessCode.backgroundColor=[UIColor whiteColor];
    vwForAccessCode.layer.borderWidth=2;
    vwForAccessCode.layer.borderColor=[UIColor blueColor].CGColor;
    
    UILabel *lblfieldTripName=[[UILabel alloc]initWithFrame:CGRectMake(20, 20.0, 120, 20.0)];
    lblfieldTripName.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblfieldTripName.text=@"Field trip name";
    // lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblfieldTripName.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:lblfieldTripName];
    
   tfFieldtripName = [[UITextField alloc]initWithFrame:CGRectMake(20, 45, vwForAccessCode.frame.size.width-40, 40)];
    tfFieldtripName.layer.cornerRadius = 5;
    tfFieldtripName.delegate = self;
    tfFieldtripName.layer.borderWidth = 1;
    tfFieldtripName.returnKeyType = UIReturnKeyDone;
    tfFieldtripName.spellCheckingType = UITextSpellCheckingTypeNo;
    tfFieldtripName.autocorrectionType = UITextAutocorrectionTypeNo;
    tfFieldtripName.autocapitalizationType = UITextAutocapitalizationTypeWords;
    tfFieldtripName.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
    tfFieldtripName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
     tfFieldtripName.text = [[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip"];
    
    tfFieldtripName.leftViewMode = UITextFieldViewModeAlways;
    
    UILabel *rightView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    tfFieldtripName.leftView = rightView;
  
    
     [vwForAccessCode addSubview:tfFieldtripName];
    
    int x = 70;
    
    
    
    UILabel *lblEnterAccessCode=[[UILabel alloc]initWithFrame:CGRectMake(20, 20.0+x, 120, 20.0)];
    lblEnterAccessCode.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblEnterAccessCode.text=@"Change date *";
    // lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblEnterAccessCode.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:lblEnterAccessCode];
    
    
    btnMonth = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMonth.frame = CGRectMake(lblEnterAccessCode.frame.origin.x, 50+x, 30, 25.0);
    [vwForAccessCode addSubview:btnMonth];
    [btnMonth setTitle:strMonth forState:UIControlStateNormal];
    btnMonth.titleLabel.textColor = [UIColor grayColor];
    btnMonth.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    
    [btnMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMonth];
    
    btnDay = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDay.frame = CGRectMake(btnMonth.frame.origin.x+40,50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnDay];
    [btnDay setTitle:strDay forState:UIControlStateNormal];
    btnDay.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnDay.titleLabel.textColor=KTextColor;
    [btnDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnDay];
    
    btnYear = [UIButton buttonWithType:UIButtonTypeCustom];
    btnYear.frame = CGRectMake(btnDay.frame.origin.x+40, 50+x, 40.0, 25.0);
    [vwForAccessCode addSubview:btnYear];
    [btnYear setTitle:strYear forState:UIControlStateNormal];
    btnYear.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnYear.titleLabel.textColor = KTextColor;
    [btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnYear];
    
    
    btnHour = [UIButton buttonWithType:UIButtonTypeCustom];
    btnHour.frame = CGRectMake(btnYear.frame.origin.x+60, 50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnHour];
    [btnHour setTitle:strHour forState:UIControlStateNormal];
    btnHour.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnHour.titleLabel.textColor=KTextColor;
    [btnHour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnHour];
    
    UILabel *labelChangeTime = [[UILabel alloc]init];
    labelChangeTime.frame = CGRectMake(btnHour.frame.origin.x, 20+x, 100, 20);
    labelChangeTime.textColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    labelChangeTime.text = @"Change time *";
    // labelChangeTime.adjustsFontSizeToFitWidth = YES;
    labelChangeTime.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:labelChangeTime];
    
    
    btnMinute = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMinute.frame = CGRectMake(btnHour.frame.origin.x+40, 50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnMinute];
    [btnMinute setTitle:strMin forState:UIControlStateNormal];
    btnMinute.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnMinute.titleLabel.textColor = KTextColor;
    [btnMinute setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMinute];
    
    btnDateSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDateSelection.frame = CGRectMake(lblEnterAccessCode.frame.origin.x, 50+x,btnYear.frame.size.width+80, 25.0);
    [vwForAccessCode addSubview:btnDateSelection];
    [btnDateSelection setBackgroundColor:[UIColor clearColor]];
    [btnDateSelection addTarget:self action:@selector(SelectDate) forControlEvents:UIControlEventTouchUpInside];
    //  [btnDateSelection setBackgroundColor:[UIColor grayColor]];
    
    btnTimeSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTimeSelection.frame = CGRectMake(btnHour.frame.origin.x, 50+x,btnMinute.frame.size.width+50 , 30);
    [btnTimeSelection setBackgroundColor:[UIColor clearColor]];
    [btnTimeSelection addTarget:self action:@selector(TimeSelection) forControlEvents:UIControlEventTouchUpInside];
    [vwForAccessCode addSubview:btnTimeSelection];
    //
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width/2-60, 100+x, 60, 35)];
    [btnDone setTitle:@"Save" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
    [btnDone addTarget:self action:@selector(CallApiForDateChange:) forControlEvents:UIControlEventTouchUpInside];
    btnDone.tag = btn.tag;
    btnDone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [vwForAccessCode addSubview:btnDone];
    
    UIButton *btnCross=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width/2+20,100+x, 60,35)];
    //[btnCross setTitle:@"*" forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(RemoveAccessView) forControlEvents:UIControlEventTouchUpInside];
    [btnCross setTitle:@"Cancel" forState:UIControlStateNormal];
    btnCross.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    btnCross.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:btnCross];
    
    
    
    [viewForDateChange addSubview:vwForAccessCode];
    [self.view addSubview:viewForDateChange];
    
    
}

#pragma mark -  Method for the Api for Date Change

-(void)CallApiForDateChange:(id)sender
{
    [self.view endEditing:YES];
    
    objPickerView.hidden = YES;
    toolBar.hidden = YES;

    
    NSString *strFieldTripName = [tfFieldtripName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (strFieldTripName.length ==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for field trip name"];
        return;
    }
    
   
    _objPickerView.hidden = YES;
    _toolBar.hidden = YES;
    
//    if (strDate.length==0) {
//        [viewForDateChange removeFromSuperview];
//    }
//    else
//    {
        //http://192.168.125.52/projects/Teacherapp/index.php/Itineraries/edit_itinerary_date?access_token=fM5FzaXP0WEuyIS2Te8r&teacher_id=10&trip_id=3&itinerary_date=2016-10-06&itinerary_datetime=2016-10-06
        UIButton *btn = (UIButton *)(id)sender;
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *accesstoken = [defaults valueForKey:@"access_token"];
            NSString *teacherId = [defaults valueForKey:@"id"];
           // NSString *grade= [defaults valueForKey:@"ClassId"];
            NSString *strFleildTripId =[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip_id"];
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            
            
            NSString *strSelectedDate = [strDate substringWithRange:NSMakeRange(0, 10)];
            if (!(strTime.length==0)) {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" "];
                strSelectedDate = [strSelectedDate stringByAppendingString:strTime];
            }
            else
            {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" 09:00"];
            }
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *date = [formatter dateFromString:strSelectedDate];
            
            if (date == nil) {
                  strDate = [strSelectedDate substringWithRange:NSMakeRange(0, 10)];
            }else{
                [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
                strSelectedDate = [formatter stringFromDate:date];
                strDate = [strSelectedDate substringWithRange:NSMakeRange(0, 10)];
                
            }
          
            
            NSString *isUpdate = @"0";
            if ( [[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip"] isEqualToString:tfFieldtripName.text]) {
                isUpdate = @"0";
            }else{
               isUpdate = @"1";
            }
            
            NSString * unescapedQuery = [NSString stringWithFormat:@"?access_token=%@&teacher_id=%@&itinerary_date=%@&trip_id=%@&itinerary_datetime=%@&trip_name=%@&is_update=%@",accesstoken,teacherId,strDate,strFleildTripId,strSelectedDate,tfFieldtripName.text,isUpdate] ;
            NSString * escapedQuery = [unescapedQuery stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSString * urlString = [[NSString alloc] initWithFormat:@"%@index.php/Itineraries/edit_itinerary_date%@",kBaseUrl, escapedQuery];
            
            NSLog(@"UrlString=%@",urlString);
            [[WebService sharedInstance]ApiOfgetMethod:urlString andcompletionhandler:^(NSArray *returArray, NSError *error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
                if (!error) {
                    NSLog(@"updateNameArray=%@",returArray);
                    if ([[returArray valueForKey:@"error"] isEqualToString:@"Date exist"]) {
                        [[CommonMethods sharedInstance]AlertAction:@"Itinerary alerady exist for this date"];
                    }else if ([[returArray valueForKey:@"error"] isEqualToString:@"Trip exist"]) {
                     [[CommonMethods sharedInstance]AlertAction:@"Itinerary name alerady exist"];
                        
                    }
                    else{
                        //NSLog(@"returArray %@",returArray);
                   
                        [self ApiForCurrentItinerary];
                       // strDate = nil;
                    }
                }
                
                    }];
        }
        
        
   // }
    
    
}

#pragma mark -  Method for Cancel Button on Date change view

-(void)RemoveAccessView
{
    objPickerView.hidden = YES;
    toolBar.hidden = YES;

    [viewForDateChange removeFromSuperview];
}

#pragma mark -  method for adding border to any view

-(void)addTopBorder:(UILabel *)view{
    UIView *topView;
    topView= nil;
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderWidth = 1;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
    topView = [[UIView alloc] initWithFrame:CGRectMake(-10.0, 0.0, mainViewSize.width+20, borderWidth)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    // topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}

-(void)addlefBorder:(UILabel *)view{
    UIView *topView;
    topView= nil;
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderheight = mainViewSize.height+30;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
    topView = [[UIView alloc] initWithFrame:CGRectMake(-10.0, 0.0,1, borderheight)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    //  topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
    [self addBottomBorder:(UITableViewCell *)view.superview];
}
-(void)addBottomBorder:(UITableViewCell *)view{
    UIView *topView;
    topView=nil;
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderWidth = 1;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
    topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, mainViewSize.height+2+400, view.frame.size.width, borderWidth)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    //   topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}
-(void)addRightBorder:(UILabel *)view{
    UIView *topView;
    [topView removeFromSuperview];
    CGSize mainViewSize = view.bounds.size;
    // CGFloat borderWidth = 1;
    CGFloat borderheight = view.frame.size.height+30;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
    topView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width+10, 0.0, 1, borderheight)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    //  topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}


#pragma mark -  Method for Calling Api FOr Submitting the Sub Category

-(void)SaveButtonPressed:(id)sender
{
    NSMutableString *strMainCategoryId;
    UIButton *btn = (UIButton *)(id)sender;
    NSMutableString *strSelect = [[arrSubCategories valueForKey:@"sub_category_id"]objectAtIndex:btn.tag];
    NSString *str =strGradeForAddition; //[[NSUserDefaults standardUserDefaults]objectForKey:@"strFieldTripGrade"];
    
    
    
    NSString *strTrip_id = @"";
    for (int k =0; k<_arrCurrentitinerary.count; k++) {
        int fldSelected = [[[_arrCurrentitinerary objectAtIndex:k] valueForKey:@"fieldtrip_selected"] intValue];
        
        if (fldSelected ==1) {
            strTrip_id = [[_arrCurrentitinerary objectAtIndex:k] valueForKey:@"fieldtrip_id"];
        }
        
        
    }

    
    
  
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        NSString *strDateOnly = [strRecommendedTitle substringWithRange:NSMakeRange(0, 10)];
        
        [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strSelect date:strRecommendedTitle dateOnly:strDateOnly MainCategoryId:strMainCategoryId Grade:str TripId:strTrip_id andcompletionhandler:^(NSArray *returArray, NSError *error ){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                
                
                NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([errorstr isEqualToString:@"already exist"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"Already exist"];
                    return;
                    
                    
                }else if([returArray valueForKey:@"success"]){
                    
                    
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    arr = [returArray valueForKey:@"result"] ;
                    
                    [self ApiForCurrentItinerary];
                    sectionClickedForTripCHange = -1;
                    firstTime=YES;
                    
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
    }
    
}

#pragma mark -  Api Method for adding main Category

-(void)AddMainCategoryToItinerary
{
    {
        
        NSMutableString *strMainCategoryId = [NSMutableString stringWithFormat:@"%@",@"0"];
        // UIButton *btn = (UIButton *)(id)sender;
        NSMutableString *strSelect = [[arrRecommendedItinerary objectAtIndex:openCellIndex]valueForKey:@"categoryid"];
        
        
        NSString *strTrip_id = @"";
        for (int k =0; k<_arrCurrentitinerary.count; k++) {
            int fldSelected = [[[_arrCurrentitinerary objectAtIndex:k] valueForKey:@"fieldtrip_selected"] intValue];
            
            if (fldSelected ==1) {
                strTrip_id = [[_arrCurrentitinerary objectAtIndex:k] valueForKey:@"fieldtrip_id"];
            }
            
            
        }
        

        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            NSString *str =strGradeForAddition; //[[arrRecommendedItinerary objectAtIndex:openCellIndex] valueForKey:@""];//[[NSUserDefaults standardUserDefaults]objectForKey:@"strFieldTripGrade"];
            NSString *strDateForFieldTrip=  strRecommendedTitle;//[[NSUserDefaults standardUserDefaults] valueForKey:@"FieldTripDate"];
            NSString *strDateOnly = [strDateForFieldTrip substringWithRange:NSMakeRange(0, 10)];
            
            [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strMainCategoryId date:strRecommendedTitle dateOnly:strDateOnly MainCategoryId:strSelect Grade:str TripId:strTrip_id andcompletionhandler:^(NSArray *returArray, NSError *error ){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    
                    
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"Already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                        
                        
                        NSMutableArray *arr = [[NSMutableArray alloc]init];
                        arr = [returArray valueForKey:@"result"] ;
                        
                        [self ApiForCurrentItinerary];
                        sectionClickedForTripCHange = -1;
                        firstTime=YES;
                        
                    }
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
    }
    
}




#pragma mark -  Click Action for date change

-(void)SelectDate
{
    [self.view endEditing:YES];
    btnDateSelection.userInteractionEnabled = NO;
    objPickerView = [[UIDatePicker alloc]init];
    objPickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-150,[[UIScreen mainScreen] bounds].size.width , 150);
   
    [self.view.superview addSubview:objPickerView];
    //objPickerView.hidden = YES;
    objPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [objPickerView setDatePickerMode:UIDatePickerModeDate];
    objPickerView.minimumDate=[NSDate date];
    [objPickerView addTarget:self action:@selector(DateSelected:) forControlEvents:UIControlEventValueChanged];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    [self.view.superview addSubview:toolBar];
    
    NSDate *date= objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    strDate = [dateFormatter stringFromDate:date];
    
}

#pragma mark -  click action for time change

-(void)TimeSelection
{
    [self.view endEditing:YES];
    btnTimeSelection.userInteractionEnabled = NO;
    _objPickerView = [[UIDatePicker alloc]init];
    
    
    _objPickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-150,[[UIScreen mainScreen] bounds].size.width , 150);
    [self.view.superview addSubview:_objPickerView];
    _objPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [_objPickerView setDatePickerMode:UIDatePickerModeTime];
    
//    NSDate *date = _objPickerView.date;
//    NSCalendar *cal = [NSCalendar currentCalendar];
//    NSDateComponents *components = [cal components:NSCalendarUnitHour fromDate:date];
//    components.hour = 9;
//    components.minute = 00;
//    components.second = 0;
//    NSDate *dateWithTime11_14 = [cal dateFromComponents:components];
//    [_objPickerView setDate:dateWithTime11_14];
    [_objPickerView addTarget:self action:@selector(TimeSelected:) forControlEvents:UIControlEventValueChanged];
    
    _toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [_toolBar setBarStyle:UIBarStyleBlackOpaque];
    _barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClickedTime:)];
    _toolBar.items = @[_barButtonDone];
    _barButtonDone.tintColor=[UIColor blackColor];
    
    NSDate *date = _objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    strTime = [dateFormatter stringFromDate:date];
    
//    NSDate *date= objPickerView.date;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//    strDate = [dateFormatter stringFromDate:date];
    [self.view.superview addSubview:_toolBar];
}

#pragma mark -   Method for date selection

-(void)DateSelected:(id)sender
{
    
     [self.view endEditing:YES];
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
    }
    NSDate *date= objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    strDate = [dateFormatter stringFromDate:date];
    NSString *year = [strDate substringWithRange:NSMakeRange(0,4)];
    NSString *day = [strDate substringWithRange:NSMakeRange(8, 2)];
    NSString *month = [strDate substringWithRange:NSMakeRange(5, 2)];
    [btnYear setTitle:year forState:UIControlStateNormal];
    [btnDay setTitle:day forState:UIControlStateNormal];
    [btnMonth setTitle:month forState:UIControlStateNormal];
    
    //isDateSet = YES;
}

#pragma mark -  method for time slection

-(void)TimeSelected:(id)sender
{
    
     [self.view endEditing:YES];
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
    }
    NSLog(@"strDate = %@ and Today = %@",strDate,[NSDate date]);
         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *stringDate = [formatter stringFromDate:[NSDate date]];
    NSString *strd = [strDate substringToIndex:10];
    if ([strd isEqualToString:stringDate]) {
        _objPickerView.minimumDate = [NSDate date];
        NSLog(@"current");
    }else{
         NSLog(@"notcurrent");
    }

    
    NSDate *date = _objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    strTime = [dateFormatter stringFromDate:date];
    NSString *minute = [strTime substringWithRange:NSMakeRange(3, 2)];
    NSString *hour = [strTime substringWithRange:NSMakeRange(0,2)];
    [btnMinute setTitle:minute forState:UIControlStateNormal];
    [btnHour setTitle:hour forState:UIControlStateNormal];
    //  isTimeSet=YES;
    
    
}

#pragma mark -  Click Action of Done Button on Date Picker

-(void)DoneBtnClicked:(id)sender
{
    
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
        btnDateSelection.userInteractionEnabled = YES;
    }
    
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
        btnTimeSelection.userInteractionEnabled = YES;
    }
       NSDate *todayDate = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
   NSString  *strCurrentDate = [dateFormatter stringFromDate:todayDate];
    NSLog(@"strCurrentDate=%@ StrDate=%@",strCurrentDate,strDate);
    
    if ([strCurrentDate isEqualToString:[strDate substringWithRange:NSMakeRange(0, 10)]]) {
        NSString *strYear = [strCurrentDate substringWithRange:NSMakeRange(0,4)];
        NSString *strMonth = [strCurrentDate substringWithRange:NSMakeRange(5, 2)];
        NSString *strDay = [strCurrentDate substringWithRange:NSMakeRange(8, 2)];
        
        [btnYear setTitle:strYear forState:UIControlStateNormal];
        [btnMonth setTitle:strMonth forState:UIControlStateNormal];
        [btnDay setTitle:strDay forState:UIControlStateNormal];
        
    }else{
        NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
        NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
        NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
        
        [btnYear setTitle:strYear forState:UIControlStateNormal];
        [btnMonth setTitle:strMonth forState:UIControlStateNormal];
        [btnDay setTitle:strDay forState:UIControlStateNormal];
        
    }
    
    
    
//    if (strDate.length==0) {
//        
//    }
//    else
//    {
//        NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
//        NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
//        NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
//        
//        [btnYear setTitle:strYear forState:UIControlStateNormal];
//        [btnMonth setTitle:strMonth forState:UIControlStateNormal];
//        [btnDay setTitle:strDay forState:UIControlStateNormal];
//        
//    }
//    
  
    btnDateSelection.userInteractionEnabled = YES;
}


#pragma mark -  Click Method for Done Button on Time Picker

-(void)DoneBtnClickedTime:(id)sender
{
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
        btnDateSelection.userInteractionEnabled = YES;
    }
    
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
        btnTimeSelection.userInteractionEnabled= YES;
    }
    
    NSString *strHour1 = [strTime substringWithRange:NSMakeRange(0, 2)];
    NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
    [btnHour setTitle:strHour1 forState:UIControlStateNormal];
    [btnMinute setTitle:strMinute forState:UIControlStateNormal];
//    NSDate *todayDate = [NSDate date];
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"HH:mm"];
//    NSString  *strCurrentTime = [dateFormatter stringFromDate:todayDate];
//    NSLog(@"strCurrentTime=%@ strTime=%@",strCurrentTime,strTime);
//    
//    if ([strCurrentTime isEqualToString:strTime]) {
//        NSString *strHour1 = [strTime substringWithRange:NSMakeRange(0, 2)];
//        NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
//        [btnHour setTitle:strHour1 forState:UIControlStateNormal];
//        [btnMinute setTitle:strMinute forState:UIControlStateNormal];
//        
//    }else{
//        NSString *strHour1 = [strTime substringWithRange:NSMakeRange(0, 2)];
//        NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
//        [btnHour setTitle:strHour1 forState:UIControlStateNormal];
//        [btnMinute setTitle:strMinute forState:UIControlStateNormal];
//        
//        
//    }
    
    
//    if (strTime.length == 0) {
//        [btnHour setTitle:strHour forState:UIControlStateNormal];
//        [btnMinute setTitle:strMin forState:UIControlStateNormal];
//    }
//    
//    else
//    {
//        NSString *strHour = [strTime substringWithRange:NSMakeRange(0, 2)];
//        NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
//        [btnHour setTitle:strHour forState:UIControlStateNormal];
//        [btnMinute setTitle:strMinute forState:UIControlStateNormal];
//    }
    btnTimeSelection.userInteractionEnabled = YES;
}

#pragma mark - Button "Send To Putnam Museum" Tap
- (void) btnSendToPutnamMuseumClicked:(UIButton *)sender
{
    btnSendTag = (int)[sender tag];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    UIToolbar *toolBar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBar1 setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar1 setItems:toolbarItems];
    
    
    viewSendToPutnam = nil;
    viewSendToPutnam = [[UIView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64)];
    [viewSendToPutnam setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [self.view addSubview:viewSendToPutnam];
    
    scrlView = nil;
    scrlView = [[UIScrollView alloc] initWithFrame:CGRectMake(15, 15, [[UIScreen mainScreen] bounds].size.width-30, [[UIScreen mainScreen] bounds].size.height-94)];
    [scrlView.layer setCornerRadius:10.0];
    [scrlView setBackgroundColor:[UIColor whiteColor]];
    [viewSendToPutnam addSubview:scrlView];
    [scrlView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-30, 1210)];
    
    UILabel *lblHeading = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, [[UIScreen mainScreen] bounds].size.width-30, 21)];
    [lblHeading setText:@"VISIT REQUEST FORM"];
    [lblHeading setFont:[UIFont fontWithName:@"Roboto-Bold" size:20]];
    [lblHeading setTextAlignment:NSTextAlignmentCenter];
    [scrlView addSubview:lblHeading];
    lblHeading = nil;
    //---
    RPFloatingPlaceholderTextField *txtfSchoolAddress = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 46, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfSchoolAddress setPlaceholder:@"School Address"];
    txtfSchoolAddress.returnKeyType = UIReturnKeyNext;
    [txtfSchoolAddress setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfSchoolAddress setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfSchoolAddress setTag:1];
    // txtfSchoolAddress.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfSchoolAddress];
    
    UILabel *lblLine1 = [[UILabel alloc] initWithFrame:CGRectMake(15, 76, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine1 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine1];
    lblLine1 = nil;
    //----
    RPFloatingPlaceholderTextField *txtfZip = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-5, 126, ([[UIScreen mainScreen] bounds].size.width/2)-50, 30)];
    [txtfZip setPlaceholder:@"Zip"];
    [txtfZip setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfZip setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfZip setKeyboardType:UIKeyboardTypeNumberPad];
    [txtfZip setTag:2];
      txtfZip.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfZip];
    
    UILabel *lblLine2 = [[UILabel alloc] initWithFrame:CGRectMake(15, 116, ([[UIScreen mainScreen] bounds].size.width/2)-45, 1)];
    [lblLine2 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine2];
    lblLine2 = nil;
    //---
    RPFloatingPlaceholderTextField *txtfSchoolDistrict = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-5, 86, ([[UIScreen mainScreen] bounds].size.width/2)-50, 30)];
    [txtfSchoolDistrict setPlaceholder:@"District"];
    [txtfSchoolDistrict setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfSchoolDistrict setFloatingLabelActiveTextColor:[UIColor redColor]];
    txtfSchoolDistrict.returnKeyType = UIReturnKeyNext;
    [txtfSchoolDistrict setTag:3];
    //txtfSchoolDistrict.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfSchoolDistrict];
    
    UILabel *lblLine3 = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-5, 116, ([[UIScreen mainScreen] bounds].size.width/2)-40, 1)];
    [lblLine3 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine3];
    lblLine3 = nil;
    //----
    RPFloatingPlaceholderTextField *txtFieldCity = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 86, ([[UIScreen mainScreen] bounds].size.width/2)-45, 30)];
    [txtFieldCity setPlaceholder:@"City"];
    [txtFieldCity setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtFieldCity setFloatingLabelActiveTextColor:[UIColor redColor]];
    txtFieldCity.returnKeyType = UIReturnKeyNext;
    [txtFieldCity setTag:13];
    //txtfSchoolDistrict.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtFieldCity];
    //--
    UILabel *lblLine14 = [[UILabel alloc] initWithFrame:CGRectMake(15, 156, ([[UIScreen mainScreen] bounds].size.width/2)-45, 1)];
    [lblLine14 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine14];
    lblLine14 = nil;
    
    //--
    
    //15, 86, ([[UIScreen mainScreen] bounds].size.width/2)-45, 30
    //[[UIScreen mainScreen] bounds].size.width/2-5, 126, ([[UIScreen mainScreen] bounds].size.width/2)-50, 30
    
    RPFloatingPlaceholderTextField *txtFieldState = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 126, ([[UIScreen mainScreen] bounds].size.width/2)-45, 30)];
    [txtFieldState setPlaceholder:@"State"];
    [txtFieldState setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtFieldState setFloatingLabelActiveTextColor:[UIColor redColor]];
     txtFieldState.returnKeyType = UIReturnKeyNext;
    txtFieldState.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [txtFieldState setTag:14];
    //txtfSchoolDistrict.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtFieldState];
    //--
    UILabel *lblLine15 = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-5, 156, ([[UIScreen mainScreen] bounds].size.width/2)-40, 1)];
    [lblLine15 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine15];
    lblLine15 = nil;
    //--
    
    UILabel *lblDateToVisit = [[UILabel alloc] initWithFrame:CGRectMake(15, 166, [[UIScreen mainScreen] bounds].size.width-75, 21)];
    [lblDateToVisit setText:@"Date you want to visit -"];
    [lblDateToVisit setFont:[UIFont fontWithName:@"Roboto-Bold" size:15]];
    [scrlView addSubview:lblDateToVisit];
    lblDateToVisit = nil;
    //----
    UILabel *lblFirstChoice = [[UILabel alloc] initWithFrame:CGRectMake(15, 191, [[UIScreen mainScreen] bounds].size.width/2-50, 30)];
    [lblFirstChoice setText:@"First choice"];
    [lblFirstChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [scrlView addSubview:lblFirstChoice];
    lblFirstChoice = nil;
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0.0, self.view.frame.size.height-150, self.view.frame.size.width, 150)];
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datePicker.backgroundColor = [UIColor lightGrayColor];
    
    UITextField *txtfFirstChoice = [[UITextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29, 191, [[UIScreen mainScreen] bounds].size.width/2-15, 30)];
    [txtfFirstChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:13]];
    [txtfFirstChoice setTag:4];
    txtfFirstChoice.inputView = datePicker;
    txtfFirstChoice.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfFirstChoice];
    
    UILabel *lblLine4 = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-30, 222, [[UIScreen mainScreen] bounds].size.width/2-15, 1)];
    [lblLine4 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine4];
    lblLine4 = nil;
    //---
    UILabel *lblSecondChoice = [[UILabel alloc] initWithFrame:CGRectMake(15, 230, [[UIScreen mainScreen] bounds].size.width/2-50, 30)];
    [lblSecondChoice setText:@"Second choice"];
    [lblSecondChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [scrlView addSubview:lblSecondChoice];
    lblSecondChoice = nil;
    
    UITextField *txtfSecondChoice = [[UITextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29, 230, [[UIScreen mainScreen] bounds].size.width/2-15, 30)];
    [txtfSecondChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:13]];
    [txtfSecondChoice setTag:5];
    txtfSecondChoice.inputView = datePicker;
    txtfSecondChoice.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfSecondChoice];
    
    UILabel *lblLine5 = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-30, 260, [[UIScreen mainScreen] bounds].size.width/2-15, 1)];
    [lblLine5 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine5];
    lblLine5 = nil;
    //---
    UILabel *lblThirdChoice = [[UILabel alloc] initWithFrame:CGRectMake(15, 269, [[UIScreen mainScreen] bounds].size.width/2-50, 30)];
    [lblThirdChoice setText:@"Third choice"];
    [lblThirdChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [scrlView addSubview:lblThirdChoice];
    lblThirdChoice = nil;
    
    UITextField *txtfThirdChoice = [[UITextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29, 269, [[UIScreen mainScreen] bounds].size.width/2-15, 30)];
    [txtfThirdChoice setFont:[UIFont fontWithName:@"Roboto-Regular" size:13]];
    [txtfThirdChoice setTag:6];
    txtfThirdChoice.inputView = datePicker;
    txtfThirdChoice.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfThirdChoice];
    
    UILabel *lblLine6 = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-30, 298, [[UIScreen mainScreen] bounds].size.width/2-15, 1)];
    [lblLine6 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine6];
    lblLine6 = nil;
    //---
    UILabel *lblBestTimeToCall = [[UILabel alloc] initWithFrame:CGRectMake(15, 308, [[UIScreen mainScreen] bounds].size.width/2-50, 30)];
    [lblBestTimeToCall setText:@"Best time to call"];
    [lblBestTimeToCall setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [scrlView addSubview:lblBestTimeToCall];
    lblThirdChoice = nil;
    
    RPFloatingPlaceholderTextField *txtfBestTimeToCallFrom = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29, 308, [[UIScreen mainScreen] bounds].size.width/4-25, 30)];
    [txtfBestTimeToCallFrom setFont:[UIFont fontWithName:@"Roboto-Regular" size:13]];
    [txtfBestTimeToCallFrom setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfBestTimeToCallFrom setPlaceholder:@"From"];
    [txtfBestTimeToCallFrom setTag:7];
    txtfBestTimeToCallFrom.inputView = datePicker;
    txtfBestTimeToCallFrom.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfBestTimeToCallFrom];
    
    UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-30, 337, [[UIScreen mainScreen] bounds].size.width/4-25, 1)];
    [lblLine setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine];
    lblLine = nil;
    
    //--
    
    UILabel *labelTo = [[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29 +[[UIScreen mainScreen] bounds].size.width/4-25, 308, 25, 30)];
    
    labelTo.text = @"-";
    [labelTo setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [scrlView addSubview:labelTo];
    labelTo.textAlignment = NSTextAlignmentCenter;
    //labelTo.backgroundColor = [UIColor redColor];
    
    //--
    
    RPFloatingPlaceholderTextField *txtfBestTimeToCallTo = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29+[[UIScreen mainScreen] bounds].size.width/4-25+30, 308, [[UIScreen mainScreen] bounds].size.width/4-15, 30)];
    [txtfBestTimeToCallTo setFont:[UIFont fontWithName:@"Roboto-Regular" size:13]];
    [txtfBestTimeToCallTo setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfBestTimeToCallTo setPlaceholder:@"To"];
    [txtfBestTimeToCallTo setTag:15];
    txtfBestTimeToCallTo.inputView = datePicker;
    txtfBestTimeToCallTo.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfBestTimeToCallTo];
    
    
    
    UILabel *lblLine30= [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-29+[[UIScreen mainScreen] bounds].size.width/4-25+30, 337,[[UIScreen mainScreen] bounds].size.width-( [[UIScreen mainScreen] bounds].size.width/2-29+[[UIScreen mainScreen] bounds].size.width/4-15+30)-35, 1)];
    [lblLine30 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine30];
    lblLine30 = nil;
    
    //-----
    UILabel *lblNeedLunchSpace = [[UILabel alloc] initWithFrame:CGRectMake(15, 347, [[UIScreen mainScreen] bounds].size.width-155, 50)];
    [lblNeedLunchSpace setNumberOfLines:0];
    [lblNeedLunchSpace setText:@"Will you need lunchroom space?"];
    [lblNeedLunchSpace setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblNeedLunchSpace];
    lblNeedLunchSpace = nil;
    
    UIButton *btnNeedLunchSpaceYes = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNeedLunchSpaceYes setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-140, 347, 50,50)];
    [btnNeedLunchSpaceYes setTitle:@"Yes" forState:UIControlStateNormal];
    [btnNeedLunchSpaceYes setSelected:YES];
    [btnNeedLunchSpaceYes.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnNeedLunchSpaceYes setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnNeedLunchSpaceYes setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnNeedLunchSpaceYes setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnNeedLunchSpaceYes.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnNeedLunchSpaceYes setTag:100];
    [btnNeedLunchSpaceYes addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnNeedLunchSpaceYes];
    
    UIButton *btnNeedLunchSpaceNo = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNeedLunchSpaceNo setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-85, 347, 50,50)];
    [btnNeedLunchSpaceNo setTitle:@"No" forState:UIControlStateNormal];
    [btnNeedLunchSpaceNo.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnNeedLunchSpaceNo setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnNeedLunchSpaceNo setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnNeedLunchSpaceNo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnNeedLunchSpaceNo.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnNeedLunchSpaceNo setTag:200];
    [btnNeedLunchSpaceNo addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnNeedLunchSpaceNo];
    //-----
    UILabel *lblUseConcessionStand = [[UILabel alloc] initWithFrame:CGRectMake(15, 400, [[UIScreen mainScreen] bounds].size.width-155, 50)];
    [lblUseConcessionStand setNumberOfLines:0];
    [lblUseConcessionStand setText:@"Use the Concession Stand?"];
    [lblUseConcessionStand setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblUseConcessionStand];
    [lblUseConcessionStand setNumberOfLines:0];
    lblUseConcessionStand = nil;
    
    UIButton *btnUseConcessionStandYes = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnUseConcessionStandYes setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-140, 400, 50,50)];
    [btnUseConcessionStandYes setTitle:@"Yes" forState:UIControlStateNormal];
    [btnUseConcessionStandYes.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnUseConcessionStandYes setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnUseConcessionStandYes setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnUseConcessionStandYes setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnUseConcessionStandYes.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnUseConcessionStandYes setTag:300];
    [btnUseConcessionStandYes addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnUseConcessionStandYes];
    
    UIButton *btnUseConcessionStandNo = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnUseConcessionStandNo setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-85, 400, 50,50)];
    [btnUseConcessionStandNo setTitle:@"No" forState:UIControlStateNormal];
    [btnUseConcessionStandNo setSelected:YES];
    [btnUseConcessionStandNo.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnUseConcessionStandNo setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnUseConcessionStandNo setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnUseConcessionStandNo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnUseConcessionStandNo.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnUseConcessionStandNo setTag:400];
    [btnUseConcessionStandNo addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnUseConcessionStandNo];
    
    //-----
    UILabel *lblVisitMuseumStore = [[UILabel alloc] initWithFrame:CGRectMake(15, 455, [[UIScreen mainScreen] bounds].size.width-155, 50)];
    [lblVisitMuseumStore setNumberOfLines:0];
    [lblVisitMuseumStore setText:@"Visit the Museum Store?"];
    [lblVisitMuseumStore setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblVisitMuseumStore];
    [lblVisitMuseumStore setNumberOfLines:0];
    lblVisitMuseumStore = nil;
    
    UIButton *btnVisitMuseumStoreYes = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVisitMuseumStoreYes setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-140, 455, 50,50)];
    [btnVisitMuseumStoreYes setTitle:@"Yes" forState:UIControlStateNormal];
    [btnVisitMuseumStoreYes.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnVisitMuseumStoreYes setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnVisitMuseumStoreYes setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnVisitMuseumStoreYes setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnVisitMuseumStoreYes.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnVisitMuseumStoreYes setSelected:YES];
    [btnVisitMuseumStoreYes setTag:500];
    [btnVisitMuseumStoreYes addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnVisitMuseumStoreYes];
    
    UIButton *btnVisitMuseumStoreNo = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVisitMuseumStoreNo setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-85, 455, 50,50)];
    [btnVisitMuseumStoreNo setTitle:@"No" forState:UIControlStateNormal];
    [btnVisitMuseumStoreNo.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnVisitMuseumStoreNo setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnVisitMuseumStoreNo setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnVisitMuseumStoreNo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnVisitMuseumStoreNo.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnVisitMuseumStoreNo setTag:600];
    [btnVisitMuseumStoreNo addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnVisitMuseumStoreNo];
    //-----
    UILabel *lblMayHavePermission = [[UILabel alloc] initWithFrame:CGRectMake(15, 515, [[UIScreen mainScreen] bounds].size.width-155, 80)];
    [lblMayHavePermission setNumberOfLines:0];
    [lblMayHavePermission setText:@"May we have your permission to take candid photos of your students/youth?"];
    [lblMayHavePermission setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblMayHavePermission];
    lblMayHavePermission = nil;
    
    UIButton *btnMayHavePermissionYes = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMayHavePermissionYes setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-140, 515, 50,80)];
    [btnMayHavePermissionYes setTitle:@"Yes" forState:UIControlStateNormal];
    [btnMayHavePermissionYes.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnMayHavePermissionYes setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnMayHavePermissionYes setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnMayHavePermissionYes setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMayHavePermissionYes.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnMayHavePermissionYes setSelected:YES];
    [btnMayHavePermissionYes setTag:700];
    [btnMayHavePermissionYes addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnMayHavePermissionYes];
    
    UIButton *btnMayHavePermissionNo = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMayHavePermissionNo setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-85, 515, 50,80)];
    [btnMayHavePermissionNo setTitle:@"No" forState:UIControlStateNormal];
    [btnMayHavePermissionNo.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [btnMayHavePermissionNo setImage:[UIImage imageNamed:@"radio_button"] forState:UIControlStateNormal];
    [btnMayHavePermissionNo setImage:[UIImage imageNamed:@"red_radio_button"] forState:UIControlStateSelected];
    [btnMayHavePermissionNo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMayHavePermissionNo.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [btnMayHavePermissionNo setTag:800];
    [btnMayHavePermissionNo addTarget:self action:@selector(btnYesNoRadioButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scrlView addSubview:btnMayHavePermissionNo];
    //----
    UILabel *lblFreePercentage = [[UILabel alloc] initWithFrame:CGRectMake(15, 600, [[UIScreen mainScreen] bounds].size.width-60, 45)];
    [lblFreePercentage setNumberOfLines:0];
    [lblFreePercentage setText:@"What is your free/reduced rate lunch school percentage?"];
    [lblFreePercentage setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblFreePercentage];
    [lblFreePercentage setNumberOfLines:0];
    lblFreePercentage = nil;
    
    UITextField *txtfFreePercentage = [[UITextField alloc] initWithFrame:CGRectMake(15, 650, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfFreePercentage setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfFreePercentage setTag:8];
    txtfFreePercentage.keyboardType = UIKeyboardTypeNumberPad;
    txtfFreePercentage.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfFreePercentage];
    
    UILabel *lblLine7 = [[UILabel alloc] initWithFrame:CGRectMake(15, 675, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine7 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine7];
    lblLine7 = nil;
    //----
    UILabel *lblWhosComming = [[UILabel alloc] initWithFrame:CGRectMake(15, 680, [[UIScreen mainScreen] bounds].size.width-75, 25)];
    [lblWhosComming setText:@"Who’s Coming?"];
    [lblWhosComming setFont:[UIFont fontWithName:@"Roboto-Bold" size:16]];
    [scrlView addSubview:lblWhosComming];
    lblWhosComming = nil;
    //----
    RPFloatingPlaceholderTextField *txtfNoOfClassrooms = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 710, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfNoOfClassrooms setPlaceholder:@"Total Number of Classrooms"];
    [txtfNoOfClassrooms setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfNoOfClassrooms setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfNoOfClassrooms setTag:9];
    [txtfNoOfClassrooms setKeyboardType:UIKeyboardTypeNumberPad];
    txtfNoOfClassrooms.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfNoOfClassrooms];
    
    UILabel *lblLine8 = [[UILabel alloc] initWithFrame:CGRectMake(15, 740, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine8 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine8];
    lblLine8 = nil;
    //---
    RPFloatingPlaceholderTextField *txtfNoOfTeachers = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 750, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfNoOfTeachers setPlaceholder:@"Total Number of Teachers"];
    [txtfNoOfTeachers setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfNoOfTeachers setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfNoOfTeachers setTag:10];
    [txtfNoOfTeachers setKeyboardType:UIKeyboardTypeNumberPad];
    txtfNoOfTeachers.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfNoOfTeachers];
    
    UILabel *lblLine9 = [[UILabel alloc] initWithFrame:CGRectMake(15, 780, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine9 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine9];
    lblLine9 = nil;
    //----
    UILabel *lblMessage1 = [[UILabel alloc] initWithFrame:CGRectMake(15, 785, [[UIScreen mainScreen] bounds].size.width-60, 85)];
    [lblMessage1 setNumberOfLines:0];
    [lblMessage1 setText:@"(One complimentary teacher per classroom or one complimentary adult per 20 youth in non-school groups)"];
    [lblMessage1 setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblMessage1];
    [lblMessage1 setNumberOfLines:0];
    lblMessage1 = nil;
    //----
    RPFloatingPlaceholderTextField *txtfNoOfStudents = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 880, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfNoOfStudents setPlaceholder:@"Total number of Students?"];
    [txtfNoOfStudents setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfNoOfStudents setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfNoOfStudents setTag:11];
    [txtfNoOfStudents setKeyboardType:UIKeyboardTypeNumberPad];
    txtfNoOfStudents.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfNoOfStudents];
    
    UILabel *lblLine10 = [[UILabel alloc] initWithFrame:CGRectMake(15, 910, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine10 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine10];
    lblLine10 = nil;
    //---
    RPFloatingPlaceholderTextField *txtfNoOfNonTeacherAdults = [[RPFloatingPlaceholderTextField alloc] initWithFrame:CGRectMake(15, 920, [[UIScreen mainScreen] bounds].size.width-60, 30)];
    [txtfNoOfNonTeacherAdults setPlaceholder:@"Total number of Non-teacher Adults?"];
    [txtfNoOfNonTeacherAdults setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    [txtfNoOfNonTeacherAdults setFloatingLabelActiveTextColor:[UIColor redColor]];
    [txtfNoOfNonTeacherAdults setTag:12];
    [txtfNoOfNonTeacherAdults setKeyboardType:UIKeyboardTypeNumberPad];
    txtfNoOfNonTeacherAdults.inputAccessoryView = toolBar1;
    [scrlView addSubview:txtfNoOfNonTeacherAdults];
    
    UILabel *lblLine11 = [[UILabel alloc] initWithFrame:CGRectMake(15, 950, [[UIScreen mainScreen] bounds].size.width-60, 1)];
    [lblLine11 setBackgroundColor:[UIColor darkGrayColor]];
    [scrlView addSubview:lblLine11];
    lblLine11 = nil;
    //----
    UILabel *lblMessage2 = [[UILabel alloc] initWithFrame:CGRectMake(15, 960, [[UIScreen mainScreen] bounds].size.width-60, 150)];
    [lblMessage2 setNumberOfLines:0];
    [lblMessage2 setText:@"(One adult for every 10 students receives the student rate. Additional adults pay the applicable adult group rate if they are included in the group payment. Adults who pay at the door are charged non-group prices.)"];
    [lblMessage2 setFont:[UIFont fontWithName:@"Roboto-Regular" size:15]];
    [scrlView addSubview:lblMessage2];
    lblMessage2 = nil;
    //----
    UIButton *btnSend = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSend setFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width-190)/2-25, 1130, 80,35)];
    [btnSend setTitle:@"SEND" forState:UIControlStateNormal];
    [btnSend setBackgroundColor:[UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0]];
    [btnSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSend.titleLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
    [btnSend addTarget:self action:@selector(btnSendClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnSend.tag = btnSendTag;
    btnSend.showsTouchWhenHighlighted = YES;
    
    [scrlView addSubview:btnSend];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width-190)/2+105, 1130, 80,35)];
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setBackgroundColor:[UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0]];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCancel.titleLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:14]];
    [btnCancel addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
    btnCancel.showsTouchWhenHighlighted = YES;
    [scrlView addSubview:btnCancel];
    
    for(UIView *i in scrlView.subviews){
        if([i isKindOfClass:[UITextField class]]){
            [(UITextField *)i setDelegate:self];
            [(UITextField *)i setAutocorrectionType:UITextAutocorrectionTypeNo];
        }
    }
    
}
#pragma mark - Button Clicks
- (void) btnCancelClicked{
    [viewSendToPutnam removeFromSuperview];
    viewSendToPutnam = nil;
}
-(void)done:(id)sender{
    for (UIView *i in scrlView.subviews){
        if([i isKindOfClass:[UITextField class]]){
            UITextField *newTxtf = (UITextField *)i;
            
            if (newTxtf.tag == 8) {
                NSString *str = newTxtf.text;
                NSInteger intNumber = [str integerValue];
                
                if (intNumber>100) {
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter valid percentage"];
                }
            }
            [newTxtf resignFirstResponder];
            
        }
    }
    
}


-(void) dateTextField:(id)sender
{
    for (UIView *i in scrlView.subviews) {
        if([i isKindOfClass:[UITextField class]]){
            UITextField *textField = (UITextField *)i;
            //    UITextField  *textField = (UITextField *)[ subview viewWithTag:(int)[sender tag]];
            
            
            if (textField.tag == 4 && textFieldTag == 4) {
                [datePicker setMinimumDate:[NSDate date]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                NSDate *eventDate = datePicker.date;
                [dateFormat setDateFormat:@"MM-dd-yyyy"];
                
                NSString *dateString = [dateFormat stringFromDate:eventDate];
                textField.text= [NSString stringWithFormat:@"%@",dateString];
                break;
            }
            else if (textField.tag == 5 && textFieldTag == 5)
            {
                [datePicker setMinimumDate:[NSDate date]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                NSDate *eventDate = datePicker.date;
                [dateFormat setDateFormat:@"MM-dd-yyyy"];
                
                NSString *dateString = [dateFormat stringFromDate:eventDate];
                textField.text= [NSString stringWithFormat:@"%@",dateString];
                break;
            }
            else if (textField.tag == 6 && textFieldTag == 6)
            {
                [datePicker setMinimumDate:[NSDate date]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                NSDate *eventDate = datePicker.date;
                [dateFormat setDateFormat:@"MM-dd-yyyy"];
                
                NSString *dateString = [dateFormat stringFromDate:eventDate];
                textField.text= [NSString stringWithFormat:@"%@",dateString];
                break;
            }
            
            else if(textField.tag == 7 && textFieldTag == 7)
            {
                [datePicker setMinimumDate:[NSDate date]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                NSDate *eventDate = datePicker.date;
                [dateFormat setDateFormat:@"HH:mm"];
                
                NSString *dateString = [dateFormat stringFromDate:eventDate];
                textField.text = [NSString stringWithFormat:@"%@",dateString];
                break;
                
            }
            else if (textField.tag==15 && textFieldTag==15)
            {
                [datePicker setMinimumDate:[NSDate date]];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                NSDate *eventDate = datePicker.date;
                [dateFormat setDateFormat:@"HH:mm"];
                
                NSString *dateString = [dateFormat stringFromDate:eventDate];
                textField.text = [NSString stringWithFormat:@"%@",dateString];
                break;
            }
            //        else if ([textField isEqual: _textFieldStartTime]) {
            //            [datePicker setMinimumDate:[NSDate date]];
            //            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            //            NSDate *eventDate = datePicker.date;
            //            [dateFormat setDateFormat:@"hh:mm a"];
            //
            //            NSString *dateString = [dateFormat stringFromDate:eventDate];
            //            _textFieldStartTime.text = [NSString stringWithFormat:@"%@",dateString];
            //        }
        }
        
    }
}

- (void) btnSendClicked:(id)sender{
    BOOL isOK = YES;
    NSString *strTimeForCall;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    for (UIView *i in scrlView.subviews){
        if([i isKindOfClass:[UITextField class]]){
            UITextField *newTxtf = (UITextField *)i;
            if([newTxtf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length  == 0){
                if(newTxtf.tag == 1){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter school address."];
                    isOK = NO;
                    break;
                }
                else if (newTxtf.tag==13)
                {
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter city."];
                    isOK = NO;
                    break;
                }
                else if (newTxtf.tag==14)
                {
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter district."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 2){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter zip code."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 3){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter state."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 4){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter first choice."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 5){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter second choice."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 6){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter third choice."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 7|| newTxtf.tag==15){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter best time to call."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 8){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter your free/reduced rate lunch school percentage."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 9){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter total number of classrooms."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 10){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter total number of teachers."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 11){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter total number of students."];
                    isOK = NO;
                    break;
                }
                else if(newTxtf.tag == 12){
                    [[CommonMethods sharedInstance]AlertAction:@"Please enter total number of non-teacher adults."];
                    isOK = NO;
                    break;
                }
            }
            
            else
            {
                if(newTxtf.tag == 1){
                    [dict setObject:newTxtf.text forKey:@"school_address"];
                    // break;
                }
                else if(newTxtf.tag == 2){
                    [dict setObject:newTxtf.text forKey:@"zip"];
                    //  break;
                }
                else if(newTxtf.tag == 3){
                    [dict setObject:newTxtf.text forKey:@"distric"];
                    //  break;
                }
                else if(newTxtf.tag == 4){
                    [dict setObject:newTxtf.text forKey:@"date_first"];
                    //  break;
                }
                else if(newTxtf.tag == 5){
                    [dict setObject:newTxtf.text forKey:@"date_second"];
                    //  break;
                }
                else if(newTxtf.tag == 6){
                    [dict setObject:newTxtf.text forKey:@"date_third"];
                    // break;
                }
                else if(newTxtf.tag == 7){
                    
                    strTimeForCall = newTxtf.text;
                    // [dict setObject:newTxtf.text forKey:@"time_arrival"];
                    //  break;
                }
                else if(newTxtf.tag == 8){
                    [dict setObject:newTxtf.text forKey:@"lunch_rate_percentage"];
                    //  break;
                }
                else if(newTxtf.tag == 9){
                    [dict setObject:newTxtf.text forKey:@"total_classrooms"];
                    // break;
                }
                else if(newTxtf.tag == 10){
                    [dict setObject:newTxtf.text forKey:@"total_teacher"];
                    // break;
                }
                else if(newTxtf.tag == 11){
                    [dict setObject:newTxtf.text forKey:@"total_student"];
                    // break;
                }
                else if(newTxtf.tag == 12){
                    [dict setObject:newTxtf.text forKey:@"total_non_adult_teacher"];
                    // break;
                }
                else if (newTxtf.tag==13)
                {
                    [dict setObject:newTxtf.text forKey:@"school_city"];
                }
                else if (newTxtf.tag==14)
                {
                    [dict setObject:newTxtf.text forKey:@"school_state"];
                }
                else if (newTxtf.tag==15)
                {
                    strTimeForCall = [strTimeForCall stringByAppendingString:[NSString stringWithFormat:@" - %@",newTxtf.text]];
                    [dict setObject:strTimeForCall forKey:@"best_time_to_call"];
                }
                
            }
        }
    }
    if(!isOK)
        return;
    else
    {
        
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)intMuseumStore] forKey:@"museum_store"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)intPermissionPhoto] forKey:@"permission_photo"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)intConsessionStand] forKey:@"consession_stand"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)intNeedLunchroom] forKey:@"need_lunchroom"];
        if (btnSendTag == 2016) {
            NSString *strFieldTripId =  [[_arrCurrentitinerary objectAtIndex:0] valueForKey:@"fieldtrip_id"];
            NSString *strdate =  [[_arrCurrentitinerary objectAtIndex:0] valueForKey:@"date"];
            strdate = [strdate substringWithRange:NSMakeRange(11, 5)];
            [dict setObject:strdate forKey:@"time_arrival"];
            [dict setObject:strFieldTripId forKey:@"trip_id"];
            [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"] forKey:@"access_token"];
        }
        else
        {
            NSString *strFieldTripId =  [[_arrCurrentitinerary objectAtIndex:btnSendTag] valueForKey:@"fieldtrip_id"];
            NSString *strdate =  [[_arrCurrentitinerary objectAtIndex:btnSendTag] valueForKey:@"date"];
            strdate = [strdate substringWithRange:NSMakeRange(11, 5)];
            [dict setObject:strdate forKey:@"time_arrival"];
            [dict setObject:strFieldTripId forKey:@"trip_id"];
            [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"] forKey:@"access_token"];
            
        }
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Requestforms/post_request",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                 [[CommonMethods sharedInstance] removeSpinner:self.view];
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                          [[CommonMethods sharedInstance] AlertAction:[returArray valueForKey:@"success"]];
                        
                        [viewSendToPutnam removeFromSuperview];
                        viewSendToPutnam = nil;
                        [self ApiForCurrentItinerary];
                        // [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                }
                            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
        
        
        
        
        
        
        
    }
}
- (void) btnYesNoRadioButtonsClicked:(UIButton*) sender{
    if(![sender isSelected]){
        [sender setSelected:YES];
        if([sender tag] == 100){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 200){
                        [newBtn setSelected:NO];
                        intNeedLunchroom = 1;
                    }
                }
            }
        }
        else if([sender tag] == 200){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 100){
                        [newBtn setSelected:NO];
                        intNeedLunchroom = 0;
                    }
                }
            }
        }
        else if([sender tag] == 300){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 400){
                        [newBtn setSelected:NO];
                        intConsessionStand = 1;
                    }
                }
            }
        }
        else if([sender tag] == 400){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 300){
                        [newBtn setSelected:NO];
                        intConsessionStand = 0;
                    }
                }
            }
        }
        else if([sender tag] == 500){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 600){
                        [newBtn setSelected:NO];
                        intMuseumStore = 1;
                    }
                }
            }
        }
        else if([sender tag] == 600){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 500){
                        [newBtn setSelected:NO];
                        intMuseumStore = 0;
                    }
                }
            }
        }
        else if([sender tag] == 700){
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 800){
                        [newBtn setSelected:NO];
                        intPermissionPhoto = 1;
                    }
                }
            }
        }
        else{
            for (UIView *i in scrlView.subviews){
                if([i isKindOfClass:[UIButton class]]){
                    UIButton *newBtn = (UIButton *)i;
                    if(newBtn.tag == 700){
                        [newBtn setSelected:NO];
                        intPermissionPhoto = 0;
                    }
                }
            }
        }
    }
}
#pragma mark - UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.tag==8) {
        NSString *str = textField.text;
        NSInteger intNumber = [str integerValue];
        if (intNumber>100) {
            [[CommonMethods sharedInstance]AlertAction:@"Please enter valid percentage"];
        }
        else
        {
            textField.text = [str stringByAppendingString:@"%"];
        }
    }
    [textField resignFirstResponder];
    return YES;
    
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if (textField.tag == 8 || textField.tag == 9 || textField.tag == 10) {
//        datePicker.datePickerMode = UIDatePickerModeDate;
//    }
//
//}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    int scrollHeight = 0;
    if([[UIScreen mainScreen] bounds].size.height == 480){
        scrollHeight = 610;
        if(textField.tag == 2 || textField.tag == 3)
            [scrlView setContentOffset:CGPointMake(0, 25) animated:YES];
        else if(textField.tag == 4)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 60) animated:YES];
        }
        else if(textField.tag == 5)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 95) animated:YES];
        }
        else if(textField.tag == 6 )
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 130) animated:YES];
        }
        else if (textField.tag == 7)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 130) animated:YES];
            
        }
        else if (textField.tag == 15)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 130) animated:YES];
            
        }
        
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568){
        scrollHeight = 520;
        
        if (textField.tag == 4) {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 5)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 6 )
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 7)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 15)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
    }
    else if([[UIScreen mainScreen] bounds].size.height == 667)
        
    {
        scrollHeight = 420;
        
        if (textField.tag == 4) {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 5)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 6 )
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 7)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 15)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
    }
    
    
    else if([[UIScreen mainScreen] bounds].size.height == 736)
    {
        scrollHeight = 360;
        
        if (textField.tag == 4) {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 5)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 6 )
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 7)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 15)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
    }
    
    
    
    else if([[UIScreen mainScreen] bounds].size.height == 1024)
    {
        scrollHeight = 170;
        
        if (textField.tag == 4) {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 5)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
        }
        else if(textField.tag == 6 )
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeDate;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 7)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
        else if (textField.tag == 15)
        {
            textFieldTag = (int)textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [scrlView setContentOffset:CGPointMake(0, 65) animated:YES];
        }
    }
    
    
    if(textField.tag == 8)
    {
        [scrlView setContentOffset:CGPointMake(0, scrollHeight) animated:YES];
    }
    else if(textField.tag == 9)
    {
        datePicker.datePickerMode = UIDatePickerModeDate;
        [scrlView setContentOffset:CGPointMake(0, scrollHeight+40) animated:YES];
    }
    else if(textField.tag == 10)
    {
        // datePicker.datePickerMode = UIDatePickerModeDate;
        [scrlView setContentOffset:CGPointMake(0, scrollHeight+180) animated:YES];
    }
    else if(textField.tag == 11)
    {
        // datePicker.datePickerMode = UIDatePickerModeTime;
        [scrlView setContentOffset:CGPointMake(0, scrollHeight+220) animated:YES];
    }
    else if(textField.tag == 12)
        [scrlView setContentOffset:CGPointMake(0, scrollHeight+220) animated:YES];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==8) {
        if (textField.text.length>0) {
            textField.text = [textField.text stringByAppendingString:@"%"];
            
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) { // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
        return NO;
    }
    if(textField.tag == 13 || textField.tag==3 || textField.tag == 14)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
        
    }
    
    
    if (textField.tag==1 ) {
        return newLength <= 100 || returnKey;
    }
    else if (textField.tag==2){
        return newLength <= 5 || returnKey;
    }else if (textField.tag==3){
        return newLength <= 30 || returnKey;
    }else if (textField.tag==8){
        return newLength <=2 || returnKey;
    }
    else if (textField.tag==9){
        return newLength <= 2 || returnKey;
    }else if (textField.tag==10){
        return newLength <= 2 || returnKey;
    }else if (textField.tag==11){
        return newLength <= 3 || returnKey;
        
    }
    else if (textField.tag ==12)
    {
        return newLength <= 2 || returnKey;
    }
    else
        return newLength <= 30 || returnKey;
    
}

#pragma mark - chooseDateForCloneFieldtrip
-(void)chooseDateForCloneFieldtrip:(id)sender
{
    UIButton *btn = (UIButton *)(id)sender;
    
//    NSString *strDateToPrint = [[_arrCurrentitinerary objectAtIndex:btn.tag ] valueForKey:@"date"];
//    NSString *strMonth = [strDateToPrint substringWithRange:NSMakeRange(0,2)];
//    NSString *strDay = [strDateToPrint substringWithRange:NSMakeRange(3, 2)];
//    NSString *strYear = [strDateToPrint substringWithRange:NSMakeRange(6, 4)];
//    strHour = [strDateToPrint substringWithRange:NSMakeRange(11, 2)];
//    strMin = [strDateToPrint substringWithRange:NSMakeRange(14, 2)];
    
    
    NSString *strDateToPrint = [[_arrCurrentitinerary objectAtIndex:btn.tag ] valueForKey:@"date"];
    NSString *strMonth = [strDateToPrint substringWithRange:NSMakeRange(0,2)];
    NSString *strDay = [strDateToPrint substringWithRange:NSMakeRange(3, 2)];
    NSString *strYear = [strDateToPrint substringWithRange:NSMakeRange(6, 4)];
    strHour = [strDateToPrint substringWithRange:NSMakeRange(11, 2)];
    strMin = [strDateToPrint substringWithRange:NSMakeRange(14, 2)];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];
    NSDate *date = [dateFormatter dateFromString:strDateToPrint];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    strDate = [dateFormatter stringFromDate:date];
    
    
    strTime = [[strHour stringByAppendingString:@":"]stringByAppendingString:strMin];
    

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    viewForDateChange= [[UIView alloc] initWithFrame:screenRect];
    viewForDateChange.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(30,180, [[UIScreen mainScreen]bounds].size.width-60, 180.0)];
    vwForAccessCode.backgroundColor=[UIColor whiteColor];
    vwForAccessCode.layer.borderWidth=2;
    vwForAccessCode.layer.borderColor=[UIColor blueColor].CGColor;
    
    UILabel *lblChangeDateForClone=[[UILabel alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-100, 15.0, 200, 20.0)];
    lblChangeDateForClone.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Select new date for clone trip"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName     value:[NSNumber numberWithInt:1]   range:(NSRange){0,[attributeString length]}];
    lblChangeDateForClone.attributedText=attributeString;
    lblChangeDateForClone.textAlignment = NSTextAlignmentCenter;
    // lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblChangeDateForClone.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:lblChangeDateForClone];

    
    int x = lblChangeDateForClone.frame.size.height+5;
    
    UILabel *lblEnterAccessCode=[[UILabel alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-100, 20.0+x, 120, 20.0)];
    lblEnterAccessCode.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblEnterAccessCode.text=@"Select date *";
    // lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblEnterAccessCode.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:lblEnterAccessCode];
    
    
    btnMonth = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMonth.frame = CGRectMake(lblEnterAccessCode.frame.origin.x, 50+x, 30, 25.0);
    [vwForAccessCode addSubview:btnMonth];
    [btnMonth setTitle:strMonth forState:UIControlStateNormal];
    btnMonth.titleLabel.textColor = [UIColor grayColor];
    btnMonth.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    
    [btnMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMonth];
    
    btnDay = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDay.frame = CGRectMake(btnMonth.frame.origin.x+40,50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnDay];
    [btnDay setTitle:strDay forState:UIControlStateNormal];
    btnDay.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnDay.titleLabel.textColor=KTextColor;
    [btnDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnDay];
    
    btnYear = [UIButton buttonWithType:UIButtonTypeCustom];
    btnYear.frame = CGRectMake(btnDay.frame.origin.x+40, 50+x, 40.0, 25.0);
    [vwForAccessCode addSubview:btnYear];
    [btnYear setTitle:strYear forState:UIControlStateNormal];
    btnYear.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnYear.titleLabel.textColor = KTextColor;
    [btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnYear];
    
    
    btnHour = [UIButton buttonWithType:UIButtonTypeCustom];
    btnHour.frame = CGRectMake(btnYear.frame.origin.x+60, 50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnHour];
    [btnHour setTitle:strHour forState:UIControlStateNormal];
    btnHour.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnHour.titleLabel.textColor=KTextColor;
    [btnHour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnHour];
    
    UILabel *labelChangeTime = [[UILabel alloc]init];
    labelChangeTime.frame = CGRectMake(btnHour.frame.origin.x-10, 20+x, 100, 20);
    labelChangeTime.textColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    labelChangeTime.text = @"Select time *";
    // labelChangeTime.adjustsFontSizeToFitWidth = YES;
    labelChangeTime.font =[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:labelChangeTime];
    
    
    btnMinute = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMinute.frame = CGRectMake(btnHour.frame.origin.x+40, 50+x, 30.0, 25.0);
    [vwForAccessCode addSubview:btnMinute];
    [btnMinute setTitle:strMin forState:UIControlStateNormal];
    btnMinute.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    btnMinute.titleLabel.textColor = KTextColor;
    [btnMinute setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMinute];
    
    btnDateSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDateSelection.frame = CGRectMake(lblEnterAccessCode.frame.origin.x, 50+x,btnYear.frame.size.width+80, 25.0);
    [vwForAccessCode addSubview:btnDateSelection];
    [btnDateSelection setBackgroundColor:[UIColor clearColor]];
    [btnDateSelection addTarget:self action:@selector(SelectDate) forControlEvents:UIControlEventTouchUpInside];
    //  [btnDateSelection setBackgroundColor:[UIColor grayColor]];
    
    btnTimeSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTimeSelection.frame = CGRectMake(btnHour.frame.origin.x, 50+x,btnMinute.frame.size.width+50 , 30);
    [btnTimeSelection setBackgroundColor:[UIColor clearColor]];
    [btnTimeSelection addTarget:self action:@selector(TimeSelection) forControlEvents:UIControlEventTouchUpInside];
    [vwForAccessCode addSubview:btnTimeSelection];
    //
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width/2-60, 100+x, 60, 30)];
    [btnDone setTitle:@"Save" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
    
    [btnDone addTarget:self action:@selector(CallApiForCloneTrip:) forControlEvents:UIControlEventTouchUpInside];
    btnDone.tag = btn.tag;
    btnDone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [vwForAccessCode addSubview:btnDone];
    
    UIButton *btnCross=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width/2+20,100+x, 60,30)];
    //[btnCross setTitle:@"*" forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(RemoveAccessView) forControlEvents:UIControlEventTouchUpInside];
    [btnCross setTitle:@"Cancel" forState:UIControlStateNormal];
    btnCross.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    btnCross.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
    [vwForAccessCode addSubview:btnCross];
    
    
    
    [viewForDateChange addSubview:vwForAccessCode];
    [self.view addSubview:viewForDateChange];
    
    
}
#pragma mark - CallApiForCloneTrip
-(void)CallApiForCloneTrip:(id)sender{
    
    objPickerView.hidden = YES;
    toolBar.hidden = YES;
    
    if (strDate.length==0) {
        [viewForDateChange removeFromSuperview];
    }
    else
    {
        //http://192.168.125.52/projects/Teacherapp/index.php/Itineraries/edit_itinerary_date?access_token=fM5FzaXP0WEuyIS2Te8r&teacher_id=10&trip_id=3&itinerary_date=2016-10-06&itinerary_datetime=2016-10-06
        UIButton *btn = (UIButton *)(id)sender;
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
                NSString *strSelectedDate = [strDate substringWithRange:NSMakeRange(0, 10)];
            
            if (!(strTime.length==0)) {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" "];
                strSelectedDate = [strSelectedDate stringByAppendingString:strTime];
            }
            else
            {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" 09:00"];
            }
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *date = [formatter dateFromString:strSelectedDate];
            [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
            strSelectedDate = [formatter stringFromDate:date];
            strDate = [strSelectedDate substringWithRange:NSMakeRange(0, 10)];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *accesstoken = [defaults valueForKey:@"access_token"];
            NSString *teacherId = [defaults valueForKey:@"id"];
            NSString *grade= [[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"grade"];
            NSString *strFleildTripId =[[_arrCurrentitinerary objectAtIndex:btn.tag] valueForKey:@"fieldtrip_id"];
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            
            

            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setValue:accesstoken forKey:@"access_token"];
            [dict setValue:strFleildTripId forKey:@"trip_id"];
            [dict setValue:teacherId forKey:@"teacher_id"];
            [dict setValue:strDate forKey:@"itinerary_date"];
            [dict setValue:strSelectedDate forKey:@"itinerary_datetime"];
            [dict setValue:grade forKey:@"grade"];
            
            NSLog(@"%@",dict);
            
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Itineraries/clone_trip",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                if (!error) {
                    if ([[returArray valueForKey:@"error"] isEqualToString:@"Date exist"]) {
                        [[CommonMethods sharedInstance]AlertAction:@"Itinerary alerady exist for this date"];
                    }else{
                        //NSLog(@"returArray %@",returArray);
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        [self ApiForCurrentItinerary];
                        strDate = nil;
                    }
                }
                
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    self.view.userInteractionEnabled = YES;
                }
            }];
        }
        
        
    }
    
    
    
    
    
    
    
}




@end



