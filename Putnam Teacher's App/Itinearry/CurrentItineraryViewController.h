//
//  CurrentItineraryViewController.h
//  Putnam Teacher's App
//
//  Created by inficare on 6/29/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CallCurrentItinerary <NSObject>

@optional
- (void) anOptionalDelegateFunction;

@required
- (void) CallChildViewController;

@end
@interface CurrentItineraryViewController : UIViewController
{
    id<CallCurrentItinerary> __weak delegate;
}
@property (nonatomic, weak) id delegate;
@end