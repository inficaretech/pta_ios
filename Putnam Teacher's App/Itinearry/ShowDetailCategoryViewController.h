//
//  ShowDetailCategoryViewController.h
//  Putnam Teacher's App
//
//  Created by inficare on 7/5/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailCategoryViewController : UIViewController
{
    
}
@property(nonatomic,strong) NSMutableArray *arrResponse;
@property(nonatomic,strong) NSString *strCategoryName;

@end
