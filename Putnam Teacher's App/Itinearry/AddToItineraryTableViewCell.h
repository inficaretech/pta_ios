//
//  AddToItineraryTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 6/24/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddToItineraryTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,weak)IBOutlet UIButton *btnSubCategory;
@property(nonatomic,weak)IBOutlet UIImageView *imageViewSubcategory;
@property(nonatomic,weak)IBOutlet UILabel *labelDescription;
@property(nonatomic,weak)IBOutlet UILabel *labelPlus;


@end
