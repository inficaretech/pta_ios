//
//  SelectItinerayTableViewCell.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/14/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "SelectItinerayTableViewCell.h"

@implementation SelectItinerayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
