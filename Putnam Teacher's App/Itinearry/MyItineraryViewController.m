//
//  MyItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/6/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "MyItineraryViewController.h"
#import "AssignGroupViewController.h"
#import "Constants.h"
#import "AssignGroupViewController.h"
#import "AssignGroupTableViewCell.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "CurrentItineraryViewController.h"
#import "SelectItineraryViewController.h"



@interface MyItineraryViewController ()<UITableViewDelegate,UITableViewDataSource,CallCurrentItinerary>
{
    UITableView *tableViewMyItinerary;
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    AssignGroupViewController *controller;
    NSString *strClass;
    WebService *webservice;
    UIButton *btnNext;
    
}

@end

@implementation MyItineraryViewController
@synthesize arrForCLassLevel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self UiForNavigationBar];
    
    [self CreateUI];
    // Do any additional setup after loading the view.
}



-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}


-(void)BackButtonPressed
{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_grades?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                
                
//                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                SelectTypeOfItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"SelectTypeOfItineraryViewController"];
                
//                MyItineraryViewController.arrForCLassLevel = [returArray mutableCopy];
//                [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
                
                
                //  MyItineraryViewController.arrForCLassLevel = [returArray mutableCopy];
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    

    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CreateUI
{
//    btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
//    btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
//    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
//    btnBuildItinerary.backgroundColor = kBtnSelectionColor;
//    [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnBuildItinerary];
//    if (IS_IPAD) {
//        btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
//    }
//    else
//    {
//        
//        btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    }
//    
//    btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
//    [btnCurrentItinerary setTitle:@"Current Itinerary" forState:UIControlStateNormal];
//    [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
//    btnCurrentItinerary.backgroundColor = kBtnUnselectedColor;
//    [self.view addSubview:btnCurrentItinerary];
//    
//    if (IS_IPAD) {
//        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
//    }
//    else
//    {
//        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    }
//    
    tableViewMyItinerary = [[UITableView alloc]init];
    
    if (IS_IPAD) {
        tableViewMyItinerary.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 550);
        
    }
    else
    {
        tableViewMyItinerary.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-150);
    }
    [self.view addSubview:tableViewMyItinerary];
    tableViewMyItinerary.delegate=self;
    tableViewMyItinerary.dataSource=self;
    tableViewMyItinerary.separatorColor = [UIColor clearColor];
    tableViewMyItinerary.scrollEnabled=NO;
    
    
//    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-35, [[UIScreen mainScreen]bounds].size.height-85,70 ,70 );
//    
//    [btnNext setImage: [UIImage imageNamed:@"next_button"] forState:UIControlStateNormal];
//    
//    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
//    [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-45.0, 0.0,0.0)];
//    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
//    [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnNext];
//    [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
//    btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
//    
    
//    if (IS_IPAD) {
//        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-90,70 ,70 )];
//    }
//    else
//    {
    //    [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-90,60 ,60 )];
//        
//    }
    
   // [[CommonMethods sharedInstance] CreateMenu:self.view];
}

-(void)BulidItinerary
{
    btnCurrentItinerary.backgroundColor =kBtnUnselectedColor;
    btnBuildItinerary.backgroundColor =kBtnSelectionColor;
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    tableViewMyItinerary.hidden = NO;
   }
-(void)CurrentItinerary
{
    btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
    btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
    
    tableViewMyItinerary.hidden = YES;
    CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
    [self addChildViewController:controller1];
    controller1.delegate = self;
    controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:controller1.view];
    [controller1 didMoveToParentViewController:self];
    btnCurrentItinerary.userInteractionEnabled=NO;
    
}
- (void) CallChildViewController{
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    tableViewMyItinerary.hidden = NO;
    [self CurrentItinerary];
    
}

-(void)goToNextView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
               // NSString *accesstoken = [defaults valueForKey:@"access_token"];
              //  NSString *teacherId = [defaults valueForKey:@"id"];
                [defaults setObject:strClass forKey:@"ClassId"];
    
            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
            if (isinternet) {
                [[CommonMethods sharedInstance] addSpinner:self.view];
                [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_grades?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
                    if (!error) {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        NSLog(@"returArray %@",returArray);
    
    
    
                        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SelectItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"SelectItineraryViewController"];
                      //   assignGroupView.arrResponse = [returArray mutableCopy];
                         self.view.userInteractionEnabled = YES;
                        [self.navigationController pushViewController:assignGroupView animated:YES];

                          
                    }
                    else
                    {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
    
                    }
                }];
    
    
            }else
            {
                [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
                
                
            }
            
            
            

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    
//    // http://14.141.136.170:8888/projects/Teacherapp/index.php/Teachers/get_teacher_grades?teacher_id=11&access_token=PK0La8my4Yjolv79TCx2
//         BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
//        if (isinternet) {
//            self.view.userInteractionEnabled= NO;
//            
//            // NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Grades/student_grades?grades=@%&access_token=fLDZ4FpiNIHljQeXkoxV";
//            
//            
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//           // NSString *accesstoken = [defaults valueForKey:@"access_token"];
//          //  NSString *teacherId = [defaults valueForKey:@"id"];
//            [defaults setObject:strClass forKey:@"ClassId"];
//            [[CommonMethods sharedInstance] addSpinner:self.view];
//            
//            http://14.141.136.170:8888/projects/Teacherapp/index.php/Teachers/get_teacher_grades?teacher_id=11&access_token=PK0La8my4Yjolv79TCx2
//            
//            //index.php/Grades/teacher_grades
//            [[WebService sharedInstance]GradeApiDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Grades/student_grades?grades&access_token",kBaseUrl] param: strClass andcompletionhandler:^(NSArray *returArray, NSError *error ){
//                if (!error) {
//                    [[CommonMethods sharedInstance] removeSpinner:self.view];
//                    NSLog(@"returArray %@",returArray);
//                    
//                    
//                    NSString *errorstr = [returArray valueForKey:@"error"];
//                    self.view.userInteractionEnabled= YES;
//                    if ([errorstr isEqualToString:@"already exist"]) {
//                        [[CommonMethods sharedInstance] AlertAction:@"Already exist"];
//                        return;
//                        
//                        
//                    }else if([returArray valueForKey:@"success"]){
//                        
//                        
//                        NSMutableArray *arr = [[NSMutableArray alloc]init];
//                        arr = [returArray valueForKey:@"result"] ;
//                        
//                        [[CommonMethods sharedInstance] removeSpinner:self.view];
//                        
//                        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                        SelectItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"SelectItineraryViewController"];
//                        // assignGroupView.arrResponse = [arrResponse mutableCopy];
//                        self.view.userInteractionEnabled = YES;
//                        [self.navigationController pushViewController:assignGroupView animated:YES];
//                        
//                    }
//                    
//                }
//                else
//                {
//                    [[CommonMethods sharedInstance] removeSpinner:self.view];
//                    
//                }
//            }];
//            
//            
//        }else
//        {
//            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
//        }
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIButton *btn;
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor whiteColor];
    
    if (indexPath.row==0) {
        cell.textLabel.text=@"SELECT GRADE LEVEL";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if (IS_IPAD) {
            cell.textLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
        }
        else
        {
            cell.textLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        }
        cell.textLabel.textColor= KTextColor;
        
    }else{
        btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(20.0, 10.0, [[UIScreen mainScreen]bounds].size.width-40, 40);
        [btn addTarget:self action:@selector(ButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag=indexPath.row;
        btn.layer.shadowColor = [UIColor grayColor].CGColor;
        btn.layer.shadowOffset = CGSizeMake(0, 1.0);
        btn.layer.shadowOpacity = 1.0;
        btn.layer.shadowRadius = 3.0;
        [cell.contentView addSubview:btn];
        
        if (indexPath.row==1) {
            
            btn.backgroundColor=kBtnUnselectedColor;
            [btn setTitle:@"Pre - k" forState:UIControlStateNormal];
            
        }else if (indexPath.row==2){
            btn.backgroundColor=kBtnUnselectedColor;
            [btn setTitle:@"K - 2" forState:UIControlStateNormal];
            
        }else if (indexPath.row==3){
            btn.backgroundColor=kBtnUnselectedColor;
            [btn setTitle:@"3rd - 5th" forState:UIControlStateNormal];
            
        }else if (indexPath.row==4){
            btn.backgroundColor=kBtnUnselectedColor;
            [btn setTitle:@"6th - 8th" forState:UIControlStateNormal];
            
        }else if (indexPath.row==5){
            btn.backgroundColor=kBtnUnselectedColor;
            [btn setTitle:@"High School +" forState:UIControlStateNormal];
        }
        
        if ([[arrForCLassLevel valueForKey:@"grade"]integerValue] == indexPath.row) {
            btn.backgroundColor =kBtnSelectionColor;
        }
        if (IS_IPAD) {
            btn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
            btn.frame=CGRectMake(20.0, 10.0, [[UIScreen mainScreen]bounds].size.width-40, 50);
            
            
        }
        else
        {
            btn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
        }
    }
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_IPAD)
    {
        if (indexPath.row==0)
        {
            return 70;
        }
        else{
            
            return 70;
        }
        
    }
    
    else
    {
        if (indexPath.row==0)
        {
            return 50;
        }
        else{
            
            return 60;
        }
    }
}


-(void)ButtonsClicked:(id)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
  //  NSLog(@"%ld",(long)btn.tag);
    NSString *strBtnTitle = btn.titleLabel.text;
    btn.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    
    UIButton *btn1 = [tableViewMyItinerary viewWithTag:1];
    UIButton *btn2 = [tableViewMyItinerary viewWithTag:2];
    UIButton *btn3 = [tableViewMyItinerary viewWithTag:3];
    UIButton *btn4 = [tableViewMyItinerary viewWithTag:4];
    UIButton *btn5 = [tableViewMyItinerary viewWithTag:5];
    
    switch (btn.tag) {
        case 1:
            btn1.backgroundColor = kBtnSelectionColor;
            btn2.backgroundColor= kBtnUnselectedColor;
            btn3.backgroundColor= kBtnUnselectedColor;
            btn4.backgroundColor=kBtnUnselectedColor;
            btn5.backgroundColor=kBtnUnselectedColor;
            //[self goToNextView];
            break;
            
        case 2:
            
            btn2.backgroundColor = kBtnSelectionColor;
            btn1.backgroundColor=kBtnUnselectedColor;
            btn3.backgroundColor=kBtnUnselectedColor;
            btn4.backgroundColor=kBtnUnselectedColor;
            btn5.backgroundColor=kBtnUnselectedColor;
         //   [self goToNextView];
            break;
            
        case 3:
            
            
            btn3.backgroundColor = kBtnSelectionColor;
            btn2.backgroundColor= kBtnUnselectedColor;
            btn1.backgroundColor=kBtnUnselectedColor;
            btn4.backgroundColor=kBtnUnselectedColor;
            btn5.backgroundColor=kBtnUnselectedColor;
        //    [self goToNextView];
            break;
            
        case 4:
            
            btn4.backgroundColor = kBtnSelectionColor;
            btn2.backgroundColor=kBtnUnselectedColor;
            btn3.backgroundColor=kBtnUnselectedColor;
            btn1.backgroundColor=kBtnUnselectedColor;
            btn5.backgroundColor=kBtnUnselectedColor;
            
        //    [self goToNextView];
            break;
            
        case 5:
            
            btn5.backgroundColor = kBtnSelectionColor;
            btn2.backgroundColor=kBtnUnselectedColor;
            btn3.backgroundColor=kBtnUnselectedColor;
            btn4.backgroundColor=kBtnUnselectedColor;
            btn1.backgroundColor=kBtnUnselectedColor;
         //   [self goToNextView];
            break;
            
        default:
            break;
    }
    
    if ([strBtnTitle isEqualToString:@"Pre - k"]) {
        strClass = @"1";
    }
    
    else if ([strBtnTitle isEqualToString:@"K - 2"])
    {
        strClass =@"2";
    }
    else if ([strBtnTitle isEqualToString:@"3rd - 5th"])
    {
        strClass =@"3";
    }
    else if ([strBtnTitle isEqualToString:@"6th - 8th"])
    {
        strClass =@"4";
    }
    else if ([strBtnTitle isEqualToString:@"High School +"])
    {
        strClass =@"5";
    }
    
    [self goToNextView];

}


@end
