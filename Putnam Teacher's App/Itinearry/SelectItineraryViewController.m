//
//  SelectItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/9/16.
//  Copyright Ã‚Â© 2016 inficare. All rights reserved.
//

#import "SelectItineraryViewController.h"
#import "SelectItinerayTableViewCell.h"
#import "AddToItineraryViewController.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "CurrentItineraryViewController.h"
#import "ShowDetailCategoryViewController.h"
#import "AppDelegate.h"
#import "Constants.h"



@interface SelectItineraryViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,CallCurrentItinerary,UIAlertViewDelegate>
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    UITableView *tableViewItinerary;
    WebService *webService;
    NSMutableArray *arrImages;
    NSMutableArray *arrSelectedImages;
    BOOL isCurrentButton;
    UIButton *btnMonth;
    UIButton *btnDay;
    UIButton *btnYear;
    UIButton *btnMinute;
    UIButton *btnHour;
    UIDatePicker *objPickerView;
    UIDatePicker *_objPickerView;
    UIToolbar *toolBar;
    UIToolbar *_toolBar;
    UIBarButtonItem *barButtonDone;
    UIBarButtonItem *_barButtonDone;
    NSString *strDate;
    AppDelegate *delegate;
    NSMutableString *strSelectedSubCategory;
    BOOL isSaved;
    NSInteger indexPathForAlert;
    NSMutableString *strMainCategoryId;
    BOOL isDateSet;
    BOOL isTimeSet;
    NSString *strTime;
    UIButton *btnDateSelection;
    UIButton *btnTimeSelection;
    BOOL isCurrentViewController;
}
@end

@implementation SelectItineraryViewController
//@synthesize arrResponse;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton=YES;
    [self UiForNavigationBar];
    
    
    [self ApiForGettingCategory];
    [self CreateUI];
    arrSelectedImages = [[NSMutableArray alloc]init];
    isCurrentButton = NO;
    strSelectedSubCategory = [[NSMutableString alloc]init];
    btnBuildItinerary.userInteractionEnabled = NO;
    strMainCategoryId = [[NSMutableString alloc]init];
    
    [[CommonMethods sharedInstance]AlertAction:@"Field trip date and time chosen is tentative.After saving this itinerary press button Send to Putnam and we will contact you."];
}


-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

#pragma back button method
-(void)BackButtonPressed
{
    [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"strSelectedSubCategory"];
    
    if (isCurrentButton) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void)CreateUI
{
    
    
    
    // if ([[arrResponse valueForKey:@"itinerary"] isEqualToString:@"0"]) {
    
    
    //        UILabel *label= [[UILabel alloc]init];
    //        label.frame = CGRectMake(0.0, 64, [[UIScreen mainScreen] bounds].size.width, 49.0);
    //        label.text = @"Build Itinerary";
    //        label.textAlignment = NSTextAlignmentCenter;
    //        label.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    //        [self.view addSubview:label];
    //        label.textColor = [UIColor whiteColor];
    //        label.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    //    }
    //    else
    //    {
    //
    //        btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
    //        btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
    //        [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
    //        btnBuildItinerary.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    //        [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
    //        [self.view addSubview:btnBuildItinerary];
    //        btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    //
    //
    //
    //        btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    //        btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
    //        [btnCurrentItinerary setTitle:@"Saved Itinerary" forState:UIControlStateNormal];
    //        [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
    //        btnCurrentItinerary.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    //        [self.view addSubview:btnCurrentItinerary];
    //        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    //    }
    //
    
    
    UILabel *lblSelect=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 124.0, [[UIScreen mainScreen]bounds].size.width, 40)];
    lblSelect.textColor=[UIColor darkGrayColor];
    lblSelect.text=@"Select Interest";
    lblSelect.font=[UIFont fontWithName:@"Roboto-Regular" size:17];
    lblSelect.textAlignment=NSTextAlignmentCenter;
    
    
    [self.view addSubview:lblSelect];
    
    [self CreateTableView];
    
    
    UILabel *linelabel=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, tableViewItinerary.frame.size.width, 1)];
    linelabel.backgroundColor=[UIColor lightGrayColor];
    [tableViewItinerary addSubview:linelabel];
    
    UILabel *labelChooseDate = [[UILabel alloc]init];
    labelChooseDate.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/2)-140, 64, 155, 30);
    [self.view addSubview:labelChooseDate];
    labelChooseDate.text =@"Choose your Date *";
    labelChooseDate.textAlignment = NSTextAlignmentLeft;
    labelChooseDate.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    
    
    
    btnMonth = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMonth.frame = CGRectMake(labelChooseDate.frame.origin.x, 94, 55.0, 30.0);
    [self.view addSubview:btnMonth];
    [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
    btnMonth.titleLabel.textColor = [UIColor grayColor];
    btnMonth.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    
    [btnMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMonth];
    
    btnDay = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDay.frame = CGRectMake(btnMonth.frame.origin.x+60,94, 55.0, 30.0);
    [self.view addSubview:btnDay];
    [btnDay setTitle:@"dd" forState:UIControlStateNormal];
    btnDay.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnDay.titleLabel.textColor=KTextColor;
    [btnDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnDay];
    
    btnYear = [UIButton buttonWithType:UIButtonTypeCustom];
    btnYear.frame = CGRectMake(btnDay.frame.origin.x+60, 94, 55.0, 30.0);
    [self.view addSubview:btnYear];
    [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
    btnYear.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnYear.titleLabel.textColor = KTextColor;
    [btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnYear];
    
    
    btnHour = [UIButton buttonWithType:UIButtonTypeCustom];
    btnHour.frame = CGRectMake(btnYear.frame.origin.x+65, 94, 45.0, 30.0);
    [self.view addSubview:btnHour];
    [btnHour setTitle:@"09" forState:UIControlStateNormal];
    btnHour.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnHour.titleLabel.textColor=KTextColor;
    [btnHour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnHour];
    
    UILabel *labelChooseTime = [[UILabel alloc]init];
    labelChooseTime.frame = CGRectMake(btnHour.frame.origin.x, 64, 100, 30);
    [self.view addSubview:labelChooseTime];
    labelChooseTime.text =@"Time *";
    labelChooseTime.textAlignment = NSTextAlignmentLeft;
    labelChooseTime.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    
    
    btnMinute = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMinute.frame = CGRectMake(btnHour.frame.origin.x+50, 94, 45.0, 30.0);
    [self.view addSubview:btnMinute];
    [btnMinute setTitle:@"00" forState:UIControlStateNormal];
    btnMinute.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnMinute.titleLabel.textColor = KTextColor;
    [btnMinute setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self BorderforButton:btnMinute];
    
    btnDateSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDateSelection.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/2)-155, 94,170, 30.0);
    [self.view addSubview:btnDateSelection];
    [btnDateSelection setBackgroundColor:[UIColor clearColor]];
    [btnDateSelection addTarget:self action:@selector(SelectDate) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnTimeSelection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTimeSelection.frame = CGRectMake(btnHour.frame.origin.x+10, 94,btnMinute.frame.size.width+50 , 30);
    [btnTimeSelection setBackgroundColor:[UIColor clearColor]];
    [btnTimeSelection addTarget:self action:@selector(TimeSelection) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnTimeSelection];
    
    //    [btnTimeSelection setBackgroundColor:[UIColor grayColor]];
    //    [btnDateSelection setBackgroundColor:[UIColor orangeColor]];
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-15,[[UIScreen mainScreen] bounds].size.height-60, 30, 30);
    
    [btnNext setImage: [UIImage imageNamed:@"save_button_red"] forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnNext];
    UILabel *labelSave = [[UILabel alloc]init];
    labelSave.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-15,[[UIScreen mainScreen] bounds].size.height-30, 30, 20);
    [self.view addSubview:labelSave];
    labelSave.text = @"Save";
    labelSave.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    labelSave.textAlignment = NSTextAlignmentCenter;
    //  [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-70,60 ,60 )];
    // [[CommonMethods sharedInstance] CreateMenu:self.view];
}


-(void)CreateTableView
{
    tableViewItinerary = [[UITableView alloc]init];
    tableViewItinerary.frame = CGRectMake(0.0,170.0 , [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen]bounds].size.height-250);
    [self.view addSubview:tableViewItinerary];
    tableViewItinerary.delegate=self;
    tableViewItinerary.dataSource=self;
    tableViewItinerary.separatorColor= [UIColor clearColor];
}
#pragma mark- ViewwillAppear
-(void)viewWillAppear:(BOOL)animated{
    
    
    
    
    
    //    if (isCurrentViewController) {
    //        [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
    //        [btnDay setTitle:@"dd" forState:UIControlStateNormal];
    //        [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
    //        [btnHour setTitle:@"09" forState:UIControlStateNormal];
    //        [btnMinute setTitle:@"00" forState:UIControlStateNormal];
    //        [self ApiForGettingCategory];
    //        [self CreateUI];
    //        arrSelectedImages = [[NSMutableArray alloc]init];
    //        isCurrentButton = NO;
    //        strSelectedSubCategory = [[NSMutableString alloc]init];
    //        btnBuildItinerary.userInteractionEnabled = NO;
    //        strMainCategoryId = [[NSMutableString alloc]init];
    //    }
    //    else
    //    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str =[defaults valueForKey:@"strSelectedSubCategory"];
    if (str.length>0) {
        
        
        if (strSelectedSubCategory.length>0) {
            [strSelectedSubCategory appendString:@","];
        }
        [strSelectedSubCategory appendString:str];
    }
    
    if(arrSelectedImages.count>0){
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"AddItinerary"])  {
            [arrSelectedImages removeLastObject];
        }
        
        
    }
    [tableViewItinerary reloadData];
    
    
    //     [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
    //     [btnDay setTitle:@"dd" forState:UIControlStateNormal];
    //     [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
    //    [btnMinute setTitle:@"00" forState:UIControlStateNormal];
    //    [btnHour setTitle:@"09" forState:UIControlStateNormal];
    btnBuildItinerary.userInteractionEnabled = NO;
    // }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSString *str =[defaults valueForKey:@"strSelectedSubCategory"];
     if (str.length>0) {
     
     
     if (strSelectedSubCategory.length>0) {
     [strSelectedSubCategory appendString:@","];
     }
     [strSelectedSubCategory appendString:str];
     }
     
     if(arrSelectedImages.count>0){
     if (![[NSUserDefaults standardUserDefaults] boolForKey:@"AddItinerary"])  {
     [arrSelectedImages removeLastObject];
     }
     
     
     }
     [tableViewItinerary reloadData];
     
     
     //     [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
     //     [btnDay setTitle:@"dd" forState:UIControlStateNormal];
     //     [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
     //    [btnMinute setTitle:@"00" forState:UIControlStateNormal];
     //    [btnHour setTitle:@"09" forState:UIControlStateNormal];
     btnBuildItinerary.userInteractionEnabled = NO;
     
     */
}

-(void)BorderforButton:(UIButton *)button
{
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1.0;
    button.layer.cornerRadius = 2;
}

-(void)SelectDate
{
    [self.view endEditing:YES];
    btnDateSelection.userInteractionEnabled = NO;
    objPickerView = [[UIDatePicker alloc]init];
    objPickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-150,[[UIScreen mainScreen] bounds].size.width , 150);
    [self.view.superview addSubview:objPickerView];
    //objPickerView.hidden = YES;
    objPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [objPickerView setDatePickerMode:UIDatePickerModeDate];
    objPickerView.minimumDate=[NSDate date];
    [objPickerView addTarget:self action:@selector(DateSelected:) forControlEvents:UIControlEventValueChanged];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    [self.view.superview addSubview:toolBar];
    
}


-(void)TimeSelection
{
    [self.view endEditing:YES];
    
    btnTimeSelection.userInteractionEnabled = NO;
    _objPickerView = [[UIDatePicker alloc]init];
    _objPickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-150,[[UIScreen mainScreen] bounds].size.width , 150);
    [self.view.superview addSubview:_objPickerView];
    _objPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [_objPickerView setDatePickerMode:UIDatePickerModeTime];
    
    NSDate *date = _objPickerView.date;
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitHour fromDate:date];
    components.hour = 9;
    components.minute = 00;
    components.second = 0;
    NSDate *dateWithTime11_14 = [cal dateFromComponents:components];
    [_objPickerView setDate:dateWithTime11_14];
    [_objPickerView addTarget:self action:@selector(TimeSelected:) forControlEvents:UIControlEventValueChanged];
    
    _toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [_toolBar setBarStyle:UIBarStyleBlackOpaque];
    _barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClickedTime:)];
    _toolBar.items = @[_barButtonDone];
    _barButtonDone.tintColor=[UIColor blackColor];
    [self.view.superview addSubview:_toolBar];
}

-(void)DateSelected:(id)sender
{
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
        btnTimeSelection.userInteractionEnabled = YES;
    }
    NSDate *date= objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    strDate = [dateFormatter stringFromDate:date];
    NSString *year = [strDate substringWithRange:NSMakeRange(0,4)];
    NSString *day = [strDate substringWithRange:NSMakeRange(8, 2)];
    NSString *month = [strDate substringWithRange:NSMakeRange(5, 2)];
    [btnYear setTitle:year forState:UIControlStateNormal];
    [btnDay setTitle:day forState:UIControlStateNormal];
    [btnMonth setTitle:month forState:UIControlStateNormal];
    
    //isDateSet = YES;
}


-(void)TimeSelected:(id)sender
{
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
        btnDateSelection.userInteractionEnabled= YES;
    }
    NSDate *date = _objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    strTime = [dateFormatter stringFromDate:date];
    NSString *minute = [strTime substringWithRange:NSMakeRange(3, 2)];
    NSString *hour = [strTime substringWithRange:NSMakeRange(0,2)];
    [btnMinute setTitle:minute forState:UIControlStateNormal];
    [btnHour setTitle:hour forState:UIControlStateNormal];
    //  isTimeSet=YES;
    
}
-(void)DoneBtnClicked:(id)sender
{
    
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
        btnDateSelection.userInteractionEnabled = YES;
    }
    
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
        btnTimeSelection.userInteractionEnabled = YES;
    }
    if (strDate.length==0) {
        NSDate *currentDate = [NSDate date];
        NSLog(@"%@",currentDate);
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *str = [formatter stringFromDate:currentDate];
        NSLog(@"%@",str);
        [btnYear setTitle:[str substringWithRange:NSMakeRange(0, 4)] forState:UIControlStateNormal];
        [btnMonth setTitle:[str substringWithRange:NSMakeRange(5, 2)] forState:UIControlStateNormal];
        [btnDay setTitle:[str substringWithRange:NSMakeRange(8, 2)] forState:UIControlStateNormal];
        
        strDate = str;
        btnDateSelection.userInteractionEnabled = YES;
        
    }
    else
    {
        NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
        NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
        NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
        
        [btnYear setTitle:strYear forState:UIControlStateNormal];
        [btnMonth setTitle:strMonth forState:UIControlStateNormal];
        [btnDay setTitle:strDay forState:UIControlStateNormal];
        btnDateSelection.userInteractionEnabled = YES;
    }
    // [objPickerView removeFromSuperview];
    //object_getClassNam
    /* if (isTimeSet) {
     NSString *strHour = [strTime substringWithRange:NSMakeRange(0, 2)];
     NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
     [btnHour setTitle:strHour forState:UIControlStateNormal];
     [btnMinute setTitle:strMinute forState:UIControlStateNormal];
     
     }
     
     else if (isDateSet)
     {
     if (strDate.length==0) {
     
     }
     else
     {
     NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
     NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
     NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
     
     [btnYear setTitle:strYear forState:UIControlStateNormal];
     [btnMonth setTitle:strMonth forState:UIControlStateNormal];
     [btnDay setTitle:strDay forState:UIControlStateNormal];
     
     }
     }*/
    
    
    // objPickerView.frame = CGRectMake(self.view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height+150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    //toolBar.hidden=YES;
    
}

-(void)DoneBtnClickedTime:(id)sender
{
    if (objPickerView !=nil) {
        objPickerView.hidden = YES;
        toolBar.hidden = YES;
        btnDateSelection.userInteractionEnabled = YES;
        
    }
    
    if (_objPickerView !=nil) {
        _objPickerView.hidden = YES;
        _toolBar.hidden = YES;
        btnTimeSelection.userInteractionEnabled = YES;
    }
    if (strTime.length == 0) {
        [btnHour setTitle:@"09" forState:UIControlStateNormal];
        [btnMinute setTitle:@"00" forState:UIControlStateNormal];
    }
    else
    {
        NSString *strHour = [strTime substringWithRange:NSMakeRange(0, 2)];
        NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
        [btnHour setTitle:strHour forState:UIControlStateNormal];
        [btnMinute setTitle:strMinute forState:UIControlStateNormal];
    }
    btnTimeSelection.userInteractionEnabled = YES;
    // [objPickerView removeFromSuperview];
    //object_getClassNam
    /*if (isTimeSet) {
     NSString *strHour = [strTime substringWithRange:NSMakeRange(0, 2)];
     NSString *strMinute = [strTime substringWithRange:NSMakeRange(3, 2)];
     [btnHour setTitle:strHour forState:UIControlStateNormal];
     [btnMinute setTitle:strMinute forState:UIControlStateNormal];
     
     }
     
     else if (isDateSet)
     {
     if (strDate.length==0) {
     
     }
     else
     {
     NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
     NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
     NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
     
     [btnYear setTitle:strYear forState:UIControlStateNormal];
     [btnMonth setTitle:strMonth forState:UIControlStateNormal];
     [btnDay setTitle:strDay forState:UIControlStateNormal];
     
     }
     }*/
    
    
    // objPickerView.frame = CGRectMake(self.view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height+150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    //toolBar.hidden=YES;
    
}

-(void)ApiForGettingCategory
{
    
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet)
    {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        //  NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Categories/get_all_categories?&access_token=%@"[[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"];
        
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Categories/get_all_categories?&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSMutableArray *arrResponse=[returArray mutableCopy];
                arrImages = [[NSMutableArray alloc]init];
                arrImages = [arrResponse valueForKey:@"result"];
                [tableViewItinerary reloadData];
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                }];
            }
        }];
        
        
    }
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrImages count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SelectItinerayTableViewCell";
    
    SelectItinerayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SelectItinerayTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    cell.imageTick.image = [UIImage imageNamed:@"Tick_mark_green"];
    BOOL found = NO;
    for(NSMutableDictionary *dict in arrSelectedImages)
    {
        if([[dict valueForKey:@"id"] isEqualToString:[[arrImages objectAtIndex:indexPath.row] valueForKey:@"id"]])
        {
            NSURL *url =[[arrImages objectAtIndex:indexPath.row]valueForKey:@"hover_image"];
            [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            cell.imageTick.hidden = NO;
            cell.labelItineraryName.textColor = kBtnSelectionColor;
            found = YES;
            break;
        }
        
        
    }
    
    if(!found){
        
        //setImageWithURL:(NSURL *)url usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
        
        NSURL *url =[[arrImages objectAtIndex:indexPath.row]valueForKey:@"basic_image"];
        
        [cell.imageIconItinerary setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.imageIconItinerary setShowActivityIndicatorView:true];
        
        [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
        cell.labelItineraryName.text = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"category"];
        cell.imageTick.hidden = YES;
        cell.labelItineraryName.textColor = KTextColor;
    }
    cell.imageIconItinerary.tag = indexPath.row;
    
    //    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //    //[prefs setBool:NO forKey:@"AddItinerary"];
    //    BOOL Add=[prefs valueForKey:@"AddItinerary"];
    //    if (!Add) {
    //        NSURL *url =[[arrImages objectAtIndex:x]valueForKey:@"basic_image"];
    //        [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    //        cell.labelItineraryName.text = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"category"];
    //        cell.imageTick.hidden = YES;
    //        cell.labelItineraryName.textColor = KTextColor;
    //
    //    }
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6) {
        return 100;
    }
    else if (IS_IPHONE_6P)
    {
        return 120;
    }
    else if (IS_IPAD)
    {
        return 150;
    }
    else
    {
        return 90;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (strDate.length==0 ) {
        [[CommonMethods sharedInstance]AlertAction:@"Please select date"];
        return;
    }else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *selectedDate =[dateFormatter dateFromString:strDate];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
        if (!(strTime.length==0)) {
            strSelectedDate = [strSelectedDate stringByAppendingString:@" "];
            strSelectedDate = [strSelectedDate stringByAppendingString:strTime];
        }
        else
        {
            strSelectedDate = [strSelectedDate stringByAppendingString:@" 09:00"];
        }
        
        NSDate *date = [self convertinDate:strSelectedDate];
        BOOL isValiddate =  [self comapredate:date];
        if (!isValiddate) {
            
            
            
            SelectItinerayTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            //[tableViewItinerary cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
            NSURL *url = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"hover_image"];
            [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            cell.labelItineraryName.textColor = kBtnSelectionColor;
            cell.imageTick.hidden = NO;
            NSString *strCategoryName = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"category"];
            
            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
            [dict setObject:[[arrImages objectAtIndex:indexPath.row]valueForKey:@"id" ]  forKey:@"id"];
            [arrSelectedImages addObject:dict];
            indexPathForAlert = indexPath.row;
            NSMutableString *strId= [[arrImages objectAtIndex:indexPath.row]valueForKey:@"id"];
            
            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
            if (isinternet) {
                self.view.userInteractionEnabled = NO;
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                NSDate *selectedDate =[dateFormatter dateFromString:strDate];
                [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];
                NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
                NSString *strDateOnly = [strSelectedDate substringWithRange:NSMakeRange(0, 10)];
                [[CommonMethods sharedInstance] addSpinner:self.view];
                //  NSMutableString * strSelected = [[NSMutableString alloc]init];
                
                //        for (int i=0 ; i<[arrSelectedImages count] ; i++) {
                //
                //            [strSelected appendString:[[arrSelectedImages objectAtIndex:i]objectForKey:@"id" ]];
                //            if (i != arrSelectedImages.count-1) {
                //                [strSelected appendString:@","];
                //            }
                //        }
                
                [[WebService sharedInstance] fetchCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/SubcategoriesAPI/get_sub_categories", kBaseUrl] param:strId strDate:strDateOnly andcompletionhandler:^(NSArray *returArray, NSError *error ){
                    if (!error) {
                         self.view.userInteractionEnabled = YES;
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        // NSLog(@"returArray %@",returArray);
                        NSString *errorstr = [returArray valueForKey:@"error"];
                        
                        if ([errorstr isEqualToString:@"already exist"]) {
                            [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                            return;
                            
                            
                        }else if([returArray valueForKey:@"success"]){
                            
                            
                            NSMutableArray *arr = [[NSMutableArray alloc]init];
                            arr = [returArray valueForKey:@"result"];
                            
                            NSMutableArray *arrTOCheck = [[returArray valueForKey:@"result"]objectAtIndex:0] ;
                            
                            
                            if (arrTOCheck.count<1) {
                                
//                                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No subcategory Present. Are you sure you want to add this category." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//                                alert.tag = indexPath.row;
//                                [alert show];
                                [arrSelectedImages removeLastObject];
                                [tableViewItinerary reloadData];
                                
                            }
                            else
                            {
                                
                                NSMutableArray *arrResponse = [[NSMutableArray alloc]init];
                                // arrResponse = [arr mutableCopy] ;
                                NSArray *arr1;
                                arr1=[[NSArray alloc]init];
                                for (int k=0; k<arr.count; k++) {
                                    arr1= [arr objectAtIndex:k];
                                    if (arr1.count>0) {
                                        [arrResponse addObject:arr1];
                                    }
                                }
                                arr1=nil;
                                
                                //                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                //                    [prefs setBool:NO forKey:@"AddItinerary"];
                                //                    [prefs synchronize];
                                
                                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                                
                                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                ShowDetailCategoryViewController *controller = [story instantiateViewControllerWithIdentifier:@"ShowDetailCategoryViewController"];
                                controller.arrResponse = arrResponse;
                                controller.strCategoryName = strCategoryName;
                                [self.navigationController pushViewController:controller animated:YES];
                                
                                arrResponse=nil;
                                arr=nil;
                            }
                        }
                    }
                    else
                    {
                         self.view.userInteractionEnabled = YES;
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        
                    }
                }];
            }
            else
            {
                [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            }
        }else{
            [[CommonMethods sharedInstance] AlertAction:@"Please select future date and time"];
        }
        
        
    }
    
    
}


-(NSDate *)convertinDate:(NSString *)dateString{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy HH:mm"];
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *dateString1 = [df dateFromString:dateString];
    
    return dateString1;
}

-(BOOL)comapredate:(NSDate *)orderdate{
    NSDate *date1 = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    NSString *stringFromDate = [formatter stringFromDate:date1];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy HH:mm"];
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *dateString1 = [df dateFromString:stringFromDate];
    
    
    
    BOOL isreturn = NO;
    if ([dateString1 compare:orderdate] == NSOrderedDescending) {
      //  NSLog(@"date1 is later than date2");
        isreturn = YES;
        return isreturn;
    }
    else if ([dateString1 compare:orderdate] == NSOrderedAscending) {
     //   NSLog(@"date1 is earlier than date2");
        isreturn = NO;
        return isreturn;
    } else {
     //   NSLog(@"dates are the same");
        isreturn = NO;
        return isreturn;
    }
    return isreturn;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        [arrSelectedImages removeLastObject];
        [tableViewItinerary reloadData];
        //Code for OK button
    }
    if (buttonIndex == 1)
    {
        NSString *strMainId  = [[arrImages objectAtIndex:indexPathForAlert]valueForKey:@"id" ];
        if (strMainCategoryId.length>0) {
            [strMainCategoryId appendString:@","];
        }
        [strMainCategoryId appendString:strMainId];
    }
    //9473632533
}
-(void)goToNextView
{
    if (strDate.length==0 ) {
        [[CommonMethods sharedInstance]AlertAction:@"Please select date"];
        return;
    }else if (strSelectedSubCategory.length==0 && strMainCategoryId.length==0){
        [[CommonMethods sharedInstance]AlertAction:@"Please add categories"];
        return;
    }
    else
    {
        if (strMainCategoryId.length==0) {
            [strMainCategoryId appendString:@"0"];
        }
        if (strSelectedSubCategory.length==0) {
            [strSelectedSubCategory appendString:@"0"];
        }
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            
            
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *selectedDate =[dateFormatter dateFromString:strDate];
            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
            NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
            
            NSString *strDateOnly = [strSelectedDate substringWithRange:NSMakeRange(0, 10)];
            if (!(strTime.length==0)) {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" "];
                strSelectedDate = [strSelectedDate stringByAppendingString:strTime];
            }
            else
            {
                strSelectedDate = [strSelectedDate stringByAppendingString:@" 09:00"];
            }
            if (!(strSelectedDate.length==0)) {
                
                [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strSelectedSubCategory date:strSelectedDate dateOnly:strDateOnly MainCategoryId:strMainCategoryId andcompletionhandler:^(NSArray *returArray, NSError *error ){
                    if (!error) {
                        
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                       // NSLog(@"returArray %@",returArray);
                        NSString *errorstr = [returArray valueForKey:@"error"];
                        
                        if ([errorstr isEqualToString:@"already exist"]) {
                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            CurrentItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //       assignGroupView.arrResponse = [returArray mutableCopy];
                            self.view.userInteractionEnabled = YES;
                            [self.navigationController pushViewController:assignGroupView animated:YES];
                            
                            
                            //                            tableViewItinerary.hidden = YES;
                            //                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //                            [self addChildViewController:controller1];
                            //                            controller1.delegate = self;
                            //                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            //                            [self.view addSubview:controller1.view];
                            //                            [controller1 didMoveToParentViewController:self];
                            //                            btnCurrentItinerary.userInteractionEnabled=NO;
                            //                            isCurrentButton = YES;
                            //                            isSaved = YES;
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strSelectedSubCategory"];
                            [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                            [[NSUserDefaults standardUserDefaults] setObject:@"isCurrent" forKey:@"CurrentView"];
                            strDate =@"";
                            btnBuildItinerary.userInteractionEnabled = YES;
                            isCurrentViewController = YES;
                            
                        }else if([returArray valueForKey:@"success"]){
                            NSMutableArray *arr = [[NSMutableArray alloc]init];
                            arr = [returArray valueForKey:@"result"];
                            
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            CurrentItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //       assignGroupView.arrResponse = [returArray mutableCopy];
                            self.view.userInteractionEnabled = YES;
                            [self.navigationController pushViewController:assignGroupView animated:YES];
                            
                            
                            //                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            //                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            //
                            //                            tableViewItinerary.hidden = YES;
                            //                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //                            [self addChildViewController:controller1];
                            //                            controller1.delegate = self;
                            //                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            //                            [self.view addSubview:controller1.view];
                            //                            [controller1 didMoveToParentViewController:self];
                            //                            btnCurrentItinerary.userInteractionEnabled=NO;
                            //                            isCurrentButton = YES;
                            //                            isSaved = YES;
                            
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strSelectedSubCategory"];
                            [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                            strDate =@"";
                            btnBuildItinerary.userInteractionEnabled = YES;
                            isCurrentViewController = YES;
                        }
                    }
                    else
                    {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        
                    }
                }];
                
                
            }
            else
            {
                [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strSelectedSubCategory date:strDate dateOnly:strDateOnly MainCategoryId:strMainCategoryId andcompletionhandler:^(NSArray *returArray, NSError *error ){
                    if (!error) {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        NSLog(@"returArray %@",returArray);
                        NSString *errorstr = [returArray valueForKey:@"error"];
                        
                        if ([errorstr isEqualToString:@"already exist"]) {
                            //                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            //                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            //
                            //                            tableViewItinerary.hidden = YES;
                            //                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //                            [self addChildViewController:controller1];
                            //                            controller1.delegate = self;
                            //                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            //                            [self.view addSubview:controller1.view];
                            //                            [controller1 didMoveToParentViewController:self];
                            //                            btnCurrentItinerary.userInteractionEnabled=NO;
                            //                            isCurrentButton = YES;
                            //                            isSaved = YES;
                            
                            
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            CurrentItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //       assignGroupView.arrResponse = [returArray mutableCopy];
                            self.view.userInteractionEnabled = YES;
                            [self.navigationController pushViewController:assignGroupView animated:YES];
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strSelectedSubCategory"];
                            [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                            
                            strDate =@"";
                            btnBuildItinerary.userInteractionEnabled = YES;
                            isCurrentViewController = YES;
                        }else if([returArray valueForKey:@"success"]){
                            NSMutableArray *arr = [[NSMutableArray alloc]init];
                            arr = [returArray valueForKey:@"result"];
                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            
                            //                            tableViewItinerary.hidden = YES;
                            //                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //                            [self addChildViewController:controller1];
                            //                            controller1.delegate = self;
                            //                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            //                            [self.view addSubview:controller1.view];
                            //                            [controller1 didMoveToParentViewController:self];
                            //                            btnCurrentItinerary.userInteractionEnabled=NO;
                            
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            CurrentItineraryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            //       assignGroupView.arrResponse = [returArray mutableCopy];
                            self.view.userInteractionEnabled = YES;
                            [self.navigationController pushViewController:assignGroupView animated:YES];
                            
                            isCurrentButton = YES;
                            isSaved = YES;
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strSelectedSubCategory"];
                            [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                            strDate =@"";
                            btnBuildItinerary.userInteractionEnabled = YES;
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strSelectedSubCategory"];
                            [[ NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
                            strDate =@"";
                            isCurrentViewController = YES;
                            btnBuildItinerary.userInteractionEnabled = YES;
                        }
                    }
                    else
                    {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        
                    }
                }];
                
                
                
            }
        }
        else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
        
    }
}


-(void)BulidItinerary
{
    [[NSUserDefaults standardUserDefaults ]removeObjectForKey:@"strSelectedSubCategory"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"strId"];
    [strSelectedSubCategory setString:@""];
    [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
    [btnDay setTitle:@"dd" forState:UIControlStateNormal];
    [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
    [btnHour setTitle:@"hrs" forState:UIControlStateNormal];
    [btnMinute setTitle:@"mins" forState:UIControlStateNormal];
    isCurrentButton=NO;
    btnCurrentItinerary.backgroundColor =kBtnUnselectedColor;
    btnBuildItinerary.backgroundColor =kBtnSelectionColor;
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    
    [self ApiForGettingCategory];
    [self CreateTableView];
    [ self viewWillAppear:YES];
    btnBuildItinerary.userInteractionEnabled = NO;
    
    
}
-(void)CurrentItinerary
{
    objPickerView.frame = CGRectMake(self.view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height+150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    toolBar.hidden=YES;
    
    
    btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
    btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AddItinerary"];
    [tableViewItinerary removeFromSuperview];
    tableViewItinerary=nil;
    CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
    [self addChildViewController:controller1];
    controller1.delegate = self;
    controller1.view.frame = CGRectMake(0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:controller1.view];
    [controller1 didMoveToParentViewController:self];
    btnCurrentItinerary.userInteractionEnabled=NO;
    isCurrentButton = YES;
    btnBuildItinerary.userInteractionEnabled = YES;
    
    
}
- (void) CallChildViewController{
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    tableViewItinerary.hidden = NO;
    [self CurrentItinerary];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
