//
//  AssignGroupViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/6/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AssignGroupViewController.h"
#import "AssignGroupTableViewCell.h"
#import "Constants.h"
#import "SelectItineraryViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "AboutGroupViewController.h"

@interface AssignGroupViewController ()<UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    UIButton *btn;
    UIPickerView *myPickerView;
    UIToolbar *toolBar;
    UIBarButtonItem *barButtonDone;
    UIBarButtonItem *barButtonCancel;
    NSMutableArray *arrNames;
    NSMutableArray *arrGroups;
    NSMutableArray *arrGroupAssignment;
    NSInteger value;
    NSMutableArray *sortedArray;
   
}
@end

@implementation AssignGroupViewController
@synthesize tableViewStudentsName;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton=YES;
    [self UiForNavigationBar];
    
    arrNames = [[NSMutableArray alloc]init];
     arrGroups = [[NSMutableArray alloc]init];
    
     arrGroupAssignment = [[NSMutableArray alloc]init];
    //[self CreateUI];
    [self getListOfStudent];
    // Do any additional setup after loading the view.
}

#pragma Navigation Bar Creation
-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Assign Groups";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

#pragma back button method
-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma creation Of Ui
-(void)CreateUI
{
    
    
    
    if (arrNames.count==0 )
    {
        
        UILabel *labelNoStudentsAreAvailable = [[UILabel alloc]init];
        labelNoStudentsAreAvailable.frame = CGRectMake(0.0, 64.0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:labelNoStudentsAreAvailable];
        labelNoStudentsAreAvailable.text =@"No students are available";
        labelNoStudentsAreAvailable.font = [UIFont fontWithName:@"Roboto-Medium_0" size:14];
        labelNoStudentsAreAvailable.textAlignment = NSTextAlignmentCenter;
        labelNoStudentsAreAvailable.textColor = KTextColor;
        
        
        UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
        btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-25,[[UIScreen mainScreen] bounds].size.height-80, 50, 50);
        
        [btnNext setImage: [UIImage imageNamed:@"next_button"] forState:UIControlStateNormal];
        
        //        [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
        //        [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-45.0, 0.0,0.0)];
        //[btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnNext];
        [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
        
    }
    else
    {
        
        UILabel *lbl = [[UILabel alloc]init];
        lbl.frame = CGRectMake( 20.0, 120.0, self.view.frame.size.width-40, 40.0);
        lbl.text = @"ASSIGN GROUPS";
        lbl.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        lbl.textColor =KTextColor;
        
        UILabel *lblName = [[UILabel alloc]init];
        lblName.frame = CGRectMake(0.0, 160.0, 100.0, 20.0);
        lblName.text=@"NAMES";
        lblName.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:lblName];
        lblName.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
        lblName.textColor = KTextColor;
        
        UIButton *btnAlphabeticSorting = [UIButton buttonWithType:UIButtonTypeCustom];
        btnAlphabeticSorting.frame = CGRectMake(26.0, 185.0, 20.0, 20.0);
        [self.view addSubview:btnAlphabeticSorting];
        [btnAlphabeticSorting setImage:[UIImage imageNamed:@"a-z sorting_icon"] forState:UIControlStateNormal];
        
        tableViewStudentsName = [[UITableView alloc]init];
        if (IS_IPHONE_5) {
            UILabel *lblGroup = [[UILabel alloc]init];
            lblGroup.frame = CGRectMake(165.0, 160.0, 100.0, 20.0);
            lblGroup.text=@"GROUPS";
            lblGroup.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:lblGroup];
            lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGroupSorting.frame = CGRectMake(190.0, 185.0, 20.0, 20.0);
            [self.view addSubview:btnGroupSorting];
            [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon"] forState:UIControlStateNormal];
            
            tableViewStudentsName.frame = CGRectMake(0.0, 210, self.view.frame.size.width,250 );
            
        }
        else if (IS_IPHONE_6)
        {
            UILabel *lblGroup = [[UILabel alloc]init];
            lblGroup.frame = CGRectMake(200.0, 160.0, 100.0, 20.0);
            lblGroup.text=@"GROUPS";
            lblGroup.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:lblGroup];
            lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGroupSorting.frame = CGRectMake(225.0, 185.0, 20.0, 20.0);
            [self.view addSubview:btnGroupSorting];
            [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon"] forState:UIControlStateNormal];
            
            tableViewStudentsName.frame = CGRectMake(0.0, 210, self.view.frame.size.width,350 );
            
        }
        else if (IS_IPHONE_6P)
        {
            UILabel *lblGroup = [[UILabel alloc]init];
            lblGroup.frame = CGRectMake(225.0, 160.0, 100.0, 20.0);
            lblGroup.text=@"GROUPS";
            lblGroup.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:lblGroup];
            lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGroupSorting.frame = CGRectMake(250.0, 185.0, 20.0, 20.0);
            [self.view addSubview:btnGroupSorting];
            [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon"] forState:UIControlStateNormal];
            
            tableViewStudentsName.frame = CGRectMake(0.0, 210, self.view.frame.size.width,400 );
            
        }
        else if (IS_IPAD)
        {
            lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
            lblName.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
            
            
            
            UILabel *lblGroup = [[UILabel alloc]init];
            lblGroup.frame = CGRectMake(500.0, 160.0, 100.0, 20.0);
            lblGroup.text=@"GROUPS";
            lblGroup.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:lblGroup];
            lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
            
            UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGroupSorting.frame = CGRectMake(525.0, 185.0, 20.0, 20.0);
            [self.view addSubview:btnGroupSorting];
            [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon"] forState:UIControlStateNormal];
            
            tableViewStudentsName.frame = CGRectMake(0.0, 210, self.view.frame.size.width,700 );
        }
        else
        {
            UILabel *lblGroup = [[UILabel alloc]init];
            lblGroup.frame = CGRectMake(340.0, 160.0, 100.0, 20.0);
            lblGroup.text=@"GROUPS";
            lblGroup.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:lblGroup];
            lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGroupSorting.frame = CGRectMake(360.0, 185.0, 20.0, 20.0);
            [self.view addSubview:btnGroupSorting];
            [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon"] forState:UIControlStateNormal];
            
            tableViewStudentsName.frame = CGRectMake(0.0, 210, self.view.frame.size.width,355 );
            
        }
        
        [self.view addSubview:tableViewStudentsName];
        tableViewStudentsName.delegate=self;
        tableViewStudentsName.dataSource = self;
        tableViewStudentsName.separatorColor=[UIColor clearColor];
        tableViewStudentsName.allowsSelection=NO;
        
        UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
        btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-25,[[UIScreen mainScreen] bounds].size.height-80, 50, 50);
        
        [btnNext setImage: [UIImage imageNamed:@"next_button"] forState:UIControlStateNormal];
        
        //    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
        //    [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-45.0, 0.0,0.0)];
        //    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnNext];
        //    [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        //    btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
        
    }
    
     [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
    
    
//    btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
//    btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
//    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
//    btnBuildItinerary.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
//    [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnBuildItinerary];
//    
//    if (IS_IPAD) {
//        btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
//    }
//    else
//    {
//        btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    }
//    
//    btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
//    [btnCurrentItinerary setTitle:@"Current Itinerary" forState:UIControlStateNormal];
//    [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
//    btnCurrentItinerary.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
//    [self.view addSubview:btnCurrentItinerary];
    
//    if (IS_IPAD) {
//        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
//    }
//    else
//    {
//        btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    }
 
    
    
//    if (IS_IPAD) {
//        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
//    }
//    else
//    {
//     [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-90,50 ,50 )];
//    
//    }
    
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.frame = CGRectMake(0.0,self.view.frame.size.height-150 , self.view.frame.size.width, 150);
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
   
    barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                        style:UIBarButtonItemStyleDone target:self action:@selector(CancelButtonPressed:)];
 
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    [self.view addSubview:toolBar];
    [self.view addSubview:myPickerView];
    toolBar.hidden=YES;
    myPickerView.hidden=YES;
    
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
    [self.view addGestureRecognizer:singleFingerTap];
     
     
}

#pragma Button Build Itinerary method
-(void)BulidItinerary
{
    btnCurrentItinerary.backgroundColor =[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    btnBuildItinerary.backgroundColor =[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
}


-(void)CurrentItinerary
{
    
    btnBuildItinerary.backgroundColor =[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    btnCurrentItinerary.backgroundColor =[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    
}


-(void)AlphabeticalSorting
{
  //  NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
   // sortedArray=[arrNames sortedArrayUsingDescriptors:@[sort]];
}
#pragma button Next button Method
-(void)goToNextView
{

    if(arrGroupAssignment.count == 0)
    {
      //  [[CommonMethods sharedInstance] AlertAction:@"Please assign groups to the students"];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                AboutGroupViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AboutGroupView"];
                [self.navigationController pushViewController:Controller animated:YES ];
    }
    else
    {
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        self.view.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
     
    
        [[WebService sharedInstance] fetchDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/GroupsAPI/group_student", kBaseUrl] param:arrGroupAssignment andcompletionhandler:^(NSArray *returArray, NSError *error ){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([errorstr isEqualToString:@"already exist"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                    self.view.userInteractionEnabled=YES;
                    return;
                    
                    
                }else if([returArray valueForKey:@"success"]){
                    
                   // [self.navigationController popViewControllerAnimated:YES];
                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    AboutGroupViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AboutGroupView"];
                    [self.navigationController pushViewController:Controller animated:YES ];
                    self.view.userInteractionEnabled=YES;
                   
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled=YES;
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
    }
     
    
}
    
}

#pragma tableview delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNames.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AssignGroupTableViewCell";
    
    AssignGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    // AssignGroupTableViewCell *cell = [[AssignGroupTableViewCell alloc] init];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AssignGroupTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    if ([cell.btnAssignGroup.titleLabel.text isEqualToString:@"Assign Group"])
    {
        cell.btnAssignGroup.titleLabel.textColor = KTextColor;
    }
    else{
        cell.btnAssignGroup.titleLabel.textColor =[UIColor colorWithRed:112/255.0 green:100.0/255.0 blue:202.0/255.00 alpha:1];
    }
    
     BOOL found = NO;
    
    NSString *strName = [[arrNames valueForKey:@"student"]objectAtIndex:indexPath.row];
    
    
    
    NSString *strGroupName = [[arrNames valueForKey:@"group_name"]objectAtIndex:indexPath.row];
    if ([strGroupName isKindOfClass:[NSNull class]] || strGroupName.length==0) {
        [cell.btnAssignGroup setTitle:@"Assign Group" forState:UIControlStateNormal];
       
    }
    else
    {
    [cell.btnAssignGroup setTitle:strGroupName forState:UIControlStateNormal];
         [cell.imageViewDropDownIcon setImage:[UIImage imageNamed:@"drop_icon_purple"]];
         [cell.btnAssignGroup setTitleColor:[UIColor colorWithRed:112/255.0 green:100.0/255.0 blue:202.0/255.00 alpha:1] forState:UIControlStateNormal];
        found = YES;
    }
//    if (![[arrNames valueForKey:@"l_name"] isKindOfClass:[NSNull class]]) {
//        strName  = [strName stringByAppendingString:@" "];
//        strName = [strName stringByAppendingString:[[arrNames valueForKey:@"l_name"]objectAtIndex:indexPath.row]];
//    }
  
    cell.labelName.text =strName;
   
    for(NSDictionary *dict in arrGroupAssignment){
        found = NO;
        if([[dict valueForKey:@"student_id"] isEqualToString:[[arrNames objectAtIndex:indexPath.row] valueForKey:@"id"]]){
            for(NSDictionary *dict2 in arrGroups){
                if([[dict2 valueForKey:@"id"] isEqualToString:[dict valueForKey:@"group_id"]]){
                    [cell.btnAssignGroup setTitle:[dict2 valueForKey:@"name"] forState:UIControlStateNormal];
                    [cell.btnAssignGroup setTitleColor:[UIColor colorWithRed:112/255.0 green:100.0/255.0 blue:202.0/255.00 alpha:1] forState:UIControlStateNormal];
                    if(IS_IPAD)
                    {
                        cell.btnAssignGroup.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
                        cell.labelName.font=[UIFont fontWithName:@"Roboto-Regular" size:15];
                    }
                    [cell.imageViewDropDownIcon setImage:[UIImage imageNamed:@"drop_icon_purple"]];
                    found = YES;
                     break;
                }
            }
            if(found)
                break;
        }
    }
    if(!found){
         [cell.btnAssignGroup setTitle:@"Assign Group" forState:UIControlStateNormal];
          [cell.btnAssignGroup setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [cell.imageViewDropDownIcon setImage:[UIImage imageNamed:@"drop_icon"]];
    }
  
    cell.btnAssignGroup.tag=indexPath.row;
    [cell.btnAssignGroup addTarget:self action:@selector(BtnAssignGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.imageViewDropDownIcon.tag = indexPath.row;
        return cell;
}


-(void)BtnAssignGroupClicked:(id)sender
{
    
    btn = (UIButton *)(id)sender;
    NSInteger btnTag = btn.tag;
    barButtonCancel.tag=btnTag;
    barButtonDone.tag=btnTag;
    toolBar.hidden=NO;
    myPickerView.hidden=NO;
    
    
    
}


-(void)DoneBtnClicked:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    AssignGroupTableViewCell *cell = [tableViewStudentsName cellForRowAtIndexPath:indexPath];
    NSString *strNameId = [[arrNames valueForKey:@"id"]objectAtIndex:btn.tag];
    [cell.btnAssignGroup setTitle:[[arrGroups valueForKey:@"name"]objectAtIndex:value ] forState:UIControlStateNormal];
    [cell.btnAssignGroup setTitleColor:[UIColor colorWithRed:112/255.0 green:100.0/255.0 blue:202.0/255.00 alpha:1] forState:UIControlStateNormal];
    [cell.imageViewDropDownIcon setImage:[UIImage imageNamed:@"drop_icon_purple"]];
    NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
    [dictGroupAsignment setObject:[[arrGroups valueForKey:@"id"]objectAtIndex:value ] forKey:@"group_id"];
    [dictGroupAsignment setObject:strNameId forKey:@"student_id"];
    
    
    for (NSDictionary *dict in arrGroupAssignment) {
        if ([[dict valueForKey:@"student_id"] isEqualToString:strNameId]) {
            [arrGroupAssignment removeObject:dict];
            break;
        }
    }
      [arrGroupAssignment addObject:dictGroupAsignment];
      myPickerView.hidden=YES;
      toolBar.hidden=YES;
   // [tableViewStudentsName reloadData];
    
}

-(void)handleSingleTap
{
    myPickerView.hidden=YES;
    toolBar.hidden=YES;
}
-(void)CancelButtonPressed:(id)sender
{
    
    [myPickerView removeFromSuperview];
    [toolBar removeFromSuperview];
    [btn setTitle:@"Assign Group" forState:UIControlStateNormal];
    [tableViewStudentsName reloadData];
    
}


#pragma mark- Picker View Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [arrGroups count];
}



-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    // [btn setTitle:[[arrGroups valueForKey:@"name"]objectAtIndex:value ] forState:UIControlStateNormal];
    value = row;
    
    
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[arrGroups valueForKey:@"name"]objectAtIndex:row];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getListOfStudent{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        self.view.userInteractionEnabled= NO;
        
        // NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Grades/student_grades?grades=@%&access_token=fLDZ4FpiNIHljQeXkoxV";
        
        
    // [defaults setObject:strClass forKey:@"ClassId"];
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/student_and_group?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] valueForKey:@"id"],[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error) {
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                  self.view.userInteractionEnabled = YES;
                //   NSMutableArray *arrResponse=[returArray mutableCopy];
                
                arrNames = [returArray valueForKey:@"result"];
                arrGroups = [returArray valueForKey:@"groups"];
                //[tableViewStudentsName reloadData];
                [self CreateUI];
//                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                SelectItineraryViewController *controller1 = [story instantiateViewControllerWithIdentifier:@"SelectItineraryViewController"];
//                self.view.userInteractionEnabled=YES;
//                [self.navigationController pushViewController:controller1 animated:YES];
                
               
            }
            

            
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
               
            }
        }];
        
        
    }

    
    
    
    
    
}


@end
