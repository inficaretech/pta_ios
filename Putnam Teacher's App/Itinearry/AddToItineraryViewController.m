


//
//  AddToItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/14/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "AddToItineraryViewController.h"
#import "Constants.h"
//#import <RATreeView.h>
//#import "RATableViewCell.h"
//#import "RADataObject.h"
//#import "LevelTwoCell.h"
#import "CommonMethods.h"
//#import "GlobalVariables.h"
#import "WebService.h"
#import "AddToItineraryTableViewCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "CurrentItineraryViewController.h"

@interface AddToItineraryViewController ()< UITableViewDelegate, UITableViewDataSource,CallCurrentItinerary>
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    UITableView *tableAddToItinerary;
    BOOL isClicked;
    NSInteger indexPathSection;
    UIButton *btnPlus;
    UIButton *btnSection;
    UILabel *lbl;
    UIDatePicker *objPickerView;
    UIToolbar *toolBar;
    UIBarButtonItem *barButtonDone;
    NSString *strDate;
    UIButton *btnMonth;
    UIButton *btnDay;
    UIButton *btnYear;
    UIButton *btnMinute;
    UIButton *btnHour;
    UILabel *desclabel;
    
    int openCellIndex;
    int openSectionIndex;
    NSMutableString *strSelectedSubcategories;
    BOOL isBtnPressed;
    BOOL isCurrentButtonPressed;
    NSString *dateToPrint;
}
@property (strong, nonatomic) NSArray *data;
//@property (weak, nonatomic) RATreeView *treeView;

@end

@implementation AddToItineraryViewController
@synthesize arrResponse;
- (void)viewDidLoad {
    [super viewDidLoad];
    
   // NSLog(@"arrResponse : %@", arrResponse);
    
    
    [self UiForNavigationBar];
    [self CreateUI];
    
    isCurrentButtonPressed = NO;
    strSelectedSubcategories = [[NSMutableString alloc]init];
    
    
    
    
    for (int i = 0; i<arrResponse.count; i++) {
        if ([[[[[[arrResponse valueForKey:@"sub_categories"]objectAtIndex:i]objectAtIndex:0]objectAtIndex:0]valueForKey:@"already_saved"] isEqualToString:@"1"]) {
            
        
        if (strSelectedSubcategories.length>0) {
            [strSelectedSubcategories appendString:@","];
        }
            NSString *strId = [[[[[arrResponse valueForKey:@"sub_categories"]objectAtIndex:i]objectAtIndex:0]objectAtIndex:0]valueForKey:@"sub_category_id"];
            [strSelectedSubcategories appendString:strId];
        }
    }    // Do any additional setup after loading the view.
    
    
}

-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}


-(void)CreateUI
{
    btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
    btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
    btnBuildItinerary.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBuildItinerary];
    btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnCurrentItinerary setTitle:@"Current Itinerary" forState:UIControlStateNormal];
    [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
    btnCurrentItinerary.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [self.view addSubview:btnCurrentItinerary];
    btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    openCellIndex = -1;
    openSectionIndex = -1;
    tableAddToItinerary = [[UITableView alloc] initWithFrame:CGRectMake(0, 119, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-280) style:UITableViewStyleGrouped];
    [self.view addSubview:tableAddToItinerary];
    [tableAddToItinerary setDelegate:self];
    [tableAddToItinerary setDataSource:self];
    tableAddToItinerary.backgroundColor = [UIColor clearColor];
    tableAddToItinerary.separatorColor=[UIColor clearColor];
    [tableAddToItinerary setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    
    if (arrResponse.count>0) {
       dateToPrint  = [[[arrResponse valueForKey:@"date"]objectAtIndex:0]objectAtIndex:0];
        NSString *month,*date,*year,*hour,*minute;
        if (dateToPrint.length ==0) {
            
        }
        else
        {
            month = [dateToPrint substringWithRange:NSMakeRange(0,2)];
            date = [dateToPrint substringWithRange:NSMakeRange(3, 2)];
            year = [dateToPrint substringWithRange:NSMakeRange(6, 4)];
            hour = [dateToPrint substringWithRange:NSMakeRange(11, 2)];
            minute = [dateToPrint substringWithRange:NSMakeRange(14, 2)];

        }
        if (IS_IPAD) {
            
            UILabel *labelChooseDate = [[UILabel alloc]init];
            labelChooseDate.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/2)-150, [[UIScreen mainScreen ] bounds].size.height-160, 200, 30);
            [self.view addSubview:labelChooseDate];
            labelChooseDate.text =@"Choose your Date *";
            labelChooseDate.textAlignment = NSTextAlignmentLeft;
            labelChooseDate.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
            
            UILabel *labelChooseTime = [[UILabel alloc]init];
            labelChooseTime.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2+100, [[UIScreen mainScreen ] bounds].size.height-160, 100, 30);
            [self.view addSubview:labelChooseTime];
            labelChooseTime.text =@"Time *";
            labelChooseDate.textColor=[UIColor darkGrayColor];
            labelChooseTime.textColor=[UIColor darkGrayColor];
            labelChooseTime.textAlignment = NSTextAlignmentLeft;
            labelChooseTime.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
            
            
            btnMonth = [UIButton buttonWithType:UIButtonTypeCustom];
            btnMonth.frame = CGRectMake(labelChooseDate.frame.origin.x, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnMonth];
            btnMonth.titleLabel.textColor = [UIColor grayColor];
            btnMonth.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            [btnMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnMonth];
            
            btnDay = [UIButton buttonWithType:UIButtonTypeCustom];
            btnDay.frame = CGRectMake(btnMonth.frame.origin.x+60, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnDay];
            
            btnDay.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnDay.titleLabel.textColor=KTextColor;
            [btnDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnDay];
            
            btnYear = [UIButton buttonWithType:UIButtonTypeCustom];
            btnYear.frame = CGRectMake(btnDay.frame.origin.x+60, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnYear];
            btnYear.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnYear.titleLabel.textColor = KTextColor;
            [btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnYear];
            
            
            btnHour = [UIButton buttonWithType:UIButtonTypeCustom];
            btnHour.frame = CGRectMake(labelChooseTime.frame.origin.x, [[UIScreen mainScreen] bounds].size.height-130, 45.0, 30.0);
            [self.view addSubview:btnHour];
            btnHour.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnHour.titleLabel.textColor=KTextColor;
            [btnHour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnHour];
            
            btnMinute = [UIButton buttonWithType:UIButtonTypeCustom];
            btnMinute.frame = CGRectMake(btnHour.frame.origin.x+50, [[UIScreen mainScreen] bounds].size.height-130, 45.0, 30.0);
            [self.view addSubview:btnMinute];
            btnMinute.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnMinute.titleLabel.textColor = KTextColor;
            [btnMinute setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnMinute];
            UIButton *btnDateSelection = [UIButton buttonWithType:UIButtonTypeCustom];
            btnDateSelection.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width/2)-150, [[UIScreen mainScreen] bounds].size.height-130, btnMinute.frame.origin.x+50, 30.0);
            [self.view addSubview:btnDateSelection];
            [btnDateSelection setBackgroundColor:[UIColor clearColor]];
            [btnDateSelection addTarget:self action:@selector(SelectDate) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            UILabel *labelChooseDate = [[UILabel alloc]init];
            labelChooseDate.frame = CGRectMake(20.0, [[UIScreen mainScreen ] bounds].size.height-160, [[UIScreen mainScreen] bounds].size.width/2, 30);
            [self.view addSubview:labelChooseDate];
            labelChooseDate.text =@"Choose your Date *";
            labelChooseDate.textAlignment = NSTextAlignmentLeft;
            labelChooseDate.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            UILabel *labelChooseTime = [[UILabel alloc]init];
            labelChooseTime.frame = CGRectMake(210, [[UIScreen mainScreen ] bounds].size.height-160, [[UIScreen mainScreen ] bounds].size.width/2, 30);
            [self.view addSubview:labelChooseTime];
            labelChooseTime.text =@"Time *";
            labelChooseTime.textAlignment = NSTextAlignmentLeft;
            labelChooseTime.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
            
            
            btnMonth = [UIButton buttonWithType:UIButtonTypeCustom];
            btnMonth.frame = CGRectMake(20.0, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnMonth];
            //  [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
            btnMonth.titleLabel.textColor = [UIColor grayColor];
            btnMonth.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            
            [btnMonth setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnMonth];
            
            btnDay = [UIButton buttonWithType:UIButtonTypeCustom];
            btnDay.frame = CGRectMake(80.0, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnDay];
            //   [btnDay setTitle:@"dd" forState:UIControlStateNormal];
            btnDay.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnDay.titleLabel.textColor=KTextColor;
            [btnDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnDay];
            
            btnYear = [UIButton buttonWithType:UIButtonTypeCustom];
            btnYear.frame = CGRectMake(140.0, [[UIScreen mainScreen] bounds].size.height-130, 55.0, 30.0);
            [self.view addSubview:btnYear];
            //   [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
            btnYear.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnYear.titleLabel.textColor = KTextColor;
            [btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnYear];
            
            
            btnHour = [UIButton buttonWithType:UIButtonTypeCustom];
            btnHour.frame = CGRectMake(210.0, [[UIScreen mainScreen] bounds].size.height-130, 45.0, 30.0);
            [self.view addSubview:btnHour];
            // [btnHour setTitle:@"hrs" forState:UIControlStateNormal];
            btnHour.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnHour.titleLabel.textColor=KTextColor;
            [btnHour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnHour];
            
            btnMinute = [UIButton buttonWithType:UIButtonTypeCustom];
            btnMinute.frame = CGRectMake(260.0, [[UIScreen mainScreen] bounds].size.height-130, 45.0, 30.0);
            [self.view addSubview:btnMinute];
            //   [btnMinute setTitle:@"mins" forState:UIControlStateNormal];
            btnMinute.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            btnMinute.titleLabel.textColor = KTextColor;
            [btnMinute setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self BorderforButton:btnMinute];
            UIButton *btnDateSelection = [UIButton buttonWithType:UIButtonTypeCustom];
            btnDateSelection.frame = CGRectMake(20.0, [[UIScreen mainScreen] bounds].size.height-130, [[UIScreen mainScreen] bounds].size.width-30, 30.0);
            [self.view addSubview:btnDateSelection];
            [btnDateSelection setBackgroundColor:[UIColor clearColor]];
            [btnDateSelection addTarget:self action:@selector(SelectDate) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        if (month.length==0) {
            [btnMonth setTitle:@"mm" forState:UIControlStateNormal];
        }
        else
        {
            [btnMonth setTitle:month forState:UIControlStateNormal];
        }
        
        if (date.length==0) {
            [btnDay setTitle:@"dd" forState:UIControlStateNormal];
        }
        else
        {
            [btnDay setTitle:date forState:UIControlStateNormal];
        }
        
        if (year.length==0) {
            [btnYear setTitle:@"yyyy" forState:UIControlStateNormal];
        }
        else
        {
            [btnYear setTitle:year forState:UIControlStateNormal];
        }
        
        if (hour.length==0) {
            [btnHour setTitle:@"hrs" forState:UIControlStateNormal];
        }
        else
        {
            [btnHour setTitle:hour forState:UIControlStateNormal];
        }
        
        if (minute.length==0) {
            [btnMinute setTitle:@"mins" forState:UIControlStateNormal];
        }
        else
        {
            [btnMinute setTitle:minute forState:UIControlStateNormal];
        }
        
//        UIImageView *imgVw=[[UIImageView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width/2-20,[[UIScreen mainScreen] bounds].size.height-80, 40, 40)];
//        imgVw.image=[UIImage imageNamed:@"save_button_red"];
//        imgVw.contentMode=UIViewContentModeScaleAspectFit;
//        [self.view addSubview:imgVw];
//        imgVw.userInteractionEnabled=YES;
//        UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]init];
//        singleTap.numberOfTouchesRequired=1;
//        [singleTap addTarget:self action:@selector(SaveButtonPressed)];
//        [imgVw addGestureRecognizer:singleTap];
//        UILabel *lblSave=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width/2-20,[[UIScreen mainScreen] bounds].size.height-40, 40, 15)];
//        lblSave.text=@"Save";
//        lblSave.textAlignment=NSTextAlignmentCenter;
//        lblSave.textColor=[UIColor darkGrayColor];
//        [self.view addSubview:lblSave];
        
        
        
        UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
        btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-35, [[UIScreen mainScreen]bounds].size.height-75,70 ,70 );
        
        [btnNext setImage: [UIImage imageNamed:@"save_button_red"] forState:UIControlStateNormal];
        [btnNext setImage:[UIImage imageNamed:@"save_button"] forState:UIControlStateSelected];
        [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
       // [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-55.0, 0.0,0.0)];
      //  [btnNext setTitle:@"Save" forState:UIControlStateNormal];
      //  [btnNext addTarget:self action:@selector(SaveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnNext];
        [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    }else{
        UILabel *lblSave=[[UILabel alloc]initWithFrame:CGRectMake(0,([[UIScreen mainScreen] bounds].size.height/2)-15, [[UIScreen mainScreen] bounds].size.width, 30)];
        lblSave.text=@"No item present";
        lblSave.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
        lblSave.textAlignment=NSTextAlignmentCenter;
        lblSave.textColor=KTextColor;
        [self.view addSubview:lblSave];
        
        
        
    }
    [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-95, [[UIScreen mainScreen]bounds].size.height-90,60 ,60 )];
    
    
    
}
#pragma mark-CreateButton


-(void)handleSingleTap
{
    toolBar.hidden=YES;
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        objPickerView.frame = CGRectMake(self.view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height+150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
}

-(void)BorderforButton:(UIButton *)button
{
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1.0;
    button.layer.cornerRadius = 2;
}
-(void)SelectDate
{
    [self.view endEditing:YES];
    
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, -150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
    objPickerView = [[UIDatePicker alloc]init];
    objPickerView.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height-150,[[UIScreen mainScreen] bounds].size.width , 150);
    [self.view.superview addSubview:objPickerView];
    //objPickerView.hidden = YES;
    objPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [objPickerView setDatePickerMode:UIDatePickerModeDateAndTime];
    objPickerView.minimumDate=[NSDate date];
    [objPickerView addTarget:self action:@selector(DateSelected:) forControlEvents:UIControlEventValueChanged];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-194, self.view.frame.size.width, 44.0)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    [self.view.superview addSubview:toolBar];
    
}


-(void)DateSelected:(id)sender
{
    NSDate *date= objPickerView.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *str = [dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateToShow = [dateFormatter dateFromString:str];
    strDate = [dateFormatter stringFromDate:dateToShow];
    
}
-(void)DoneBtnClicked:(id)sender
{
    if (strDate.length==0) {
        
    }
    else
    {
        NSString *strYear = [strDate substringWithRange:NSMakeRange(0,4)];
        NSString *strMonth = [strDate substringWithRange:NSMakeRange(5, 2)];
        NSString *strDay = [strDate substringWithRange:NSMakeRange(8, 2)];
        NSString *strHour = [strDate substringWithRange:NSMakeRange(11, 2)];
        NSString *strMinute = [strDate substringWithRange:NSMakeRange(14, 2)];
        
        [btnYear setTitle:strYear forState:UIControlStateNormal];
        [btnMonth setTitle:strMonth forState:UIControlStateNormal];
        [btnDay setTitle:strDay forState:UIControlStateNormal];
        [btnHour setTitle:strHour forState:UIControlStateNormal];
        [btnMinute setTitle:strMinute forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        objPickerView.frame = CGRectMake(self.view.frame.origin.x,[[UIScreen mainScreen]bounds].size.height+150, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
    
    toolBar.hidden=YES;
    
}


/*-(void)SaveButtonPressed
{
    if (strDate.length==0 && dateToPrint.length==0) {
        [[CommonMethods sharedInstance]AlertAction:@"Please select date"];
        return;
    }else if (strSelectedSubcategories.length==0){
        [[CommonMethods sharedInstance]AlertAction:@"Please add categories"];
        return;
    }
    else
    {
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//            NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//            [dateFormatter setTimeZone:gmt];
            NSDate *selectedDate =[dateFormatter dateFromString:strDate];
            [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];
            NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
            
            if (!(strSelectedDate.length==0)) {
                
            [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strSelectedSubcategories date:strSelectedDate andcompletionhandler:^(NSArray *returArray, NSError *error ){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                        NSMutableArray *arr = [[NSMutableArray alloc]init];
                        arr = [returArray valueForKey:@"result"];
                        btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                        btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                        
                        tableAddToItinerary.hidden = YES;
                        CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                        [self addChildViewController:controller1];
                        controller1.delegate = self;
                        controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                        [self.view addSubview:controller1.view];
                        [controller1 didMoveToParentViewController:self];
                        btnCurrentItinerary.userInteractionEnabled=NO;
                        isCurrentButtonPressed = YES;
                    }
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
        }
            else
            {
                [[WebService sharedInstance] SubmitSubCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/Itineraries/add_itinerary", kBaseUrl] param:strSelectedSubcategories date:dateToPrint andcompletionhandler:^(NSArray *returArray, NSError *error ){
                    if (!error) {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        NSLog(@"returArray %@",returArray);
                        NSString *errorstr = [returArray valueForKey:@"error"];
                        
                        if ([errorstr isEqualToString:@"already exist"]) {
                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            
                            tableAddToItinerary.hidden = YES;
                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            [self addChildViewController:controller1];
                            controller1.delegate = self;
                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            [self.view addSubview:controller1.view];
                            [controller1 didMoveToParentViewController:self];
                            btnCurrentItinerary.userInteractionEnabled=NO;
                            isCurrentButtonPressed = YES;
                            
                            
                            
                        }else if([returArray valueForKey:@"success"]){
                            NSMutableArray *arr = [[NSMutableArray alloc]init];
                            arr = [returArray valueForKey:@"result"];
                            btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
                            btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
                            
                            tableAddToItinerary.hidden = YES;
                            CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
                            [self addChildViewController:controller1];
                            controller1.delegate = self;
                            controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                            [self.view addSubview:controller1.view];
                            [controller1 didMoveToParentViewController:self];
                            btnCurrentItinerary.userInteractionEnabled=NO;
                            isCurrentButtonPressed = YES;
                        }
                    }
                    else
                    {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        
                    }
                }];
                
                

            }
        }
        else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
    
}
}


*/
-(void)BackButtonPressed
{
    if (isCurrentButtonPressed) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)BulidItinerary
{
    isCurrentButtonPressed=NO;
    btnCurrentItinerary.backgroundColor =kBtnUnselectedColor;
    btnBuildItinerary.backgroundColor =kBtnSelectionColor;
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    tableAddToItinerary.hidden = NO;
}
-(void)CurrentItinerary
{
    isCurrentButtonPressed=YES;
    btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
    btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
    
    tableAddToItinerary.hidden = YES;
    CurrentItineraryViewController* controller1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentItineraryViewController"];
    [self addChildViewController:controller1];
    controller1.delegate = self;
    controller1.view.frame = CGRectMake(0, 110.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:controller1.view];
    [controller1 didMoveToParentViewController:self];
    btnCurrentItinerary.userInteractionEnabled=NO;
    
}
- (void) CallChildViewController{
    CurrentItineraryViewController *vc = [self.childViewControllers lastObject];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    btnCurrentItinerary.userInteractionEnabled=YES;
    tableAddToItinerary.hidden = NO;
    [self CurrentItinerary];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Anand's Table Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [arrResponse count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //Apply Null
    return [[[[arrResponse objectAtIndex:section] objectAtIndex:0] valueForKey:@"sub_categories"] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *CellIdentifier = @"Cell";
    UIView *vwLine;
    UILabel *lblTime;
    AddToItineraryTableViewCell *cell = (AddToItineraryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddToItineraryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        vwLine=[[UIView alloc]init];
        lblTime=[[UILabel alloc]init];
        lblTime.textColor=[UIColor darkGrayColor];
        [vwLine addSubview:lblTime];
        vwLine.layer.borderColor=[UIColor darkGrayColor].CGColor;
        vwLine.layer.borderWidth=1;
        [cell.contentView addSubview:vwLine];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.btnSubCategory setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1]];
    [cell.btnSubCategory setTag:indexPath.section];
    [cell.btnSubCategory  setAccessibilityValue:[NSString stringWithFormat:@"%d", (int)indexPath.row]];
    [cell.btnSubCategory  addTarget:self action:@selector(expandRow:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnSubCategory setTitle:[[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"title"] forState:UIControlStateNormal];
    
    if(indexPath.row == openCellIndex && indexPath.section == openSectionIndex)
    {
        cell.labelDescription.textColor=[UIColor darkGrayColor];
        [cell.btnSubCategory setBackgroundColor: [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
        
        NSURL *url = [NSURL URLWithString:[[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"image"]];
        [cell.imageViewSubcategory sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
        
        cell.imageViewSubcategory.contentMode = UIViewContentModeScaleToFill;
        
        
        [cell.labelDescription setNumberOfLines:0];
        NSString *descstr = [[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"description"];
        NSAttributedString *_descstr = [[NSAttributedString alloc] initWithString:descstr];
        CGFloat hieght1 =[[CommonMethods sharedInstance]textViewHeightForAttributedText:_descstr andWidth:tableView.frame.size.width-40];
        
        CGRect initialFrame = CGRectMake(20.0, 110.0, [[UIScreen mainScreen] bounds].size.width-60, hieght1);
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(5, 10, 0, 0);
        CGRect paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets);
        
        cell.labelDescription.frame = paddedFrame;
        cell.labelPlus.text = @"-";
        
        
        
        cell.labelDescription.attributedText=_descstr;
        [cell.labelDescription sizeToFit];
        
        
        if (IS_IPAD) {
            vwLine.frame=CGRectMake(20.0, 110.0, [[UIScreen mainScreen] bounds].size.width-40, cell.labelDescription.frame.size.height+150);
            
        }else{
            vwLine.frame=CGRectMake(20.0, 110.0, [[UIScreen mainScreen] bounds].size.width-40,  cell.labelDescription.frame.size.height+150);
        }
        NSString *strTime=[@"Time - " stringByAppendingString:[[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"time_duration"]];
        lblTime.frame=CGRectMake(20.0, vwLine.frame.size.height-120, 200.0, 20.0) ;
        lblTime.text=strTime;
        
        UIButton *btnAddToItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAddToItinerary removeFromSuperview];
        
        btnAddToItinerary.frame = CGRectMake((vwLine.frame.size.width/2)-75.0,vwLine.frame.size.height-70, 150.0, 40.0);
        if ([[[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"already_saved"] isEqualToString:@"0"]) {
            [vwLine addSubview:btnAddToItinerary];
            
            [btnAddToItinerary setTitle:@"Add to itinerary" forState:UIControlStateNormal];
            [btnAddToItinerary addTarget:self action:@selector(BtnAddToItineraryClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        else{
            
            [btnAddToItinerary setTitle:@"Already Added" forState:UIControlStateNormal];
            btnAddToItinerary.userInteractionEnabled = NO;
            [vwLine addSubview:btnAddToItinerary];
        }
        btnAddToItinerary.backgroundColor = [UIColor grayColor];
        NSString *strSubCategoryId = [[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"sub_category_id"];
        btnAddToItinerary.tag = [(strSubCategoryId) integerValue];
        
        
        
        btnAddToItinerary.hidden=NO;
        cell.imageViewSubcategory.hidden=NO;
        cell.labelDescription.hidden = NO;
        [btnAddToItinerary setShowsTouchWhenHighlighted:TRUE];
    }
    else
    {
        cell.imageViewSubcategory.hidden=YES;
        cell.labelDescription.hidden = YES;
        cell.labelDescription.text = @"";
        cell.labelPlus.text = @"+";
        
        
        
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == openCellIndex && indexPath.section == openSectionIndex)
    {
        NSString *descstr = [[[[[arrResponse objectAtIndex:indexPath.section] objectAtIndex:0] valueForKey:@"sub_categories"] objectAtIndex:indexPath.row] valueForKey:@"description"];
        NSAttributedString *_descstr = [[NSAttributedString alloc] initWithString:descstr];
        CGFloat size =[[CommonMethods sharedInstance]textViewHeightForAttributedText:_descstr andWidth:tableView.frame.size.width-80];
        return size+400.0;
    }
    else
        return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 55)];
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, [[UIScreen mainScreen] bounds].size.width-40, 45)];
    [lblHeader setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1]];
    [lblHeader setTextColor:[UIColor whiteColor]];
    lblHeader.font = [UIFont fontWithName:@"Roboto-Bold" size:14];
    [lblHeader setText:[[[arrResponse objectAtIndex:section] objectAtIndex:0] valueForKey:@"category_name"]];
    [lblHeader setTextAlignment:NSTextAlignmentCenter];
    [header addSubview:lblHeader];
    return header;
}


-(void)BtnAddToItineraryClicked:(id)sender
{
    UIButton *btn = (id)sender;
  //  NSLog(@"%ld", (long)btn.tag);
    
    
    NSString *strId = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    if(strSelectedSubcategories.length > 0)
    {
        [strSelectedSubcategories appendString:@","];
    }
    [strSelectedSubcategories appendString:strId];
    isBtnPressed=YES;
    [btn setTitle:@"Added to Itinerary" forState:UIControlStateNormal];
    btn.userInteractionEnabled= NO;
}
- (void) expandRow:(UIButton*) sender{
    if(openCellIndex == [[sender accessibilityValue] intValue] && openSectionIndex == [sender tag]){
        openCellIndex = -1;
        openSectionIndex = -1;
        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:[sender tag]];
        [tableAddToItinerary beginUpdates];
        [tableAddToItinerary reloadRowsAtIndexPaths:@[tempIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        [tableAddToItinerary endUpdates];
        
        [tableAddToItinerary reloadData];
        //[tableAddToItinerary reloadData];
    }
    else{
        openCellIndex = [[sender accessibilityValue] intValue];
        openSectionIndex = (int)[sender tag];
        //[tableAddToItinerary reloadData];
        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:0 inSection:[sender tag]];
        [tableAddToItinerary beginUpdates];
        [tableAddToItinerary reloadRowsAtIndexPaths:@[tempIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        [tableAddToItinerary endUpdates];
        [tableAddToItinerary reloadData];
    }
}


- (CGSize)getSizeForText:(NSAttributedString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    
    
    
    NSAttributedString *attributedText = text;
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.frame.size.width, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];//you need to specify the some width, height will be calculated
    CGSize requiredSize = rect.size;
    
    return requiredSize;
    /*  CGSize constraintSize;
     constraintSize.height = MAXFLOAT;
     constraintSize.width = width;
     
     
     NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
     nil];
     
     CGRect frame = [text boundingRectWithSize:constraintSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:attributesDictionary
     context:nil];
     
     CGSize stringSize = frame.size;
     return stringSize;*/
}


//-(void)addTopBorder:(UILabel *)view{
//    UIView *topView;
//    topView= nil;
//    CGSize mainViewSize = view.bounds.size;
//    CGFloat borderWidth = 1;
//    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
//    topView = [[UIView alloc] initWithFrame:CGRectMake(-10.0, 0.0, mainViewSize.width+20, borderWidth)];
//    topView.opaque = YES;
//    topView.backgroundColor = borderColor;
//    // topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//    [view addSubview:topView];
//}
//
//-(void)addlefBorder:(UILabel *)view{
//    UIView *topView;
//    topView= nil;
//    CGSize mainViewSize = view.bounds.size;
//    CGFloat borderheight = mainViewSize.height+30;
//    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
//    topView = [[UIView alloc] initWithFrame:CGRectMake(-10.0, 0.0,1, borderheight)];
//    topView.opaque = YES;
//    topView.backgroundColor = borderColor;
//    //  topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//    [view addSubview:topView];
//    [self addBottomBorder:(UITableViewCell *)view.superview];
//}
//-(void)addBottomBorder:(UITableViewCell *)view{
//    UIView *topView;
//    topView=nil;
//    CGSize mainViewSize = view.bounds.size;
//    CGFloat borderWidth = 1;
//    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
//    topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, mainViewSize.height+2+400, lbl.frame.size.width, borderWidth)];
//    topView.opaque = YES;
//    topView.backgroundColor = borderColor;
//    //   topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//    [view addSubview:topView];
//}
//-(void)addRightBorder:(UILabel *)view{
//    UIView *topView;
//    [topView removeFromSuperview];
//    CGSize mainViewSize = view.bounds.size;
//    // CGFloat borderWidth = 1;
//    CGFloat borderheight = view.frame.size.height+30;
//    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
//    topView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width+10, 0.0, 1, borderheight)];
//    topView.opaque = YES;
//    topView.backgroundColor = borderColor;
//    //  topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//    [view addSubview:topView];
//}


@end
