//
//  ChaperoneInstructionsViewController.h
//  Putnam Teacher's App
//
//  Created by Inficare on 3/1/17.
//  Copyright © 2017 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChaperoneInstructionsViewController : UIViewController
@property(nonatomic,assign) BOOL isChaperone;

@end
