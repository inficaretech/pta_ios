//
//  CustomCell_Language.h
//  LanguageSelection
//
//  Created by puran on 12/4/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell_Language : UITableViewCell
{
//    UIButton *btnStar3;
//    UIButton *btnStar2;
}
@property (weak, nonatomic) IBOutlet UILabel *_lblTitleText;
@property (weak, nonatomic) IBOutlet UIImageView *_imgCell;
@property (weak, nonatomic) IBOutlet UILabel *_lblDivider;
@property (weak, nonatomic) UIButton *btnStar2;
@property (weak, nonatomic) UIButton *btnStar3;

@end
