//
//  LoginViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/27/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"
#import "HomeViewController.h"
#import "ChaperoneStrtViewController.h"
#import "AppDelegate.h"
#import "DataBaseFetchValues.h"

@interface LoginViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfpasswrd;
    IBOutlet UIButton *btnLogIn;
    IBOutlet UIButton *btnForgotPassrwd;
    IBOutlet UIButton *btnChaperoneaccess;
    IBOutlet UIButton *btnSignUp;
    IBOutlet UIButton *btnLoginAsGuest;
    IBOutlet UILabel *lblTeacher;
    IBOutlet UILabel *lblEmail;
    IBOutlet UILabel *lblPasswrd;
    UITextField *tfForEnterEmail;
    UITextField *tfForEnterCode;
    UIButton *btnremeber;
    UIView *vwForAccessCode;
    //int direction;
    //int shakes;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self CreateUi];
    
    DataBaseFetchValues *dbFetch = [[DataBaseFetchValues alloc] init];
    [dbFetch deleteQuerry:@"DELETE from StudentList"];
    [dbFetch deleteQuerry:@"DELETE from ChaperoneList"];
    // Do any additional setup after loading the view.
}

-(void)CreateUi{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
    tfEmail.delegate=self;
    tfpasswrd.delegate=self;
    tfEmail.leftView=lblEmail;
    tfEmail.leftViewMode = UITextFieldViewModeAlways;
    tfpasswrd.leftView=lblPasswrd;
    tfpasswrd.leftViewMode = UITextFieldViewModeAlways;
    tfEmail.frame=CGRectMake(20.0, tfEmail.frame.origin.y, [[UIScreen mainScreen]bounds].size.width-40, tfEmail.frame.size.height);
    tfpasswrd.frame=CGRectMake(20.0, tfEmail.frame.origin.y, [[UIScreen mainScreen]bounds].size.width-40, tfEmail.frame.size.height);
    
    btnSignUp.titleLabel.numberOfLines = 2;
    [btnSignUp setTitle:@"       Sign Up\n(For teacher only)" forState:UIControlStateNormal];
    
    btnLoginAsGuest.titleLabel.numberOfLines = 2;
    [btnLoginAsGuest setTitle:@"     Virtual Tour\n (Just exploring)" forState:UIControlStateNormal];
    
    
    //tfEmail.layer.borderColor = [[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1]CGColor];
    //  tfEmail.layer.borderWidth = 1;
    
    
    //Bottom border
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, 0.0, tfEmail.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
    [tfEmail.layer addSublayer:bottomBorder];
    
    //left border
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, tfEmail.frame.size.height);
    leftBorder.backgroundColor = [UIColor blackColor].CGColor;
    [tfEmail.layer addSublayer:leftBorder];
    
    //right border
    CALayer *rightBorder = [CALayer layer];
    rightBorder.frame = CGRectMake(tfEmail.frame.size.width-1, 0.0f, 1.0f, tfEmail.frame.size.height);
    rightBorder.backgroundColor = [UIColor blackColor].CGColor;
    [tfEmail.layer addSublayer:rightBorder];
    
    //    CALayer *bottomBorder = [CALayer layer];
    //    bottomBorder.frame = CGRectMake(0.0f, tfEmail.frame.size.height -1, tfEmail.frame.size.width, 2.0f);
    //    bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
    //    [tfEmail.layer addSublayer:bottomBorder];
    
    
    tfpasswrd.layer.borderColor = [[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1]CGColor];
    tfpasswrd.layer.borderWidth=1.0;
    
    
    UIView *Vw=[[UIView alloc]initWithFrame:CGRectMake(tfpasswrd.frame.size.width-50, 5, 50, tfpasswrd.frame.size.height-10)];
    tfpasswrd.rightView=Vw;
    tfpasswrd.rightViewMode = UITextFieldViewModeAlways;// Set rightview mode
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 5, 60, tfpasswrd.frame.size.height-20);
    [Vw addSubview:btn];
    [btn setImage:[UIImage imageNamed:@"eye_icon"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(ShowPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate=self;
    [tapper setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapper];
    
    [btnForgotPassrwd addTarget:self action:@selector(ButtonForgotPasswordPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(20.0, 40.0, 20.0, 20.0);
    [btnBack setImage:[UIImage imageNamed:@"back_button2"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    
}
-(void)handleSingleTap:(id)sender{
    
    [self.view endEditing:YES];
    tfEmail.layer.borderColor=[UIColor blackColor].CGColor;
    tfpasswrd.layer.borderColor=[UIColor blackColor].CGColor;
}
#pragma mark- ShowPassword
-(void)ShowPassword:(id)sender{
    
    [tfpasswrd setSecureTextEntry:!tfpasswrd.secureTextEntry];
}
#pragma mark- back
-(void)BackButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- LoginButtonAction
- (IBAction)LoginButtonPressed:(id)sender {
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    BOOL isEmailValid=[self NSStringIsValidEmail:tfEmail.text];
    
    if (tfEmail.text.length==0) {
        // direction  = 1;
        // shakes = 0;
        //tfEmail.layer.borderColor=[UIColor redColor].CGColor;
      //  [self shake:tfEmail];
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for email"];
        return;
        
    }else if (!isEmailValid){
        // direction  = 1;
        // shakes = 0;
        // tfEmail.layer.borderColor=[UIColor redColor].CGColor;
        // [self shake:tfEmail];
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for email"];
        return;
    }else if (tfpasswrd.text.length==0){
        //direction  = 1;
        //shakes = 0;
        // tfpasswrd.layer.borderColor=[UIColor redColor].CGColor;
        // [self shake:tfpasswrd];
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for password"];
        
    }else if (tfpasswrd.text.length<8){
        // direction  = 1;
        // shakes = 0;
        //tfpasswrd.layer.borderColor=[UIColor redColor].CGColor;
        // [self shake:tfpasswrd];Please enter password between 8-15
        //[[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for password"];
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for password"];
        
    }else{
        [dict setValue:tfEmail.text forKey:@"email"];
        [dict setValue:tfpasswrd.text forKey:@"password"];
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/login?",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([returArray valueForKey:@"error"]) {
                        // [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid email or password"];
                        [[CommonMethods sharedInstance] AlertAction:errorstr];
                        return;
                    }else if([returArray valueForKey:@"success"]){
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        // saving an NSString
                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"id"];
                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"access_token"];
                        
                        [prefs setObject:tfEmail.text forKey:@"EmailId"];
                        [prefs removeObjectForKey:@"Class"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        HomeViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"HomeView"];
                        
                        // Anvesh
                        
                        NSArray *substrings = [returArray valueForKey:@"name"];// componentsSeparatedByString:@"`"];
                        
                        
                        NSMutableString * string = [substrings mutableCopy];
                        [string replaceOccurrencesOfString:@"`"
                                                withString:@" "
                                                   options:0
                                                     range:NSMakeRange(0, string.length)];
                        //  if (substrings.count>=2) {
                        //      NSString * strLastName =[substrings objectAtIndex:1];
                        [prefs setObject:string forKey:@"name"];
                        //  }
                        UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appdel.window.rootViewController = obj;
                    }
                    
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    
                }
            }];
            
            
            
            
        }else{
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
        }
        
        
    }
    
    
}
/*
 #pragma mark-animation
 -(void)shake:(UIView *)theOneYouWannaShake
 {
 //int  direction;
 // direction  = 1;
 // shakes = 0;
 [UIView animateWithDuration:0.03 animations:^
 {
 theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
 }
 completion:^(BOOL finished)
 {
 if(shakes >= 10)
 {
 theOneYouWannaShake.transform = CGAffineTransformIdentity;
 return;
 }
 shakes++;
 direction = direction * -1;
 [self shake:theOneYouWannaShake];
 }];
 }
 */
#pragma mark- ChaperoneButtonAction
- (IBAction)ChaperoneButtonPressed:(id)sender {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //create a new view with the same size
    UIView* vw = [[UIView alloc] initWithFrame:screenRect];
    vw.tag=1;
    // change the background color to black and the opacity to 0.7
    vw.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.9];
    
    //    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    //    vw.backgroundColor=[UIColor lightGrayColor];
    
    // UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(240.0, 330.0, 300.0, 300.0)];
    
    vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(30,150, [[UIScreen mainScreen]bounds].size.width-60, 250.0)];
    vwForAccessCode.backgroundColor=[UIColor whiteColor];
    vwForAccessCode.layer.borderWidth=2;
    vwForAccessCode.layer.borderColor=[UIColor blueColor].CGColor;
    
    UILabel *lblEnterAccessCode=[[UILabel alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-100, 70.0, 200, 20.0)];
    lblEnterAccessCode.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblEnterAccessCode.text=@"Please enter access code";
    lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblEnterAccessCode.font =[UIFont fontWithName:@"Roboto-Regular" size:22];
    [vwForAccessCode addSubview:lblEnterAccessCode];
    
    tfForEnterCode=[[UITextField alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-70, 120.0, 140, 30.0)];
    tfForEnterCode.textAlignment=NSTextAlignmentCenter;
    tfForEnterCode.delegate=self;
    tfForEnterCode.returnKeyType=UIReturnKeyDone;
    tfForEnterCode.spellCheckingType = UITextSpellCheckingTypeNo;
    tfForEnterCode.autocapitalizationType=UITextAutocapitalizationTypeNone;
    tfForEnterCode.autocorrectionType = UITextAutocorrectionTypeNo;
    [vwForAccessCode addSubview:tfForEnterCode];
    
    tfForEnterCode.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"Chaperone_Access_code"];
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1].CGColor;
    border.frame = CGRectMake(0, tfForEnterCode.frame.size.height - borderWidth, tfForEnterCode.frame.size.width, tfForEnterCode.frame.size.height);
    border.borderWidth = borderWidth;
    [tfForEnterCode.layer addSublayer:border];
    tfForEnterCode.layer.masksToBounds = YES;
    
    btnremeber=[UIButton buttonWithType:UIButtonTypeCustom];
    btnremeber.frame=CGRectMake(10, 180, 120, 40);
    [btnremeber setSelected:YES];
    [btnremeber setTitle:@"Remember" forState:UIControlStateNormal];
    [btnremeber setImage:[UIImage imageNamed:@"box_icon"] forState:UIControlStateNormal];
    [btnremeber setImage:[UIImage imageNamed:@"tick_icon"] forState:UIControlStateSelected];
    btnremeber.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
    [btnremeber setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnremeber addTarget:self action:@selector(RememberPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    [vwForAccessCode addSubview:btnremeber];
    
    
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)+20, 180, 80, 40)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:22];
    
    [btnDone addTarget:self action:@selector(goToChaperonepage:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [vwForAccessCode addSubview:btnDone];
    
    UIButton *btnCross=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width-30, 10.0, 20, 20)];
    //[btnCross setTitle:@"*" forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(RemoveAccessView:) forControlEvents:UIControlEventTouchUpInside];
    [btnCross setImage:[UIImage imageNamed:@"Cross_mark"] forState:UIControlStateNormal];
    //btnCross.backgroundColor=[UIColor redColor];
    [vwForAccessCode addSubview:btnCross];
    
    
    
    [vw addSubview:vwForAccessCode];
    [self.view addSubview:vw];

    vwForAccessCode.alpha     = 0.0f;
    vwForAccessCode.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
         usingSpringWithDamping:2.0f
          initialSpringVelocity:1
                        options:0
                     animations:^{
                         vwForAccessCode.alpha     = 1.0f;
                         vwForAccessCode.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                     }
                     completion:nil];
    
    
}
#pragma mark- RememberPassword
-(void)RememberPassword:(id)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
    btn.selected=!btn.selected;
    
    
}
//#pragma mark-goToChaperonepage
//-(void)goToChaperonepage:(id)sender{
//    
//    if (tfForEnterCode.text.length==0) {
//        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for access code"];
//        return;
//        
//    }else if (tfForEnterCode.text.length<5){
//        [[CommonMethods sharedInstance] AlertAction:@"Please enter correct 5-digit chaperone access code"];
//        return;
//        
//    }else{
//        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//        [ dict setValue:tfForEnterCode.text forKey:@"access_code"];
//        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
//        
//        if (isinternet) {
//            
//            if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text]){
//                [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Chaperone_Student_List"];
//                [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
//            }
//            
//            [[CommonMethods sharedInstance] addSpinner:self.view];
//            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/chaperon_login",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
//                
//                if (!error) {
//                    [[CommonMethods sharedInstance] removeSpinner:self.view];
//                    NSLog(@"returArray %@",returArray);
//                    NSString *errorstr = [returArray valueForKey:@"error"];
//                    
//                    if ([returArray valueForKey:@"error"]) {
//                        [[CommonMethods sharedInstance] AlertAction:errorstr];
//                        return;
//                    }else if([returArray valueForKey:@"access_token"]){
//                        
//                        
//                        
//                        
//                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                        // saving an NSString
//                        
//                        if (btnremeber.isSelected) {
//                            [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Access_code"];
//                            
//                        }else{
//                            [prefs removeObjectForKey:@"Chaperone_Access_code"];
//                            
//                        }
//                        
//                        //  NSString *strFullgroupName=[@"Group-" stringByAppendingString:[returArray valueForKey:@"group_name"] ];
//                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"Chaperone_id"];
//                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"chaperone_access_token"];
//                        [prefs setObject:[returArray valueForKey:@"teacher_id"] forKey:@"teacher_id"];
//                        [prefs setObject:[returArray valueForKey:@"group_id"] forKey:@"group_id"];
//                        [prefs setObject:[returArray valueForKey:@"trip_id"] forKey:@"trip_id"];
//                        [prefs setObject: [@"Group -" stringByAppendingString:[returArray valueForKey:@"group_name"] ]   forKey:@"group_name"];
//                        [prefs setObject: [returArray valueForKey:@"group_id"]    forKey:@"group_id"];
//                        [prefs setObject:[returArray valueForKey:@"score"] forKey:@"group_score"];
//                        [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Code"];
//                        
//                        
//                        [[NSUserDefaults standardUserDefaults] synchronize];
//                        
//                        
//                        UIView *vw=[self.view viewWithTag:1];
//                        [vw removeFromSuperview];
//                        
//                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                        ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
//                        WallOfFameViewController.Islogin=YES;
//                        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
//                        [self presentViewController:navController animated:YES completion:nil];
//                        
//                        
//                    }
//                    
//                    
//                }
//                else
//                {
//                    [[CommonMethods sharedInstance] removeSpinner:self.view];
//                    [[CommonMethods sharedInstance] AlertAction:@"Server error"];
//                    
//                }
//            }];
//            
//            
//            
//            
//        }else{
//            
//            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
//            
//        }
//        
//        
//        
//        
//        
//        
//    }
//    
//}

#pragma mark-goToChaperonepage
-(void)goToChaperonepage:(id)sender{
    
    if (tfForEnterCode.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for access code"];
        return;
        
    }else if (tfForEnterCode.text.length<5){
        [[CommonMethods sharedInstance] AlertAction:@"Please enter correct 5-digit chaperone access code"];
        return;
        
    }else{
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [ dict setValue:tfForEnterCode.text forKey:@"access_code"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/chaperon_login",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([returArray valueForKey:@"error"]) {
                        [[CommonMethods sharedInstance] AlertAction:errorstr];
                        return;
                    }else if([returArray valueForKey:@"access_token"]){
                        
                        
                        if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text] || ![[returArray valueForKey:@"group_id"]isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]] ){
                            [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Chaperone_Student_List"];
                            [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        // saving an NSString
                        
                        if (btnremeber.isSelected) {
                            [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Access_code"];
                            
                        }else{
                            [prefs removeObjectForKey:@"Chaperone_Access_code"];
                            
                        }
                        
                        //  NSString *strFullgroupName=[@"Group-" stringByAppendingString:[returArray valueForKey:@"group_name"] ];
                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"Chaperone_id"];
                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"chaperone_access_token"];
                        [prefs setObject:[returArray valueForKey:@"teacher_id"] forKey:@"teacher_id"];
                        [prefs setObject:[returArray valueForKey:@"group_id"] forKey:@"group_id"];
                        [prefs setObject:[returArray valueForKey:@"trip_id"] forKey:@"trip_id"];
                        [prefs setObject: [@"Group -" stringByAppendingString:[returArray valueForKey:@"group_name"] ]   forKey:@"group_name"];
                        [prefs setObject: [returArray valueForKey:@"group_id"]    forKey:@"group_id"];
                        [prefs setObject:[returArray valueForKey:@"score"] forKey:@"group_score"];
                        [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Code"];
                        [prefs setObject:[returArray valueForKey:@"completed"] forKey:@"completed"];
                        
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        UIView *vw=[self.view viewWithTag:1];
                        [vw removeFromSuperview];
                        
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                        WallOfFameViewController.Islogin=YES;
                        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
                        [self presentViewController:navController animated:YES completion:nil];
                        
                        
                    }
                    
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    
                }
            }];
            
            
            
            
        }else{
            
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text]){
                UIView *vw=[self.view viewWithTag:1];
                [vw removeFromSuperview];
                
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                WallOfFameViewController.Islogin=YES;
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
                [self presentViewController:navController animated:YES completion:nil];
            }
        }
        
        
        
        
        
        
    }
    
}
#pragma mark- LoginAsGuestButtonAction
- (IBAction)LoginAsguestButtonPressed:(id)sender {
    // UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    // HomeViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"HomeView"];
    //[self.navigationController pushViewController:Controller animated:YES];
    
    
}

#pragma mark- RemoveAccessView
-(void)RemoveAccessView:(id)sender{
    
    UIView *vw=[self.view viewWithTag:1];
    //[vw removeFromSuperview];
    vwForAccessCode.alpha     = 0.0f;
    vwForAccessCode.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    vw.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
         usingSpringWithDamping:2.0f
          initialSpringVelocity:1
                        options:0
                     animations:^{
                         vwForAccessCode.alpha     = 1.0f;
                         vwForAccessCode.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
                     }
                     completion:^(BOOL finished){
                         [vw removeFromSuperview];
                         
                     }];
}
#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.layer.borderColor=[UIColor blackColor].CGColor;
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField==tfForEnterEmail) {
        [textField resignFirstResponder];
    }
    
    if (textField==tfEmail) {
        if (tfEmail.text.length==0) {
            [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for email address"];
            
            return YES;
            
        }else if (![self NSStringIsValidEmail:tfEmail.text]){
            [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for email address"];
            
            return YES;
        }else{
            [tfpasswrd becomeFirstResponder];
        }
        
        
    }else if (textField==tfpasswrd)
    {if (tfpasswrd.text.length==0){
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide  input for password"];
        
        return YES;
        
    }else if (tfpasswrd.text.length<8){
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for password"];
        return YES;
        
    }else{
        [tfpasswrd resignFirstResponder];
        
    }
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    
    
    if (textField==tfpasswrd) {
        return newLength <= 15;
    }else if (textField==tfForEnterCode){
        
        return newLength <= 5;
    }
    
    
    
    return newLength <= 100;
    
    
    
}
#pragma mark- SignUpButtonAction
- (IBAction)ButtonSignUpIsPressed:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpViewController *signUpView = [story instantiateViewControllerWithIdentifier:@"signUpView"];
    [self.navigationController pushViewController:signUpView animated:YES];
}

#pragma mark- EmailValidation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (void)ButtonForgotPasswordPressed:(id)sender{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //create a new view with the same size
    UIView* vw = [[UIView alloc] initWithFrame:screenRect];
    vw.tag=1;
    // change the background color to black and the opacity to 0.7
    vw.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.9];
    
    //    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    //    vw.backgroundColor=[UIColor lightGrayColor];
    
    // UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(240.0, 330.0, 300.0, 300.0)];
    
    UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(30,150, [[UIScreen mainScreen]bounds].size.width-60, 250.0)];
    vwForAccessCode.backgroundColor=[UIColor whiteColor];
    vwForAccessCode.layer.borderWidth=2;
    vwForAccessCode.layer.borderColor=[UIColor blueColor].CGColor;
    
    UILabel *lblEnterAccessCode=[[UILabel alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-100, 70.0, 200, 20.0)];
    lblEnterAccessCode.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblEnterAccessCode.text=@"Please enter your email address";
    lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblEnterAccessCode.font =[UIFont fontWithName:@"Roboto-Regular" size:22];
    [vwForAccessCode addSubview:lblEnterAccessCode];
    
    tfForEnterEmail=[[UITextField alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-120, 120.0, 240, 30.0)];
    tfForEnterEmail.textAlignment=NSTextAlignmentCenter;
    tfForEnterEmail.delegate=self;
    tfForEnterEmail.autocapitalizationType=UITextAutocapitalizationTypeNone;
    tfForEnterEmail.returnKeyType=UIReturnKeyDone;
    tfForEnterEmail.keyboardType=UIKeyboardTypeEmailAddress;
    tfForEnterEmail.spellCheckingType = UITextSpellCheckingTypeNo;
    tfForEnterEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    tfForEnterEmail.layer.borderWidth=1;
    tfForEnterEmail.layer.borderColor=[UIColor blackColor].CGColor;
    [vwForAccessCode addSubview:tfForEnterEmail];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,5, tfForEnterEmail.frame.size.height)];
    tfForEnterEmail.leftView = paddingView;
    tfForEnterEmail.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-40, 180, 80, 40)];
    [btnDone setTitle:@"Send" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:22];
    
    [btnDone addTarget:self action:@selector(sendPasswordToEmail:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [vwForAccessCode addSubview:btnDone];
    
    UIButton *btnCross=[UIButton buttonWithType:UIButtonTypeCustom];
    btnCross.frame=CGRectMake(vwForAccessCode.frame.size.width-30, 10.0, 20, 20);
    //UIButton *btnCross=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width-30, 10.0, 20, 20)];
    //[btnCross setTitle:@"*" forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(RemoveAccessView:) forControlEvents:UIControlEventTouchUpInside];
    [btnCross setImage:[UIImage imageNamed:@"Cross_mark"] forState:UIControlStateNormal];
    //btnCross.backgroundColor=[UIColor redColor];
    [vwForAccessCode addSubview:btnCross];
    
    
    
    [vw addSubview:vwForAccessCode];
    [self.view addSubview:vw];
    
    
}
#pragma mark- SendEmail
-(void)sendPasswordToEmail:(id)sender{
    
    if (tfForEnterEmail.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Please enter your email address"];
    }else if (![self NSStringIsValidEmail:tfForEnterEmail.text]){
        
        [[CommonMethods sharedInstance] AlertAction:@"Please enter your valid email address"];
        
    }else{
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:tfForEnterEmail.text forKey:@"email"];
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/forget_password",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    //   NSString *errorstr = [returArray valueForKey:@"error"];
                    if ( [returArray valueForKey:@"error"]) {
                        [[CommonMethods sharedInstance] AlertAction: [returArray valueForKey:@"error"]];
                    }else{
                        [[CommonMethods sharedInstance] AlertAction:@"Please check your email for link to reset your password"];
                        [self RemoveAccessView:self];
                    }
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
        }
        else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
        
        
        
        
    }
    
    
}

@end
