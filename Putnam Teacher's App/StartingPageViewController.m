//
//  StartingPageViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/26/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "StartingPageViewController.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "WallOfFameViewController.h"
#import "OutreachProgramsViewController.h"
#import "FieldTripPricingViewController.h"
#import "CommonMethods.h"
//#import <Crashlytics/Crashlytics.h>
#import "GalleryViewController.h"


@interface StartingPageViewController ()

@end
@implementation StartingPageViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self UiDesign];
   
    
}


-(void)UiDesign
{
//    scrollView = [[UIScrollView alloc]init];
//    scrollView.frame= CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
//    [self.view addSubview:scrollView];
//    scrollView.contentSize =CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    
    
    if (IS_IPAD) {
        lblTeacherApp.font =[UIFont fontWithName:@"Roboto-Regular" size:25];
        lblAlcoaFoundation.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        
    }
    
    // btn.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:188.0/255.0 blue:212.0/255.0 alpha:1];
  //  topView.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:138.0/255.0 alpha:1];
    btnWallOfFame.layer.shadowColor = [UIColor grayColor].CGColor;
    btnWallOfFame.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnWallOfFame.layer.shadowOpacity = 1.0;
    btnWallOfFame.layer.shadowRadius = 3.0;
    
    btnGallery.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:188.0/255.0 blue:212.0/255.0 alpha:1];
    btnGallery.layer.shadowColor = [UIColor grayColor].CGColor;
    btnGallery.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnGallery.layer.shadowOpacity = 1.0;
    btnGallery.layer.shadowRadius = 3.0;
    
    
    btnFieldTripPricing.layer.shadowColor = [UIColor grayColor].CGColor;
    btnFieldTripPricing.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnFieldTripPricing.layer.shadowOpacity = 1.0;
    btnFieldTripPricing.layer.shadowRadius = 3.0;
    [btnFieldTripPricing addTarget:self action:@selector(FieldTripPricingButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    btnManageFieldTrip.layer.shadowColor = [UIColor grayColor].CGColor;
    btnManageFieldTrip.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnManageFieldTrip.layer.shadowOpacity = 1.0;
    btnManageFieldTrip.layer.shadowRadius = 3.0;
   // [btnManageFieldTrip addTarget:self action:@selector(ManageFieldTrip:) forControlEvents:UIControlEventTouchUpInside];
    
    btnOutreachProgram.layer.shadowColor = [UIColor grayColor].CGColor;
    btnOutreachProgram.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnOutreachProgram.layer.shadowOpacity = 1.0;
    btnOutreachProgram.layer.shadowRadius = 3.0;
    
    btnLogin.layer.shadowColor = [UIColor grayColor].CGColor;
    btnLogin.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnLogin.layer.shadowOpacity = 1.0;
    btnLogin.layer.shadowRadius = 3.0;
    
    UIColor *borderColor10 = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:138.0/255.0 alpha:1];
    [self addlefBorder:btnGallery and:borderColor10];

    
    UIColor *borderColor = [UIColor colorWithRed:77.0/255.0 green:143.0/255.0 blue:0.0/255.0 alpha:1];
    [self addlefBorder:btnWallOfFame and:borderColor];
    
    UIColor *borderColor1 = [UIColor colorWithRed:164.0/255.0 green:0.0/255.0 blue:7.0/255.0 alpha:1];
    [self addlefBorder:btnFieldTripPricing and:borderColor1];
    
    UIColor *borderColor2 = [UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:162.0/255.0 alpha:1];
    [self addlefBorder:btnManageFieldTrip and:borderColor2];
    
    UIColor *borderColor3 = [UIColor colorWithRed:239.0/255.0 green:58.0/255.0 blue:0.0/255.0 alpha:1];
    [self addlefBorder:btnOutreachProgram and:borderColor3];
    
    
    
  
}


#pragma mark- viewwillappear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
}
#pragma mark- btnManageFieldTrip
-(IBAction)ManageFieldTrip1:(id)sender{
    
    
    [[CommonMethods sharedInstance] AlertAction:@"You can manage field trip after login"];
    
}


- (IBAction)ButtonLoginPressed:(id)sender {
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *loginView =[story instantiateViewControllerWithIdentifier:@"loginView"];
    [self.navigationController pushViewController:loginView animated:YES];
}

- (IBAction)WallOfFamButtonClicked:(id)sender {

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WallOfFameViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"WallOfFameViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
    [self presentViewController:navController animated:YES completion:nil];
}
- (IBAction)OutreachProgramsButtonPressed:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OutreachProgramsViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"OutreachProgramsViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)addlefBorder:(UIButton *)view and:(UIColor *)color
{
    UIColor *borderColor = color;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0,5.0,40)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    [view addSubview:topView];
}


- (void)FieldTripPricingButtonPressed {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FieldTripPricingViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"FieldTripPricingViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
    [self presentViewController:navController animated:YES completion:nil];

}

- (IBAction)GalleryView:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GalleryViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"GalleryViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:WallOfFameViewController];
    WallOfFameViewController.IsChaperone=YES;
    [self presentViewController:navController animated:YES completion:nil];

    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
