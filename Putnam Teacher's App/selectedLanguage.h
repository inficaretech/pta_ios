//
//  selectedLanguage.h
//  Museum
//
//  Created by puran on 12/5/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface selectedLanguage : NSObject

@property (nonatomic, copy) NSString *languageName;
@property (nonatomic, assign) BOOL isChecked;

@end
