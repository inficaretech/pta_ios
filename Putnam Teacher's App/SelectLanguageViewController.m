//
//  SelectLanguageViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 8/1/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "SelectLanguageViewController.h"
#import "AppDelegate.h"
#import "LanguageSelection.h"
#import "WebService.h"
#import "CommonMethods.h"

@interface SelectLanguageViewController ()
{
    AppDelegate *appDelegate;
    LanguageSelection *objLanguage;
    NSMutableArray *arrLanguageNames;
    UIViewController *vc;
    UIWindow *window;
    

}
@end

@implementation SelectLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    self.navigationController.navigationBar.hidden = YES;
    [self.imgViewLogo setAlpha:0];
    
    [self.imgViewLogo setUserInteractionEnabled:YES];
    [self.imgViewLogo startAnimating];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:2];
    [self.imgViewLogo setAlpha:1];
    [UIView commitAnimations];
    
     [self getLanguageRequest];
    
    
    [super viewDidLoad];
    //NSLog(@"Select Language Called");
    
    [__btnSelectLanguage.titleLabel setFont:[UIFont fontWithName:@"Roboto-regular" size:15]];
    
    [__btnSelectLanguage setHidden:NO];
   

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getLanguageRequest
{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
      //  http://14.141.136.170:8888/projects/Teacherapp/index.php/Languages/get_language
        
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Languages/get_language",kBaseUrl] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                arrLanguageNames = [[NSMutableArray alloc]init];
                arrLanguageNames = [[returArray valueForKey:@"result" ] mutableCopy];

            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    

}

- (IBAction)btnPressed:(id)sender {
    
    //TO DO : Shefali
    //Done because selected row dnt perform
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"LanguageName"];
    [defaults setObject:@""  forKey:@"LanguageId"];
    [defaults synchronize];
    
    
    
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:arrLanguageNames];
    //NSLog(@"Select Language Button Pressed");
    objLanguage = [[[NSBundle mainBundle] loadNibNamed:@"LanguageSelection"  owner:self    options:nil] lastObject];
    [objLanguage callWithArrayItems:arr];
    objLanguage.delegate = self;
    objLanguage.frame = self.view.frame;
    [objLanguage setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
    objLanguage.transform = CGAffineTransformMakeScale(1.3, 1.3);
    objLanguage.alpha = 0;
    [objLanguage._tblView setSeparatorInset:UIEdgeInsetsZero];
    
    [UIView animateWithDuration:.35 animations:^{
        objLanguage.alpha = 1;
        objLanguage.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
    //[objLanguage addSubview:objLanguage._viewLanguage];
    [self.view addSubview:objLanguage];
    
    //    HomeViewController *objHome = [self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"];
    //    [self.navigationController pushViewController:objHome animated:YES];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
