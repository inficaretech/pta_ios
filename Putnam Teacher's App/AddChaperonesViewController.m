//
//  AddChaperonesViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/3/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AddChaperonesViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "WebService.h"

@interface AddChaperonesViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate>{
    UIButton *btnTeacher,*btnParent,*btnOther;
    UITextField *tfName,*tfFieldTrip,*tfEmail,*tfPhn;
    int chaperoneIs;
   //CGPoint svos;
    UIScrollView  *sclVw;
    int fieldtripId;
    UIButton *btnDelete;
    
    
}

@end

@implementation AddChaperonesViewController
@synthesize IsEditChaperone;
@synthesize dictChaperone;
@synthesize arrfieldTrip;

- (void)viewDidLoad {
    [super viewDidLoad];
    chaperoneIs=1;
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *button =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =button;
    
    [self CreateUi];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- Backbuttonpressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark-CreateUi
-(void)CreateUi{
    
  sclVw=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    sclVw.contentSize=CGSizeMake( [[UIScreen mainScreen]bounds].size.width, 550);
    [self.view addSubview:sclVw];
    
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate=self;
    [tapper setCancelsTouchesInView:NO];
    [sclVw addGestureRecognizer:tapper];
    
    
    int k,x;
    UIImageView *imgvwBoy;
    if (IS_IPAD) {
        k=100;
         x=320;
          imgvwBoy=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, 70.0, 240, 200)];
        
    }else{
         x=220;
        k=[[UIScreen mainScreen]bounds].size.width/3;
          imgvwBoy=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, 10.0, 240, 200)];
    }

   // UIImageView *imgvwBoy=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, 20.0, 240, 200)];
    imgvwBoy.contentMode=UIViewContentModeScaleAspectFit;
    imgvwBoy.image=[UIImage imageNamed:@"boy1_icon"];
    [sclVw addSubview:imgvwBoy];
    
    
    
    tfName=[self creatertextfild:CGRectMake(20.0, x,[[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"user_name"]];
    tfName.placeholder=@"Name";
    tfName.returnKeyType=UIReturnKeyNext;
    
    tfFieldTrip=[self creatertextfild:CGRectMake(20.0, tfName.frame.origin.y+45,[[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"field_trip_icon"]];
    tfFieldTrip.placeholder=@"Field trip name";

    
    for (int k=0; k<arrfieldTrip.count; k++) {
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[arrfieldTrip objectAtIndex:k];
        if ([[dict valueForKey:@"selected"] isEqualToString:@"1"]) {
            tfFieldTrip.text=[[arrfieldTrip objectAtIndex:k]valueForKey:@"fieldtrip"];
            fieldtripId=k;
        }
        
    }
    
    UIPickerView *myPickerView = [[UIPickerView alloc]init];
    myPickerView.frame = CGRectMake(0.0,self.view.frame.size.height-150 , self.view.frame.size.width, 150);
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    


   

    tfEmail=[self creatertextfild:CGRectMake(20.0, tfFieldTrip.frame.origin.y+45, [[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"mail_icon"]];
    tfEmail.placeholder=@"Email address";
     tfEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    tfEmail.keyboardType = UIKeyboardTypeEmailAddress;
    tfEmail.returnKeyType=UIReturnKeyNext;
    
    tfPhn=[self creatertextfild:CGRectMake(20.0, tfEmail.frame.origin.y+45,[[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"phone_icon"]];
    tfPhn.placeholder=@" Phone no#";
    tfPhn.returnKeyType=UIReturnKeyDone;
    tfPhn.keyboardType=UIKeyboardTypePhonePad;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                                  target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
  tfPhn.inputAccessoryView = toolBar;
    tfFieldTrip.inputView=myPickerView;
    tfFieldTrip.inputAccessoryView=toolBar;
    
    
    if(IS_IPAD){
        btnTeacher=[self createButton:CGRectMake(60, tfPhn.frame.origin.y+60, k, 30) andView:sclVw];
        btnParent=[self createButton:CGRectMake([[UIScreen mainScreen]bounds].size.width/3+60, tfPhn.frame.origin.y+60, k, 30) andView:sclVw];
        btnOther=[self createButton:CGRectMake(2*[[UIScreen mainScreen]bounds].size.width/3+20, tfPhn.frame.origin.y+60, k, 30) andView:sclVw];

        
        
    }else{
         btnTeacher=[self createButton:CGRectMake(20, tfPhn.frame.origin.y+60, k, 30) andView:sclVw];
         btnParent=[self createButton:CGRectMake([[UIScreen mainScreen]bounds].size.width/3+20,tfPhn.frame.origin.y+60, k, 30) andView:sclVw];
         btnOther=[self createButton:CGRectMake(2*[[UIScreen mainScreen]bounds].size.width/3, tfPhn.frame.origin.y+60, k, 30) andView:sclVw];
        
    }
    
   
    [btnTeacher setTitle:@"Teacher" forState:UIControlStateNormal];
    btnTeacher.tag=1;
    [btnTeacher addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    btnTeacher.selected=YES;
    
   
    btnParent.tag=2;
    [btnParent addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [btnParent setTitle:@"Parent" forState:UIControlStateNormal];
    
   
    btnOther.tag=3;
    [btnOther addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [btnOther setTitle:@"Other" forState:UIControlStateNormal];
    
    UIButton *BtnAdd=[UIButton buttonWithType:UIButtonTypeCustom];
    BtnAdd.frame=CGRectMake(20.0,tfPhn.frame.origin.y+120,[[UIScreen mainScreen]bounds].size.width-40, 45);
    [BtnAdd addTarget:self action:@selector(Addchaperone:) forControlEvents:UIControlEventTouchUpInside];
    BtnAdd.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    
    [sclVw addSubview:BtnAdd];
    
    
    if (IsEditChaperone) {
        
        if ([[dictChaperone valueForKey:@"Is_teacher"]isEqualToString:@"1"]) {
            [tfEmail setEnabled:NO];
            
            
        }else{
        btnDelete= [UIButton buttonWithType:UIButtonTypeCustom];
        btnDelete.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width-100, 10.0, 20.0, 24.0);
        [btnDelete setImage:[UIImage imageNamed:@"delete_icon2"] forState:UIControlStateNormal];
        [btnDelete addTarget:self action:@selector(deleteChaperone:) forControlEvents:UIControlEventTouchUpInside];

        
         [self.navigationController.navigationBar addSubview:btnDelete];

        }
         self.navigationItem.title = @"Edit Details";
        BtnAdd.frame=CGRectMake(20, tfPhn.frame.origin.y+120, ([[UIScreen mainScreen]bounds].size.width/2)-30, 45);
        
        
        UIButton *BtnCancel=[UIButton buttonWithType:UIButtonTypeCustom];
        BtnCancel.frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)+10,tfPhn.frame.origin.y+120,([[UIScreen mainScreen]bounds].size.width/2)-30, 45);
        [BtnCancel addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        BtnCancel.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
         [BtnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [sclVw addSubview:BtnCancel];
        
        [BtnAdd setTitle:@"Save" forState:UIControlStateNormal];
        tfPhn.text=[dictChaperone valueForKey:@"contact"];
        
        
        NSString *substrings = [dictChaperone valueForKey:@"name"];
        NSMutableString * string = [substrings mutableCopy];
        [string replaceOccurrencesOfString:@"`"
                                withString:@" "
                                   options:0
                                     range:NSMakeRange(0, string.length)];
      //  cell.lblName.text=string;

        tfName.text= string;
         tfEmail.text=[dictChaperone valueForKey:@"email"];
         tfFieldTrip.text=[dictChaperone valueForKey:@"fieldtrip"];
        // tfSchoolName.text=[dictChaperone valueForKey:@"school"];
        if ([[dictChaperone valueForKey:@"chaperonIs"] isEqualToString:@"1"]) {
            [btnParent setSelected:NO];
            [btnTeacher setSelected:YES];
            [btnOther setSelected:NO];
            
            
        }else if ([[dictChaperone valueForKey:@"chaperonIs"] isEqualToString:@"2"]){
            [btnParent setSelected:YES];
            [btnTeacher setSelected:NO];
            [btnOther setSelected:NO];
            
            
        }else if ([[dictChaperone valueForKey:@"chaperonIs"] isEqualToString:@"3"]){
            [btnParent setSelected:NO];
            [btnTeacher setSelected:NO];
            [btnOther setSelected:YES];
            
            
        }
        
    }else{
         self.navigationItem.title = @"Add Chaperones";
        [BtnAdd setTitle:@"Add" forState:UIControlStateNormal];
        
    }
    
   /*
    UIButton *BtnAdd=[UIButton buttonWithType:UIButtonTypeCustom];
    BtnAdd.frame=CGRectMake(0.0, Vw.frame.size.height-45, Vw.frame.size.width, 45);
    BtnAdd.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    [BtnAdd setTitle:@"Add" forState:UIControlStateNormal];
    [self.view addSubview:BtnAdd];
    int x=20;
    
    UITextField *tfName=[self creatertextfild:CGRectMake(20.0, x, Vw.frame.size.width-40, 30) andView:Vw];
    tfName.placeholder=@"Name";
     tfName.returnKeyType=UIReturnKeyNext;
    
    UITextField *tfSchoolName=[self creatertextfild:CGRectMake(20.0, x+40, Vw.frame.size.width-40, 30) andView:Vw];
    tfSchoolName.placeholder=@"School name";
     tfSchoolName.returnKeyType=UIReturnKeyNext;
    
    UITextField *tfEmail=[self creatertextfild:CGRectMake(20.0, x+40*2, Vw.frame.size.width-40, 30) andView:Vw];
    tfEmail.placeholder=@"Email address";
     tfEmail.returnKeyType=UIReturnKeyNext;
    
    UITextField *tfPhn=[self creatertextfild:CGRectMake(20.0, x+40*3, Vw.frame.size.width-40, 30) andView:Vw];
    tfPhn.placeholder=@" Phone no#";
     tfPhn.returnKeyType=UIReturnKeyDone;
    
    int k;
    if (IS_IPAD) {
         k=100;
        
    }else{
     k=Vw.frame.size.width/3;
    }
    btnTeacher=[self createButton:CGRectMake(20, x+170, k, 30) andView:Vw];
    [btnTeacher setTitle:@"Teacher" forState:UIControlStateNormal];
    btnTeacher.tag=1;
    [btnTeacher addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    btnTeacher.selected=YES;
    
    btnParent=[self createButton:CGRectMake(Vw.frame.size.width/3+30, x+170, k, 30) andView:Vw];
    btnParent.tag=2;
      [btnParent addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [btnParent setTitle:@"Parent" forState:UIControlStateNormal];
    
    btnOther=[self createButton:CGRectMake(2*Vw.frame.size.width/3+40, x+170, k, 30) andView:Vw];
    btnOther.tag=3;
    [btnOther addTarget:self action:@selector(IsSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [btnOther setTitle:@"Other" forState:UIControlStateNormal];*/
    
    for (int k=0; k<arrfieldTrip.count; k++) {
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[arrfieldTrip objectAtIndex:k];
        if ([[dict valueForKey:@"selected"] isEqualToString:@"1"]) {
            tfFieldTrip.text=[[arrfieldTrip objectAtIndex:k]valueForKey:@"fieldtrip"];
            fieldtripId=k;
        }
        
    }

 }
#pragma mark-DoneButton
-(void)done:(id)sender{
    [tfFieldTrip resignFirstResponder];
    [tfPhn resignFirstResponder];
    
    if (tfFieldTrip.text.length == 0) {
[tfFieldTrip setText:[[arrfieldTrip objectAtIndex:0] valueForKey:@"fieldtrip"]];
        fieldtripId=0;
}
  
    
}
#pragma mark - viewWillDissappear
-(void)viewWillDisappear:(BOOL)animated{
    
    [btnDelete removeFromSuperview];
    
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [arrfieldTrip count];
}
#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [tfFieldTrip setText:[[arrfieldTrip objectAtIndex:row] valueForKey:@"fieldtrip"]];
    fieldtripId=(int)row;
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[arrfieldTrip objectAtIndex:row] valueForKey:@"fieldtrip"];
}



-(void)handleSingleTap:(id)sender{
    
    [self.view endEditing:YES];

}

#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (!(IS_IPAD)) {
        if (textField!=tfName && textField!=tfFieldTrip ) {
//            svos = sclVw.contentOffset;
//            CGPoint pt;
//            CGRect rc = [textField bounds];
//            rc = [textField convertRect:rc toView:sclVw];
//            pt = rc.origin;
//            pt.x = 0;
//            pt.y = pt.y+100;
            [sclVw setContentOffset:CGPointMake(0, 100) animated:YES];
            
        }
    }

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (!(IS_IPAD)) {
         if (textField!=tfName && textField!=tfFieldTrip ) {
//             svos = sclVw.contentOffset;
//             CGPoint pt;
//             CGRect rc = [textField bounds];
//             rc = [textField convertRect:rc toView:sclVw];
//             pt = rc.origin;
//             pt.x = 0;
//             pt.y = pt.y-100;
        //[sclVw setContentOffset:pt animated:YES];
             [sclVw setContentOffset:CGPointMake(0, -64) animated:YES];
            }
    
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  
    if (textField==tfName) {
        [tfEmail becomeFirstResponder];
        
//    }else if (textField==tfSchoolName){
//        [tfEmail becomeFirstResponder];
        
    }else if (textField==tfEmail){
        
        [tfPhn becomeFirstResponder];
    }else if (textField==tfPhn){
        [textField resignFirstResponder];
        
    }
    
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField==tfPhn) {
        int length = [self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-",num];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        
        else if(length == 7)
        {
            NSString *num = [self formatNumber:textField.text];
            
            textField.text = [NSString stringWithFormat:@"%@-%@",[num  substringToIndex:3],[num substringFromIndex:3]];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
        
        else if(length == 8)
        {
            
            NSString *num = [self formatNumber:textField.text];
            
            
            NSString *subString = [num substringWithRange: NSMakeRange(3,3)];
            
            
            
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num  substringToIndex:3],subString,[num substringFromIndex:6]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num substringToIndex:3],subString,[num substringFromIndex:6]];
        }
    }
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
     if (textField==tfPhn ){
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return ([string isEqualToString:filtered] && newLength <= 15) || returnKey ;
     }else if (textField==tfName){
          return newLength <= 30;
         
     }
    return newLength <= 80;
}

#pragma mark-createButton
-(UIButton *)createButton:(CGRect)frame andView:(UIView *)vw{
      UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=frame;
   // btn.backgroundColor=[UIColor blueColor];
    
    [btn setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateSelected];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0,5, 0, 0)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0,12, 0, 0)];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [vw addSubview:btn];
    
    
    return btn;
    
}

#pragma mark-createTextfield
-(UITextField *)creatertextfild:(CGRect )frame andView:(UIView *)vw andImage:(UIImage *)Img{
    
    UITextField *txtfld=[[UITextField alloc]initWithFrame:frame];
    txtfld.delegate=self;
    txtfld.spellCheckingType = UITextSpellCheckingTypeNo;
    txtfld.autocorrectionType = UITextAutocorrectionTypeNo;
    txtfld.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    UIImageView *imgVw = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,40, txtfld.frame.size.height-5)];
    txtfld.leftView = imgVw;
    imgVw.image=Img;
    imgVw.contentMode=UIViewContentModeScaleAspectFit;
    txtfld.leftViewMode = UITextFieldViewModeAlways;
    
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, txtfld.frame.size.height - borderWidth, txtfld.frame.size.width, txtfld.frame.size.height);
    border.borderWidth = borderWidth;
    [txtfld.layer addSublayer:border];
    txtfld.layer.masksToBounds = YES;
    
    [vw addSubview:txtfld];
        return txtfld;
    
}
#pragma mark-IsSelectedButton
-(void)IsSelectedButton:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    NSLog(@"%ld",(long)btn.tag);
    chaperoneIs=(int)btn.tag;
    
    btn.selected=YES;
    if (btn.tag==1) {
        btnOther.selected=NO;
        btnParent.selected=NO;
        
    }else if (btn.tag==2){
        btnOther.selected=NO;
        btnTeacher.selected=NO;
        
        
    }else if (btn.tag==3){
        
        btnParent.selected=NO;
        btnTeacher.selected=NO;
        
    }
    
    
}
#pragma mark-Addchaperone
-(void)Addchaperone:(id)sender{
     NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSString *strEditOrAdd;
    if (IsEditChaperone) {
         strEditOrAdd=@"edit";
        [dict setValue:[dictChaperone valueForKey:@"id"] forKey:@"id"];
        
    }else{
    strEditOrAdd=@"add";
    }
   
    BOOL isEmailValid=[self NSStringIsValidEmail:tfEmail.text];
    
    
        if (tfName.text.length==0) {
         [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for name"];
        return;
    }else if (tfFieldTrip.text.length==0) {
         [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for field trip name"];
        return;
    }else if (tfEmail.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for email"];
        return;
        
    }else if (!isEmailValid){
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for email"];
        return;
      }
      else  if (tfPhn.text.length==0) {
         [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for contact number"];
        return;
    }else if (tfPhn.text.length<10){
         [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid contact number"];
        return;
    }else{
        
        
        [dict setValue:tfEmail.text forKey:@"email"];
        [dict setValue:tfName.text forKey:@"name"];
       // [dict setValue:tfSchoolName.text forKey:@"school"];
        [dict setValue:tfPhn.text forKey:@"phoneno"];
        [dict setValue:[NSNumber numberWithInt:chaperoneIs] forKey:@"chaperon"];
         [dict setValue:[[arrfieldTrip objectAtIndex:fieldtripId]valueForKey:@"id"] forKey:@"trip_id"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
      [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
              if (isinternet) {

            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/%@_chaperones",kBaseUrl,strEditOrAdd] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                        
                        [self.navigationController popViewControllerAnimated:YES];
                    }                    
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];

            
        }

        
        }
}
#pragma mark- deleteChaperone
-(void)deleteChaperone:(id)sender
{
    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Delete" message:@"Are you sure you want to delete " delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"OK", nil];
    [Alert show];
    
  }
#pragma mark-AlertViewDelegte
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
     if (buttonIndex==1) {
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
    [dict setValue:[dictChaperone  valueForKey:@"id"] forKey:@"id"];
BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
if (isinternet) {
    
    
    [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/remove_chaperones",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
        
        if (!error) {
             NSLog(@"returArray %@",returArray);
            if ([returArray valueForKey:@"success"]) {
                [self.navigationController popViewControllerAnimated:YES];
                 [[CommonMethods sharedInstance] AlertAction:@"Deleted successfully"];
            }
           
            
        }
        else
        {
           [[CommonMethods sharedInstance] AlertAction:@"Server error"];
            
        }
    }];
    
}else{
     [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
}
     }
}


#pragma mark- EmailValidation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark- PhoneMasking
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
        
    }
    
    
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
    
    
}

@end
