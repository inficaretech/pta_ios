//
//  ChaperoneListTableViewCell.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/3/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "ChaperoneListTableViewCell.h"

@implementation ChaperoneListTableViewCell
@synthesize lblName;
@synthesize lblGmail;
@synthesize lblGroupName;
@synthesize lblPhonenumber;
@synthesize lblColour;
@synthesize btnEdit,btnDelete;
@synthesize btn1,btn2,btn3,btn4,btn5,btn6;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
     [self createCirclebutton:btn1];
     [self createCirclebutton:btn2];
     [self createCirclebutton:btn3];
     [self createCirclebutton:btn4];
     [self createCirclebutton:btn5];
     [self createCirclebutton:btn6];
   
    // Initialization code
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}
-(void)createCirclebutton:(UIButton *)btn
{
    btn.layer.cornerRadius=btn.frame.size.height/2;
    btn.layer.borderColor=[UIColor darkGrayColor].CGColor;
    btn.layer.borderWidth=1;
    
    
}

@end
