//
//  LanguageSelection.h
//  LanguageSelection
//
//  Created by puran on 12/4/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "selectedLanguage.h"
//#import "ScratchView.h"
#import "AppDelegate.h"
@protocol languageDelegate <NSObject>

@required
-(void)onRecievingLanguage;

@optional
-(void)onRecievingQuestionType:(NSString *)difficultylevel;
@end

@interface LanguageSelection : UIView<UITableViewDataSource , UITableViewDelegate>{
    
    NSMutableArray *arrData;
    NSMutableArray *arrSelectedLanguageClass;
    selectedLanguage *selectedLanguageClass;
    id<languageDelegate>__weak delegate;
    __weak IBOutlet UIView *_viewLanguage;
    AppDelegate *_appdelegate;
    
}
@property(nonatomic, weak)id delegate;

@property (weak, nonatomic) IBOutlet UILabel *_lblSelectLanguage;
@property (weak, nonatomic) IBOutlet UITableView *_tblView;
@property (weak, nonatomic) IBOutlet UIView *_viewLanguage;

@property (weak, nonatomic)NSIndexPath *lastSelectedIndex;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;
-(void)callWithArrayItems:(NSArray *)arrLanguageData;


@end
