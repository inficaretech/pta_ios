//
//  CustomCellWallOfFameTableViewCell.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/30/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "CustomCellWallOfFameTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomCellWallOfFameTableViewCell
@synthesize lblLine;
@synthesize lblDetail;
@synthesize lblAlphabet;
- (void)awakeFromNib {
    [super awakeFromNib];
    lblAlphabet = [[UILabel alloc]init];
    lblAlphabet.frame = CGRectMake(20.0, 7.0,55.0 , 55.0);
    lblAlphabet.adjustsFontSizeToFitWidth=YES;
  
    [self addSubview:lblAlphabet];
    lblAlphabet.layer.cornerRadius = lblAlphabet.frame.size.width/2 ;
    lblAlphabet.clipsToBounds = YES;
    
    lblDetail = [[UILabel alloc]init];
    lblDetail.frame = CGRectMake(90.0, 0.0, self.frame.size.width-100, 72.0);
    [self addSubview:lblDetail];
    
    lblLine = [[UILabel alloc]init];
    lblLine.frame = CGRectMake(0.0,72.0,[[UIScreen mainScreen] bounds].size.width, 1);
    [self addSubview:lblLine];
    lblLine.backgroundColor = [UIColor grayColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
