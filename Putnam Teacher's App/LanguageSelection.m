//
//  LanguageSelection.m
//  LanguageSelection
//
//  Created by puran on 12/4/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#import "LanguageSelection.h"
#import "CustomCell_Language.h"
#import "selectedLanguage.h"
#import "Constants.h"
#import "Reachability.h"


@implementation LanguageSelection
@synthesize _lblSelectLanguage;
@synthesize _tblView;
@synthesize lastSelectedIndex;
@synthesize delegate;



-(void)callWithArrayItems:(NSArray *)arrLanguageData{
    
    
//    if (isDataViewController) {
//      arrData = [[NSMutableArray alloc] initWithArray:arrLanguageData];
//        _lblSelectLanguage.text = @"Click on the difficulty level";
//    }
//    else
//    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strLanguageName = [defaults objectForKey:@"LanguageName"];
    NSString *strLanguageId = [defaults objectForKey:@"LanguageId"];
    //NSLog(@"%@",strLanguageName);
    //NSLog(@"%@",strLanguageId);
    [defaults synchronize];
    
 //   _viewLanguage.backgroundColor = kViewBackgroundColor;
    
    selectedLanguageClass = [[selectedLanguage alloc]init];
    selectedLanguageClass.languageName = @"";
    selectedLanguageClass.isChecked = NO;
    arrData = [[NSMutableArray alloc] initWithArray:arrLanguageData];
    arrSelectedLanguageClass = [[NSMutableArray alloc] init];
    for(int i = 0 ;i< arrData.count ; i++){
        
        selectedLanguageClass = [[selectedLanguage alloc]init];
        selectedLanguageClass.languageName = [NSString stringWithFormat:@"%@",[arrData objectAtIndex:i]];
        selectedLanguageClass.isChecked = NO;
        if([selectedLanguageClass.languageName isEqualToString:strLanguageName]){
            
            selectedLanguageClass.isChecked = YES;
        }
        [arrSelectedLanguageClass addObject: selectedLanguageClass];
    }
    [_lblSelectLanguage  setFont:FontAleo(22.0)];
  //  }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        if (finished) {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self removeFromSuperview];
        }
    }];
    
    
    
   // NSLog(@"previous selected INdex value ===%d",previousSelectedIndex);
  //  selectedMenuTag = [NSString stringWithFormat:@"%d",previousSelectedIndex];
    
//     indexing = [NSString stringWithFormat:@"%@",selectedMenuTag];
//    NSLog(@"selected menu tag ====%@",selectedMenuTag);
}

#pragma ------------------------------------
#pragma mark - UITableView Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCell";
    
    CustomCell_Language *cell = (CustomCell_Language *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCell_Language" owner:self options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]]){
                cell =  (CustomCell_Language *) currentObject;
                
                break;
            }
        }
    }
    [cell._lblTitleText setText:[NSString stringWithFormat:@"%@",[[arrData valueForKey:@"language" ] objectAtIndex:indexPath.row]]];
    selectedLanguageClass = [arrSelectedLanguageClass objectAtIndex:indexPath.row];
    [cell._lblTitleText  setFont:FontAleo(18.0)];
    [cell._imgCell setHidden:NO];
         UIImage *img = [UIImage imageNamed:@"check"];
        
        cell._imgCell.image = img;
        cell._imgCell.image = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell._imgCell.tintColor = kOragneColor;
     return cell ;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *viewHeader = [[UIView alloc] init];
    viewHeader.backgroundColor = kOragneColor;
    return  viewHeader;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49 ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (isDataViewController)
//    {
//         NSString *strType = [arrData objectAtIndex:indexPath.row];
//        [self addScratchView:strType];
////        if (self.delegate && [self.delegate respondsToSelector:@selector(onRecievingQuestionType:)])
////        {
////            [[self delegate] onRecievingQuestionType:strType];
////        }
//    }
//    else
//    {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSString *strLanguageName = [defaults objectForKey:@"LanguageName"];
    NSString *strLanguageId = [defaults objectForKey:@"LanguageId"];
    //NSLog(@"%@",strLanguageName);
    //NSLog(@"%@",strLanguageId);
    
    
  //  NSLog(@"previous ===%d",previousSelectedIndex);
   // selectedMenuTag = [NSString stringWithFormat:@"%d",previousSelectedIndex];
    
    selectedLanguageClass = [arrSelectedLanguageClass objectAtIndex:indexPath.row];
    NSMutableArray *arrModelData = [[NSMutableArray alloc] init];
    for(int i = 0 ; i< arrSelectedLanguageClass.count ; i++){
        
        
        if(i == indexPath.row){
            
            selectedLanguageClass.languageName = [NSString stringWithFormat:@"%@",[[arrData valueForKey:@"language" ] objectAtIndex:i]];
            selectedLanguageClass.isChecked= YES;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
    //        DataBaseInteraction *dbInteraction = [[DataBaseInteraction alloc] init];
      //      NSMutableArray *arrLanguages = [dbInteraction fetch_Language:@"Languages"];
      //      DataBaseModel *info = [arrLanguages objectAtIndex:i];
      //      NSString *strId = [NSString stringWithFormat:@"%d",info.intLanguageID];
//            currentLanguageID = strId;
//            int totalclicks = info.intTotalClicks;
//            if([strId isEqualToString:strLanguageId]){
//                
//                [UIView animateWithDuration:.35 animations:^{
//                    
//                    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
//                    self.alpha = 0.0;
//                    
//                } completion:^(BOOL finished) {
//                    if (finished) {
//                        [[NSNotificationCenter defaultCenter] removeObserver:self];
//                        [self removeFromSuperview];
//                        
////                        if (self.delegate && [self.delegate respondsToSelector:@selector(onRecievingLanguage)])
////                        {
////                            [[self delegate] onRecievingLanguage];
////                        }
//                    }
//                }];
//                return;
//            }
//            BOOL ischeckInternet = [self checkInternetConnection];
//            if (ischeckInternet) {
//                [defaults setObject:selectedLanguageClass.languageName forKey:@"LanguageName"];
//                [defaults setObject:strId forKey:@"LanguageId"];
//                currentLanguageID = strId;
//                [defaults setInteger:totalclicks forKey:@"TotalClicks"];
//                [defaults synchronize];
//                
//                
//                
//            }
//            
//            
            
            
            
            
        }else{
            
            selectedLanguageClass.languageName = [NSString stringWithFormat:@"%@",[[arrData valueForKey:@"language" ] objectAtIndex:i]];
            selectedLanguageClass.isChecked= NO;
            
            //[_tblView cellForRowAtIndexPath:indexPath].accessoryType=UITableViewCellAccessoryCheckmark;
        }
        [arrModelData addObject:selectedLanguageClass];
        selectedLanguageClass = [[selectedLanguage alloc] init];
        
    }
   
    arrSelectedLanguageClass = [[NSMutableArray alloc] initWithArray:arrModelData];
    [_tblView reloadData];
    
    
    
    [UIView animateWithDuration:.35 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        if (finished) {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self removeFromSuperview];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(onRecievingLanguage)])
            {
                [[self delegate] onRecievingLanguage];
            }
        }
    }];
    
}
//}



-(BOOL)checkInternetConnection
{
    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))/*14-10-14*/
    {
        internet = NO;
        
    } else
    {
        internet = YES;
    }
    return internet;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // [[_tblView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];

}


//-(void)addScratchView:(NSString *)difficultylevel{
//    
//    [self removeFromSuperview];
//    
// //   isDataViewController=YES;
//    
// //   ScratchView   *objScratchView;
//    
//    NSMutableArray *arrGalleryTrivia = [[NSMutableArray alloc]init];
//      NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//     NSString *strGalleryId = [defaults objectForKey:@"GalleryId"];
//    DatabaseFetchValues *fetchValues = [[DatabaseFetchValues alloc] init];
//    arrGalleryTrivia = [fetchValues fetchAllDataWithTableName:@"GalleryTrivia" withTableNumber:121 andWithColumID:strGalleryId];
//    
//    if ([difficultylevel isEqualToString:@"High"]) {
//        difficultylevel = @"3";
//    }
//    else if ([difficultylevel isEqualToString:@"Medium"]) {
//        difficultylevel = @"2";
//    }
//    else  if ([difficultylevel isEqualToString:@"Easy"]) {
//        difficultylevel = @"1";
//    }
//    NSMutableDictionary *result = [[NSMutableDictionary alloc]init];
//    
//    for (NSDictionary *dict in arrGalleryTrivia)
//    {
//        if ([[dict valueForKey:@"Type"] isEqualToString:difficultylevel]) {
//            result = [dict mutableCopy];
//            NSLog(@"print %@", result);
//        }
//    }
//    //    for (NSDictionary *dict in arrGalleryTrivia)
//    //    {
//    //        [result addEntriesFromDictionary:dict];
//    //    }
//    
//    
//    //NSLog(@"button view answer get called");
//    //NSLog(@"%ld",(long)tag);
//    
//    
//    
//    if (IS_IPAD) {
//        objScratchView = [[[NSBundle mainBundle] loadNibNamed:@"GalleryTrivia"  owner:self    options:nil] lastObject];
//    }
//    else{
//        objScratchView = [[[NSBundle mainBundle] loadNibNamed:@"GalleryTrivia"  owner:self    options:nil] lastObject];
//    }
//    
//    
//    
//    //    objScratchView.delegate = self;
//     _appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    CGRect frame;
//    frame.origin.x  = 0;
//    frame.origin.y  = 0;
//    frame.size.width  = _appdelegate.window.frame.size.width;
//    frame.size.height  = _appdelegate.window.frame.size.height+64;
//    
//    objScratchView.frame = frame;
//    
//    [objScratchView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
//    objScratchView.transform = CGAffineTransformMakeScale(1.0, 1.0);
//    
//    objScratchView.alpha = 0;
//    // objScratchView.delegate = self;
//    // objScratchView.callViewOnce = YES;
//    [objScratchView callScratchingClass:result];
//    // objScratchView._viewScratching.backgroundColor = kViewBackgroundColor;
//    [UIView animateWithDuration:.35 animations:^{
//        objScratchView.alpha = 1;
//        objScratchView.transform = CGAffineTransformMakeScale(1, 1);
//    }];
//    
//    [_appdelegate.window addSubview:objScratchView];
//    
//    
//    
//    
//}

@end
