//
//  AddStudentViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/6/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AddStudentViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"
#import "TTTAttributedLabel.h"

@interface AddStudentViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,TTTAttributedLabelDelegate>{
    UITextField *tfFirstName,*tfLastName,*tfClass,*tf1stContact,*tf2ndContact,*tfSpecialNeeds,*tfSpendingMoney,*tfAllergies,*tfFieldtrip;
    UIScrollView *sclVw;
    BOOL IsPhotograps,IsSpecialNeeds,IsMedicines,IsSpendingMoney,IsAllergies;
   
    int fieldtripId;
    CGPoint svos;
    
    
}

@end

@implementation AddStudentViewController
@synthesize IsEditStudent;
@synthesize dictStudent;
@synthesize arrGrades;
- (void)viewDidLoad {
    [super viewDidLoad];
    //arrGrades=[[NSMutableArray alloc]init];
    
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    
    [self CreateUi];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- BackbuttonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark-CreateUi
-(void)CreateUi{
    
    
    sclVw=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    sclVw.contentSize=CGSizeMake( [[UIScreen mainScreen]bounds].size.width, 1100);
    [self.view addSubview:sclVw];
    
    int x=40.0;
    tfFirstName=[self creatertextfild:CGRectMake(20.0, x, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tfFirstName.placeholder=@"FirstName";
    tfLastName=[self creatertextfild:CGRectMake(20.0, x+70.0, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tfLastName.placeholder=@"LastName";
    tfClass=[self creatertextfild:CGRectMake(20.0, x+140, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tfClass.placeholder=@"Class";
    tfFieldtrip=[self creatertextfild:CGRectMake(20.0, x+210, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tfFieldtrip.placeholder=@"Field trip name";
    
    for (int k=0; k<arrGrades.count; k++) {
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[arrGrades objectAtIndex:k];
        if ([[dict valueForKey:@"selected"] isEqualToString:@"1"]) {
            tfFieldtrip.text=[[arrGrades objectAtIndex:k]valueForKey:@"fieldtrip"];
            fieldtripId=k;
        }
        
    }
    

  
   
    tf1stContact=[self creatertextfild:CGRectMake(20.0, x+280, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tf1stContact.placeholder=@"ICE Contact #1";
    tf2ndContact=[self creatertextfild:CGRectMake(20.0, x+350, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    tf2ndContact.placeholder=@"ICE Contact #2";
    tf2ndContact.returnKeyType=UIReturnKeyDone;
    tf2ndContact.keyboardType=UIKeyboardTypePhonePad;
    tf1stContact.keyboardType=UIKeyboardTypePhonePad;
    
    x=370;
    
    [self CreatelabelOfchecklist:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-70, x+100, 140, 30) andTitle:@"Student Checklist"andView:sclVw];
    
    UIView *VwofCheckList=[[UIView alloc]initWithFrame:CGRectMake(0.0, x+140, [[UIScreen mainScreen]bounds].size.width, 320)];
    VwofCheckList.backgroundColor=[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:0.2];
    [sclVw addSubview:VwofCheckList];
    
    [self CreateImageView:[UIImage imageNamed:@"camera_grey"] andFrame:CGRectMake(20.0, 10.0, 40, 40) andView:VwofCheckList];
    [self CreateImageView:[UIImage imageNamed:@"wheelchair_grey"] andFrame:CGRectMake(20.0, 70.0, 40, 40) andView:VwofCheckList];
    [self CreateImageView:[UIImage imageNamed:@"medicine_grey"] andFrame:CGRectMake(20.0, 130.0, 40, 40) andView:VwofCheckList];
    [self CreateImageView:[UIImage imageNamed:@"dollar_grey"] andFrame:CGRectMake(20.0, 190.0, 40, 40) andView:VwofCheckList];
    [self CreateImageView:[UIImage imageNamed:@"allergy_grey"] andFrame:CGRectMake(20.0, 250.0, 40, 40) andView:VwofCheckList];
    
    
    //    for (int k=0; k<5; k++) {
    //        UISwitch *switchbtn=[[UISwitch alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-80, 10+60*k, 60, 40)];
    //        [switchbtn setOn:NO animated:YES];
    //        [switchbtn addTarget:self action:@selector(SelectRequirement:) forControlEvents:UIControlEventValueChanged];
    //        switchbtn.tag=10+k;
    //        [VwofCheckList addSubview:switchbtn];
    //
    //
    //    }
    //
    UISwitch *btnPhotograph=[self createSwitch:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 10, 60, 40) andView:VwofCheckList andTag:10];
    
    
    UISwitch *btnSpecialneeds=[self createSwitch:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 10+60*1, 60, 40) andView:VwofCheckList andTag:11];
    UISwitch *btnmedicines=[self createSwitch:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 10+60*2, 60, 40) andView:VwofCheckList andTag:12];
    UISwitch *btnspendingMoney=[self createSwitch:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 10+60*3, 60, 40) andView:VwofCheckList andTag:13];
    UISwitch *btnAllergies=[self createSwitch:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 10+60*4, 60, 40) andView:VwofCheckList andTag:14];
    
    [self CreatelabelOfchecklist:CGRectMake(80.0, 10.0,[[UIScreen mainScreen]bounds].size.width-160, 30) andTitle:@"Photograph Allowed" andView:VwofCheckList];
    [self CreatelabelOfchecklist:CGRectMake(80.0, 60.0,[[UIScreen mainScreen]bounds].size.width-160, 30) andTitle:@"Special Needs" andView:VwofCheckList];
    [self CreatelabelOfchecklist:CGRectMake(80.0, 130.0,[[UIScreen mainScreen]bounds].size.width-160, 30) andTitle:@"Medicines" andView:VwofCheckList];
    [self CreatelabelOfchecklist:CGRectMake(80.0, 180.0,[[UIScreen mainScreen]bounds].size.width-160, 30) andTitle:@"Spending Money" andView:VwofCheckList];
    [self CreatelabelOfchecklist:CGRectMake(80.0, 240.0,[[UIScreen mainScreen]bounds].size.width-160, 30) andTitle:@"Allergies" andView:VwofCheckList];
    x=470;
    
    if (IS_IPAD) {
        tfSpecialNeeds=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-100, 90.0,200, 30) andView:VwofCheckList];
        tfSpendingMoney=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-100, 210.0,200, 30) andView:VwofCheckList];
        tfAllergies=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-100, 270.0,200, 30) andView:VwofCheckList];
        
    }else{
        tfSpecialNeeds=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-80, 90.0,160, 30) andView:VwofCheckList];
        tfSpendingMoney=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-80, 210.0,160, 30) andView:VwofCheckList];
        tfAllergies=[self creatertextfild:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-80, 270.0,160, 30) andView:VwofCheckList];
        
    }
    
    tfAllergies.autocapitalizationType = UITextAutocapitalizationTypeNone;
    tfSpendingMoney.hidden=YES;
    tfSpecialNeeds.hidden=YES;
    tfAllergies.hidden=YES;
    tfSpendingMoney.keyboardType=UIKeyboardTypeNumberPad;
    
    UILabel *lbldollar=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 25, 20)];
    lbldollar.text=@"$";
    lbldollar.textAlignment=NSTextAlignmentRight;
    tfSpendingMoney.leftView = lbldollar;
    tfSpendingMoney.leftViewMode = UITextFieldViewModeAlways;
    
    
    if (IsEditStudent) {
        self.navigationItem.title = @"Edit Detail";
        
        tfFirstName.text=[dictStudent valueForKey:@"f_name"];
        tfLastName.text=[dictStudent valueForKey:@"l_name"];
        tf1stContact.text=[dictStudent valueForKey:@"contact1"];
        tf2ndContact.text=[dictStudent valueForKey:@"contact2"];
        tfClass.text=[dictStudent valueForKey:@"class"];
        tfFieldtrip.text=[dictStudent valueForKey:@"fieldtrip"];
        
        //  NSString *str=[[dictStudent valueForKey:@"check_list"] valueForKey:@"allergies"];
        if ([[[dictStudent valueForKey:@"check_list"] valueForKey:@"allergies"] isEqualToString:@"1"]) {
            [btnAllergies setOn:YES];
            tfAllergies.hidden=NO;
            IsAllergies=YES;
            tfAllergies.text=[[dictStudent valueForKey:@"check_list"] valueForKey:@"allergy_details"];
            
        }
        
        if ([[[dictStudent valueForKey:@"check_list"] valueForKey:@"medicine"] isEqualToString:@"1"]) {
            [btnmedicines setOn:YES];
            IsMedicines=YES;
            
        }
        
        if ([[[dictStudent valueForKey:@"check_list"] valueForKey:@"photography_allowed"] isEqualToString:@"1"]) {
            [btnPhotograph setOn:YES];
            IsPhotograps=YES;
        }
        
        if ([[[dictStudent valueForKey:@"check_list"] valueForKey:@"special_need"] isEqualToString:@"1"]) {
            [btnSpecialneeds setOn:YES];
            tfSpecialNeeds.hidden=NO;
            IsSpecialNeeds=YES;
            tfSpecialNeeds.text=[[dictStudent valueForKey:@"check_list"] valueForKey:@"special_need_type"];
        }
        
        if ([[[dictStudent valueForKey:@"check_list"] valueForKey:@"spending_money"] isEqualToString:@"1"]) {
            [btnspendingMoney setOn:YES];
            tfSpendingMoney.hidden=NO;
            IsSpendingMoney=YES;
            tfSpendingMoney.text=[[dictStudent valueForKey:@"check_list"] valueForKey:@"spending_money_amt"];
        }
        
        
        UIButton *btnSave=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnSave setImage:[UIImage imageNamed:@"save_button_red"] forState:UIControlStateNormal];
        [btnSave setImage:[UIImage imageNamed:@"save_button"] forState:UIControlStateSelected];
        [btnSave addTarget:self action:@selector(AddStudent:) forControlEvents:UIControlEventTouchUpInside];
        btnSave.frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-40, x+400, 80, 45);
        [sclVw addSubview:btnSave];
        UILabel *lblSave=[self CreateLabel:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-40, x+440, 80, 30) andTitle:@"Save"];
        lblSave.textColor=[UIColor darkGrayColor];
        lblSave.textAlignment=NSTextAlignmentCenter;
        lblSave.font=[UIFont fontWithName:@"Roboto-Regular" size:20];
        
    }else{
        
        self.navigationItem.title = @"Add Student";
        
        [btnPhotograph setOn:YES animated:YES];
        IsPhotograps=YES;
        
        tfClass.text=[[NSUserDefaults standardUserDefaults] stringForKey:@"Class"];
        UIButton *btnAdd=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnAdd setTitle:@"Add" forState:UIControlStateNormal];
        btnAdd.frame=CGRectMake(20.0, x+380, [[UIScreen mainScreen]bounds].size.width-40, 45);
        [btnAdd addTarget:self action:@selector(AddStudent:) forControlEvents:UIControlEventTouchUpInside];
        btnAdd.backgroundColor=[UIColor redColor];
        [sclVw addSubview:btnAdd];
        
        UILabel *lblLine=[self CreateLabel:CGRectMake(20.0, x+460, [[UIScreen mainScreen]bounds].size.width-40, 2) andTitle:@""];
        lblLine.backgroundColor=[UIColor lightGrayColor];
        
        UILabel *lblOr=[[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-20, x+450, 40, 20)];
        lblOr.text=@"OR";
        lblOr.textAlignment=NSTextAlignmentCenter;
        lblOr.backgroundColor=[UIColor whiteColor];
        [sclVw addSubview:lblOr];
        
        UILabel *lblUploadCSV=[self CreateLabel:CGRectMake(20.0, x+500, 250, 30.0) andTitle:@"Upload CSV file"];
        lblUploadCSV.textColor=[UIColor darkGrayColor];
        
        
        UIImageView  *ImgVw=[self CreateImageView:[UIImage imageNamed:@"upload_icon"] andFrame:CGRectMake(150.0, x+500, 30, 30.0) andView:sclVw];
        ImgVw.userInteractionEnabled=YES;
        UITapGestureRecognizer *singleTapUploadCSVFile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(UploadCSVFile:)];
        singleTapUploadCSVFile.numberOfTapsRequired=1;
        [ImgVw addGestureRecognizer:singleTapUploadCSVFile];
        
        UITextField *TfUploadFiles=[[UITextField alloc]initWithFrame:CGRectMake(20.0, x+550,([[UIScreen mainScreen]bounds].size.width/2)+20 , 45.0)];
        TfUploadFiles.layer.borderWidth=1;
        TfUploadFiles.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [sclVw addSubview:TfUploadFiles];
        
        UIButton *btnBrowse=[UIButton buttonWithType:UIButtonTypeCustom];
        btnBrowse.frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)+40, x+550,100, 45);
        btnBrowse.backgroundColor=[UIColor redColor];
        [btnBrowse setTitle:@"Browse" forState:UIControlStateNormal];
        [sclVw addSubview:btnBrowse];
        
        
        
        NSURL *url =[NSURL URLWithString:@"download"];
        
        NSString *myString = @"Need template, download or send via e-mail!";
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:myString];
        
        //Fing range of the string you want to change colour
        //If you need to change colour in more that one place just repeat it
        
        
        TTTAttributedLabel *lblAttributed= [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(20.0, x+600, [[UIScreen mainScreen]bounds].size.width-60, 35)];
        [lblAttributed setFont:[UIFont fontWithName:@"Roboto-Regular" size:18]];
        [lblAttributed setTextColor:[UIColor darkGrayColor]];
        lblAttributed.numberOfLines=0;
        lblAttributed.delegate = self;
        
        //lblAttributed.enabledTextCheckingTypes=NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber;
        [sclVw addSubview:lblAttributed];
        
        NSRange range = [myString rangeOfString:@"download"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        [lblAttributed addLinkToURL:url withRange:range];
        
        url = [NSURL URLWithString:@"send via e-mail!"];
        range=[myString rangeOfString:@"send via e-mail!"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        //range = [myString rangeOfString:@"send via e-mail!"];
        [lblAttributed addLinkToURL:url withRange:range];
        lblAttributed.attributedText=attString;
        
        
        
        //        UILabel *lblNeedTemplate=[self CreateLabel:CGRectMake(20.0, x+600, [[UIScreen mainScreen]bounds].size.width-60, 35) andTitle:@""];
        //        lblNeedTemplate.userInteractionEnabled=YES;
        //        lblNeedTemplate.textColor=[UIColor darkGrayColor];
        //        lblNeedTemplate.adjustsFontSizeToFitWidth=YES;
        
        //Create mutable string from original one
        
        // lblNeedTemplate.attributedText=attString;
        
        //        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(downloadtemplate:)];
        //        singleTap.numberOfTapsRequired=1;
        //        [lblNeedTemplate addGestureRecognizer:singleTap];
        
    }
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate=self;
    [tapper setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapper];
    
    
    UIPickerView *myPickerView = [[UIPickerView alloc]init];
    myPickerView.frame = CGRectMake(0.0,self.view.frame.size.height-150 , self.view.frame.size.width, 150);
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    
   
    
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                                  target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    
    
    if (!IS_IPAD) {
        tf1stContact.inputAccessoryView = toolBar;
        tf2ndContact.inputAccessoryView = toolBar;
        tfSpendingMoney.inputAccessoryView = toolBar;
    }
    
     tfFieldtrip.inputView = myPickerView;
    tfFieldtrip.inputAccessoryView = toolBar;
   // [self getFieldTripfromApi];
    
    
}
#pragma mark- Linkofdownload
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    // NSLog(@"u",url);
    NSURL *url1=[NSURL URLWithString:@"download"];
    if ([url isEqual:url1]) {
        NSString *str=[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Students/create_csv?access_token=%@",str]]];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Email address" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       if ([[[alert textFields] objectAtIndex:0].text length]==0) {
                                                           
                                                           [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for email address"];
                                                           
                                                       }else if(![self NSStringIsValidEmail:[[alert textFields] objectAtIndex:0].text]){
                                                           [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid input for email address"];
                                                          
                                                       }else{
                                                           
                                                           [self sendCsvFiletoEmail:[[alert textFields] objectAtIndex:0].text];
                                                           
                                                       }
                                                   }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:ok];
        [alert addAction:cancel];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            
            // textField.placeholder =[arr1 objectAtIndex:LangId];
            
            textField.textColor=[UIColor darkGrayColor];
            textField.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"EmailId"];
            textField.returnKeyType=UIReturnKeyDone;
            //textField.keyboardType=UIKeyboardTypeNumberPad;
            textField.delegate=self;
        }];
        
        
        if (IS_IPAD) {
            alert.popoverPresentationController.sourceView = self.view;
            alert.popoverPresentationController.sourceRect = CGRectMake((self.view.bounds.size.width / 2.0)-20, 300, 1.0, 1.0);
            
        }
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    
 }
#pragma mark-SendcsVfile
-(void)sendCsvFiletoEmail:(NSString *)emailId{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/get_csv_email?access_token=%@&email=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],emailId] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                if ([returArray valueForKey:@"success"]) {
                     [[CommonMethods sharedInstance]AlertAction:@"Check email address for csv template"];
                    
                }else{
                    [[CommonMethods sharedInstance]AlertAction:[returArray valueForKey:@"error"]];
                    
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
              
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
        
    }

    
    
    
    
}

#pragma mark- EmailValidation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark-DoneButton
-(void)done:(id)sender{
    [tfFieldtrip resignFirstResponder];
    [tf2ndContact resignFirstResponder];
    [tf1stContact resignFirstResponder];
    [tfSpendingMoney resignFirstResponder];
    
    
    
  
    
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [arrGrades count];
}
#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [tfFieldtrip setText:[[arrGrades objectAtIndex:row] valueForKey:@"fieldtrip"]];
    fieldtripId=(int)row;
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[arrGrades objectAtIndex:row] valueForKey:@"fieldtrip"];
}

#pragma mark- ViewWillAppear
-(void)viewWillAppear:(BOOL)animated{
    
    
    
    
}

#pragma mark- hangleSingletap
-(void)handleSingleTap:(id)sender{
    
    [self.view endEditing:YES];
    
    
   
}
#pragma mark- UploadCSVFile
-(void)UploadCSVFile:(id)sender{
    
    
    
}

#pragma mark- SelectRequirement
-(void)SelectRequirement:(id)sender{
    [self.view endEditing:YES];
    UISwitch *btn=(UISwitch *)(id)sender;
    NSLog(@"%ld",(long)btn.tag);
    
    if (btn.tag==10) {
        if (btn.on) {
            IsPhotograps=YES;
            
            
        }else{
            IsPhotograps=NO;
            
        }
        
    }else if (btn.tag==11){
        if (btn.on) {
            tfSpecialNeeds.hidden=NO;
            IsSpecialNeeds=YES;
            
        }else{
            IsSpecialNeeds=NO;
            tfSpecialNeeds.hidden=YES;
            
        }
        
    }else if (btn.tag==12){
        if (btn.on) {
            IsMedicines=YES;
            
        }else{
            IsMedicines=NO;
            
        }
        
    }else if (btn.tag==13){
        if (btn.on) {
            tfSpendingMoney.hidden=NO;
            IsSpendingMoney=YES;
            
        }else{
            IsSpendingMoney=NO;
            tfSpendingMoney.hidden=YES;
            
        }
        
    }else if (btn.tag==14){
        if (btn.on) {
            tfAllergies.hidden=NO;
            IsAllergies=YES;
            
        }else{
            IsAllergies=NO;
            tfAllergies.hidden=YES;
            
        }
        
    }
    
}
#pragma mark-SaveTheEditDetails
-(void)SaveTheEditDetails:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    [btn setSelected:YES];
    
    
}

#pragma mark-TextFieldDelegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (!(IS_IPAD) ) {
        [sclVw setContentOffset:svos animated:YES];
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //    if (textField==tfClass && arrGrades.count==0) {
    //       // [self getGradesfromApi];
    //    }
    if (!(IS_IPAD)) {
        if (textField!=tfLastName && textField!=tfFirstName && textField!=tfClass) {
            svos = sclVw.contentOffset;
            CGPoint pt;
            CGRect rc = [textField bounds];
            rc = [textField convertRect:rc toView:sclVw];
            pt = rc.origin;
            pt.x = 0;
            pt.y -= 255;
            [sclVw setContentOffset:pt animated:YES];
            
        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (!(IS_IPAD)) {
        [sclVw setContentOffset:svos animated:YES];
    }
    [textField resignFirstResponder];
    if (textField==tfFirstName) {
        [tfLastName becomeFirstResponder];
        
    }else if (textField==tfLastName){
        [tfClass becomeFirstResponder];
        
    }else if (textField==tfClass){
        
        [tf1stContact becomeFirstResponder];
    }else if (textField==tf1stContact){
        [tf2ndContact becomeFirstResponder];
        
    }else if (textField==tf2ndContact){
        
        [textField resignFirstResponder];
        
    }
    
    
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField==tf1stContact || textField==tf2ndContact) {
//        int length = [self getLength:textField.text];
//        if(length == 10)
//        {
//            if(range.length == 0)
//                return NO;
//        }
//        
//        if(length == 3)
//        {
//            NSString *num = [self formatNumber:textField.text];
//            textField.text = [NSString stringWithFormat:@"%@-",num];
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
//        }
//        
//        else if(length == 7)
//        {
//            NSString *num = [self formatNumber:textField.text];
//            
//            textField.text = [NSString stringWithFormat:@"%@-%@",[num  substringToIndex:3],[num substringFromIndex:3]];
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
//        }
//        
//        else if(length == 8)
//        {
//            
//            NSString *num = [self formatNumber:textField.text];
//            
//            
//            NSString *subString = [num substringWithRange: NSMakeRange(3,3)];
//            
//            
//            
//            textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num  substringToIndex:3],subString,[num substringFromIndex:6]];
//            
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num substringToIndex:3],subString,[num substringFromIndex:6]];
//        }
        
        
        
        NSInteger length = [self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if ([textField.text hasPrefix:@"1"]) {
            if(length == 11)
            {
                if(range.length == 0)
                    return NO;
            }
            if(length == 4)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"%@ (%@) ",[num substringToIndex:1],[num substringFromIndex:1]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:4]];
            }
            else if(length == 7)
            {
                NSString *num = [self formatNumber:textField.text];
                NSRange numRange = NSMakeRange(1, 3);
                textField.text = [NSString stringWithFormat:@"%@ (%@) %@-",[num substringToIndex:1] ,[num substringWithRange:numRange],[num substringFromIndex:4]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
            
        } else {
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:textField.text];
                
                textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        return YES;
        
        
           }
    
    
        
        
        
        
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    if (textField==tfClass) {
        return newLength <= 25;
    }else if (textField==tfFirstName){
        return newLength <= 15;
    }else if (textField==tfLastName){
        return newLength <= 15;
    }else if (textField==tf1stContact || textField==tf2ndContact){
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return ([string isEqualToString:filtered] && newLength <= 15) || returnKey ;
    }else if (textField==tfSpendingMoney){
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return ([string isEqualToString:filtered] && newLength <= 5) || returnKey ;
        
    }else if (textField==tfSpecialNeeds || textField==tfAllergies){
        return newLength <= 100;
    }
    return newLength <= 50;
}



#pragma mark-AddStudent
-(void)AddStudent:(id)sender{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSString *strEditOrAdd,*strStudentsorStudent;
    if (IsEditStudent) {
        strEditOrAdd=@"edit";
        strStudentsorStudent=@"student";
        [dict setValue:[dictStudent valueForKey:@"id"] forKey:@"id"];
        
    }else{
        strEditOrAdd=@"add";
        strStudentsorStudent=@"students";
        
        
    }
    
//    NSString *tfPhone1 = [[tf2ndContact.text componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"()-"]] componentsJoinedByString: @""];
//    
//     NSString *tfPhone2 = [[tf1stContact.text componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"()-"]] componentsJoinedByString: @""];
//    
    
    
    
    if (IsPhotograps) {
        [dict setValue:[NSNumber numberWithInt:1] forKey:@"photography_allowed"];
        
    }else{
        [dict setValue:[NSNumber numberWithInt:0] forKey:@"photography_allowed"];
        
    }
    
    if (IsSpecialNeeds) {
        if (tfSpecialNeeds.text.length==0) {
            [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for special needs"];
            return;
        }else{
            [dict setValue:[NSNumber numberWithInt:1] forKey:@"special_need"];
            [dict setValue:tfSpecialNeeds.text forKey:@"special_need_type"];
            //special_need_type
        }
    }else{
        [dict setValue:[NSNumber numberWithInt:0] forKey:@"special_need"];
        
    }
    
    if (IsMedicines) {
        [dict setValue:[NSNumber numberWithInt:1] forKey:@"medicine"];
    }else{
        [dict setValue:[NSNumber numberWithInt:0] forKey:@"medicine"];
    }
    
    if (IsSpendingMoney) {
        
        
        if (tfSpendingMoney.text.length==0) {
            [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for spending money"];
            return;
        }else{
            [dict setValue:[NSNumber numberWithInt:1] forKey:@"spending_money"];
            [dict setValue:tfSpendingMoney.text forKey:@"spending_money_amt"];
            //special_need_type spending_money_am
        }
        
    }else{
        [dict setValue:[NSNumber numberWithInt:0] forKey:@"spending_money"];
    }
    
    if (IsAllergies) {
        if (tfAllergies.text.length==0) {
            [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for allergies"];
            return;
        }else{
            [dict setValue:[NSNumber numberWithInt:1] forKey:@"allergies"];
            [dict setValue:tfAllergies.text forKey:@"allergy_details"];
            //special_need_type
        }
    }else{
        [dict setValue:[NSNumber numberWithInt:0] forKey:@"allergies"];
    }
    
    
    if (tfFirstName.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for firstname"];
        return;
    }else if (tfLastName.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for lastname"];
        return;
            }else if (tfFieldtrip.text.length==0) {
                [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for field trip name"];
                return;
    }else if (tf1stContact.text.length<10  && tf1stContact.text.length>1){
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid 1st contact number"];
        return;
    }else if (tf2ndContact.text.length<10 && tf2ndContact.text.length>1){
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid 2nd contact number"];
        return;
        
    }else if ([[[tf2ndContact.text componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"()-"]] componentsJoinedByString: @""] isEqualToString:[[tf1stContact.text componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"()-"]] componentsJoinedByString: @""]]  && tf1stContact.text.length>1){
        
        [[CommonMethods sharedInstance] AlertAction:@"Both contact number can't be same"];
        return;
    }
//
//    
//    else if ([tf2ndContact.text  isEqualToString:tf1stContact.text] && tf1stContact.text.length>1){
//      
//        
//    }
    else{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:tfClass.text forKey:@"Class"];
        
        [dict setValue:tf1stContact.text forKey:@"ice_contact_1"];
        [dict setValue:tf2ndContact.text forKey:@"ice_contact_2"];
        [dict setValue:tfFirstName.text forKey:@"first_name"];
        [dict setValue:tfLastName.text forKey:@"last_name"];
        [dict setValue:tfClass.text forKey:@"class"];
        [dict setValue:[[arrGrades objectAtIndex:fieldtripId]valueForKey:@"id"] forKey:@"trip_id"];
        
        
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Students/%@_%@",kBaseUrl,strEditOrAdd,strStudentsorStudent] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    }
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
        }else{
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
        }
    }
}
#pragma mark-tapgesture
- (void)downloadtemplate:(UITapGestureRecognizer *)tapRecognizer
{
    
    // CGPoint touchPoint = [tapRecognizer locationInView: tapRecognizer.view];
    // NSLog(@)
    
    //Modify the validFrame that you would like to enable the touch
    //or get the frame from _yourLabel using the NSMutableAttributedString, if possible
    //CGRect validFrame = CGRectMake(0, 0, 300, 44);
    
}

#pragma mark-createTextfield
-(UITextField *)creatertextfild:(CGRect )frame{
    
    UITextField *txtfld=[[UITextField alloc]initWithFrame:frame];
    txtfld.delegate=self;
    txtfld.spellCheckingType = UITextSpellCheckingTypeNo;
    txtfld.autocorrectionType = UITextAutocorrectionTypeNo;
    txtfld.returnKeyType=UIReturnKeyNext;
    txtfld.autocapitalizationType = UITextAutocapitalizationTypeWords;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,5, txtfld.frame.size.height)];
    txtfld.leftView = paddingView;
    txtfld.leftViewMode = UITextFieldViewModeAlways;
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, txtfld.frame.size.height - borderWidth, txtfld.frame.size.width, txtfld.frame.size.height);
    border.borderWidth = borderWidth;
    [txtfld.layer addSublayer:border];
    txtfld.layer.masksToBounds = YES;
    [sclVw addSubview:txtfld];
    
    return txtfld;
    
}
#pragma mark-CreateLabel
-(UILabel *)CreateLabel:(CGRect)frame  andTitle:(NSString *)Titl {
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.text=Titl;
    lbl.textAlignment=NSTextAlignmentLeft;
    lbl.textColor=[UIColor lightGrayColor];
    [sclVw addSubview:lbl];
    return lbl;
    
}

#pragma mark-CreateLabel
-(UILabel *)CreatelabelOfchecklist:(CGRect)frame  andTitle:(NSString *)Titl andView:(UIView *)View{
    
    UILabel *lblNeeds=[[UILabel alloc]initWithFrame:frame];
    lblNeeds.text=Titl;
    lblNeeds.textAlignment=NSTextAlignmentCenter;
    lblNeeds.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    [View addSubview:lblNeeds];
    return lblNeeds;
    
}

#pragma mark-CreateImageView
-(UIImageView *)CreateImageView:(UIImage *)img andFrame:(CGRect)frame andView:(UIView *)Vw{
    UIImageView *ImgVw=[[UIImageView alloc]initWithFrame:frame];
    ImgVw.image=img;
    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
    [Vw addSubview:ImgVw];
    return ImgVw;
    
}

#pragma mark-createTextieldforchecklist
-(UITextField *)creatertextfild:(CGRect )frame andView:(UIView *)Vw{
    
    UITextField *txtfld=[[UITextField alloc]initWithFrame:frame];
    txtfld.delegate=self;
    txtfld.spellCheckingType = UITextSpellCheckingTypeNo;
    txtfld.autocorrectionType = UITextAutocorrectionTypeNo;
    txtfld.returnKeyType=UIReturnKeyDone;
    txtfld.layer.borderWidth=1;
    txtfld.layer.borderColor=[UIColor blackColor].CGColor;
    txtfld.autocapitalizationType = UITextAutocapitalizationTypeWords;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,5, txtfld.frame.size.height)];
    txtfld.leftView = paddingView;
    txtfld.leftViewMode = UITextFieldViewModeAlways;
    [Vw addSubview:txtfld];
    
    return txtfld;
    
}
#pragma mark-CreateSwitch
-(UISwitch*)createSwitch:(CGRect)frame andView:(UIView *)Vw andTag:(int)tag{
    UISwitch *switchbtn=[[UISwitch alloc]initWithFrame:frame];
    [switchbtn setOn:NO animated:YES];
    [switchbtn addTarget:self action:@selector(SelectRequirement:) forControlEvents:UIControlEventValueChanged];
    switchbtn.tag=tag;
    [Vw addSubview:switchbtn];
    return switchbtn;
    
}
#pragma mark- PhoneMasking
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
        
    }
    
    
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
    
    
}





@end
