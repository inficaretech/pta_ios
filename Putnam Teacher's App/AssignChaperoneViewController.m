//
//  AssignChaperoneViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/24/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AssignChaperoneViewController.h"
#import "AssignChaperoneTableViewCell.h"
#import "Constants.h"
#import "WebService.h"
#import "CommonMethods.h"

@interface AssignChaperoneViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *arrListOfChaperones;
    UITableView *tblVw;
    int secondaryChaperoneId,primaryChaperoneId;
}

@end

@implementation AssignChaperoneViewController
@synthesize x,FieldTripId;
- (void)viewDidLoad {
    [super viewDidLoad];
    arrListOfChaperones=[[NSMutableArray alloc]init];
    self.navigationItem.title=@"Assign Chaperone";
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self createUi];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark- CreateUi
-(void)createUi{
    
    
    UILabel *lblStudentList=[[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-75, 70.0, 150.0, 30)];
    lblStudentList.text=@"Chaperone List";
    lblStudentList.textColor=[UIColor darkGrayColor];
    lblStudentList.textAlignment=NSTextAlignmentCenter;
    lblStudentList.font=[UIFont fontWithName:@"Roboto-Regular" size:20];
    [self.view addSubview:lblStudentList];
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1].CGColor;
    border.frame = CGRectMake(0, lblStudentList.frame.size.height - borderWidth, lblStudentList.frame.size.width, lblStudentList.frame.size.height);
    border.borderWidth = borderWidth;
    [lblStudentList.layer addSublayer:border];
    lblStudentList.layer.masksToBounds = YES;
    
    tblVw=[[UITableView alloc]initWithFrame:CGRectMake(0.0, 110.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-210)];
    tblVw.delegate=self;
    tblVw.dataSource=self;
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tblVw];
    
    UIButton *BtnSendAccessCode=[UIButton buttonWithType:UIButtonTypeCustom];
    BtnSendAccessCode.frame=CGRectMake(20.0, [[UIScreen mainScreen]bounds].size.height-70, [[UIScreen mainScreen]bounds].size.width-40, 45);
    [BtnSendAccessCode setTitle:@"Send Access Code" forState:UIControlStateNormal];
    BtnSendAccessCode.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [self.view addSubview:BtnSendAccessCode];
    [BtnSendAccessCode addTarget:self action:@selector(AssignChaperone:) forControlEvents:UIControlEventTouchUpInside];
    [self GetListOfChaperoneFromApi];
}

#pragma mark- TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrListOfChaperones.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"AssignChaperoneTableViewCell";
    
    AssignChaperoneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 74, [[UIScreen mainScreen]bounds].size.width, 1)];
        lblline.backgroundColor=[UIColor lightGrayColor];
        [cell.contentView addSubview:lblline];
        cell.tag=indexPath.row;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (arrListOfChaperones.count>0) {
        // NSString *strType=[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"type"];
        if ([[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"type"] isEqualToString:@"1"] && [[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"group_id"]intValue]==x) {
            cell.btnPrimary.selected=YES;
            primaryChaperoneId=[[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"chaperon_id"]intValue];
        }else if ([[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"type"] isEqualToString:@"2"] && [[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"group_id"]intValue]==x){
            cell.btnSecondary.selected=YES;
            secondaryChaperoneId=[[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"chaperon_id"]intValue];
            
        }
        
        cell.lblName.text=[[arrListOfChaperones objectAtIndex:indexPath.row ] valueForKey:@"name"];
        [cell.btnPrimary addTarget:self action:@selector(SelectPrimaryBtn:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnPrimary.tag=indexPath.row+10;
        [cell.btnSecondary addTarget:self action:@selector(SelectSeconadryBtn:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSecondary.tag=indexPath.row+100;
    }
    
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
    
}

#pragma mark- GetChaperoneList
-(void)GetListOfChaperoneFromApi{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/%@/get_chaperon_assign?access_token=%@&teacher_id=%@&trip_id=%d",kBaseUrl,@"Chaperones",[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],FieldTripId] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                
                
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                if (arr.count>0) {
                    arrListOfChaperones=[arr mutableCopy];
                    [tblVw reloadData];
                }else{
                    UILabel *Lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( [[UIScreen mainScreen]bounds].size.height/2)-20, [[UIScreen mainScreen]bounds].size.width-40, 40)];
                    Lbl.text=@"No chaperones managed";
                    Lbl.textAlignment=NSTextAlignmentCenter;
                    
                    Lbl.textColor=[UIColor darkGrayColor];
                    [self.view addSubview:Lbl];
                    
                    
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    } else
    {
        
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
    
    
}
#pragma mark-SelectSeconadryBtn
-(void)SelectSeconadryBtn:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    
    secondaryChaperoneId=[[[arrListOfChaperones objectAtIndex:btn.tag-100] valueForKey:@"chaperon_id"]intValue];
    if (secondaryChaperoneId==primaryChaperoneId) {
        primaryChaperoneId=0;
        
    }
    NSString *titl=btn.titleLabel.text;
    NSLog(@"%@",titl);
    for (int k=0; k<=arrListOfChaperones.count; k++) {
        UIButton *btn1=[self.view viewWithTag:100+k];
        [btn1 setSelected:NO];
    }
    UIButton *BtnPrimary=[self.view viewWithTag:btn.tag-90];
    [BtnPrimary setSelected:NO];
    
    [btn setSelected:YES];
    
}
#pragma mark- SelectPrimaryBtn
-(void)SelectPrimaryBtn:(id)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
    
    int Type=[[[arrListOfChaperones objectAtIndex:btn.tag-10]valueForKey:@"type"]intValue];
    
    // NSString *strGroup=[[[arrListOfChaperones objectAtIndex:btn.tag-10]valueForKey:@"type"]intValue];
    if (Type==1) {
        [[CommonMethods sharedInstance]AlertAction:[NSString stringWithFormat:@"Already a primary chaperone for group-%d",[[[arrListOfChaperones objectAtIndex:btn.tag-10]valueForKey:@"group_id"]intValue]]];
        return;
    }
    
    
    
    primaryChaperoneId=[[[arrListOfChaperones objectAtIndex:btn.tag-10] valueForKey:@"chaperon_id"]intValue];
    if (secondaryChaperoneId==primaryChaperoneId) {
        secondaryChaperoneId=0;
        
    }
    
    NSString *titl=btn.titleLabel.text;
    NSLog(@"%@",titl);
    for (int k=0; k<=arrListOfChaperones.count; k++) {
        UIButton *btn1=[self.view viewWithTag:10+k];
        [btn1 setSelected:NO];
    }
    UIButton *BtnSecondary=[self.view viewWithTag:btn.tag+90];
    [BtnSecondary setSelected:NO];
    
    [btn setSelected:YES];
}

#pragma mark- SendAccessCode
-(void)AssignChaperone:(id)sender{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSMutableArray *ArrMapping=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictPrimary=[[NSMutableDictionary alloc]init];
    
    if (primaryChaperoneId !=0) {
        [dictPrimary setValue:[NSNumber numberWithInt:primaryChaperoneId] forKey:@"chaperon_id"];
        [dictPrimary setValue:[NSNumber numberWithInt:1] forKey:@"type"];
        [ArrMapping addObject:dictPrimary];
        
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please select the primary chaperone for this group"];
        return;
    }
    
    NSMutableDictionary *dictSecondary=[[NSMutableDictionary alloc]init];
    if (secondaryChaperoneId !=0) {
        [dictSecondary setValue:[NSNumber numberWithInt:secondaryChaperoneId] forKey:@"chaperon_id"];
        [dictSecondary setValue:[NSNumber numberWithInt:2] forKey:@"type"];
        [ArrMapping addObject:dictSecondary];
    }
    
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
    [dict setValue:[NSNumber numberWithInt:x] forKey:@"group_id"];
    [dict setValue:ArrMapping forKey:@"mapping_id"];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/assign_chaperon",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            
            if (!error) {
                //  [[CommonMethods sharedInstance] AlertAction:[returArray valueForKey:@"result"]];
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                // NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([returArray valueForKey:@"success"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"Successfully chaperone is assigned to the group"];
                    
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
}





@end
