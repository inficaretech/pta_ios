//
//  UsefulLinkTableViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/9/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "UsefulLinkTableViewController.h"
#import "CommonMethods.h"
#import "GetDirectionViewController.h"
#import <MapKit/MapKit.h>
#import "Constants.h"

@interface UsefulLinkTableViewController ()
{
    NSMutableArray *arr;
    
}

@end

@implementation UsefulLinkTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self CreateUi];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark- CreateUi
-(void)CreateUi{
    arr=[[NSMutableArray alloc]initWithObjects:@"Putnam Visitor app Android",@"Putnam Visitor app ios",@"Putnam Website",@"Share this App",@"Museum Directions", nil];
    self.navigationItem.title = @"Useful Links";
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
//    [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
    
}
#pragma mark- BackbuttonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier=@"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setTintColor:[UIColor blackColor]];
        
        UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 49, [[UIScreen mainScreen]bounds].size.width, 1)];
        lblline.backgroundColor=[UIColor darkGrayColor];
        [cell.contentView addSubview:lblline];
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.text=[arr objectAtIndex:indexPath.row];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
      return 50;
    }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.putnam.org"]];
    }
    if (indexPath.row==4) {
        
//        CLLocationCoordinate2D center;
//        center=[self getLocationFromAddressString:@"1717 W 12th St, Davenport, IA 52804, United States"];
//        double  latFrom=center.latitude;
//        double  lonFrom=center.longitude;
//        
//        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latFrom,lonFrom);
//        
//        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
//                                                       addressDictionary:nil];
//        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
//        [mapItem setName:@"Putnam Museum"];
//        // Pass the map item to the Maps app
//        [mapItem openInMapsWithLaunchOptions:nil];
        
        NSString *addressString = @"http://maps.apple.com/?q=1717+W+12th+St,+Davenport,+IA+52804,+United+States";
        NSURL *url = [NSURL URLWithString:addressString];
        [[UIApplication sharedApplication] openURL:url];
        

    }

}


-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
    
}

@end
