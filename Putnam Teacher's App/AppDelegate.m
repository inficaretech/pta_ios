//
//  AppDelegate.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/13/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "StartingPageViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "SelectLanguageViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize navController;
@synthesize btnAdd;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //[NSThread sleepForTimeInterval:10];
    BOOL changeInVersion = [self checkVersionNumber];
    
    if (changeInVersion) {
        
        
        //Remove the Data Base and Create New One
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"Teacher.sqlite"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        //Create New DataBase
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        NSString *dbPath = [self getDBPath];
        BOOL success = [fileManager fileExistsAtPath:dbPath];
        
        if(!success)
        {
            NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Teacher.sqlite"];
            success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        }
        
        
        
    }
    
    else{
        
        [self copyDatabaseIfNeeded];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL isVerified = [defaults boolForKey:@"isVerified"];
    //      [self.window setRootViewController:navController];
//     UIStoryboard *story=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    StartingPageViewController  *controller=[story instantiateViewControllerWithIdentifier:@"StartingPageView"];
//    self.navController=[[UINavigationController alloc]initWithRootViewController:controller];
//    self.window.rootViewController = self.navController;
    
    if (isVerified) {
        UIStoryboard *story=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HomeViewController *controller=[story instantiateViewControllerWithIdentifier:@"HomeView"];
        self.navController=[[UINavigationController alloc]initWithRootViewController:controller];
        self.window.rootViewController = self.navController;
        //[self.navController pushViewController:controller animated:NO];
    }else{
        
//        UIStoryboard *story=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        SelectLanguageViewController *viewcontroller = [story instantiateViewControllerWithIdentifier:@"SelectLanguage"];
//        self.navController = [[UINavigationController alloc]initWithRootViewController:viewcontroller];
//        self.window.rootViewController = self.navController;
       
        
        
         UIStoryboard *story=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        StartingPageViewController  *controller=[story instantiateViewControllerWithIdentifier:@"StartingPageView"];
        self.navController=[[UINavigationController alloc]initWithRootViewController:controller];
        self.window.rootViewController = self.navController;
        
        
    }
    
    
    // TODO: Move this to where you establish a user session
    //[self logUser];

    return YES;
}

//- (void) logUser {
//    // TODO: Use the current user's information
//    // You can call any combination of these three methods
//    [CrashlyticsKit setUserIdentifier:@"12345"];
//    [CrashlyticsKit setUserEmail:@"user@fabric.io"];
//    [CrashlyticsKit setUserName:@"Test User"];
//}

-(BOOL)checkVersionNumber{
    
    
    float newVersionNumber = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] floatValue];
    
    float oldVersionNumber = [[NSUserDefaults standardUserDefaults] floatForKey:@"AppVersionNumber"];
    
    if (newVersionNumber > oldVersionNumber) {
        
        [[NSUserDefaults standardUserDefaults] setFloat:newVersionNumber forKey:@"AppVersionNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
        
    }
    
    
    
    
    
    return NO;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"Finish"]) {
        [self PostOfflineData];
    }
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark----------- data base related methods-----------
- (void) copyDatabaseIfNeeded
{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getDBPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success)
    {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Teacher.sqlite"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        //NSLog(@"database created successfully");
        //NSLog(@"success value====%c",success);
        //  if (!success)
        //NSLog( @"Failed to create database with message '%@'.", [error localizedDescription]);
    }
    NSLog(@" %@ ",dbPath);
}
- (NSString *) getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir =[paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"Teacher.sqlite"];
    
}
-(void)PostOfflineData{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"] forKey:@"access_token"];
    [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"] forKey:@"teacher_id"];
    [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"] forKey:@"trip_id"];
    [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"group_id"] forKey:@"group_id"];
     [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"Chaperone_id"] forKey:@"cheperone_id"];
    
    [dict setValue:[NSNumber numberWithInt:[[[NSUserDefaults standardUserDefaults] valueForKey:@"Finish"] intValue]]  forKey:@"score"];
     [dict setValue:@"1" forKey:@"completed"];
    
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        
       // [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/finish_quiz",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            
            if (!error) {
              //  [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Your Quiz has been saved"];
                
                [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"Finish"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else
            {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                //[[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
        
        
        
    }
    
    
    
    

    
    
    
}


@end
