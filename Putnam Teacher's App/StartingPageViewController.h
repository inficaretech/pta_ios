//
//  StartingPageViewController.h
//  Putnam Teacher's App
//
//  Created by inficare on 5/26/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartingPageViewController : UIViewController
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView *imageViewPutnamLogo;
    IBOutlet UILabel *lblTeacherApp;
    IBOutlet UILabel *lblAlcoaFoundation;
    IBOutlet UIImageView *imageViewAlcoaFoundation;
    IBOutlet UIButton *btnWallOfFame;
    IBOutlet UIButton *btnFieldTripPricing;
    IBOutlet UIButton *btnManageFieldTrip;
    IBOutlet UIButton *btnOutreachProgram;
    IBOutlet UIButton *btnLogin;
     IBOutlet UIButton *btnGallery;
    IBOutlet UIView *viewOverScrollView;
    
}

@end
