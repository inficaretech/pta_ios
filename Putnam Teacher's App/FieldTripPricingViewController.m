//
//  FieldTripPricingViewController.m
//  PutnamTeacher
//
//  Created by Inficare on 5/27/16.
//  Copyright © 2016 Inficare. All rights reserved.
//

#import "FieldTripPricingViewController.h"
#import "Constants.h"

@interface FieldTripPricingViewController ()<UIScrollViewDelegate>

@end

@implementation FieldTripPricingViewController
@synthesize IsEditStudent;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self CustomNavigationBar];
    [self createUi];
    // Do any additional setup after loading the view.
}
#pragma mark- CustomNavigation
-(void)CustomNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Field Trip Pricing";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
}
#pragma mark- BackButton Action
-(void)BackButtonPressed
{
    if (IsEditStudent) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
     [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- CreateUi
-(void)createUi{
    
    UIScrollView *sclVw=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
      sclVw.delegate=self;
    sclVw.contentSize=CGSizeMake([[UIScreen mainScreen]bounds].size.width, 1030);
    [self.view addSubview:sclVw];
    int x,y;
    
    if (IS_IPAD) {
        x=70;
        sclVw.scrollEnabled=NO;
    }else{
        x=200;
        y=100;
    }

    
    
    UILabel *lblPricing=[[UILabel alloc]initWithFrame:CGRectMake(20, 20.0, [[UIScreen mainScreen]bounds].size.width-40, x)];
    lblPricing.numberOfLines=0;
    lblPricing.text=@"Student Pricing (per student)/Chaperone Pricing (available for one chaperone for 10 students) \nOne complimentary teacher for every classroom; additional teachers pay the chaperone rate up to the 1:10 ratio. Special needs teachers, please contact us so we can accommodate you.";
    lblPricing.textColor=[UIColor darkGrayColor];
    lblPricing.font=[UIFont fontWithName:@"Roboto-Regular" size:14];
    [lblPricing sizeToFit];

    
    UIView *Vwpricing=[[UIView alloc]initWithFrame:CGRectMake(8.0, x,[[UIScreen mainScreen]bounds].size.width-16 , 250)];
    Vwpricing.layer.borderWidth=1;
    Vwpricing.tag=10;
    Vwpricing.layer.borderColor=[UIColor darkGrayColor].CGColor;
    
    [sclVw addSubview:Vwpricing];
    
    [self createTableOfPricing:Vwpricing];
    
        UILabel *lblPricingAdult=[[UILabel alloc]initWithFrame:CGRectMake(20, x+270.0, [[UIScreen mainScreen]bounds].size.width-40, x)];
    lblPricingAdult.numberOfLines=0;
    lblPricingAdult.text=@"Additional Group Adult Pricing (Beyond the 1:10 adult/student ratio. NOTE: Additional group adults MUST register and check in with their group. Adults who use regular ticketing lines will be charged the regular adult visitor price.)";
    
    lblPricingAdult.textColor=[UIColor darkGrayColor];
    lblPricingAdult.font=[UIFont fontWithName:@"Roboto-Regular" size:14];
    [lblPricingAdult sizeToFit];
    UIView *VwpricingAdult=[[UIView alloc]initWithFrame:CGRectMake(8.0, x+y+300.0,[[UIScreen mainScreen]bounds].size.width-16 , 250)];
    VwpricingAdult.layer.borderWidth=1;
    VwpricingAdult.tag=20;
    VwpricingAdult.layer.borderColor=[UIColor darkGrayColor].CGColor;
    
    [sclVw addSubview:VwpricingAdult];
     [self createTableOfPricing:VwpricingAdult];

    [sclVw addSubview:lblPricing];
    [sclVw addSubview:lblPricingAdult];
    
    
}
#pragma mark-TickImageView
-(void)CreateTickPng:(CGRect)frame andView:(UIView *)vw{
    
    UIImageView *imgVw=[[UIImageView alloc]initWithFrame:frame];
    imgVw.contentMode=UIViewContentModeScaleAspectFit;
    imgVw.image=[UIImage imageNamed:@"Tick_mark"];
      [vw addSubview:imgVw];
}
#pragma mark-CreateLabel
-(void)CreateLabel:(CGRect)frame andView:(UIView *)Vw andTitle:(NSString *)Titl andNumberofLines:(int)number{
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.numberOfLines=number;
    lbl.text=Titl;
    lbl.textColor=[UIColor darkGrayColor];
    lbl.textAlignment=NSTextAlignmentCenter;
     lbl.adjustsFontSizeToFitWidth = YES;
    
    [Vw addSubview:lbl];
    
}
#pragma mark-CreateTableofPricing
-(void)createTableOfPricing :(UIView *)Vw{
    
    
    for (int i=0; i<4; i++) {
        
        UILabel *lblLineHorizontal=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 50*(i+1), [[UIScreen mainScreen]bounds].size.width-16, 1)];
        lblLineHorizontal.backgroundColor=[UIColor blackColor];
        [Vw addSubview:lblLineHorizontal];
        
         UILabel *lblLineVertical=[[UILabel alloc]initWithFrame:CGRectMake((Vw.frame.size.width/2-Vw.frame.size.width/10)+Vw.frame.size.width/10*(i+1), 0, 1, 250)];
        lblLineVertical.backgroundColor=[UIColor blackColor];
       // [Vw addSubview:lblLineVertical];
    }
    
    for (int i=0; i<5; i++) {
        
        UILabel *lblLineHorizontal=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 50*(i+1), [[UIScreen mainScreen]bounds].size.width-16, 1)];
        lblLineHorizontal.backgroundColor=[UIColor blackColor];
       // [Vw addSubview:lblLineHorizontal];
        
        UILabel *lblLineVertical=[[UILabel alloc]initWithFrame:CGRectMake((Vw.frame.size.width/2-Vw.frame.size.width/10)+Vw.frame.size.width/10*(i+1), 0, 1, 250)];
        lblLineVertical.backgroundColor=[UIColor blackColor];
         [Vw addSubview:lblLineVertical];
    }

    int l;
    if (IS_IPAD) {
          l=25;
    }else{
        l=6;
    }
    
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+l, 10, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+2*(Vw.frame.size.width/10)+l, 10, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+3*(Vw.frame.size.width/10)+l, 10, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+4*(Vw.frame.size.width/10)+l, 10, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    
    
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+2*(Vw.frame.size.width/10)+l, 60, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+4*(Vw.frame.size.width/10)+l, 60, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+(Vw.frame.size.width/10)+l, 110, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+3*(Vw.frame.size.width/10)+l, 160, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    
    [self CreateTickPng:CGRectMake((Vw.frame.size.width/2)+4*(Vw.frame.size.width/10)+l, 160, (Vw.frame.size.width/10)-2*l, 35) andView:Vw];
    
    
    if (Vw.tag==10) {
        
    
    [self CreateLabel:CGRectMake(10.0, 10.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Museum Admission\n($3.50)(self-guided tour)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 60.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Giant Screen Movie Add-On\n($3.50)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 110.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Giant Screen Movie ONLY\n($5.00)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 160.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"The Discovery of King Tut\n($5.00)" andNumberofLines:2];
          [self CreateLabel:CGRectMake(10.0, 210.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Total" andNumberofLines:2];
      
//      //  UILabel *lblTotal=  [self CreateLabel:CGRectMake(10.0, 210.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Total" andNumberofLines:1];
//        UILabel *lblTotal=[[UILabel alloc]initWithFrame:CGRectMake(10.0, 210.0,(Vw.frame.size.width/2)-15, 30)];
//        [Vw addSubview:lblTotal];
//        lblTotal.text=@"Total";
//        lblTotal.textColor=[UIColor darkGrayColor];
//        lblTotal.textAlignment=NSTextAlignmentCenter;
        
        [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+0*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$3.50" andNumberofLines:1];
    [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+1*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$5.00" andNumberofLines:1];
    [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+2*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$7.00" andNumberofLines:1];
       [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+3*(Vw.frame.size.width/10)+2, 210.0,2*(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$8.50   $12.00" andNumberofLines:1];
    
    }else{
        
        [self CreateLabel:CGRectMake(10.0, 10.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Museum Admission\n($6.00)(self-guided tour)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 60.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Giant Screen Movie Add-On\n($4.50)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 110.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Giant Screen Movie ONLY\n($7.50)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 160.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"The Discovery of King Tut\n($10.00)" andNumberofLines:2];
        [self CreateLabel:CGRectMake(10.0, 210.0,(Vw.frame.size.width/2)-15, 30) andView:Vw andTitle:@"Total" andNumberofLines:2];
        
//        UILabel *lblTotal=[[UILabel alloc]initWithFrame:CGRectMake(10.0, 210.0,(Vw.frame.size.width/2)-15, 30)];
//        [Vw addSubview:lblTotal];
//        lblTotal.text=@"Total";
//        lblTotal.textColor=[UIColor darkGrayColor];
//        lblTotal.textAlignment=NSTextAlignmentCenter;

        
        [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+0*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$7.00" andNumberofLines:1];
        [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+1*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$7.50" andNumberofLines:1];
        [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+2*(Vw.frame.size.width/10)+2, 210.0,(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$12.00" andNumberofLines:1];
        [self CreateLabel:CGRectMake((Vw.frame.size.width/2)+3*(Vw.frame.size.width/10)+2, 210.0,2*(Vw.frame.size.width/10)-4, 30) andView:Vw andTitle:@"$17.00   $22.00" andNumberofLines:1];

    }
    
}

@end
