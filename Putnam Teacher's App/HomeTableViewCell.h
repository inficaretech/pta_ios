//
//  HomeTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 7/25/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *ImgVw;

@end
