//
//  LoginViewController.h
//  Putnam Teacher's App
//
//  Created by inficare on 5/27/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
{
    
}
- (IBAction)LoginButtonPressed:(id)sender;
- (IBAction)ChaperoneButtonPressed:(id)sender;
- (IBAction)LoginAsguestButtonPressed:(id)sender;

@end
