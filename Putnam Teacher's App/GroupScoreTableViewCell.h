//
//  GroupScoreTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/28/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupScoreTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,strong) IBOutlet UILabel *labelAlphabet;
@property(nonatomic,strong) IBOutlet UILabel *labelDetail;
@property(nonatomic,strong) IBOutlet UILabel *labelMarks;
@property(nonatomic,strong) IBOutlet UIButton *buttonTick;
@end
