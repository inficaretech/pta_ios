//
//  StudentListTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/29/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSSwipeableCells/GSTableViewCell/GSSwipeableCell.h"

@interface StudentListTableViewCell : GSSwipeableCell

@property (retain, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lbClass;
@property (weak, nonatomic) IBOutlet UILabel *lblContact1;
@property (weak, nonatomic) IBOutlet UILabel *lblContact2;
@property (weak, nonatomic) IBOutlet UILabel *lblColour;
@property (weak, nonatomic) IBOutlet UILabel *lblFieldTrip;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIImageView *imageCamera;
@property (weak, nonatomic) IBOutlet UIImageView *imageSpecialNeeds;
@property (weak, nonatomic) IBOutlet UIImageView *imageSpendingMoney;
@property (weak, nonatomic) IBOutlet UIImageView *imageAllergy;
@property (weak, nonatomic) IBOutlet UIImageView *imageMedicine;

@end
