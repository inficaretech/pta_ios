//
//  GroupDetailsViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/13/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "GroupDetailsViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"
#import "AssignChaperoneViewController.h"
#import "GroupDetailsTableViewCell.h"

@interface GroupDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>{
    //NSMutableArray *arrGroups;
    UITableView *tblVw;
      NSMutableArray *arrStudentList;
    
}

@end

@implementation GroupDetailsViewController
@synthesize x;
@synthesize  FieldTripId;
@synthesize strNavigationtitl;
- (void)viewDidLoad {
    [super viewDidLoad];
    arrStudentList=[[NSMutableArray alloc]init];
    self.navigationItem.title=[NSString stringWithFormat:@"%@ Students",strNavigationtitl];
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    
    [self createUi];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark- AssignChaperone
-(void)AssignChaperone:(id)sender{
    
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AssignChaperoneViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AssignChaperoneView"];
    Controller.x=x;
    Controller.FieldTripId=FieldTripId;
    [self.navigationController pushViewController:Controller animated:YES ];
   }
#pragma mark-CreateUi
-(void)createUi{
    
//    UILabel *lblStudentList=[[UILabel alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-60, 70.0, 120.0, 30)];
//    lblStudentList.text=@"Student List";
//    lblStudentList.textColor=[UIColor darkGrayColor];
//    lblStudentList.textAlignment=NSTextAlignmentCenter;
//    lblStudentList.font=[UIFont fontWithName:@"Roboto-Regular" size:20];
//    [self.view addSubview:lblStudentList];
    
//    CALayer *border = [CALayer layer];
//    CGFloat borderWidth = 1;
//    border.borderColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1].CGColor;
//    border.frame = CGRectMake(0, lblStudentList.frame.size.height - borderWidth, lblStudentList.frame.size.width, lblStudentList.frame.size.height);
//    border.borderWidth = borderWidth;
//    [lblStudentList.layer addSublayer:border];
//    lblStudentList.layer.masksToBounds = YES;
//    
    
    tblVw=[[UITableView alloc]initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen]bounds].size.width , [[UIScreen mainScreen]bounds].size.height)];
    tblVw.delegate=self;
    tblVw.dataSource=self;
    tblVw.separatorStyle=UITableViewCellSelectionStyleNone;
    [self.view addSubview:tblVw];
    
//    UIButton *BtnAssignChaperone=[UIButton buttonWithType:UIButtonTypeCustom];
//    BtnAssignChaperone.frame=CGRectMake(20.0, [[UIScreen mainScreen]bounds].size.height-70, [[UIScreen mainScreen]bounds].size.width-40, 45);
//    [BtnAssignChaperone setTitle:@"Assign Chaperone" forState:UIControlStateNormal];
//    BtnAssignChaperone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
//    [self.view addSubview:BtnAssignChaperone];
//    [BtnAssignChaperone addTarget:self action:@selector(AssignChaperone:) forControlEvents:UIControlEventTouchUpInside];
    [self getStudentListForChaperone];
    
    
    
}
/*
#pragma mark- ApiResponse
-(void)GetApiResponse
{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/GroupsAPI/get_students_by_group_id?&group_id=%d&access_token=%@&teacher_id=%@&trip_id=%d",kBaseUrl,x,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],FieldTripId] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                
                if ([returArray valueForKey:@"error"]) {
                    UILabel *Lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( [[UIScreen mainScreen]bounds].size.height/2)-20, [[UIScreen mainScreen]bounds].size.width-40, 40)];
                    Lbl.text=[returArray valueForKey:@"error"];
                    Lbl.textAlignment=NSTextAlignmentCenter;
                    
                    Lbl.textColor=[UIColor darkGrayColor];
                    [self.view addSubview:Lbl];
                    
                }else{
                    NSArray *arr=[[NSArray alloc]init];
                    arr=[returArray valueForKey:@"result"];
                    
                    arrGroups=[arr mutableCopy];
                    [tblVw reloadData];
                    
                }
                
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }
    else
    {
        
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
 
    
}
 */
#pragma mark-TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrStudentList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"GroupDetailsTableViewCell";
    
    GroupDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0,49, [[UIScreen mainScreen]bounds].size.width, 1)];
        lblline.backgroundColor=[UIColor lightGrayColor];
        [cell.contentView addSubview:lblline];
        
    }
    cell.imgVwMoney.image=nil;
    cell.imgVwCamera.image=nil;
    cell.imgVwAllergies.image=nil;
    cell.imgVwMedicines.image=nil;
    cell.imgVwSpecialNeeds.image=nil;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(arrStudentList.count>0){
        cell.lblStudentName.numberOfLines=2;
        cell.lblStudentName.text=[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"name"];
        NSMutableArray *arrImgaes=[[NSMutableArray alloc]init];
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"photography_allowed"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"photography_allowed"];
            
        }else if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"photography_allowed"]isEqualToString:@"0"]){
            [arrImgaes addObject:@"camera_icon - cross"];
        }
        
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"special_need"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"special_need"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"medicine"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"medicine_icon"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"spending_money"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"dollar_icon"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"allergies"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"allergy_icon"];
            
        }
        
        if (arrImgaes.count==1) {
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            
        }else if (arrImgaes.count==2){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
        }else if (arrImgaes.count==3){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
        }else if (arrImgaes.count==4){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
            cell.imgVwMoney.image=[UIImage imageNamed:[arrImgaes objectAtIndex:3]];
        }else if (arrImgaes.count==5){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
            cell.imgVwMoney.image=[UIImage imageNamed:[arrImgaes objectAtIndex:3]];
            cell.imgVwAllergies.image=[UIImage imageNamed:[arrImgaes objectAtIndex:4]];
            
            
        }
        
        arrImgaes=nil;
        
    }
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self showViewAtIndex:indexPath.row];
    
    
}

#pragma mark- getListFromApi
-(void)getStudentListForChaperone{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/students_by_group_id?group_id=%d&access_token=%@&teacher_id=%@&trip_id=%d",kBaseUrl,x,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],FieldTripId] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                if ([returArray valueForKey:@"success"]) {
                    arrStudentList=[returArray valueForKey:@"result"];
                    [tblVw reloadData];
                    
                    
                    
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }
    else
    {
        
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
}

-(void)showViewAtIndex:(NSInteger) index {
    NSLog(@"Data to show on popup: %@", [arrStudentList objectAtIndex:index]);
    
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    vw.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
    vw.tag=100;
    [self.view addSubview:vw];
    UIView *vwAlerrt=[[UIView alloc]init];
    
    vwAlerrt.frame=CGRectMake(10, ([[UIScreen mainScreen]bounds].size.height/2)-200, [[UIScreen mainScreen]bounds].size.width-20, 450);
    vwAlerrt.layer.cornerRadius=5;
    vwAlerrt.backgroundColor=[UIColor whiteColor];
    
    [vw addSubview:vwAlerrt];
    
    UILabel *lblLine1=[[UILabel alloc]initWithFrame:CGRectMake(0, 40,vwAlerrt.frame.size.width , 1)];
    lblLine1.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine1];
    
    UILabel *lblStudentDetails=[[UILabel alloc]initWithFrame:CGRectMake(0, 10,vwAlerrt.frame.size.width , 30)];
    lblStudentDetails.textColor=[UIColor darkGrayColor];
    lblStudentDetails.text=@"Student Details";
    lblStudentDetails.textAlignment=NSTextAlignmentCenter;
    lblStudentDetails.font=[UIFont fontWithName:@"Roboto-regular" size:18];
    [vwAlerrt addSubview:lblStudentDetails];
    // Text Label Start
    
    
    UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(5, 40, vwAlerrt.frame.size.width-10, vwAlerrt.frame.size.height-85)];
    [lblDetail setTextColor:[UIColor darkGrayColor]];
    [lblDetail setFont:[UIFont fontWithName:@"Roboto-regular" size:16]];
    [lblDetail setNumberOfLines:0];
    
    NSMutableArray *arrDetail = [[NSMutableArray alloc]init];
    [arrDetail addObject:[NSString stringWithFormat:@"  %@",[[arrStudentList objectAtIndex:index] valueForKey:@"name"]]];
    
    NSString *strClass = @"\n\n  Class : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"class"] length] > 0)
    {
        strClass = [strClass stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"class"]];
        [arrDetail addObject:strClass];
    }
    
    NSString *strContact1 = @"\n\n  ICE Contact #1 : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"contact1"] length] > 0)
    {
        strContact1 = [strContact1 stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"contact1"]];
        [arrDetail addObject:strContact1];
    }
    NSString *strContact2 = @"\n\n  ICE Contact #2 : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"contact2"] length] > 0)
    {
        strContact2 = [strContact2 stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"contact2"]];
        [arrDetail addObject:strContact2];
    }
    
    NSString *strPhoto = @"\n\n  Photograph Allowed : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"photography_allowed"] intValue] == 1)
    {
        strPhoto =[strPhoto stringByAppendingString:@"YES"];
        [arrDetail addObject:strPhoto];
    }
    else
    {
        strPhoto =[strPhoto stringByAppendingString:@"NO"];
        [arrDetail addObject:strPhoto];
    }
   

    NSString *strSpecialNeeds = @"\n\n  Special Needs : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"special_need"] intValue] > 0)
    {
        strSpecialNeeds =[strSpecialNeeds stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"special_need_details"]];
        [arrDetail addObject:strSpecialNeeds];
    }
    NSString *strMedicin = @"\n\n  Medicines : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"medicine"] intValue] == 1)
    {
        strMedicin = [strMedicin stringByAppendingString:@"YES"];
        [arrDetail addObject: strMedicin];
    }
    else
    {
        strMedicin = [strMedicin stringByAppendingString:@"NO"];
        [arrDetail addObject: strMedicin];
    }
    NSString *strSpendingMoney = @"\n\n Spending Money : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"spending_money"] intValue] > 0)
    {
        strSpendingMoney = [strSpendingMoney stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"spending_money_amount"]];
        [arrDetail addObject:strSpendingMoney];
    }
    
    NSString *strAllergies = @"\n\n  Allergies : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"allergies"] intValue] > 0)
    {
        strAllergies = [strAllergies stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"allergy_details"]];
        [arrDetail addObject:strAllergies];
    }
    
    NSString *strText; //= [NSString stringWithFormat:@"%@\n\nClass : %@\n\nICE Contact #1 : %@\n\nICE Contact #2 : %@\n\nPhotograph Allowed : %@\n\nSpecial Needs : %@\n\nMedicines : %@\n\nSpending Money : %@\n\nAllergies : %@", [[arrStudentList objectAtIndex:index] valueForKey:@"name"], strClass, strContact1, strContact2, strPhoto, strSpecialNeeds, strMedicin, strSpendingMoney, strAllergies];
    NSMutableString *str = [[NSMutableString alloc]init];
    for (int i=0; i< arrDetail.count; i++) {
        strText = [arrDetail objectAtIndex:i];
        [str appendString:strText];
    }
    [lblDetail setText:str];
    [lblDetail sizeToFit];
    [vwAlerrt addSubview:lblDetail];
    //Text Label End
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, vwAlerrt.frame.size.height-45,vwAlerrt.frame.size.width , 1)];
    lblLine.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine];
    
    UIButton *btnOk=[UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.frame=CGRectMake((vwAlerrt.frame.size.width/2)-60, vwAlerrt.frame.size.height-40, 120, 35);
    [btnOk setTitle:@"OK" forState:UIControlStateNormal];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk setBackgroundColor:[UIColor redColor]];
    [btnOk addTarget:self  action:@selector(DismisstheView:) forControlEvents:UIControlEventTouchUpInside];
    [vwAlerrt addSubview:btnOk];
    
    
}
#pragma mark- DismisstheView
-(void)DismisstheView:(id)sender{
    UIView *removeView;
    while((removeView = [self.view viewWithTag:100]) != nil) {
        [removeView removeFromSuperview];
        
    }
    
    
}
#pragma mark- Addlabel
-(UILabel *)addlabel:(CGRect )frame andvw:(UIView *)vw andtitl:(NSString *)strTitl{
    UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.text=strTitl;
    lbl.textAlignment=NSTextAlignmentLeft;
    
    
    
    [vw addSubview:lbl];
    
    
    return lbl;
    
    
}




@end
