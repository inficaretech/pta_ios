//
//  GroupScoreTableViewCell.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/28/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "GroupScoreTableViewCell.h"

@implementation GroupScoreTableViewCell
@synthesize labelMarks,labelDetail,labelAlphabet,buttonTick;
- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    labelAlphabet.layer.cornerRadius = labelAlphabet.frame.size.width/2 ;
    labelAlphabet.clipsToBounds = YES;
    
//    buttonTick.layer.borderColor = [UIColor blackColor].CGColor;
//    buttonTick.layer.borderWidth = 1.0;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
