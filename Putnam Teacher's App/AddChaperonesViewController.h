//
//  AddChaperonesViewController.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/3/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddChaperonesViewController : UIViewController

@property (nonatomic, assign) BOOL IsEditChaperone;
@property (nonatomic, copy) NSDictionary *dictChaperone;
@property(nonatomic,strong) NSMutableArray *arrfieldTrip;

@end
