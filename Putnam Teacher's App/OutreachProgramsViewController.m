//
//  OutreachProgramsViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/30/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "OutreachProgramsViewController.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import "TTTAttributedLabel.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "CommonMethods.h"
@interface OutreachProgramsViewController ()<MFMailComposeViewControllerDelegate,TTTAttributedLabelDelegate>{
    NSTextContainer *textContainer;
    UILabel *lbl;
    NSLayoutManager *layoutManager;
    NSRange range ;
   
    
}

@end

@implementation OutreachProgramsViewController
@synthesize IsEditStudent;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self CustomNavigationBar];
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
}



-(void)CustomNavigationBar
{
    // [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    // btnBack.backgroundColor = [UIColor redColor];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Outreach Programs";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
    
    [self createUi];
    
}

-(void)BackButtonPressed
{
    if (IsEditStudent) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    textContainer.size = lbl.bounds.size;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark-CreateUi
-(void)createUi{
    
    
//    lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0, 70.0, [[UIScreen mainScreen]bounds].size.width-40, 430)];
//    lbl.numberOfLines=0;
//    lbl.textColor=[UIColor darkGrayColor];
//    lbl.userInteractionEnabled=YES;
//    //lbl.lineBreakMode = NSLineBreakByWordWrapping;
//    lbl.font= [UIFont fontWithName:@"Roboto-regular" size:20.0];
    NSString *myString =@"Putnam outreach programs are engaging, unforgettable learning experiences that happen right in the classroom! Museum educators bring artifacts, experiments and enthusiasm (and the occasional hissing cockroach!) to your class for a lively presentation that is sure to spark the imagination and lead to deeper learning.\n\n For more information on all of our outreach programs please contact our Group Sales Coordinator Alice Loff at aloff@putnam.org or (563) 336-7296.";
    
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:myString];
    
    //Fing range of the string you want to change colour
    //If you need to change colour in more that one place just repeat it
    
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    range=[myString rangeOfString:@"(563) 336-7296"];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    lbl.attributedText=attString;
    [lbl sizeToFit];
    
    
    lbl.adjustsFontSizeToFitWidth=YES;
    int x;
    if (IS_IPAD) {
        x=350;
    }else{
        x=540;
    }
      NSURL *url = [NSURL URLWithString:@"tel:5633367296"];
    
    TTTAttributedLabel *lblAttributed= [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(20.0, 72.0, [[UIScreen mainScreen]bounds].size.width-40, x)];
    [lblAttributed setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    if (IS_IPAD) {
        [lblAttributed setFont:[UIFont fontWithName:@"Roboto-Regular" size:18]];
    }
    [lblAttributed setTextColor:[UIColor darkGrayColor]];
    
    lblAttributed.numberOfLines=0;
    lblAttributed.delegate = self;
    lblAttributed.text=myString;
   //lblAttributed.enabledTextCheckingTypes=NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber;
    [self.view addSubview:lblAttributed];
    [lblAttributed sizeToFit];
     [lblAttributed addLinkToURL:url withRange:range];
    url = [NSURL URLWithString:@"mail"];
     range = [myString rangeOfString:@"aloff@putnam.org"];
    [lblAttributed addLinkToURL:url withRange:range];
    
    
    
    
    
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(CallOnNumber:)];
//    singleTap.numberOfTapsRequired=1;
//    [lbl addGestureRecognizer:singleTap];
   // [self.view addSubview:lbl];
    
    
    
}
/*
-(void)CallOnNumber:(UITapGestureRecognizer *)tapGesture{
    
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGSize labelSize = tapGesture.view.bounds.size;
    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                         locationOfTouchInLabel.y - textContainerOffset.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                            inTextContainer:textContainer
                                   fractionOfDistanceBetweenInsertionPoints:nil];
   // NSRange linkRange = NSMakeRange(14, 4); // it's better to save the range somewhere when it was originally used for marking link in attributed string
    if (NSLocationInRange(indexOfCharacter, range)) {
        // Open an URL, or handle the tap on the link in any other way
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://stackoverflow.com/"]];
    }
    
    
    
    
    
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:5633367296"]];
//    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
//        mail.mailComposeDelegate = self;
//        [mail setToRecipients:@[@"aloff@putnam.org"]];
//        
//        [self presentViewController:mail animated:YES completion:NULL];
//    } else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
//                                                        message:@"Your device doesn't support the composer sheet"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    
    
    
}
 */
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
  NSURL   *url1 = [NSURL URLWithString:@"mail"];
    
    if ([url isEqual:url1]) {
        if ([MFMailComposeViewController canSendMail])
                {
                    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                    mail.mailComposeDelegate = self;
                    [mail setToRecipients:@[@"aloff@putnam.org"]];
            
                    [self presentViewController:mail animated:YES completion:NULL];
                } else
                {
                    
                    [[CommonMethods sharedInstance] AlertAction:@"Please configure your emailid"];
                    
        
                }
 
        
        
    }else{
        
         [[UIApplication sharedApplication] openURL:url];
//        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
//            // Check if iOS Device supports phone calls
//            CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
//            CTCarrier *carrier = [netInfo subscriberCellularProvider];
//            NSString *mnc = [carrier mobileNetworkCode];
//            // User will get an alert error when they will try to make a phone call in airplane mode.
//            if (([mnc length] == 0)) {
//                // Device cannot place a call at this time.  SIM might be removed.
//               // [[CommonMethods sharedInstance] AlertAction:@"Your device doesn't make call right now"];
//
//            } else {
//                // iOS Device is capable for making calls
//                 [[UIApplication sharedApplication] openURL:url];
//            }
//        } else {
//            // iOS Device is not capable for making calls
//             [[CommonMethods sharedInstance] AlertAction:@"Your device doesn't make call right now"];
//        }
//    
   }
    
}


@end
