//
//  FeedbackTableViewCell.h
//  Putnam Teacher's App
//
//  Created by inficare on 8/5/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTableViewCell : UITableViewCell

{
    
}
@property(nonatomic, strong) IBOutlet UIButton *buttonAnswer1;
@property(nonatomic, strong) IBOutlet UIButton *buttonAnswer2;
@property(nonatomic, strong) IBOutlet UIButton *buttonAnswer3;
@property(nonatomic, strong) IBOutlet UIButton *buttonAnswer4;
@property(nonatomic, strong) IBOutlet UIButton *buttonAnswer5;
@property(nonatomic, weak) IBOutlet UILabel *labelQuestion;
@property(nonatomic,weak) IBOutlet UILabel *labelanswer1;
@property(nonatomic,weak) IBOutlet UILabel *labelanswer2;
@property(nonatomic,weak) IBOutlet UILabel *labelanswer3;
@property(nonatomic,weak) IBOutlet UILabel *labelanswer4;
@property(nonatomic,weak) IBOutlet UILabel *labelanswer5;

@end
