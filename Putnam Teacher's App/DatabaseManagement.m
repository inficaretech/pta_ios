//
//  DatabaseManagement.m
//  Water Soldiers
//
//  Created by Inficare on 5/2/16.
//  Copyright © 2016 Inficare. All rights reserved.
//

#import "DatabaseManagement.h"
#import "DataBaseFetchValues.h"


@implementation DatabaseManagement


+(DatabaseManagement *)sharedInstance
{
    static DatabaseManagement *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DatabaseManagement alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}



+ (NSString *)applicationDocumentsDirectoryPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
-(NSString *)getDbPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *str=[path objectAtIndex:0];
    dbPath=[str stringByAppendingPathComponent:@"Teacher.sqlite"];
    return [str stringByAppendingPathComponent:@"Teacher.sqlite"];
}

-(void)SavedataInDatabase:(NSDictionary *)dict{
    
    stmt = nil;
    dbPath=[self getDbPath];
    
    sqlite3_exec(db, "BEGIN", 0, 0, 0);
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK)
    {
        
        NSString *sql=nil;
        
        // NSLog(@"MuseumDetails %@", modelInfo.strClosingDays);
        sql =[NSString stringWithFormat:@"INSERT INTO %@(name,school,email,contact,chaperonIs,id) values(?,?,?,?,?,?)",@"ChaperoneList"];
        
        //sqlite3_bind_int64(stmt, 9, modelInfo.intAlarm);
        //NSLog(@"%@",modelInfo.strClosingDays);
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil)==SQLITE_OK)
        {
        }
        sqlite3_bind_text(stmt, 1, [[dict valueForKey:@"name"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 2,[[dict valueForKey:@"school"]UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 3, [[dict valueForKey:@"email"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 4, [[dict valueForKey:@"contact"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 5,[[dict valueForKey:@"chaperonIs"]integerValue]);
        sqlite3_bind_int64(stmt, 6,[[dict valueForKey:@"id"]integerValue]);
        //        sqlite3_bind_int64(stmt, 6,[[dict valueForKey:@"lat"]floatValue]);
        //        sqlite3_bind_int64(stmt, 7,[[dict valueForKey:@"lang"]floatValue]);
        //        sqlite3_bind_text(stmt, 8,[[dict valueForKey:@"house"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 9,[[dict valueForKey:@"street"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 10,[[dict valueForKey:@"city"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_int64(stmt, 11,[[dict valueForKey:@"zip"]integerValue]);
        //        sqlite3_bind_text(stmt, 12,[[[dict valueForKey:@"picture_present"]stringValue] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 13,[[dict valueForKey:@"ImagePath"] UTF8String], -1, SQLITE_TRANSIENT);
        //              sqlite3_bind_text(stmt, 14, [[dict valueForKey:@"nearest_landmark"] UTF8String], -1, SQLITE_TRANSIENT);
        //           sqlite3_bind_text(stmt, 15,[[dict valueForKey:@"remarks"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 16,[[dict valueForKey:@"name"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 17,[[dict valueForKey:@"phone"] UTF8String], -1, SQLITE_TRANSIENT);
        //              sqlite3_bind_text(stmt, 18,[[dict valueForKey:@"email"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 19,[[dict valueForKey:@"user_house"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 20,[[dict valueForKey:@"user_street"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_text(stmt, 21,[[dict valueForKey:@"user_city"] UTF8String], -1, SQLITE_TRANSIENT);
        //       // sqlite3_bind_text(stmt, 22,[[dict valueForKey:@"user_zip"] UTF8String], -1, SQLITE_TRANSIENT);
        //        sqlite3_bind_int64(stmt, 22,[[dict valueForKey:@"user_zip"]integerValue]);
        //        sqlite3_bind_text(stmt, 23,[[dict valueForKey:@"ticket_id"] UTF8String], -1, SQLITE_TRANSIENT);
        //
        //        sqlite3_bind_text(stmt, 24,[[dict valueForKey:@"other_type"] UTF8String], -1, SQLITE_TRANSIENT);
        //         sqlite3_bind_text(stmt, 25,[[dict valueForKey:@"ImagePath2"] UTF8String], -1, SQLITE_TRANSIENT);
        //         sqlite3_bind_text(stmt, 26,[[dict valueForKey:@"ImagePath3"] UTF8String], -1, SQLITE_TRANSIENT);
        //         sqlite3_bind_text(stmt, 27,[[dict valueForKey:@"ImagePath4"] UTF8String], -1, SQLITE_TRANSIENT);
        //
        //
        sqlite3_step(stmt);
        sqlite3_close(db);
        
        
        if (sqlite3_step(stmt) == SQLITE_DONE)
        {
        }
        else
        {
            sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
        }
        
        
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        
        
        
    }
    
    
}
#pragma mark- SaveStudentList
-(void)SaveStudentListInDatabase:(NSDictionary *)dict{
    
    stmt = nil;
    dbPath=[self getDbPath];
    
    sqlite3_exec(db, "BEGIN", 0, 0, 0);
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK)
    {
        
        NSString *sql=nil;
        
        sql =[NSString stringWithFormat:@"INSERT INTO StudentList (f_name, l_name, contact1, contact2, grade,id) values(?,?,?,?,?,?)"];
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil)==SQLITE_OK)
        {
        }
        sqlite3_bind_text(stmt, 1, [[dict valueForKey:@"f_name"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 2,[[dict valueForKey:@"l_name"]UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 3, [[dict valueForKey:@"contact1"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 4, [[dict valueForKey:@"contact2"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 5,[[dict valueForKey:@"grade"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 6,[[dict valueForKey:@"id"]integerValue]);
        sqlite3_step(stmt);
        sqlite3_close(db);
        
        
        if (sqlite3_step(stmt) == SQLITE_DONE)
        {
        }
        else
        {
            sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
        }
        
        
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        
        
        
    }
    
    
}

-(void)SavedataInDatabaseforChaperoneStudentList:(NSDictionary *)dict{
    
    stmt = nil;
    dbPath=[self getDbPath];
    
    sqlite3_exec(db, "BEGIN", 0, 0, 0);
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK)
    {
        
        NSString *sql=nil;
        
        // NSLog(@"MuseumDetails %@", modelInfo.strClosingDays);
        sql =[NSString stringWithFormat:@"INSERT INTO %@(allergies,allergy_details,class,contact1,contact2,medicine,name,photography_allowed,special_need,special_need_details,spending_money,spending_money_amount) values(?,?,?,?,?,?,?,?,?,?,?,?)",@"Chaperone_Student_List"];
        
        //sqlite3_bind_int64(stmt, 9, modelInfo.intAlarm);
        //NSLog(@"%@",modelInfo.strClosingDays);
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil)==SQLITE_OK)
        {
        }
        sqlite3_bind_int64(stmt, 1,[[dict valueForKey:@"allergies"]integerValue]);
        sqlite3_bind_text(stmt, 2,[[dict valueForKey:@"allergy_details"]UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 3, [[dict valueForKey:@"class"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 4, [[dict valueForKey:@"contact1"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 5, [[dict valueForKey:@"contact2"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 6,[[dict valueForKey:@"medicine"]integerValue]);
        sqlite3_bind_text(stmt, 7, [[dict valueForKey:@"name"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 8,[[dict valueForKey:@"photography_allowed"]integerValue]);
        sqlite3_bind_int64(stmt, 9,[[dict valueForKey:@"special_need"]integerValue]);
        sqlite3_bind_text(stmt, 10, [[dict valueForKey:@"special_need_details"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 11,[[dict valueForKey:@"spending_money"]integerValue]);
        sqlite3_bind_text(stmt, 12, [[dict valueForKey:@"spending_money_amount"] UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        sqlite3_step(stmt);
        sqlite3_close(db);
        
        
        if (sqlite3_step(stmt) == SQLITE_DONE)
        {
        }
        else
        {
            sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
        }
        
        
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        
        
        
    }
    
    
}

-(void)SavedataInDatabaseforQuiz:(NSMutableDictionary *)dict{
    stmt = nil;
    dbPath=[self getDbPath];
    sqlite3_exec(db, "BEGIN", 0, 0, 0);
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK)
    {
        NSString *sql=nil;
        // NSLog(@"MuseumDetails %@", modelInfo.strClosingDays);
        sql =[NSString stringWithFormat:@"INSERT INTO %@(QuestionCategory,Question,Answer1,Answer2,Answer3,Answer4,Correct_answer,Marks,Question_id) values(?,?,?,?,?,?,?,?,?)",@"Quiz_Game"];
        //sqlite3_bind_int64(stmt, 9, modelInfo.intAlarm);
        //NSLog(@"%@",modelInfo.strClosingDays);
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil)==SQLITE_OK)
        {
        }
        sqlite3_bind_text(stmt, 1,[[dict valueForKey:@"QuestionCategory"]UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 2, [[dict valueForKey:@"Question"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 3, [[dict valueForKey:@"Answer1"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 4, [[dict valueForKey:@"Answer2"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 5, [[dict valueForKey:@"Answer3"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 6, [[dict valueForKey:@"Answer4"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(stmt, 7,[[dict valueForKey:@"Correct_answer"]integerValue]);
        sqlite3_bind_int64(stmt, 8,[[dict valueForKey:@"Marks"]integerValue]);
        sqlite3_bind_int64(stmt, 9,[[dict valueForKey:@"Question_id"]integerValue]);
        
        sqlite3_step(stmt);
        sqlite3_close(db);
        if (sqlite3_step(stmt) == SQLITE_DONE)
        {
        }
        else
        {
            sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
        }
        sqlite3_finalize(stmt);
        sqlite3_close(db);
    }
}

-(NSInteger)UpdateDataInDatabaseforQuizForAnswered:(int) Answered andQuestion_id:(int) Question_id{
    NSInteger success = 0;
    stmt = nil;
    dbPath=[self getDbPath];
    sqlite3_exec(db, "BEGIN", 0, 0, 0);
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK)
    {
        NSString *sql = [NSString stringWithFormat:@"update Quiz_Game Set Answered = ? Where Question_id = ?"];
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil)==SQLITE_OK)
        {
            sqlite3_bind_int64(stmt,1, Answered);
            sqlite3_bind_int64(stmt, 2, Question_id);
            
            if (sqlite3_step(stmt) == SQLITE_DONE)
            {
                success++;
            }
            else
            {
                sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
            }
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            return success;
        }
        char *err;
        if(sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) == SQLITE_OK)
        {
            success++;
        }
        else
        {
            sqlite3_close(db);
        }
    }
    return success;
}
@end
