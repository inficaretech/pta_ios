//
//  GroupDetailsTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 7/1/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwCamera;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwSpecialNeeds;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwMedicines;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwMoney;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwAllergies;
@property (weak, nonatomic) IBOutlet UILabel *lblStudentName;

@end
