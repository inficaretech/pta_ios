//
//  PanormaViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 7/6/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "PanormaViewController.h"
#import "PanoramaView.h"

@interface PanormaViewController (){
    PanoramaView *panoramaView;
    
}

@end

@implementation PanormaViewController
@synthesize workingImage;
@synthesize strNavTitil;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self UiForNavigationBar];
      // [[self navigationController] setNavigationBarHidden:YES animated:YES];
    panoramaView = [[PanoramaView alloc] init];
  // [panoramaView setImageWithName:@"image.png"];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",workingImage]];
    NSData *pngData=[NSData dataWithContentsOfFile:localFilePath];

   // NSData *pngData = [NSData dataWithContentsOfFile:workingImage];
    UIImage *image = [UIImage imageWithData:pngData];
    [panoramaView setImage:image];
    [panoramaView setOrientToDevice:YES];
    [panoramaView setTouchToPan:YES];
    [panoramaView setPinchToZoom:YES];
    [panoramaView setShowTouches:NO];
        [self setView:panoramaView];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) glkView:(GLKView *)view drawInRect:(CGRect)rect{
    [panoramaView draw];
}
#pragma mark- Customnavigationbar
-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =strNavTitil;
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}




@end
