//
//  ChaperoneListTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/3/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ChaperoneListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblGmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhonenumber;
@property (weak, nonatomic) IBOutlet UILabel *lblColour;
@property (weak, nonatomic) IBOutlet UILabel *lblfieldTrip;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupName;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UILabel *lblAccesscode;

@end
//lblfieldTrip