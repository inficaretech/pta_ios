//
//  CloseFieldTripViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 7/8/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "CloseFieldTripViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "FeedBackViewController.h"

@interface CloseFieldTripViewController ()

@end

@implementation CloseFieldTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self UiForNavigationBar];
    [self CreateUI];
    // Do any additional setup after loading the view.
}


-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Close Field Trip";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}
//f1eeee

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)CreateUI{
    
    if (IS_IPHONE_5)
    {
        
        UIView *labelDetail = [[UIView alloc]init];
        labelDetail.frame = CGRectMake(20.0, 82.0, [[UIScreen mainScreen] bounds].size.width-40, [[UIScreen mainScreen ] bounds].size.height/2-60);
        [self.view addSubview:labelDetail];
        labelDetail.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
        
        
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(10.0, 30.0, labelDetail.frame.size.width-20, labelDetail.frame.size.height-50);
        [labelDetail addSubview:label];
        label.text= @"1. Remove all Chaperon's access to the student data\n\n2. All Students data will be delelted\n\n3. School name, grade, fieldtrip date, highest score, and related information may be saved on the Putnam Wall of Fame page";
        label.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = KTextColor;
        
        UIButton *buttonCLoseFieldTrip = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonCLoseFieldTrip.frame = CGRectMake(20.0, [[UIScreen mainScreen] bounds].size.height/2+100,[[UIScreen mainScreen] bounds].size.width-40, 40.0);
        [buttonCLoseFieldTrip setTitle:@"Close this Field Trip" forState:UIControlStateNormal];
        [self.view addSubview:buttonCLoseFieldTrip];
        buttonCLoseFieldTrip.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        [buttonCLoseFieldTrip setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
        buttonCLoseFieldTrip.layer.shadowColor = [UIColor grayColor].CGColor;
        buttonCLoseFieldTrip.layer.shadowOffset = CGSizeMake(0, 1.0);
        [buttonCLoseFieldTrip addTarget:self action:@selector(GotoHomeview:) forControlEvents:UIControlEventTouchUpInside];
        buttonCLoseFieldTrip.layer.shadowOpacity = 1.0;
        buttonCLoseFieldTrip.layer.shadowRadius = 4.0;
    }
    else{
    UIView *labelDetail = [[UIView alloc]init];
    labelDetail.frame = CGRectMake(20.0, 82.0, [[UIScreen mainScreen] bounds].size.width-40, [[UIScreen mainScreen ] bounds].size.height/2-150);
    [self.view addSubview:labelDetail];
    labelDetail.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    
    
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(10.0, 30.0, labelDetail.frame.size.width-20, labelDetail.frame.size.height-50);
    [labelDetail addSubview:label];
    label.text= @"1. Remove all Chaperon's access to the student data\n\n2. All Students data will be delelted\n\n3.  School name, grade, fieldtrip date, highest score, and related information may be saved on the Putnam Wall of Fame page";
    label.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    label.numberOfLines = 0;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = KTextColor;
    
    UIButton *buttonCLoseFieldTrip = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonCLoseFieldTrip.frame = CGRectMake(20.0, [[UIScreen mainScreen] bounds].size.height/2,[[UIScreen mainScreen] bounds].size.width-40, 40.0);
    [buttonCLoseFieldTrip setTitle:@"Close this Field Trip" forState:UIControlStateNormal];
    [self.view addSubview:buttonCLoseFieldTrip];
        
         [buttonCLoseFieldTrip addTarget:self action:@selector(GotoHomeview:) forControlEvents:UIControlEventTouchUpInside];
    buttonCLoseFieldTrip.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    [buttonCLoseFieldTrip setBackgroundColor:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
    buttonCLoseFieldTrip.layer.shadowColor = [UIColor grayColor].CGColor;
    buttonCLoseFieldTrip.layer.shadowOffset = CGSizeMake(0, 1.0);
    buttonCLoseFieldTrip.layer.shadowOpacity = 1.0;
    buttonCLoseFieldTrip.layer.shadowRadius = 4.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- GotoHomeview
-(void)GotoHomeview:(id)sender{
    [self closefieldtripApi];
   // [self.navigationController popToRootViewControllerAnimated:YES];

    
}

-(void)closefieldtripApi{
    //Itineraries/close_trip?trip_id=283&access_token=KkmlFfJgPbSGd56joc4U
    //tripFieldIdname
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Itineraries/close_trip?trip_id=%@&access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"tripFieldIdname"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];

                                      
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    FeedBackViewController *controller = [story instantiateViewControllerWithIdentifier:@"FeedBackViewController"];
                    [self.navigationController pushViewController:controller animated:YES ];
                  
                    
                }
                else
                {
                }
            }];
        } else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
            
        }
    
 
    
}
 

@end
