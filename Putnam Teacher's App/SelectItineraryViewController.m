//
//  SelectItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/9/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "SelectItineraryViewController.h"
#import "SelectItinerayTableViewCell.h"
#import "AddToItineraryViewController.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

@interface SelectItineraryViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    UITableView *tableViewItinerary;
    WebService *webService;
    NSMutableArray *arrImages;
    NSMutableArray *arrSelectedImages;
}
@end

@implementation SelectItineraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton=YES;
    [self UiForNavigationBar];
    [self ApiForGettingCategory];
    [self CreateUI];
    arrSelectedImages = [[NSMutableArray alloc]init];
}


-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

#pragma back button method
-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)CreateUI
{
    btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
    btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
    btnBuildItinerary.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBuildItinerary];
    btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnCurrentItinerary setTitle:@"Current Itinerary" forState:UIControlStateNormal];
    [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
    btnCurrentItinerary.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [self.view addSubview:btnCurrentItinerary];
    btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    tableViewItinerary = [[UITableView alloc]init];
    if (IS_IPHONE_6) {
        tableViewItinerary.frame = CGRectMake(0.0,113.0 , [[UIScreen mainScreen] bounds].size.width, 430.0);

    }
    else
    {
    tableViewItinerary.frame = CGRectMake(0.0,113.0 , [[UIScreen mainScreen] bounds].size.width, 350.0);
    }
    [self.view addSubview:tableViewItinerary];
    tableViewItinerary.delegate=self;
    tableViewItinerary.dataSource=self;
    tableViewItinerary.separatorColor= [UIColor clearColor];
    tableViewItinerary.allowsMultipleSelection=YES;
    
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-30,[[UIScreen mainScreen] bounds].size.height-80, 60, 60);
    
    [btnNext setImage: [UIImage imageNamed:@"next_button"] forState:UIControlStateNormal];
    
    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
    [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-45.0, 0.0,0.0)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnNext];
    [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];

    
    
}

-(void)ApiForGettingCategory
{
    
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet)
    {
      [[CommonMethods sharedInstance] addSpinner:self.view];
       //  NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Categories/get_all_categories?&access_token=%@"[[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"];
        
      [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Categories/get_all_categories?&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSMutableArray *arrResponse=[returArray mutableCopy];
                arrImages = [[NSMutableArray alloc]init];
                arrImages = [arrResponse valueForKey:@"result"];
                [tableViewItinerary reloadData];
            }
            
            
            
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                }];
            }
        }];
        
        
    }
    
    
}

        
        



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrImages count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SelectItinerayTableViewCell";
    
    SelectItinerayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SelectItinerayTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
     cell.imageTick.image = [UIImage imageNamed:@"Tick_mark_green"];
     BOOL found = NO;
    for(NSMutableDictionary *dict in arrSelectedImages)
    {
        if([[dict valueForKey:@"id"] isEqualToString:[[arrImages objectAtIndex:indexPath.row] valueForKey:@"id"]])
        {
            NSURL *url =[[arrImages objectAtIndex:indexPath.row]valueForKey:@"hover_image"];
            [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            cell.imageTick.hidden = NO;
            cell.labelItineraryName.textColor = kBtnSelectionColor;
            found = YES;
            break;
        }
        
 
    }
    
    if(!found){
    NSURL *url =[[arrImages objectAtIndex:indexPath.row]valueForKey:@"basic_image"];
    [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    cell.labelItineraryName.text = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"category"];
    cell.imageTick.hidden = YES;
        cell.labelItineraryName.textColor = KTextColor;
    }
    cell.imageIconItinerary.tag = indexPath.row;
    
    

    
      return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6) {
        return 120;
    }
    else
    {
    return 90;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectItinerayTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //[tableViewItinerary cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    NSURL *url = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"hover_image"];
    [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    cell.labelItineraryName.textColor = kBtnSelectionColor;
    cell.imageTick.hidden = NO;
    
    
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setObject:[[arrImages objectAtIndex:indexPath.row]valueForKey:@"id" ]  forKey:@"id"];
    [arrSelectedImages addObject:dict];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectItinerayTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSURL *url = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"basic_image"];
    [cell.imageIconItinerary sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    cell.labelItineraryName.textColor = [UIColor grayColor];
    cell.imageTick.hidden = YES;
    
    NSString *str = [[arrImages objectAtIndex:indexPath.row]valueForKey:@"id"];
    for (NSMutableDictionary *dict in arrSelectedImages) {
        if ([[dict valueForKey:@"id"] isEqualToString:str]) {
            [arrSelectedImages removeObject:dict];
            break;
        }
    }
    
}

-(void)goToNextView
{if (arrSelectedImages.count==0)
{
    
}
else{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        
        [[CommonMethods sharedInstance] addSpinner:self.view];
        NSMutableString * strSelected = [[NSMutableString alloc]init];
        
        for (int i=0 ; i<[arrSelectedImages count] ; i++) {
            
            [strSelected appendString:[[arrSelectedImages objectAtIndex:i]objectForKey:@"id" ]];
            if (i != arrSelectedImages.count-1) {
                [strSelected appendString:@","];
            }
            
            //[location addObject:images];
            
            
        }
        
        [[WebService sharedInstance] fetchCategoryDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/SubcategoriesAPI/get_sub_categories", kBaseUrl] param:strSelected andcompletionhandler:^(NSArray *returArray, NSError *error ){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([errorstr isEqualToString:@"already exist"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                    return;
                    
                    
                }else if([returArray valueForKey:@"success"]){
                    
                    
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    arr = [returArray valueForKey:@"result"] ;
                    // [self.navigationController popViewControllerAnimated:YES];
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    AddToItineraryViewController *controller = [story instantiateViewControllerWithIdentifier:@"AddToItineraryViewController"];
                    controller.arrResponse = arr;
                    [self.navigationController pushViewController:controller animated:YES];
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
    }
    
}
}

-(void)BulidItinerary
{
    btnCurrentItinerary.backgroundColor =kBtnUnselectedColor;
    btnBuildItinerary.backgroundColor =kBtnSelectionColor;
}
-(void)CurrentItinerary
{
    
    btnBuildItinerary.backgroundColor =kBtnUnselectedColor;
    btnCurrentItinerary.backgroundColor =kBtnSelectionColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
