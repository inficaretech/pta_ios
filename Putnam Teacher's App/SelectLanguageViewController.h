//
//  SelectLanguageViewController.h
//  Putnam Teacher's App
//
//  Created by inficare on 8/1/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLanguageViewController : UIViewController

@property (strong, nonatomic) NSArray *languageOptions;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewLogo;
@property (weak, nonatomic) IBOutlet UIButton *_btnSelectLanguage;
- (IBAction)btnPressed:(id)sender;
//-(void)callRegistration:(NSString *)devicetoken;
@end
