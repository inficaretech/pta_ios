//
//  AddToItineraryViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 6/14/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AddToItineraryViewController.h"
#import "Constants.h"

@interface AddToItineraryViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIButton *btnBuildItinerary;
    UIButton *btnCurrentItinerary;
    UITableView *tableAddToItinerary;
    BOOL isClicked;
    NSInteger indexPathSection;
    UIButton *btnPlus;
    UIButton *btnSection;
    UILabel *lbl;
}
@end

@implementation AddToItineraryViewController
@synthesize arrResponse;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self UiForNavigationBar];
    [self CreateUI];
    // Do any additional setup after loading the view.
}

-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"My Itinerary";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}


-(void)CreateUI
{
    btnBuildItinerary= [UIButton buttonWithType:UIButtonTypeCustom];
    btnBuildItinerary.frame = CGRectMake(0.0, 64.0, [[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnBuildItinerary setTitle:@"Build Itinerary" forState:UIControlStateNormal];
    btnBuildItinerary.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    [btnBuildItinerary addTarget:self action:@selector(BulidItinerary) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBuildItinerary];
    btnBuildItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    btnCurrentItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCurrentItinerary.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64.0,[[UIScreen mainScreen] bounds].size.width/2, 49.0);
    [btnCurrentItinerary setTitle:@"Current Itinerary" forState:UIControlStateNormal];
    [btnCurrentItinerary addTarget:self action:@selector(CurrentItinerary) forControlEvents:UIControlEventTouchUpInside];
    btnCurrentItinerary.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [self.view addSubview:btnCurrentItinerary];
    btnCurrentItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    tableAddToItinerary = [[UITableView alloc]init];
    tableAddToItinerary.frame = CGRectMake(0.0, 113.0, [[UIScreen mainScreen] bounds].size.width, 350.0);
    [self.view addSubview:tableAddToItinerary];
    tableAddToItinerary.separatorColor=[UIColor clearColor];
    tableAddToItinerary.delegate=self;
    tableAddToItinerary.dataSource=self;
    
    
    UILabel *labelChooseDate = [[UILabel alloc]init];
    labelChooseDate.frame = CGRectMake(20.0, [[UIScreen mainScreen ] bounds].size.height-100, [[UIScreen mainScreen] bounds].size.width/2, 30);[self.view addSubview:labelChooseDate];
    labelChooseDate.text =@"Choose your Date *";
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableAddToItinerary.frame.size.width-15, 60)];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0,tableAddToItinerary.frame.size.width , 15)];
    label.backgroundColor = [UIColor whiteColor];
    [viewHeader addSubview:label];
    btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPlus.frame = CGRectMake(15.0, 15.0, 25, viewHeader.frame.size.height-15);
    btnPlus.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:28.0/255.0 blue:35.0/255.0 alpha:1];
    
     if (!isClicked) {
        [btnPlus setTitle:@"+" forState:UIControlStateNormal];
    }
    else
    {     if(indexPathSection==section)
    {
        [btnPlus setTitle:@"-" forState:UIControlStateNormal];
    }
    else{
        [btnPlus setTitle:@"+" forState:UIControlStateNormal];
    }
    }
    btnPlus.tag=section;
    btnPlus.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnPlus addTarget:self action:@selector(OpenDetailsForTryThis:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btnPlus];
    
    NSString *strTitle = @"Science Center";
    btnSection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSection.frame = CGRectMake(40.0, 15.0, viewHeader.frame.size.width-40, viewHeader.frame.size.height-15);
    btnSection.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:28.0/255.0 blue:35.0/255.0 alpha:1];
    [btnSection setTitle:strTitle  forState:UIControlStateNormal];
    btnSection.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    btnSection.tag=section;
    [btnSection addTarget:self action:@selector(OpenDetailsForTryThis:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btnSection];
    
//    btnSection.layer.shadowColor = [UIColor grayColor].CGColor;
//    btnSection.layer.shadowOffset = CGSizeMake(0, 1.0);
//    btnSection.layer.shadowOpacity = 1.0;
//    btnSection.layer.shadowRadius = 3.0;
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor whiteColor];

    lbl = [[UILabel alloc]init];
    lbl.frame = CGRectMake(15.0, 7.0, tableAddToItinerary.frame.size.width-30, 120);
    [cell addSubview:lbl];
    
    
    UIButton *btnAddToItinerary = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddToItinerary.frame = CGRectMake(lbl.frame.size.width/2-60.0, lbl.frame.size.height-40, 120.0, 30.0);
    [lbl addSubview:btnAddToItinerary];
    [btnAddToItinerary setTitle:@"Add to itinerary" forState:UIControlStateNormal];
    [btnAddToItinerary setBackgroundColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1]];
    btnAddToItinerary.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    
    
  btnAddToItinerary.layer.shadowColor = [UIColor grayColor].CGColor;
btnAddToItinerary.layer.shadowOffset = CGSizeMake(0, 1.0);
    btnAddToItinerary.layer.shadowOpacity = 1.0;
    btnAddToItinerary.layer.shadowRadius = 10.0;

    
    [self addTopBorder:lbl];
    [self addlefBorder:lbl];
    [self addRightBorder:lbl];
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (isClicked) {
        if (indexPathSection==section) {
            return 1;
            //s viewCell=nil;
        }
        return 0;
    }
    else
        return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}



-(void)OpenDetailsForTryThis:(id)sender
{
    
    UIButton *btn=(id)sender;
    
    if (btn.tag==indexPathSection) {
        if (!isClicked) {
            isClicked=YES;
            indexPathSection = btn.tag;
            [tableAddToItinerary reloadData];
            
        }
        else
        {
            isClicked=NO;
//            [btnSection setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
//          [btnPlus setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
            [tableAddToItinerary reloadData];
        }
        
    }else{
        isClicked=YES;
        indexPathSection = btn.tag;
        NSLog(@"%ld",(long)indexPathSection);
        [btnSection setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
        [btnPlus setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
       [tableAddToItinerary reloadData];
        
        
        
        
    }
    
    
}

-(void)addTopBorder:(UILabel *)view{
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderWidth = 1;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, mainViewSize.width, borderWidth)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}

-(void)addlefBorder:(UILabel *)view{
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderheight = mainViewSize.height;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0,1, borderheight+20)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
    [self addBottomBorder:view];
}
-(void)addBottomBorder:(UILabel *)view{
    CGSize mainViewSize = view.bounds.size;
    CGFloat borderWidth = 1;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, mainViewSize.height+2, lbl.frame.size.width, borderWidth)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}
-(void)addRightBorder:(UILabel *)view{
    CGSize mainViewSize = view.bounds.size;
    // CGFloat borderWidth = 1;
    CGFloat borderheight = lbl.frame.size.height;
    UIColor *borderColor = [UIColor colorWithRed:98.0/255.0 green:103.0/255.0 blue:119.0/255.0 alpha:1];//98 103 119
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width-1, 0.0, 1, borderheight+21)];
    topView.opaque = YES;
    topView.backgroundColor = borderColor;
    topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:topView];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)BulidItinerary
{
    
}

-(void)CurrentItinerary
{
    
}


@end
