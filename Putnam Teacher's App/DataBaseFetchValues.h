//
//  DataBaseFetchValues.h
//  Water Soldiers
//
//  Created by Inficare on 5/2/16.
//  Copyright © 2016 Inficare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DataBaseFetchValues : NSObject{
    
    NSString					*databaseName;
    NSString					*databasePath;
    sqlite3_stmt				*addStmt;
    sqlite3						*database;
    NSMutableDictionary         *currentRow;
    NSString                    *columnData;
    NSString                    *columnName;
    NSMutableArray              *tagsValuesArr;

}

@property(nonatomic,retain)NSMutableArray    *tagsValuesArr;
+(DataBaseFetchValues *) sharedInstance;
- (NSMutableArray *)fetchAllDataWithTableName:(NSString *)tblName;
-(void)deleteQuerry:(NSString*)_query;
-(void)UpdateWithTableName:(NSString *)Oldtkt andNewTkt:(NSString *)newTkt;
@end
