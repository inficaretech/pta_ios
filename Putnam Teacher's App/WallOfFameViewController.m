//
//  WallOfFameViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/30/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "WallOfFameViewController.h"
#import "CustomCellWallOfFameTableViewCell.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "Constants.h"

@interface WallOfFameViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tableViewWallOffame;
    NSArray *arrOfWalloffame;
}
@end

@implementation WallOfFameViewController
@synthesize IsEditStudent;
- (void)viewDidLoad {
    [super viewDidLoad];
    arrOfWalloffame=[[NSArray alloc]init];
    
    [self CustomNavigationBar];
    [self setNeedsStatusBarAppearanceUpdate];
    [self CreateUI];
    [self GetResponseFromApi];
    
    // UINib *nib = [UINib nibWithNibName:@"CustomCellWallOfFameTableViewCell" bundle:nil];
    // [tableViewWallOffame registerNib:nib forCellReuseIdentifier:@"CustomCellWallOfFameTableViewCell"];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)CustomNavigationBar
{
    // [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Wall of Fame";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
}

-(void)CreateUI
{
    
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentJustified;
    style.firstLineHeadIndent = 15.0f;
    style.headIndent = 15.0f;
    style.tailIndent = -15.0f;
    NSString *title = @"Looking forward to see your school trip on this Wall of Fame!!!";
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:title attributes:@{ NSParagraphStyleAttributeName : style}];
    
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(0.0, 64.0, self.view.frame.size.width, 50.0);
    [self.view addSubview:label];
    label.attributedText = attrText;
    label.font = [UIFont fontWithName:@"Roboto-regular" size:14];
    label.backgroundColor = [UIColor colorWithRed:149.0/255.0 green:149.0/255.0 blue:149.0/255.0 alpha:1.0];
    label.numberOfLines=2;
    label.textColor = [UIColor whiteColor];
    
    
    label.layer.shadowColor = [UIColor grayColor].CGColor;
    label.layer.shadowOffset = CGSizeMake(0, 1.0);
    label.layer.shadowOpacity = 1.0;
    label.layer.shadowRadius = 3.0;
    
    
    
    
    tableViewWallOffame = [[UITableView alloc]init];
    tableViewWallOffame.frame = CGRectMake(0.0, 114.0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:tableViewWallOffame];
    tableViewWallOffame.separatorStyle=UITableViewCellSeparatorStyleNone;
    tableViewWallOffame.delegate=self;
    tableViewWallOffame.dataSource =self;
    tableViewWallOffame.userInteractionEnabled= NO;
}

-(void)BackButtonPressed
{
    if (IsEditStudent) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOfWalloffame.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomCellWallOfFameTableViewCell";
    
    CustomCellWallOfFameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    UILabel *lblScore;
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        lblScore=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-45, 15, 40, 40)];
        
        lblScore.layer.masksToBounds = YES;
        lblScore.layer.cornerRadius = 20;
        lblScore.layer.borderColor = [UIColor darkGrayColor].CGColor;
        lblScore.layer.borderWidth = 1;
        lblScore.textAlignment=NSTextAlignmentCenter;
        lblScore.textColor=[UIColor darkGrayColor];
        [cell addSubview:lblScore];
        
    }
    if (arrOfWalloffame.count>0) {
        
        
        NSString *str=[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"teacher"];
        
        if ([str isKindOfClass:[NSNull class]]) {
            str = @"N/A";
        }
        else
        {
            str= [str stringByReplacingOccurrencesOfString:@"`" withString:@" "];
        }
        NSLog(@"%@",str);
        
        
        NSArray *subStrings = [str componentsSeparatedByString:@" "]; //or rather @" - "
        NSString *firstString = [subStrings objectAtIndex:0];
        NSString *lastString = [subStrings objectAtIndex:1];
        
        str = [firstString substringWithRange:NSMakeRange(0, 1)];
        str = [str stringByAppendingString:@". "];
        str = [str stringByAppendingString:lastString];

        NSString *joinString=[NSString stringWithFormat:@"%@\n%@",str,@""];
        
        lblScore.text=[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"score"];
        
        NSString *str1=[NSString stringWithFormat:@"%@",[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"school"]];
        
        if ([str1 isKindOfClass:[NSNull class]] || [str1 isEqualToString:@"<null>"]) {
            str1 =@"N/A";
        }
        joinString = [joinString stringByAppendingString:str1];
        cell.lblDetail.font = [UIFont fontWithName:@"Roboto-regular" size:15];
        NSRange range = NSMakeRange(0, str.length);
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:joinString];
        
        
        
        [attString addAttribute:NSFontAttributeName  value:[UIFont fontWithName:@"Roboto-regular" size:20] range:range];
        cell.lblAlphabet.text =[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"grade"];
        cell.lblAlphabet.textColor = [UIColor whiteColor];
        cell.lblAlphabet.textAlignment = NSTextAlignmentCenter;
        cell.lblDetail.textColor=[UIColor darkGrayColor];
        cell.lblDetail.attributedText =attString;
        cell.lblDetail.numberOfLines = 2;
        
        for (int k=0; k<=arrOfWalloffame.count; k+=4) {
            if (indexPath.row==k) {
                [attString addAttribute:NSForegroundColorAttributeName value: [UIColor colorWithRed:139.0/255.0 green:195.0/255.0 blue:74.0/255.0 alpha:1] range:range];
                cell.lblAlphabet.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:195.0/255.0 blue:74.0/255.0 alpha:1];
                
            }else if (indexPath.row==k+1){
                [attString addAttribute:NSForegroundColorAttributeName value: [UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1] range:range];
                cell.lblAlphabet.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1];
                
            }else if (indexPath.row==k+2){
                [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:174.0/255.0  blue:239.0/255.0 alpha:1] range:range];
                cell.lblAlphabet.backgroundColor =[UIColor colorWithRed:0 green:174.0/255.0  blue:239.0/255.0 alpha:1];
            }else if (indexPath.row==k+3){
                [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:248.0/255.0 green:88.0/255.0 blue:37.0/255.0 alpha:1] range:range];
                cell.lblAlphabet.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:88.0/255.0 blue:37.0/255.0 alpha:1];
                
                
            }
            
        }
        
        
    }
    
    
    //    if (arrOfWalloffame.count>0) {
    //        NSString *joinString=[NSString stringWithFormat:@"%@, %@, %@\n%@",[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"name"],[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"group"],[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"score"],[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"school"]];
    //        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:joinString];
    //        NSRange range = [joinString rangeOfString:[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"name"]];
    //        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    //
    //    cell.lblAlphabet.text =[[[[arrOfWalloffame objectAtIndex:indexPath.row]valueForKey:@"name"] substringToIndex:1] uppercaseString];
    //    cell.lblAlphabet.backgroundColor = [UIColor greenColor];
    //    cell.lblAlphabet.textColor = [UIColor whiteColor];
    //    cell.lblAlphabet.textAlignment = NSTextAlignmentCenter;
    //   cell.lblDetail.attributedText =attString;
    //    cell.lblDetail.numberOfLines = 2;
    //    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 73;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-GetApiofWallofFame
-(void)GetResponseFromApi{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Wallframes/index",kBaseUrl] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                //                arrListOfChaperones=[returArray mutableCopy];
                //                [tblVw reloadData];
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                arrOfWalloffame=[arr mutableCopy];
                [tableViewWallOffame reloadData];
                
            }
            
            
            
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
        
        
        
    }
    
    
}



@end