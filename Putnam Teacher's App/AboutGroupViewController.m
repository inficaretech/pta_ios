//
//  AboutGroupViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/13/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AboutGroupViewController.h"
#import "GroupDetailsViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "ManageChaperonesViewController.h"
#import "Constants.h"

@interface AboutGroupViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *arrGroups;
    UITableView *tblVw;
}

@end

@implementation AboutGroupViewController
@synthesize FieldTripId;
- (void)viewDidLoad {
    [super viewDidLoad];
    //arrGroups =[[NSMutableArray alloc]initWithObjects:@"Group1",@"Group2",@"Group3",@"Group4",@"Group5",@"Group6", nil];
    self.navigationItem.title=@"Group - Students";
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
   
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getGroupListFromApi];
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark-CreateUi
-(void)createUi{
    
    tblVw=[[UITableView alloc]initWithFrame:CGRectMake(0.0, 62.0, [[UIScreen mainScreen]bounds].size.width,  [[UIScreen mainScreen]bounds].size.height) ];
    tblVw.delegate=self;
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    tblVw.dataSource=self;
    [self.view addSubview:tblVw];
//    if (IS_IPHONE_5) {
//        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-80,70 ,70 )];
//    }else{
//        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
//    }

    tblVw.contentInset = UIEdgeInsetsMake(0.0, 0.0, 110, 0.0);
    
    
    UIButton *buttonAssignStudentsToGroup = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAssignStudentsToGroup.frame =CGRectMake(40.0, [[UIScreen mainScreen] bounds].size.height-60, [[UIScreen mainScreen] bounds].size.width-80, 40);
    
    [self.view addSubview:buttonAssignStudentsToGroup];
    buttonAssignStudentsToGroup.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    [buttonAssignStudentsToGroup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonAssignStudentsToGroup addTarget:self action:@selector(buttonAssignStudentsToGroupPressed) forControlEvents:UIControlEventTouchUpInside];
    [buttonAssignStudentsToGroup setBackgroundColor:[UIColor  colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
    
    buttonAssignStudentsToGroup.layer.shadowColor = [UIColor grayColor].CGColor;
    buttonAssignStudentsToGroup.layer.shadowOffset = CGSizeMake(0, 1.0);
    buttonAssignStudentsToGroup.layer.shadowOpacity = 2.0;
    buttonAssignStudentsToGroup.layer.shadowRadius = 3.0;
    
    
    [buttonAssignStudentsToGroup setTitle:@"Assign Chaperones to Groups" forState:UIControlStateNormal];
    
}

-(void)buttonAssignStudentsToGroupPressed
{
   // BOOL isStudent=NO;
    UIStoryboard *story =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ManageChaperonesViewController *controller = [story instantiateViewControllerWithIdentifier:@"ManageChaperonesView"];
   // controller.IsStudent = isStudent;
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- getGroupList
-(void)getGroupListFromApi{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/GroupsAPI/get_all_groups?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
              //  NSLog(@"returArray %@",returArray);
                arrGroups = [[NSMutableArray alloc]init];
                if (!(returArray.count==0)) {
                    NSArray *arr = [[returArray valueForKey:@"result"]mutableCopy];
                  //  arrGroups=[arr mutableCopy];
                    // NSString *errorstr = [returArray valueForKey:@"error"];
                    for (int i =0; i<arr.count; i++) {
                        if (![[[arr objectAtIndex:i]valueForKey:@"total_students"] isEqualToString:@"0"] || !([[[arr objectAtIndex:i]valueForKey:@"chaperone"] length]==0)) {
                             [arrGroups addObject:[arr objectAtIndex:i]];
                        }
                        
                }
                
                }
                 [self createUi];
                
               // [tblVw reloadData];
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    
    
    
}

#pragma mark- TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrGroups.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier=@"Cell";
    UILabel *lblChaperone,*LblGroupName,*lblCount;
    UIImageView *ImGV;
    
    UITableViewCell *Cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (Cell==nil) {
        Cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    UILabel *LblLine=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 69, tableView.frame.size.width, 1)];
        LblLine.backgroundColor=[UIColor lightGrayColor];
        [Cell.contentView addSubview:LblLine];
        LblGroupName=[[UILabel alloc]initWithFrame:CGRectMake(70.0, 10.0, 200.0, 20.0)];
        lblChaperone=[[UILabel alloc]initWithFrame:CGRectMake(70, 35, [[UIScreen mainScreen]bounds].size.width-130, 30)];
        lblChaperone.textColor=[UIColor darkGrayColor];
        lblChaperone.font=[UIFont fontWithName:@"Roboto-regular" size:13];
        LblGroupName.font=[UIFont fontWithName:@"Roboto-regular" size:13];
        LblGroupName.textColor=[UIColor darkGrayColor];
        [Cell.contentView addSubview:lblChaperone];
        [Cell.contentView addSubview:LblGroupName];
        
        lblCount=[[UILabel alloc]init];
        lblCount.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-60, 15, 40, 40);
        lblCount.layer.cornerRadius=lblCount.frame.size.height/2;
        lblCount.clipsToBounds=YES;
        lblCount.layer.borderColor=[UIColor darkGrayColor].CGColor;
        lblCount.layer.borderWidth=1;
        [Cell.contentView addSubview:lblCount];
        lblCount.textAlignment=NSTextAlignmentCenter;
       

        ImGV =[[UIImageView alloc]initWithFrame:CGRectMake(10.0, 10.0, 50.0, 50)];
        [Cell.contentView addSubview:ImGV];
        
        
    }
    
    for (int k=0; k<=arrGroups.count; k+=4) {
        if (indexPath.row==k) {
            ImGV.image=[UIImage imageNamed:@"icon"];
        }else if (indexPath.row==k+1){
            ImGV.image=[UIImage imageNamed:@"icon1"];
        }else if (indexPath.row==k+2){
            ImGV.image=[UIImage imageNamed:@"icon2"];
        }else if (indexPath.row==k+3){
            ImGV.image=[UIImage imageNamed:@"icon3"];
            
        }
        
    }

    
    if (arrGroups.count>0) {
        lblCount.text=[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"total_students"];
        NSString *strFullgroupName=[@"Group - " stringByAppendingString:[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"name"]] ;
        
        
        
        
        NSString *chaperoneName=[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"chaperone"];
       
        NSString *strChaperoneName;
        if (chaperoneName.length>0) {
            NSMutableString * string = [chaperoneName mutableCopy];
            [string replaceOccurrencesOfString:@"`" withString:@" " options:0 range:NSMakeRange(0, string.length)];
            strChaperoneName=[@"Chaperone : " stringByAppendingString:string] ;
        }else{
            strChaperoneName=[@"Chaperone : " stringByAppendingString:@"Not yet assigned"];
        }
        
        
        // LblGroupName.text=[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"name"];
        LblGroupName.text=strFullgroupName;
        LblGroupName.font = [UIFont fontWithName:@"Roboto-regular" size:15];
        lblChaperone.text=strChaperoneName;
    }

    
    
    Cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return Cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // GroupDetailsView
    
    NSString *strFullgroupName=[@"Group-" stringByAppendingString:[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"name"]];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GroupDetailsViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"GroupDetailsView"];
    Controller.x=[[[arrGroups objectAtIndex:indexPath.row] valueForKey:@"id"]intValue];
    Controller.strNavigationtitl=strFullgroupName;
    Controller.FieldTripId=FieldTripId;
    [self.navigationController pushViewController:Controller animated:YES ];
    
    
    
}
@end
