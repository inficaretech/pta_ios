//
//  QuizPageViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/2/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "QuizPageViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "DatabaseManagement.h"
#import "DataBaseFetchValues.h"
#import "Constants.h"
#import "LoginViewController.h"

@interface QuizPageViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    BOOL IsShowAnswer;
    int x,y,cellHeight;
    UITableView *tblVw;
    BOOL isAnswerd;
    NSMutableArray *ArrQuestion;
    NSMutableArray *arrSaveAttempt;
    int questionIndex,attemptAnswer,correctmarks;
    int QuestionDoneSection;
    NSMutableArray *arrQuestionSection;
    int MarksObtained;
    BOOL isGotoHome;
}

@end

@implementation QuizPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self CustomNavigationBar];
    MarksObtained = 0;
    questionIndex=0;
    attemptAnswer=1;
    ArrQuestion=[[NSMutableArray alloc]init];
    arrSaveAttempt=[[NSMutableArray alloc]init];
    [self createUi];
    IsShowAnswer=NO;
    isAnswerd=NO;
    arrQuestionSection=[[NSMutableArray alloc]init];
    
    // markOfAnswer=[[[NSUserDefaults standardUserDefaults] valueForKey:@"group_score"]intValue];
    ArrQuestion = [[DataBaseFetchValues sharedInstance] fetchAllDataWithTableName:@"Quiz_Game"];
    if([ArrQuestion count] == 0)
        [self getQustionAndAnswerFromServer];
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"completed"]intValue] ==1)
        [self fetchFromDBAndPrepareQuizIfCompleted];
    else
        [self fetchFromDBAndPrepareQuiz];
    
    // [self FinishView];
    
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-CustomNavigationBar
-(void)CustomNavigationBar
{
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    self.navigationItem.title = @"Questions & Answers";
    
}

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES ];
}
#pragma mark-CreateUi
-(void)createUi{
    
    tblVw=[[UITableView alloc]initWithFrame:CGRectMake(20.0, 0, [[UIScreen mainScreen]bounds].size.width-40, [[UIScreen mainScreen]bounds].size.height) style:UITableViewStyleGrouped];
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    tblVw.delegate=self;
    tblVw.dataSource=self;
    tblVw.showsVerticalScrollIndicator=NO;
    tblVw.backgroundColor=[UIColor whiteColor];
    tblVw.contentInset = UIEdgeInsetsMake(0.0, 0.0, 66.0, 0.0);
    [self.view addSubview:tblVw];
    MarksObtained=[[[NSUserDefaults standardUserDefaults] valueForKey:@"MarksObtained"] intValue];
    [tblVw reloadData];
    
    
}

#pragma mark-TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return ArrQuestion.count+1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (IsShowAnswer) {
        if (section==x) {
            return 1;
        }else{
            return 0;
        }
    }else{
        return 0;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"Cell";
    
    
    UITableViewCell *cell;
    // UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (isAnswerd) {
        if (y==4) {
            [cell.contentView addSubview:[self CreateYouRockView]];
            isAnswerd=NO;
            
        }else{
            [cell.contentView addSubview:[self CreateDifferentViewforWrongAnswer:y]];
            isAnswerd=NO;
            
        }
    }else{
        [cell.contentView addSubview:[self CreateQuestionView:(int)indexPath.section]];
    }
    //[cell.contentView addSubview:[self CreateDifferentViewforWrongAnswer:3]];
    
    
    return cell;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSMutableArray *arrBtnTitle=[[NSMutableArray alloc]init];
    [arrBtnTitle addObjectsFromArray:[NSArray arrayWithObjects:@"Hall of Mammals",@"Ocean Experience",@"Science Center",@"Black Earth| Big River",@"River, prairie & People", nil]];
    if (section==0) {
        UIView *vwScore=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 90)];
        
        UILabel *lblScore=[[UILabel alloc]initWithFrame:CGRectMake((vwScore.frame.size.width/2)-40,10.0, 80, 30)];
        lblScore.text=[NSString stringWithFormat:@"%d",MarksObtained];;
        lblScore.font=[UIFont fontWithName:@"Roboto-Regular" size:25];
        lblScore.textColor=[UIColor darkGrayColor];
        lblScore.textAlignment=NSTextAlignmentCenter;
        lblScore.adjustsFontSizeToFitWidth=YES;
        [vwScore addSubview:lblScore];
        
        UILabel *lblScore1=[[UILabel alloc]initWithFrame:CGRectMake((vwScore.frame.size.width/2)-70, 45.0, 140, 40)];
        lblScore1.text=[NSString stringWithFormat:@"%@ Score",[[NSUserDefaults standardUserDefaults] valueForKey:@"group_name"]];
        lblScore1.textColor=[UIColor whiteColor];
        lblScore1.textAlignment=NSTextAlignmentCenter;
        lblScore1.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
        lblScore1.backgroundColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
        [vwScore addSubview:lblScore1];
        return vwScore;
        
    }else{
        
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        /* Create custom view to display section header... */
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        UILabel *lblTitl=[[UILabel alloc]initWithFrame:CGRectMake(30, 0, tableView.frame.size.width-70, 50)];
        lblTitl.numberOfLines=0;
        lblTitl.text=[[ArrQuestion objectAtIndex:section-1]valueForKey:@"QuestionCategory"];
        [btn addSubview:lblTitl];
        lblTitl.textColor=[UIColor whiteColor];
        lblTitl.textAlignment=NSTextAlignmentCenter;
        lblTitl.font=[UIFont fontWithName:@"Roboto-regular" size:16];
        
        
        btn.frame=CGRectMake(0, 0, tableView.frame.size.width, 50);
        UIImageView *imgVw=[[UIImageView alloc]init];
        
        imgVw.frame=CGRectMake( tableView.frame.size.width-40, 15, 30, 20) ;
        imgVw.contentMode=UIViewContentModeScaleAspectFit;
        [btn addSubview:imgVw];
        //[btn setTitle:[[ArrQuestion objectAtIndex:section-1]valueForKey:@"QuestionCategory"] forState:UIControlStateNormal];
        
        
        if ([arrQuestionSection containsObject:[NSNumber numberWithInt:(int)section]]) {
            // imgVw.image=[UIImage imageNamed:@"Tick_mark_green"];
            imgVw.image=nil;
            [btn setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
            imgVw.image=[UIImage imageNamed:@"tick_icon-1"];
            
        }else{
            imgVw.image=   [UIImage imageNamed:@"question_icon"];
            btn.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        }
        
        if([[[ArrQuestion objectAtIndex:section-1] valueForKey:@"Questions"] count] == 0){
            imgVw.image=nil;
            [btn setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
            imgVw.image=[UIImage imageNamed:@"tick_icon-1"];
            // QuestionDoneSection++;
        }
        if ( [[NSUserDefaults standardUserDefaults]boolForKey:@"finishGame"]) {
            if (QuestionDoneSection==ArrQuestion.count) {
                [self createFinish:QuestionDoneSection];
            }
            
        }
        
        [btn addTarget:self action:@selector(ExpandtableView:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag=section;
        [view addSubview:btn];
        [view setBackgroundColor:[UIColor whiteColor]]; //your background color...
        return view;
    }
}
#pragma mark-SelectAnswer
-(void)SelectAnswer:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    NSLog(@"%ld",(long)btn.tag);
    
    isAnswerd=YES;
    btn.selected=YES;
    UIButton *btn1=[self.view viewWithTag:10];
    UIButton *btn2=[self.view viewWithTag:11];
    UIButton *btn3=[self.view viewWithTag:12];
    UIButton *btn4=[self.view viewWithTag:13];
    
    if (btn.tag==10) {
        btn2.selected=NO;
        btn3.selected=NO;
        btn4.selected=NO;
        
        y=1;
        
        
    }else if (btn.tag==11){
        btn1.selected=NO;
        btn3.selected=NO;
        btn4.selected=NO;
        y=2;
        
    }else if (btn.tag==12){
        btn2.selected=NO;
        btn1.selected=NO;
        btn4.selected=NO;
        y=3;
        
    }else{
        btn2.selected=NO;
        btn1.selected=NO;
        btn3.selected=NO;
        y=4;
    }
    
    
    
    
}
#pragma mark-SubmitTheAnswer
-(void)SubmitTheAnswer:(id)sender{
    //    [prefs setObject:[returArray valueForKey:@"id"] forKey:@"Chaperone_id"];
    //    [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"access_token"];
    //    [prefs setObject:[returArray valueForKey:@"teacher_id"] forKey:@"teacher_id"];
    //    [prefs setObject:[returArray valueForKey:@"group_id"] forKey:@"group_id"];
    //    [prefs setObject:[returArray valueForKey:@"group_name"] forKey:@"group_name"];
    //    [prefs setObject:[returArray valueForKey:@"group_score"] forKey:@"group_score"];
    //answer
    
    if (isAnswerd) {
        
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[NSNumber numberWithInt:attemptAnswer] forKey:@"attempt"];
        [dict setValue:[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Question_id"] forKey:@"question_id"];
        [dict setValue:[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Correct_answer"] forKey:@"Correct_answer"];
        [dict setValue:[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Marks"] forKey:@"Marks"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"Chaperone_id"] forKey:@"cheperone_id"];
        [dict setValue:[NSNumber numberWithInt:y] forKey:@"answer"];
        
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"] forKey:@"access_token"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"group_id"] forKey:@"group_id"];
        
        // BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        //if (isinternet) {
        
        //[[CommonMethods sharedInstance] addSpinner:self.view];
        //[[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/check_answer",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
        
        //if (!error) {
        // [[CommonMethods sharedInstance] removeSpinner:self.view];
        //   NSLog(@"returArray %@",returArray);
        //NSString *errorstr = [returArray valueForKey:@"error"];
        if (attemptAnswer == 1 && [[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Correct_answer"] intValue] != y) {
            y=1;
            attemptAnswer=2;
        }else if (attemptAnswer == 2 && [[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Correct_answer"] intValue] != y){
            y=2;
            attemptAnswer=1;
            [[DatabaseManagement sharedInstance] UpdateDataInDatabaseforQuizForAnswered:2 andQuestion_id:[[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Question_id"] intValue]];
        }
        else if ([[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Correct_answer"] intValue] == y) {
            
            correctmarks=[[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Marks"] intValue];
            
            if (attemptAnswer == 2){
                y=3;
                MarksObtained =MarksObtained+correctmarks/2;
            }else{
                y=4;
                MarksObtained =MarksObtained+correctmarks;
            }
            attemptAnswer=1;
            
            [[DatabaseManagement sharedInstance] UpdateDataInDatabaseforQuizForAnswered:1 andQuestion_id:[[[[[ArrQuestion objectAtIndex:x-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Question_id"] intValue]];
        }
        if (isAnswerd) {
            [tblVw reloadData];
            
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:MarksObtained] forKey:@"MarksObtained"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSelector:@selector(InsertView)  withObject:nil afterDelay:2.0];
        
        
        
        
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Select your answer"];
        
    }
    
    
    
    //
    // isAnswerd=NO;
    
    
}
#pragma mark- Delay
-(void)InsertView{
    
    if (attemptAnswer==1) {
        NSArray *arr=[[ArrQuestion objectAtIndex:x-1] valueForKey:@"Questions"];
        if (questionIndex+1<=arr.count) {
            questionIndex++;
        }
    }
    
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    arr=[arrSaveAttempt mutableCopy];
    
    for (NSDictionary *dict in arr) {
        NSLog(@"%@arrSaveAttempt",arrSaveAttempt);
        NSLog(@"%@dict",dict);
        
        if ([[dict valueForKey:@"section"]intValue]==x) {
            [arrSaveAttempt removeObject:dict];
            NSMutableDictionary *dictSaveAttempt=[[NSMutableDictionary alloc]init];
            [dictSaveAttempt setValue:[NSNumber numberWithInt:x] forKey:@"section"];
            [dictSaveAttempt setValue:[NSNumber numberWithInt:questionIndex] forKey:@"question_index"];
            [dictSaveAttempt setValue:[NSNumber numberWithInt:attemptAnswer] forKey:@"attempt_answer"];
            NSLog(@"%@dictSaveAttempt",dictSaveAttempt);
            [arrSaveAttempt addObject:dictSaveAttempt];
            
            
        }
        
    }
    
    
    
    isAnswerd=NO;
    [tblVw reloadData];
}
#pragma mark- ExpandtableView
-(void)ExpandtableView:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    
    
    NSLog(@"%ld",(long)btn.tag);
    
    questionIndex=0;
    attemptAnswer=1;
    
    
    
    for(NSDictionary *dict in arrSaveAttempt)
    {
        if ([[dict valueForKey:@"section"]intValue]==(int)btn.tag) {
            questionIndex=[[dict valueForKey:@"question_index"]intValue];
            attemptAnswer=[[dict valueForKey:@"attempt_answer"]intValue];
        }
        
        
    }
    
    if ( x==(int)btn.tag && IsShowAnswer==YES) {
        IsShowAnswer=NO;
        // isAnswerd=NO;
        [tblVw reloadData];
        
    }else{
        //        attemptAnswer=1;
        //        questionIndex=0;
        isAnswerd=NO;
        IsShowAnswer=YES;
        x=(int)btn.tag;
        
        
        NSMutableDictionary *dictSaveAttempt=[[NSMutableDictionary alloc]init];
        [dictSaveAttempt setValue:[NSNumber numberWithInt:x] forKey:@"section"];
        [dictSaveAttempt setValue:[NSNumber numberWithInt:questionIndex] forKey:@"question_index"];
        [dictSaveAttempt setValue:[NSNumber numberWithInt:attemptAnswer] forKey:@"attempt_answer"];
        NSLog(@"%@dictSaveAttempt",dictSaveAttempt);
        [arrSaveAttempt addObject:dictSaveAttempt];
        
        
        [tblVw reloadData];
        
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section==0) {
        return 90;
    }else
        return 55;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return cellHeight;
    
}
#pragma mark-UiViewOfQuestion
-(UIView *)CreateQuestionView:(int )section{
    UIView *vwAnswer=[[UIView alloc]init];
    
    NSArray *arr=[[ArrQuestion objectAtIndex:section-1] valueForKey:@"Questions"];
    if (questionIndex>arr.count-1) {
        //        vwAnswer.frame=CGRectMake(10.0, 5.0, tblVw.frame.size.width-20, 100);
        //        vwAnswer.layer.borderWidth=1;
        //
        //        vwAnswer.layer.borderColor=[UIColor darkGrayColor].CGColor;
        //        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( vwAnswer.frame.size.height/2)-15, vwAnswer.frame.size.width-40, 30)];
        //        lbl.text=@"No more questions available";
        //        lbl.textAlignment=NSTextAlignmentCenter;
        //        lbl.textColor=[UIColor darkGrayColor];
        //        lbl.numberOfLines=0;
        //        [vwAnswer addSubview:lbl];
        [arrQuestionSection addObject:[NSNumber numberWithInt:section]];
        
        if (QuestionDoneSection==ArrQuestion.count) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"finishGame"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self createFinish:QuestionDoneSection];
            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
            if (isinternet) {
                
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"] forKey:@"access_token"];
                [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"] forKey:@"teacher_id"];
                [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"] forKey:@"trip_id"];
                [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"group_id"] forKey:@"group_id"];
                [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"Chaperone_id"] forKey:@"cheperone_id"];
                [dict setValue:@"1" forKey:@"completed"];
                
                [dict setValue: [NSNumber numberWithInt:MarksObtained] forKey:@"score"];
                
                
                // [[CommonMethods sharedInstance] addSpinner:self.view];
                [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/finish_quiz",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                    
                    if (!error) {
                        [[CommonMethods sharedInstance] removeSpinner:self.view];
                        NSLog(@"returArray %@",returArray);
                        
                    }
                    else
                    {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                        // [[CommonMethods sharedInstance] removeSpinner:self.view];
                        
                    }
                }];
                
            }else{
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your Quiz is saved offline. Will be updated once you go online." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
                [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:MarksObtained] forKey:@"Finish"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
        }
        
        
        cellHeight=0;
        
        QuestionDoneSection++;
        
        return vwAnswer;
        
    }else
        if (arr.count>0) {
            NSMutableArray *arrAnswer=[[NSMutableArray alloc]initWithObjects:[[[[ArrQuestion objectAtIndex:section-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Answer1"] ,[[[[ArrQuestion objectAtIndex:section-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Answer2"] ,[[[[ArrQuestion objectAtIndex:section-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Answer3"] ,[[[[ArrQuestion objectAtIndex:section-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Answer4"] , nil];
            
            
            
            
            NSString *strQuestion = [[[[ArrQuestion objectAtIndex:section-1]valueForKey:@"Questions"]objectAtIndex:questionIndex]valueForKey:@"Question"];
            NSAttributedString *attrbutedQuestion = [[NSAttributedString alloc] initWithString:strQuestion];
            CGFloat hieght1 =[[CommonMethods sharedInstance]textViewHeightForAttributedText:attrbutedQuestion andWidth:tblVw.frame.size.width-80];
            
            vwAnswer.layer.borderWidth=1;
            vwAnswer.layer.borderColor=[UIColor blackColor].CGColor;
            
            UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(5.0, 5.0, tblVw.frame.size.width-30,hieght1+80)];
            lbl.attributedText=attrbutedQuestion;
            lbl.textColor=[UIColor darkGrayColor];
            lbl.numberOfLines=0;
            [lbl sizeToFit];
            
            
            
            [vwAnswer addSubview:lbl];
            CGSize size1=[self getSizeForText:[arrAnswer objectAtIndex:0] maxWidth:lbl.frame.size.width-60 font:@"Roboto-regular"  fontSize:18];
            [self createradioButton:CGRectMake(5.0,90+hieght1, size1.width+70, size1.height) andTitl:[arrAnswer objectAtIndex:0]  andView:vwAnswer andTag:10];
            
            CGSize size2=[self getSizeForText:[arrAnswer objectAtIndex:1] maxWidth:lbl.frame.size.width-60 font:@"Roboto-regular"  fontSize:18];
            [self createradioButton:CGRectMake(5.0,90+hieght1+size1.height, size2.width+70, size2.height) andTitl:[arrAnswer objectAtIndex:1]  andView:vwAnswer andTag:11];
            
            CGSize size3=[self getSizeForText:[arrAnswer objectAtIndex:2] maxWidth:lbl.frame.size.width-60 font:@"Roboto-regular"  fontSize:18];
            [self createradioButton:CGRectMake(5.0,90+hieght1+size2.height+size1.height, size3.width+70, size3.height) andTitl:[arrAnswer objectAtIndex:2]  andView:vwAnswer andTag:12];
            
            CGSize size4=[self getSizeForText:[arrAnswer objectAtIndex:3] maxWidth:lbl.frame.size.width-60 font:@"Roboto-regular"  fontSize:18];
            [self createradioButton:CGRectMake(5.0,90+hieght1+size3.height+size1.height+size2.height, size4.width+70, size4.height) andTitl:[arrAnswer objectAtIndex:3]  andView:vwAnswer andTag:13];
            
            vwAnswer.frame=CGRectMake(10.0, 5.0, tblVw.frame.size.width-20, 130+hieght1+size4.height+size2.height+size1.height+size3.height+50);
            UIButton *btnSubmit=[UIButton buttonWithType:UIButtonTypeCustom];
            btnSubmit.frame=CGRectMake((vwAnswer.frame.size.width/2)-50,vwAnswer.frame.size.height-50, 100, 40);
            btnSubmit.backgroundColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
            btnSubmit.tag=section+20;
            [btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
            [btnSubmit addTarget:self action:@selector(SubmitTheAnswer:) forControlEvents:UIControlEventTouchUpInside];
            [vwAnswer addSubview:btnSubmit];
            cellHeight=vwAnswer.frame.size.height+20;
            
            //    for (int k=0; k<4; k++) {
            //          }
            
            
            return vwAnswer;
        }
        else{
            
            //            //            UIButton *btn=[self.view viewWithTag:3];
            //            //            btn.backgroundColor=[UIColor greenColor];
            //            vwAnswer.frame=CGRectMake(10.0, 5.0, tblVw.frame.size.width-20, 100);
            //            vwAnswer.layer.borderWidth=1;
            //            vwAnswer.layer.borderColor=[UIColor darkGrayColor].CGColor;
            //            UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( vwAnswer.frame.size.height/2)-8, vwAnswer.frame.size.width-40, 16)];
            //            lbl.text=@"No questions available";
            //            lbl.textAlignment=NSTextAlignmentCenter;
            //            lbl.textColor=[UIColor darkGrayColor];
            //            [vwAnswer addSubview:lbl];
            //
            //            cellHeight=vwAnswer.frame.size.height+20;
            //            cellHeight = 0;
            //            [arrQuestionSection addObject:[NSNumber numberWithInt:section]];
            //            QuestionDoneSection++;
            //            if (QuestionDoneSection==ArrQuestion.count) {
            //                [self createFinish:QuestionDoneSection];
            //            }
            //
            
            
            
            return vwAnswer;
        }
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark- ViewWillDissappear
-(void)viewWillDisappear:(BOOL)animated{
    
    
    
    
    if (MarksObtained>0) {
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"] forKey:@"access_token"];
            [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"] forKey:@"teacher_id"];
            [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"] forKey:@"trip_id"];
            [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"group_id"] forKey:@"group_id"];
            [dict setValue: [[NSUserDefaults standardUserDefaults] stringForKey:@"Chaperone_id"] forKey:@"cheperone_id"];
            
            [dict setValue: [NSNumber numberWithInt:MarksObtained] forKey:@"score"];
            if (isGotoHome) {
                [dict setValue:@"1" forKey:@"completed"];
            }
            else
            {
                [dict setValue:@"0" forKey:@"completed"];
            }
            
            
            // [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/finish_quiz",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    
                    
                    
                    
                }
                else
                {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    // [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
            
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:MarksObtained] forKey:@"Finish"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
    }
    
    
    
    
    
}
#pragma mark- CreateRadiobutton
-(void)createradioButton:(CGRect )frame andTitl:(NSString *)titl andView:(UIView *)vw andTag:(int)tag{
    
    
    
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    // btn.tag=k+10;
    // [btn setTitle:[arrAnswer objectAtIndex:k] forState:UIControlStateNormal];
    [btn setTitle:titl forState:UIControlStateNormal];
    btn.tag=tag;
    // btn.backgroundColor=[UIColor blueColor];
    [btn setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(SelectAnswer:) forControlEvents:UIControlEventTouchUpInside];
    // btn.frame=CGRectMake(35.0,50+hieght1+25*k, 200, 25) ;
    btn.frame=frame;
    btn.titleLabel.numberOfLines=0;
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setImageEdgeInsets:UIEdgeInsetsMake(4,5, 0, 0)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0,12, 0, 0)];
    btn.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    
    [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [vw addSubview:btn];
    
    
    
    
}

#pragma mark - getSizeForString
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}

#pragma mark -RockView
-(UIView *)CreateYouRockView{
    
    UIView *RockView=[[UIView alloc]initWithFrame:CGRectMake(10.0, 5.0, tblVw.frame.size.width-20, 200)];
    RockView.layer.borderWidth=1;
    RockView.layer.borderColor=[UIColor blackColor].CGColor;
    
    UIImageView *imgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"star_icon"]];
    imgVw.frame=CGRectMake((RockView.frame.size.width)/2-100, 10, 200, 40) ;
    imgVw.contentMode=UIViewContentModeScaleAspectFit;
    [RockView addSubview:imgVw];
    
    UILabel *lbl10=[[UILabel alloc]initWithFrame:CGRectMake((RockView.frame.size.width)/2-30, 60, 60, 35)];
    lbl10.font=[UIFont fontWithName:@"Roboto-Regular" size:40];
    lbl10.text=[NSString stringWithFormat:@"%d",correctmarks];
    lbl10.textAlignment=NSTextAlignmentCenter;
    
    lbl10.textColor=[UIColor colorWithRed:0.0/255.0 green:47.0/255.0 blue:134.0/255.0 alpha:1];
    
    UILabel *lblYouRock=[[UILabel alloc]initWithFrame:CGRectMake((RockView.frame.size.width)/2-100, 110, 200, 40)];
    lblYouRock.font=[UIFont fontWithName:@"Roboto-Regular" size:35];
    lblYouRock.text=@"You Rock!";
    lblYouRock.textAlignment=NSTextAlignmentCenter;
    lblYouRock.textColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    
    UILabel *lblWonder=[[UILabel alloc]initWithFrame:CGRectMake(0, 150, RockView.frame.size.width, 20)];
    lblWonder.font=[UIFont fontWithName:@"Roboto-Regular" size:16];
    lblWonder.text=@"I wonder how others are doing...";
    lblWonder.textAlignment=NSTextAlignmentCenter;
    lblWonder.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    
    
    cellHeight=RockView.frame.size.height+20;
    
    [RockView addSubview:lbl10];
    [RockView addSubview:lblYouRock];
    [RockView addSubview:lblWonder];
    return RockView;
    
}
#pragma mark-CreateDifferentViewforWrongAnswer
-(UIView *)CreateDifferentViewforWrongAnswer:(int)number{
    
    
    UIView *RockView=[[UIView alloc]initWithFrame:CGRectMake(10.0, 5.0, tblVw.frame.size.width-20, 200)];
    RockView.layer.borderWidth=1;
    RockView.layer.borderColor=[UIColor blackColor].CGColor;
    
    
    UIImageView *imgVw=[[UIImageView alloc]initWithFrame:CGRectMake((tblVw.frame.size.width)/2-75, 20, 150, 80)];
    imgVw.contentMode=UIViewContentModeScaleAspectFit;
    [RockView addSubview:imgVw];
    
    
    
    UILabel *lblYouRock=[[UILabel alloc]initWithFrame:CGRectMake((tblVw.frame.size.width)/2-100, 110, 200, 40)];
    lblYouRock.font=[UIFont fontWithName:@"Roboto-Regular" size:30];
    lblYouRock.textAlignment=NSTextAlignmentCenter;
    lblYouRock.textColor=[UIColor redColor];
    
    UILabel *lblWonder=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 150, RockView.frame.size.width, 20)];
    lblWonder.font=[UIFont fontWithName:@"Roboto-Regular" size:16];
    lblWonder.textAlignment=NSTextAlignmentCenter;
    lblWonder.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblWonder.adjustsFontSizeToFitWidth=YES;
    
    
    if (number==1) {
        lblWonder.text=@"You get one more chance to try for half the points.";
        lblYouRock.text=@"Wrong Answer";
        imgVw.image=[UIImage imageNamed:@"sad_icon"];
        
    }else if (number==2){
        lblWonder.text=@"That is still incorrect! Let's move on..";
        lblYouRock.text=@"Oh no!";
        imgVw.image=[UIImage imageNamed:@"confused_icon"];
        
        
    }else if (number==3){
        lblWonder.text=@"Right this time!!";
        lblYouRock.text=@"Yes!";
        imgVw.image=[UIImage imageNamed:@"thumb_icon"];
        
        
    }
    cellHeight=RockView.frame.size.height+20;
    
    [RockView addSubview:lblYouRock];
    [RockView addSubview:lblWonder];
    //isAnswerd=NO;
    
    return RockView;
}

#pragma mark- GetQuestion
-(void)getQustionAndAnswerFromServer{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/get_question_detail?access_token=%@&teacher_id=%@&trip_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                ArrQuestion=[returArray valueForKey:@"result"];
                
                
                if ([returArray valueForKey:@"success"]) {
                    
                    if (ArrQuestion.count==0) {
                        
                        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( self.view.frame.size.height/2)-8, self.view.frame.size.width-40, 16)];
                        lbl.text=@"No questions available";
                        lbl.textAlignment=NSTextAlignmentCenter;
                        lbl.textColor=[UIColor darkGrayColor];
                        [self.view addSubview:lbl];
                        
                    }else{
                        //[self createUi];
                        
                        [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
                        for (int k=0; k<ArrQuestion.count; k++) {
                            NSDictionary *dict=[[NSDictionary alloc]init];
                            dict=[ArrQuestion objectAtIndex:k];
                            NSArray *arr=[[NSArray alloc]init];
                            arr=[dict valueForKey:@"Questions"];
                            for (int m=0; m<arr.count; m++) {
                                NSMutableDictionary *dictQuestionsave=[[NSMutableDictionary alloc]init];
                                [ dictQuestionsave setValue:[dict valueForKey:@"QuestionCategory"] forKey:@"QuestionCategory"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Question"] forKey:@"Question"];
                                
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer1"] forKey:@"Answer1"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer2"] forKey:@"Answer2"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer3"] forKey:@"Answer3"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer4"] forKey:@"Answer4"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"Correct_answer"] intValue]] forKey:@"Correct_answer"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"Marks"] intValue]] forKey:@"Marks"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"id"] intValue]] forKey:@"Question_id"];
                                [[DatabaseManagement sharedInstance]SavedataInDatabaseforQuiz:dictQuestionsave];
                                
                            }
                            
                        }
                        
                        //[DatabaseManagement sharedInstance]SavedataInDatabaseforQuiz:<#(NSDictionary *)#>
                        
                    }
                }
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"completed"]intValue] == 1) {
                    [self fetchFromDBAndPrepareQuizIfCompleted];
                }
                else
                    [self fetchFromDBAndPrepareQuiz];
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }
    else
    {
        
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"completed"]intValue] == 1) {
            [self fetchFromDBAndPrepareQuizIfCompleted];
        }
        else
            [self fetchFromDBAndPrepareQuiz];
        
    }
    
    
}

#pragma mark- FinishView
-(void)FinishView{
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"finishGame"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    vw.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
    vw.tag=100;
    [self.view addSubview:vw];
    UIView *vwFinish=[[UIView alloc]initWithFrame:CGRectMake(20, ([[UIScreen mainScreen]bounds].size.height/2)-175,  [[UIScreen mainScreen]bounds].size.width-40, 350)];
    
    vwFinish.backgroundColor=[UIColor whiteColor];
    [vw addSubview:vwFinish];
    
    UIView *vwYellow=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, vwFinish.frame.size.width, vwFinish.frame.size.height/2)];
    vwYellow.backgroundColor=[UIColor yellowColor];
    [vwFinish addSubview:vwYellow];
    
    UIImageView *imgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"finish_icon"]];
    imgVw.frame=CGRectMake((vwYellow.frame.size.width/2)-30, 40, 60, 80);
    [vwYellow addSubview:imgVw];
    
    UILabel *lbltnhnks=[[UILabel alloc]init];
    lbltnhnks.frame=CGRectMake(20, vwYellow.frame.size.height+10, vwFinish.frame.size.width-40, 30);
    lbltnhnks.textColor=[UIColor darkGrayColor];
    lbltnhnks.text=@"Thanks for participating!";
    lbltnhnks.font=[UIFont fontWithName:@"Roboto-regular" size:20];
    lbltnhnks.textAlignment=NSTextAlignmentCenter;
    [vwFinish addSubview:lbltnhnks];
    
    UILabel *lblWinner=[[UILabel alloc]init];
    lblWinner.frame=CGRectMake(20, vwYellow.frame.size.height+40, vwFinish.frame.size.width-40, 20);
    lblWinner.textColor=[UIColor darkGrayColor];
    lblWinner.text=@"Winners will be announced by";
    lblWinner.font=[UIFont fontWithName:@"Roboto-regular" size:16];
    lblWinner.textAlignment=NSTextAlignmentCenter;
    [vwFinish addSubview:lblWinner];
    
    UILabel *lblorganizer=[[UILabel alloc]init];
    lblorganizer.frame=CGRectMake(20, vwYellow.frame.size.height+60, vwFinish.frame.size.width-40, 20);
    lblorganizer.textColor=[UIColor darkGrayColor];
    lblorganizer.text=@"orgainizer of this field trip.";
    lblorganizer.font=[UIFont fontWithName:@"Roboto-regular" size:16];
    lblorganizer.textAlignment=NSTextAlignmentCenter;
    [vwFinish addSubview:lblorganizer];
    
    UIButton *btnHomePage=[UIButton buttonWithType:UIButtonTypeCustom];
    btnHomePage.frame=CGRectMake(40, vwYellow.frame.size.height+100, vwYellow.frame.size.width-80, 50);
    [btnHomePage setTitle:@"Go To Homepage" forState:UIControlStateNormal];
    [vwFinish addSubview:btnHomePage];
    btnHomePage.backgroundColor= [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    [btnHomePage addTarget:self action:@selector(GoToHome:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
}

#pragma mark- DismisstheView
-(void)DismisstheView:(id)sender{
    UIView *removeView;
    while((removeView = [self.view viewWithTag:100]) != nil) {
        [removeView removeFromSuperview];
        
    }
    
    
}
#pragma mark- GoToHome
-(void)GoToHome:(id)sender{
    isGotoHome = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}

#pragma mark- createFinish
-(void)createFinish:(int)countSection{
    
    UIButton *btnHomePage=[UIButton buttonWithType:UIButtonTypeCustom];
    btnHomePage.frame=CGRectMake((tblVw.frame.size.width/2)-100, 70*countSection+150, 200, 50);
    [btnHomePage setTitle:@"Finish" forState:UIControlStateNormal];
    [tblVw addSubview:btnHomePage];
    btnHomePage.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    [btnHomePage addTarget:self action:@selector(FinishView) forControlEvents:UIControlEventTouchUpInside];
    
}



- (void) fetchFromDBAndPrepareQuiz{
    ArrQuestion = [[DataBaseFetchValues sharedInstance] fetchAllDataWithTableName:@"Quiz_Game"];
    NSLog(@"ArrQuestion : %@", ArrQuestion);
    if([ArrQuestion count] == 0)
        return;
    NSString *strCategory = [[ArrQuestion objectAtIndex:0] valueForKey:@"QuestionCategory"];
    NSMutableArray *arrTemp1 = [[NSMutableArray alloc] init];
    NSMutableArray *arrTemp2 = [[NSMutableArray alloc] init];
    for(NSDictionary *dic in ArrQuestion){
        //if([[dic valueForKey:@"Answered"] intValue] == 1)
        // MarksObtained += [[dic valueForKey:@"Marks"] intValue];
        
        if([strCategory isEqualToString:[dic valueForKey:@"QuestionCategory"]])
            [arrTemp2 addObject:dic];
        else{
            //---
            NSMutableArray *arrTemp = [arrTemp2 mutableCopy];
            for(NSDictionary *dicti in arrTemp2){
                if([[dicti valueForKey:@"Answered"] intValue] == 1 || [[dicti valueForKey:@"Answered"] intValue] == 2)
                    [arrTemp removeObject:dicti];
            }
            arrTemp2 = [arrTemp mutableCopy];
            //---
            [arrTemp1 addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:strCategory, [arrTemp2 copy], nil] forKeys:[NSArray arrayWithObjects:@"QuestionCategory", @"Questions", nil]]];
            [arrTemp2 removeAllObjects];
            [arrTemp2 addObject:dic];
            strCategory = [dic valueForKey:@"QuestionCategory"];
        }
    }
    if([arrTemp2 count] > 0){
        NSMutableArray *arrTemp = [arrTemp2 mutableCopy];
        for(NSDictionary *dicti in arrTemp2){
            if([[dicti valueForKey:@"Answered"] intValue] == 1 || [[dicti valueForKey:@"Answered"] intValue] == 2)
                [arrTemp removeObject:dicti];
        }
        arrTemp2 = [arrTemp mutableCopy];
        [arrTemp1 addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:strCategory, [arrTemp2 copy], nil] forKeys:[NSArray arrayWithObjects:@"QuestionCategory", @"Questions", nil]]];
    }
    NSLog(@"arrTemp1 : %@", arrTemp1);
    ArrQuestion = [arrTemp1 copy];
    arrTemp1 = nil;
    arrTemp2 = nil;
    strCategory = nil;
    
    //--
    QuestionDoneSection = 0;
    for(NSDictionary *dict in ArrQuestion){
        if([[dict valueForKey:@"Questions"] count] == 0)
            QuestionDoneSection++;
    }
    //--
    
    [tblVw reloadData];
    //[self createUi];
}
- (void) fetchFromDBAndPrepareQuizIfCompleted{
    ArrQuestion = [[DataBaseFetchValues sharedInstance] fetchAllDataWithTableName:@"Quiz_Game"];
    NSLog(@"ArrQuestion : %@", ArrQuestion);
    if([ArrQuestion count] == 0)
        return;
    NSString *strCategory = [[ArrQuestion objectAtIndex:0] valueForKey:@"QuestionCategory"];
    NSMutableArray *arrTemp1 = [[NSMutableArray alloc] init];
    NSMutableArray *arrTemp2 = [[NSMutableArray alloc] init];
    for(NSDictionary *dic in ArrQuestion){
        //if([[dic valueForKey:@"Answered"] intValue] == 1)
        // MarksObtained += [[dic valueForKey:@"Marks"] intValue];
        
        if([strCategory isEqualToString:[dic valueForKey:@"QuestionCategory"]]){
            //[arrTemp2 addObject:dic];
        }
        else{
            [arrTemp1 addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:strCategory, [arrTemp2 copy], nil] forKeys:[NSArray arrayWithObjects:@"QuestionCategory", @"Questions", nil]]];
            [arrTemp2 removeAllObjects];
            //[arrTemp2 addObject:dic];
            strCategory = [dic valueForKey:@"QuestionCategory"];
        }
    }
    if([arrTemp2 count] > 0){
        //        NSMutableArray *arrTemp = [arrTemp2 mutableCopy];
        //        for(NSDictionary *dicti in arrTemp2){
        //            if([[dicti valueForKey:@"Answered"] intValue] == 1 || [[dicti valueForKey:@"Answered"] intValue] == 2)
        //                [arrTemp removeObject:dicti];
        //        }
        //        arrTemp2 = [arrTemp mutableCopy];
        [arrTemp1 addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:strCategory, [arrTemp2 copy], nil] forKeys:[NSArray arrayWithObjects:@"QuestionCategory", @"Questions", nil]]];
    }
    NSLog(@"arrTemp1 : %@", arrTemp1);
    ArrQuestion = [arrTemp1 copy];
    arrTemp1 = nil;
    arrTemp2 = nil;
    strCategory = nil;
    
    //--
    QuestionDoneSection = 0;
    for(NSDictionary *dict in ArrQuestion){
        if([[dict valueForKey:@"Questions"] count] == 0)
            QuestionDoneSection++;
    }
    //--
    
    [tblVw reloadData];
    //[self createUi];
}

@end
