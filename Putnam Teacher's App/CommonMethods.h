//
//  CommonMethods.h
//  Putnam Teacher's App
//
//  Created by Inficare on 5/31/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@interface CommonMethods : NSObject

+(CommonMethods *) sharedInstance;
-(void)AlertAction:(NSString *)msg;
-(void)addSpinner:(UIView *)view1;
-(void)removeSpinner:(UIView *)view1;
-(UIImageView *)AddMenu:(UIView *)Vw andFrame:(CGRect)frame;
-(BOOL)checkInternetConnection;
- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width;
-(UIImageView *)AddMenuBaronChaperone:(UIView *)Vw andFrame:(CGRect)frame;
@end
