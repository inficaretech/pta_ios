//
//  ManageChaperonesViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/3/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "ManageChaperonesViewController.h"
#import "AddChaperonesViewController.h"
#import "ChaperoneListTableViewCell.h"
#import "AddStudentViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "DatabaseManagement.h"
#import "DataBaseFetchValues.h"
#import "HomeViewController.h"
#import "StudentListTableViewCell.h"
#import "MarqueeLabel.h"
#import "GroupDetailsViewController.h"
#import "AssignGroupToStudentViewController.h"





@interface ManageChaperonesViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>{
    
    NSArray *arrListOfChaperones;
    UITableView *tblVw;
    UIButton *btnPlus;
    int y;
    MarqueeLabel *titlelbl ;
    int sendemailid;
    UIButton *buttonAssignStudentsToGroup;
    // UIImageView *imgVwMenu;
    //NSMutableArray *arrIndex;
    
}

@end

@implementation ManageChaperonesViewController
@synthesize IsStudent;
- (void)viewDidLoad {
    [super viewDidLoad];
    //  arrIndex=[[NSMutableArray alloc]init];
    
    
    
    
    
    
    arrListOfChaperones =[[NSArray alloc]init];
    self.navigationController.navigationItem.hidesBackButton=YES;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self createUi];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark-ViewWillApppear
-(void)viewWillAppear:(BOOL)animated{
    
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //  appdel.navController=self.navigationController;
    [super viewWillAppear:YES];
    
    btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPlus.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width-100, 10.0, 30.0, 25.0);
    [btnPlus setImage:[UIImage imageNamed:@"add button"] forState:UIControlStateNormal];
    [btnPlus addTarget:self action:@selector(AddButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // UIBarButtonItem *btnAdd =[[UIBarButtonItem alloc] initWithCustomView:btnPlus];
    //self.navigationItem.rightBarButtonItem =btnAdd;
    
    appdel.btnAdd=btnPlus;
    
    
    
    titlelbl = [[MarqueeLabel alloc] init];
    [titlelbl setFrame:CGRectMake(60, 0,[[UIScreen mainScreen] bounds].size.width-170, 40)];
    //titlelbl.text = @"Appointment Details";
    [titlelbl setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    titlelbl.textColor = [UIColor whiteColor];
    [titlelbl setTextAlignment:NSTextAlignmentCenter];
    titlelbl.marqueeType = MLContinuous;
    titlelbl.scrollDuration = 15.0;
    titlelbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    titlelbl.fadeLength = 10.0f;
    titlelbl.leadingBuffer = 30.0f;
    titlelbl.trailingBuffer = 20.0f;
    
    
    [self.navigationController.navigationBar addSubview:titlelbl];
    [self.navigationController.navigationBar addSubview:btnPlus];
    
    
    
    
    [self GetListfromApi];
    
    
}
#pragma mark-CreateUi
-(void)createUi{
    tblVw =[[UITableView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    
    tblVw.delegate=self;
    tblVw.dataSource=self;
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tblVw];
    //    if (IS_IPHONE_5) {
    //        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-80,70 ,70 )];
    //    }else{
    //        [[CommonMethods sharedInstance] AddMenu:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
    //    }
    tblVw.contentInset = UIEdgeInsetsMake(0.0, 0.0, 110, 0.0);
    
    if(IsStudent)
    {
        tblVw.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-80);
        buttonAssignStudentsToGroup = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonAssignStudentsToGroup.frame =CGRectMake(40.0, [[UIScreen mainScreen] bounds].size.height-60, [[UIScreen mainScreen] bounds].size.width-80, 40);
        [self.view addSubview:buttonAssignStudentsToGroup];
        buttonAssignStudentsToGroup.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        [buttonAssignStudentsToGroup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonAssignStudentsToGroup addTarget:self action:@selector(buttonAssignStudentsToGroupPressed) forControlEvents:UIControlEventTouchUpInside];
        [buttonAssignStudentsToGroup setBackgroundColor:[UIColor  colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
        
        buttonAssignStudentsToGroup.layer.shadowColor = [UIColor grayColor].CGColor;
        buttonAssignStudentsToGroup.layer.shadowOffset = CGSizeMake(0, 1.0);
        buttonAssignStudentsToGroup.layer.shadowOpacity = 2.0;
        buttonAssignStudentsToGroup.layer.shadowRadius = 3.0;
        [buttonAssignStudentsToGroup setTitle:@"Assign Students to Groups" forState:UIControlStateNormal];
    }

    
    
    // tblVw.scrollIndicatorInsets = contentInsets;
    
    
}

-(void)buttonAssignStudentsToGroupPressed
{
    
    UIStoryboard *story =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AssignGroupToStudentViewController *controller = [story instantiateViewControllerWithIdentifier:@"AssignGroupToStudentViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    
    
    
}

#pragma mark- ShowTablelist
-(void)GetListfromApi{
    
    
    NSString *StrStudentorChaperone,*strdbTableName,*strNolist,*strNavigationTitle;
    if (IsStudent) {
        strNavigationTitle= @"Student List ";
        StrStudentorChaperone=@"Students";
        strdbTableName=@"StudentList";
        strNolist=@" No students managed";
        
    }else{
        strNavigationTitle = @"Manage Chaperones ";
        StrStudentorChaperone=@"Chaperones";
        strdbTableName=@"ChaperoneList";
        strNolist=@"No chaperones managed";
    }
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        btnPlus.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/%@/get_all?access_token=%@&teacher_id=%@",kBaseUrl,StrStudentorChaperone,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
              [[CommonMethods sharedInstance] removeSpinner:self.view];
            if (!error) {
                btnPlus.userInteractionEnabled=YES;
                NSLog(@"returArray %@",returArray);
              
                arrListOfChaperones =[[NSArray alloc]init];
                arrListOfChaperones=[returArray mutableCopy];
                
                [tblVw reloadData];
                
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                if (arr.count>0) {
                    titlelbl.text=[NSString stringWithFormat:@"%@- %@",strNavigationTitle,[[arr objectAtIndex:0] valueForKey:@"fieldtrip"]];
                }else{
                    titlelbl.text=strNavigationTitle;
                }
                
                UILabel *lblShow= [self LabelShow:strNolist];
                lblShow.tag=100;
                if (arr.count<1) {
                    lblShow.hidden=NO;
                    
                    
                }else{
                    lblShow.hidden=YES;
                }
                
            }
            else
            {    btnPlus.userInteractionEnabled=YES;
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    } else
    {
        //        NSArray *arr =[[DataBaseFetchValues sharedInstance] fetchAllDataWithTableName:strdbTableName];
        //        NSMutableDictionary *arr2=[[NSMutableDictionary alloc]init];
        //        [arr2 setValue:arr forKey:@"result"];
        //        arrListOfChaperones =[arr2 mutableCopy];
        //        [tblVw reloadData];
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
}
#pragma mark-ViewWillDissappear
-(void)viewWillDisappear:(BOOL)animated{
    UILabel *removeView;
    while((removeView = [self.view viewWithTag:100]) != nil) {
        [removeView removeFromSuperview];
    }
    [titlelbl removeFromSuperview];
    [btnPlus removeFromSuperview];
    // [imgVwMenu removeFromSuperview];
    
}
#pragma mark-AlertViewDelegte
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==1000) {
        if (buttonIndex==1) {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
            [dict setValue:[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:y-6 ] valueForKey:@"id"] forKey:@"id"];
            NSString *strStudentOrchaperone,*strStudentandchaperone;
            if (IsStudent) {
                strStudentOrchaperone=@"Students";
                strStudentandchaperone=@"student";
                
            }else{
                strStudentOrchaperone=@"Chaperones";
                strStudentandchaperone=@"chaperones";
                
            }
            
            BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
            if (isinternet) {
                
                [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/%@/remove_%@",kBaseUrl,strStudentOrchaperone,strStudentandchaperone] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                    
                    if (!error) {
                        
                        NSLog(@"returArray %@",returArray);
                        
                        if ([returArray valueForKey:@"success"]) {
                            [[CommonMethods sharedInstance] AlertAction:@"Deleted successfully"];
                            [self GetListfromApi];
                        }
                        
                        
                    }
                    else
                    {
                        [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                        
                    }
                }];
                
            }else{
                [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            }
        }
        
        
        
        
        
        
        
        
        
    }else{
        if (buttonIndex==1) {
            
            [self sendAccesscodetoChaperonGmailAddress];
        }
        
    }
    
    
}

#pragma mark-AddChaperone
-(void)AddButtonPressed:(id)sender{
    //AddStudentViewController.h
    
    //[self getFieldTripfromApi];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_fieldtrip?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                NSLog(@"returArray %@",returArray);
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                // NSMutableArray *arrFieldTrip=[[NSMutableArray alloc]init];
                if (arr.count>0) {
                    if (IsStudent) {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AddStudentViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddStudentView"];
                        Controller.IsEditStudent=NO;
                        Controller.arrGrades=[arr mutableCopy];
                        [self.navigationController pushViewController:Controller animated:YES ];
                        
                    }else{
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AddChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddChaperoneView"];
                        Controller.IsEditChaperone=NO;
                        Controller.arrfieldTrip=[arr mutableCopy];
                        [self.navigationController pushViewController:Controller animated:YES ];
                        
                    }
                    
                }else{
                    if (IsStudent) {
                        [[CommonMethods sharedInstance] AlertAction:@"Build Itinerary first to add student."];
                    }else{
                        [[CommonMethods sharedInstance] AlertAction:@"Build Itinerary first to add chaperone."];
                    }
                    
                    return ;
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance]AlertAction:@"Server error"];
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
        
    }
    
    
    
    
}

#pragma mark- getFieldTripfromApi
-(void)getFieldTripfromApi{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_fieldtrip?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                NSLog(@"returArray %@",returArray);
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                if (arr.count>0) {
                    
                    //                    for (int k=0; k<arr.count; k++) {
                    //                        NSString *str=[[arr objectAtIndex:k] valueForKey:@"fieldtrip"];
                    //                        [arrGrades addObject:str];
                    //
                    //                    }
                }
                
            }
            else
            {
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
        
    }
    
}


#pragma mark- TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr=[[NSArray alloc]init];
    arr=[arrListOfChaperones valueForKey:@"result"];
    if (arr.count==0) {
        return 0;
    }else
        return arr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier ;
    
    if (IsStudent) {
        simpleTableIdentifier = @"StudentListTableViewCell";
        
        StudentListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 89, [[UIScreen mainScreen]bounds].size.width, 1)];
            lblline.backgroundColor=[UIColor darkGrayColor];
            [cell.contentView addSubview:lblline];
            
        }
        //cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.btnEdit.tag=6+indexPath.row;
        cell.btnDelete.tag=6+indexPath.row;
        //        cell.imageAllergy= nil;
        //        cell.imageSpendingMoney=nil;
        //        cell.imageMedicine=nil;
        //        cell.imageSpecialNeeds=nil;
        //        cell.imageCamera=nil;
        
        [cell.btnEdit addTarget:self action:@selector(IsEditStudent:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDelete addTarget:self action:@selector(IsDeleteStudent:) forControlEvents:UIControlEventTouchUpInside];
        if (arrListOfChaperones.count>0) {
            NSString *fullname = [[[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"f_name"] stringByAppendingString:@" "] stringByAppendingString:[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"l_name"]];
            {
                
                //                MarqueeLabel *lblName1 = [[MarqueeLabel alloc] init];
                //                [lblName1 setFrame:CGRectMake(cell.frame.origin.x,0.0,cell.imageCamera.frame.origin.x, cell.lblName.frame.size.height)];
                //                [cell.contentView addSubview:lblName1];
                //                //titlelbl.text = @"Appointment Details";
                //               // [titlelbl setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
                //              //  titlelbl.textColor = [UIColor whiteColor];
                //                [lblName1 setTextAlignment:NSTextAlignmentLeft];
                //                lblName1.marqueeType = MLContinuous;
                //                lblName1.scrollDuration = 15.0;
                //                lblName1.animationCurve = UIViewAnimationOptionCurveEaseInOut;
                //                lblName1.fadeLength = 10.0f;
                //                lblName1.leadingBuffer = 30.0f;
                //                lblName1.trailingBuffer = 20.0f;
                //                lblName1.backgroundColor = [UIColor redColor];
                //
                //
                //
                //
                //           lblName1.text=fullname;
                cell.lblName.text =fullname;
            }
            //            if ([[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"fieldtrip"]!=(id)[NSNull null]) {
            //                cell.lblFieldTrip.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"fieldtrip"];
            //            }else{
            //                cell.lblFieldTrip.text=@"N/A";
            //            }
            
            
            NSMutableArray *arrImgaes=[[NSMutableArray alloc]init];
            
            if ([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"photography_allowed"] isEqualToString:@"1"]) {
                
                [arrImgaes addObject:@"photography_allowed"];
                //cell.imageCamera.image = [UIImage imageNamed:@"camera_icon - cross"];
                
            }
            else if ([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"photography_allowed"] isEqualToString:@"0"])
            {
                [arrImgaes addObject:@"camera_icon - cross"];
            }
            
            if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"special_need"] isEqualToString:@"1"])
            {
                [arrImgaes addObject:@"special_need"];
            }
            
            if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"medicine"] isEqualToString:@"1"] )
            {
                [arrImgaes addObject:@"medicine_icon"];
            }
            
            
            if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"spending_money"] isEqualToString:@"1"])
            {
                // cell.imageSpendingMoney.hidden = YES;
                [arrImgaes addObject:@"dollar_icon"];
            }
            if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:indexPath.row] valueForKey:@"allergies"] isEqualToString:@"1"])
            {
                //cell.imageAllergy.hidden = YES;
                [arrImgaes addObject:@"allergy_icon"];
            }
            
            cell.imageCamera.hidden = NO;
            cell.imageSpecialNeeds.hidden = NO;
            cell.imageMedicine.hidden = NO;
            cell.imageAllergy.hidden = NO;
            cell.imageSpendingMoney.hidden = NO;
            switch (arrImgaes.count) {
                case 1:
                    cell.imageCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
                    cell.imageSpecialNeeds.hidden = YES;
                    cell.imageMedicine.hidden = YES;
                    cell.imageAllergy.hidden = YES;
                    cell.imageSpendingMoney.hidden = YES;
                    break;
                case 2:
                    cell.imageCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
                    cell.imageSpecialNeeds.image =[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
                    cell.imageMedicine.hidden = YES;
                    cell.imageAllergy.hidden = YES;
                    cell.imageSpendingMoney.hidden = YES;
                    break;
                case 3:
                    cell.imageCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
                    cell.imageSpecialNeeds.image =[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
                    cell.imageMedicine.image = [UIImage imageNamed:[arrImgaes objectAtIndex:2]];
                    cell.imageAllergy.hidden = YES;
                    cell.imageSpendingMoney.hidden = YES;
                    break;
                case 4:
                    cell.imageCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
                    cell.imageSpecialNeeds.image =[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
                    cell.imageMedicine.image = [UIImage imageNamed:[arrImgaes objectAtIndex:2]];
                    cell.imageSpendingMoney.image = [UIImage imageNamed:[arrImgaes objectAtIndex:3]];
                    cell.imageAllergy.hidden = YES;
                    break;
                case 5:
                    cell.imageCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
                    cell.imageSpecialNeeds.image =[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
                    cell.imageMedicine.image = [UIImage imageNamed:[arrImgaes objectAtIndex:2]];
                    cell.imageSpendingMoney.image = [UIImage imageNamed:[arrImgaes objectAtIndex:3]];
                    cell.imageAllergy.image = [UIImage imageNamed:[arrImgaes objectAtIndex:4]];
                    break;
                    
                default:
                    break;
            }
            
            if ([[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"contact1"] isEqualToString:@""]) {
                cell.lblContact1.text=@"N/A";
            }else{
                cell.lblContact1.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"contact1"];
            }
            
            if ([[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"contact2"] isEqualToString:@""]) {
                cell.lblContact2.text=@"N/A";
            }else{
                cell.lblContact2.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"contact2"];
                
            }
            if ([[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"class"] isEqualToString:@""]) {
                cell.lbClass.text=@"N/A";
            }else{
                cell.lbClass.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"class"];
                
            }
            
        }
        cell.tag=indexPath.row;
        
        //[cell addUtilityButtons:[self utilButtons]];
        //[cell setGSSwipeCelldelegate:self];
        
        if(indexPath.row % 2 == 0)
            cell.lblColour.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        else
            cell.lblColour.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:239.0/255.0 alpha:1];
        
        
        return cell;
        
    }else{
        
        simpleTableIdentifier= @"ChaperoneListTableViewCell";
        
        ChaperoneListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        //UIButton *btn;
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 119, [[UIScreen mainScreen]bounds].size.width, 1)];
            lblline.backgroundColor=[UIColor darkGrayColor];
            [cell.contentView addSubview:lblline];
            // [cell.btnDelete setImage:[UIImage imageNamed:@"send_icon"] forState:UIControlStateNormal];
            
        }
        cell.btnEdit.tag=6+indexPath.row;
        cell.btnDelete.tag=6+indexPath.row;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [cell.btn1 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn2 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn3 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn4 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn5 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn6 addTarget:self action:@selector(SelectTheGroupForChaperone:) forControlEvents:UIControlEventTouchUpInside];
        
        //        if (indexPath.row == 1) {
        //            cell.btn1.userInteractionEnabled = NO;
        //            cell.btn2.userInteractionEnabled = NO;
        //            cell.btn3.userInteractionEnabled = NO;
        //            cell.btn4.userInteractionEnabled = NO;
        //            cell.btn5.userInteractionEnabled = NO;
        //            cell.btn5.userInteractionEnabled = NO;
        //        }
        
        
        if (![[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:indexPath.row] valueForKey:@"Is_completed"] isKindOfClass:[NSNull class]]) {
            
            
            if ([[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:indexPath.row] valueForKey:@"Is_completed"] isEqualToString:@"1"]) {
                cell.btn1.userInteractionEnabled = NO;
                cell.btn2.userInteractionEnabled = NO;
                cell.btn3.userInteractionEnabled = NO;
                cell.btn4.userInteractionEnabled = NO;
                cell.btn5.userInteractionEnabled = NO;
                cell.btn6.userInteractionEnabled = NO;
            }
        }
        NSString *str=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row]valueForKey:@"group"];
        if (str != (id)[NSNull null] && ![str isEqualToString:@""] ) {
            for(UIButton *Vw in cell.contentView.subviews){
                //  NSLog(@"%d",indexPath.row);
                if ([Vw isKindOfClass:[UIButton class]] && Vw.tag==[[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:indexPath.row] valueForKey:@"group"]intValue]-1) {
                    [Vw setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
                    [Vw setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    Vw.layer.borderColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
                    
                    
                }else if ([Vw isKindOfClass:[UIButton class]]){
                    [Vw setBackgroundColor:[UIColor whiteColor]];
                    [Vw setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    Vw.layer.borderColor=[UIColor darkGrayColor].CGColor;
                    
                    
                    
                }
                
            }
            
            
            
        }
        else{
            for(UIButton *Vw in cell.contentView.subviews){
                if([Vw isKindOfClass:[UIButton class]]){
                    [Vw setBackgroundColor:[UIColor whiteColor]];
                    [Vw setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    Vw.layer.borderColor=[UIColor darkGrayColor].CGColor;
                }
            }
        }
        
        
        
        
        
        [cell.btnDelete addTarget:self action:@selector(SendAccessCodeToChaperone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit addTarget:self action:@selector(IsEditStudent:) forControlEvents:UIControlEventTouchUpInside];
        //[cell.btnDelete addTarget:self action:@selector(IsDeleteStudent:) forControlEvents:UIControlEventTouchUpInside];
        if (arrListOfChaperones.count>0 ) {
            cell.lblPhonenumber.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"contact"];
            
            NSString *substrings = [[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"name"];
            NSMutableString * string = [substrings mutableCopy];
            [string replaceOccurrencesOfString:@"`"
                                    withString:@" "
                                       options:0
                                         range:NSMakeRange(0, string.length)];
            cell.lblName.text=string;
            cell.lblGmail.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"email"];
            cell.lblAccesscode.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"accesscode"];
            
            //            if ([[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"fieldtrip"]!=(id)[NSNull null]) {
            //                cell.lblGroupName.text=[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row ] valueForKey:@"fieldtrip"];
            //            }else{
            //                cell.lblGroupName.text=@"N/A";
            //            }
            
            
        }
        
        
        
        if(indexPath.row % 2 == 0)
            cell.lblColour.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
        else
            cell.lblColour.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:239.0/255.0 alpha:1];
        
        return cell;
        
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsStudent) {
        return 90;
    }
    return 120;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IsStudent) {
        [self showViewAtIndex:indexPath.row];
    }
    
    
    
}
#pragma mark-SelectTheGroupForChaperone
-(void)SelectTheGroupForChaperone:(UIButton *)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblVw];
    NSIndexPath *indexPath = [tblVw indexPathForRowAtPoint:buttonPosition];
    for(NSDictionary *dict in [arrListOfChaperones valueForKey:@"result"]){
        if ([[dict valueForKey:@"group"]intValue]==(int)btn.tag+1) {
            [[CommonMethods sharedInstance]AlertAction:@"Please assign unique group to each chaperone"];
            return;
            
        }
        
    }
    //  NSLog(@"%d",indexPath.row);
    
    NSLog(@"btn.tag %ld",(long)btn.tag);
    
    NSMutableDictionary *dictarrName=[[NSMutableDictionary alloc]init];
    dictarrName =[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:indexPath.row]mutableCopy];
    [dictarrName setValue:[NSString stringWithFormat:@"%d",(int)btn.tag+1] forKey:@"group"];
    
    //----
    NSMutableArray *arrTemp = [[arrListOfChaperones valueForKey:@"result"] mutableCopy];
    [arrTemp replaceObjectAtIndex:indexPath.row withObject:dictarrName];
    NSDictionary *dictTemp = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:arrTemp, @"success", nil] forKeys:[NSArray arrayWithObjects:@"result", @"success", nil]];
    arrListOfChaperones = (NSArray*)dictTemp;
    
    [tblVw reloadData];
    
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, [[UIScreen mainScreen]bounds].size.height-100, [[UIScreen mainScreen]bounds].size.width, 100)];
    vw.tag=1000;
    vw.backgroundColor=[UIColor whiteColor];
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.frame = CGRectMake((vw.frame.size.width/2)-20,10, 40, 40);
    
    [btnNext setImage: [UIImage imageNamed:@"save_button_red"] forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(SaveChaperonesGroup:) forControlEvents:UIControlEventTouchUpInside];
    [vw addSubview:btnNext];
    UILabel *labelSave = [[UILabel alloc]init];
    labelSave.frame = CGRectMake((vw.frame.size.width/2)-18,50, 38, 20);
    [vw addSubview:labelSave];
    labelSave.textColor=[UIColor darkGrayColor];
    labelSave.text = @"Save";
    labelSave.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    
    
    
    
    
    //    UIButton *btnSave=[UIButton buttonWithType:UIButtonTypeCustom];
    //    btnSave.frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-35, [[UIScreen mainScreen]bounds].size.height-80, 70, 50);
    //    btnSave.layer.cornerRadius=5;
    //
    //    btnSave.backgroundColor=[UIColor redColor];
    //    btnSave.tag=1000;
    //    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    //    [btnSave addTarget:self action:@selector(SaveChaperonesGroup:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *removeView;
    while((removeView = [self.view viewWithTag:1000]) == nil) {
        [self.view addSubview:vw];
    }
    
    
    // arr=nil;
    //
    
}
#pragma mark- SendAccessCodeToChaperone
-(void)SendAccessCodeToChaperone:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    // NSMutableArray *ArrMapping=[[NSMutableArray alloc]init];
    sendemailid=(int)btn.tag;
    
    
    NSString *str=[[[arrListOfChaperones valueForKey:@"result"]  objectAtIndex:btn.tag-6]valueForKey:@"group"];
    if (str != (id)[NSNull null] && ![str isEqualToString:@""] ) {
        
        // NSMutableDictionary *dictChaperone=[[NSMutableDictionary alloc]init];
        [dict setValue:[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:btn.tag-6] valueForKey:@"id"] forKey:@"chaperon_id"];
        [dict setValue:[NSNumber numberWithInt:1] forKey:@"type"];
        [dict setValue:[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:btn.tag-6] valueForKey:@"group"] forKey:@"group_id"];
        //[ArrMapping addObject:dictChaperone];
    }else{
        [[CommonMethods sharedInstance]AlertAction:@"Please assign group to Chaperone"];
        return;
        
    }
    
    
    
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
    //[dict setValue:ArrMapping forKey:@"mapping_id"];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        self.view.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/get_chaperon_accesscode",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            
            if (!error) {
                self.view.userInteractionEnabled=YES;
                //  [[CommonMethods sharedInstance] AlertAction:[returArray valueForKey:@"result"]];
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                
                if ([returArray valueForKey:@"success"]) {
                    //[returArray valueForKey:@"result"]
                    /* NSString *str=[NSString stringWithFormat:@"Access code=%@ \nDo you want to email Access Code and a link to this app to this   chaperone?",[returArray valueForKey:@"result"]];
                     NSMutableAttributedString *mutablestr = [[NSMutableAttributedString alloc] initWithString:str];
                     NSRange  range=[str rangeOfString:[returArray valueForKey:@"result"]];
                     
                     UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, ([[UIScreen mainScreen]bounds].size.height/2)-70, 240, 140)];
                     vw.backgroundColor=[UIColor lightGrayColor];
                     //[self.view addSubview:vw];
                     
                     
                     UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 220, 30)];
                     lbl.textColor=[UIColor darkGrayColor];
                     lbl.backgroundColor = [UIColor redColor];
                     //lbl.backgroundColor=[UIColor redColor];
                     
                     [mutablestr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
                     lbl.attributedText = mutablestr;
                     lbl.numberOfLines=0;
                     //[vw addSubview:lbl];
                     
                     lbl.textAlignment = NSTextAlignmentCenter;
                     UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"No",@"Yes", nil];
                     [Alert setValue:lbl forKey:@"accessoryView"];
                     [Alert show];
                     Alert=nil;*/
                    
                    NSString *str=[NSString stringWithFormat:@"Access code = %@ \nDo you want to email Access Code and a link of this app to this chaperone?",[returArray valueForKey:@"result"]];
                    //   NSMutableAttributedString *mutablestr = [[NSMutableAttributedString alloc] initWithString:str];
                    NSRange range=[str rangeOfString:[returArray valueForKey:@"result"]];
                    
                    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, ([[UIScreen mainScreen]bounds].size.height/2)-70, 240, 140)];
                    vw.backgroundColor=[UIColor lightGrayColor];
                    //[self.view addSubview:vw];
                    NSMutableParagraphStyle *style =[[NSParagraphStyle defaultParagraphStyle] mutableCopy];
                    style.alignment = NSTextAlignmentJustified;
                    style.firstLineHeadIndent = 15.0f;
                    style.headIndent = 15.0f;
                    style.tailIndent = -15.0f;
                    
                    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:str attributes:@{ NSParagraphStyleAttributeName : style}];
                    [attrText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
                    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 220, 30)];
                    lbl.numberOfLines = 0;
                    lbl.textColor=[UIColor darkGrayColor];
                    lbl.attributedText = attrText;
                    lbl.textAlignment = NSTextAlignmentLeft;
                    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"No",@"Yes", nil];
                    [Alert setValue:lbl forKey:@"accessoryView"];
                    [Alert show];
                    Alert=nil;
                    
                    
                    // [[CommonMethods sharedInstance] AlertAction:@"Access Code has been successfully sent to chaperones email address."];
                    UIButton *removeView;
                    while((removeView = [self.view viewWithTag:1000]) != nil) {
                        [removeView removeFromSuperview];
                        
                    }
                    
                }
                
            }
            else
            {
                self.view.userInteractionEnabled=YES;
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
}

#pragma mark- SendAccesscodeToGmail
-(void)sendAccesscodetoChaperonGmailAddress{
    
    
    //UIButton *btn=(UIButton *)(id)sender;
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    // NSMutableArray *ArrMapping=[[NSMutableArray alloc]init];
    
    
    
    NSString *str=[[[arrListOfChaperones valueForKey:@"result"]  objectAtIndex:sendemailid-6]valueForKey:@"group"];
    if (str != (id)[NSNull null] && ![str isEqualToString:@""] ) {
        
        // NSMutableDictionary *dictChaperone=[[NSMutableDictionary alloc]init];
        [dict setValue:[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:sendemailid-6] valueForKey:@"id"] forKey:@"chaperon_id"];
        [dict setValue:[NSNumber numberWithInt:1] forKey:@"type"];
        [dict setValue:[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:sendemailid-6] valueForKey:@"group"] forKey:@"group_id"];
        //[ArrMapping addObject:dictChaperone];
    }else{
        [[CommonMethods sharedInstance]AlertAction:@"Please assign group to Chaperone"];
        return;
        
    }
    
    
    
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
    //[dict setValue:ArrMapping forKey:@"mapping_id"];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        self.view.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/assign_chaperon_accesscode",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            
            if (!error) {
                self.view.userInteractionEnabled=YES;
                //  [[CommonMethods sharedInstance] AlertAction:[returArray valueForKey:@"result"]];
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                // NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([returArray valueForKey:@"success"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"Access Code has been successfully sent to chaperones email address."];
                    UIButton *removeView;
                    while((removeView = [self.view viewWithTag:1000]) != nil) {
                        [removeView removeFromSuperview];
                        
                    }
                    
                }
                
            }
            else
            {
                self.view.userInteractionEnabled=YES;
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
    
    
}
#pragma mark- editstudent
-(void)IsEditStudent:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_fieldtrip?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                NSLog(@"returArray %@",returArray);
                NSArray *arr=[[NSArray alloc]init];
                arr=[returArray valueForKey:@"result"];
                // NSMutableArray *arrFieldTrip=[[NSMutableArray alloc]init];
                if (arr.count>0) {
                    if (IsStudent) {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AddStudentViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddStudentView"];
                        Controller.IsEditStudent=YES;
                        Controller.dictStudent=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:btn.tag-6];
                        Controller.arrGrades=[arr mutableCopy];
                        [self.navigationController pushViewController:Controller animated:YES ];
                        
                    }else{
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AddChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddChaperoneView"];
                        Controller.IsEditChaperone=YES;
                        Controller.dictChaperone=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:btn.tag-6];
                        Controller.arrfieldTrip=[arr mutableCopy];
                        [self.navigationController pushViewController:Controller animated:YES ];
                        
                    }
                    
                }else{
                    [[CommonMethods sharedInstance] AlertAction:@"First build itinerary "];
                    return ;
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance]AlertAction:@"Server error"];
                return;
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        return;
        
        
        
    }
    
    
    
}

#pragma mark- editstudent
-(void)IsDeleteStudent:(id)sender{
    UIButton *btn=(UIButton *)(id)sender;
    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Delete" message:@"Are you sure you want to delete" delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"OK", nil];
    Alert.tag=1000;
    [Alert show];
    
    y=(int)btn.tag;
    
    
    
}


#pragma mark- SaveChaperoneGroups
-(void)SaveChaperonesGroup:(id)sender{
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSMutableArray *ArrMapping=[[NSMutableArray alloc]init];
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    arr=[arrListOfChaperones valueForKey:@"result"];
    
    
    
    for (int k=0; k<arr.count; k++) {
        NSString *str=[[arr  objectAtIndex:k]valueForKey:@"group"];
        if (str != (id)[NSNull null] && ![str isEqualToString:@""] ) {
            
            NSMutableDictionary *dictChaperone=[[NSMutableDictionary alloc]init];
            [dictChaperone setValue:[[arr objectAtIndex:k] valueForKey:@"id"] forKey:@"chaperon_id"];
            [dictChaperone setValue:[NSNumber numberWithInt:1] forKey:@"type"];
            [dictChaperone setValue:[[arr objectAtIndex:k] valueForKey:@"group"] forKey:@"group_id"];
            [ArrMapping addObject:dictChaperone];
        }
        
    }
    
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
    [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
    [dict setValue:ArrMapping forKey:@"mapping_id"];
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        self.view.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/assign_chaperon_edit",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            
            if (!error) {
                self.view.userInteractionEnabled=YES;
                //  [[CommonMethods sharedInstance] AlertAction:[returArray valueForKey:@"result"]];
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                // NSString *errorstr = [returArray valueForKey:@"error"];
                
                if ([returArray valueForKey:@"success"]) {
                    [[CommonMethods sharedInstance] AlertAction:@"Chaperone is successfully assigned to the group"];
                    UIButton *removeView;
                    while((removeView = [self.view viewWithTag:1000]) != nil) {
                        [removeView removeFromSuperview];
                        
                    }
                    arrListOfChaperones=nil;
                    [self GetListfromApi];
                    
                    //[self viewWillAppear:YES];
                    //arrIndex=nil;
                    // arrIndex=[[NSMutableArray alloc]init];
                    // [self.navigationController popToRootViewControllerAnimated:YES];
                }
                
            }
            else
            {
                self.view.userInteractionEnabled=YES;
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
    
    
    
    
}

#pragma  mark Details of Student

-(void)showViewAtIndex:(NSInteger) index {
    NSLog(@"Data to show on popup: %@", [arrListOfChaperones valueForKey:@"result"]);
    
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    vw.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
    vw.tag=100;
    [self.view addSubview:vw];
    UIView *vwAlerrt=[[UIView alloc]init];
    
    vwAlerrt.frame=CGRectMake(10, ([[UIScreen mainScreen]bounds].size.height/2)-200, [[UIScreen mainScreen]bounds].size.width-20, 450);
    vwAlerrt.layer.cornerRadius=5;
    vwAlerrt.backgroundColor=[UIColor whiteColor];
    
    [vw addSubview:vwAlerrt];
    
    UILabel *lblLine1=[[UILabel alloc]initWithFrame:CGRectMake(0, 40,vwAlerrt.frame.size.width , 1)];
    lblLine1.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine1];
    
    UILabel *lblStudentDetails=[[UILabel alloc]initWithFrame:CGRectMake(0, 10,vwAlerrt.frame.size.width , 30)];
    lblStudentDetails.textColor=[UIColor darkGrayColor];
    lblStudentDetails.text=@"Student Details";
    lblStudentDetails.textAlignment=NSTextAlignmentCenter;
    lblStudentDetails.font=[UIFont fontWithName:@"Roboto-regular" size:18];
    [vwAlerrt addSubview:lblStudentDetails];
    // Text Label Start
    UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(5, 40, vwAlerrt.frame.size.width-10, vwAlerrt.frame.size.height-85)];
    [lblDetail setTextColor:[UIColor darkGrayColor]];
    [lblDetail setFont:[UIFont fontWithName:@"Roboto-regular" size:16]];
    [lblDetail setNumberOfLines:0];
    
    NSMutableArray *arrDetails= [[NSMutableArray alloc]init];
    
    NSString *strName = [NSString stringWithFormat:@"  %@  %@\n\n",[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:index] valueForKey:@"f_name"],[[[arrListOfChaperones valueForKey:@"result"] objectAtIndex:index] valueForKey:@"l_name"]];
    [arrDetails addObject:strName];
    
    
    
    if([[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"class"]objectAtIndex:index] length] > 0)
    {
        NSString *strClass =@"  Class : ";
        strClass =  [strClass stringByAppendingString:[ NSString stringWithFormat:@"%@",[NSString stringWithString:[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"class"]objectAtIndex:index]]]];
        [arrDetails addObject:strClass ];
        
    }
    
    
    
    NSString *strContact1 = @"\n\n   ICE Contact #1 : ";
    if([[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"contact1"]objectAtIndex:index]  length] > 0)
    {
        strContact1 =[strContact1 stringByAppendingString:[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:index] valueForKey:@"contact1"]];
        [arrDetails addObject:strContact1];
    }
    
    NSString *strContact2 = @"\n\n  ICE Contact #2 : ";
    if([[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"contact2"]objectAtIndex:index] length] > 0)
    {
        strContact2 = [strContact2 stringByAppendingString:[[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:index] valueForKey:@"contact2"]];
        [arrDetails addObject:strContact2];
        
    }
    NSString *strPhoto = @"\n\n  Photograph Allowed : ";
    
    if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"photography_allowed"] intValue] == 1)
    {
        strPhoto = [strPhoto stringByAppendingString:@"YES"];
        [arrDetails addObject:strPhoto];
    }
    else
    {
        strPhoto = [strPhoto stringByAppendingString:@"NO"];
        [arrDetails addObject:strPhoto];
    }
    
    
    
    
    
    
    NSString *strSpecialNeeds = @"\n\n  Special Needs : ";
    if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"special_need"] intValue] > 0)
    {
        strSpecialNeeds =[ strSpecialNeeds stringByAppendingString:[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"special_need_type"]];
        [arrDetails addObject:strSpecialNeeds];
        
    }
    NSString *strMedicin = @"\n\n  Medicines : ";
    if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"medicine"] intValue] == 1)
    {
        strMedicin = [strMedicin stringByAppendingString: @"YES"];
        [arrDetails addObject:strMedicin];
        
    }
    else
    {
        strMedicin = [strMedicin stringByAppendingString: @"NO"];
        [arrDetails addObject:strMedicin];
    }
    
    NSString *strSpendingMoney = @"\n\n  Spending Money : ";
    if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"spending_money"] intValue] > 0)
    {
        strSpendingMoney = [strSpendingMoney stringByAppendingString:[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"spending_money_amt"]];
        [arrDetails addObject:strSpendingMoney];
    }
    
    NSString *strAllergies = @"\n\n Allergies : ";
    if([[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"allergies"] intValue] > 0)
    {
        strAllergies =
        [strAllergies stringByAppendingString:[NSString stringWithString:[[[[arrListOfChaperones valueForKey:@"result"] valueForKey:@"check_list"]objectAtIndex:index] valueForKey:@"allergy_details"]]];
        [arrDetails addObject:strAllergies];
    }
    
    NSString *strText;// = [NSString stringWithFormat:@"%@\n\nClass : %@\n\nICE Contact #1 : %@\n\nICE Contact #2 : %@\n\nPhotograph Allowed : %@\n\nSpecial Needs : %@\n\nMedicines : %@\n\nSpending Money : %@\n\nAllergies : %@", strName , strClass, strContact1, strContact2, strPhoto, strSpecialNeeds, strMedicin, strSpendingMoney, strAllergies];
    
    NSMutableString *str = [[NSMutableString alloc]init];
    for (int i = 0 ;i< arrDetails.count; i++)
    {
        
        strText = [arrDetails objectAtIndex:i];
        [str appendString:strText];
        
        
    }
    [lblDetail setText:str];
    [lblDetail sizeToFit];
    [vwAlerrt addSubview:lblDetail];
    //Text Label End
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, vwAlerrt.frame.size.height-45,vwAlerrt.frame.size.width , 1)];
    lblLine.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine];
    
    UIButton *btnOk=[UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.frame=CGRectMake((vwAlerrt.frame.size.width/2)-60, vwAlerrt.frame.size.height-40, 120, 35);
    [btnOk setTitle:@"OK" forState:UIControlStateNormal];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk setBackgroundColor:[UIColor redColor]];
    [btnOk addTarget:self  action:@selector(DismisstheView:) forControlEvents:UIControlEventTouchUpInside];
    [vwAlerrt addSubview:btnOk];
    
    
}
#pragma mark- DismisstheView
-(void)DismisstheView:(id)sender{
    UIView *removeView;
    while((removeView = [self.view viewWithTag:100]) != nil) {
        [removeView removeFromSuperview];
        
    }
    
    
}



#pragma mark- ShowLabelOfNoList
-(UILabel *)LabelShow:(NSString *)strTitl{
    UILabel *Lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( [[UIScreen mainScreen]bounds].size.height/2)-20, [[UIScreen mainScreen]bounds].size.width-40, 40)];
    Lbl.text=strTitl;
    Lbl.textAlignment=NSTextAlignmentCenter;
    
    Lbl.textColor=[UIColor darkGrayColor];
    [self.view addSubview:Lbl];
    
    return Lbl;
    
}

/*
 #pragma mark- EditDeleteButtons
 - (NSArray*)utilButtons
 {
 if (IsStudent) {
 NSDictionary *moreButton = @{ButtonTitle: @"",
 ButtonColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"edit_icon_50"]]};
 NSDictionary *deleteButton = @{ButtonTitle: @"",
 ButtonColor:  [UIColor colorWithPatternImage:[UIImage imageNamed:@"delete_icon_50"]]};
 
 return @[moreButton, deleteButton];
 
 }else{
 
 NSDictionary *moreButton = @{ButtonTitle: @"",
 ButtonColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"edit_icon_50"]]};
 NSDictionary *deleteButton = @{ButtonTitle: @"",
 ButtonColor:  [UIColor colorWithPatternImage:[UIImage imageNamed:@"delete_icon_50"]]};
 
 return @[moreButton, deleteButton];
 
 
 }
 
 }
 
 
 
 #pragma mark- tableViewDelegateSwipe
 
 -(void)didClickOnButtonWithIdentifier:(NSInteger)buttonIdentifier onCell:(id)cell
 {
 ChaperoneListTableViewCell *tableViewCell = (ChaperoneListTableViewCell*)cell;
 switch (buttonIdentifier) {
 case 0:
 {
 
 BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
 if (isinternet) {
 [[CommonMethods sharedInstance] addSpinner:self.view];
 [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_fieldtrip?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
 if (!error) {
 [[CommonMethods sharedInstance] removeSpinner:self.view];
 
 NSLog(@"returArray %@",returArray);
 NSArray *arr=[[NSArray alloc]init];
 arr=[returArray valueForKey:@"result"];
 // NSMutableArray *arrFieldTrip=[[NSMutableArray alloc]init];
 if (arr.count>0) {
 if (IsStudent) {
 UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 AddStudentViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddStudentView"];
 Controller.IsEditStudent=YES;
 Controller.dictStudent=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:tableViewCell.tag];
 Controller.arrGrades=[arr mutableCopy];
 [self.navigationController pushViewController:Controller animated:YES ];
 
 }else{
 UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 AddChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddChaperoneView"];
 Controller.IsEditChaperone=YES;
 Controller.dictChaperone=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:tableViewCell.tag];
 Controller.arrfieldTrip=[arr mutableCopy];
 [self.navigationController pushViewController:Controller animated:YES ];
 
 }
 
 }else{
 [[CommonMethods sharedInstance] AlertAction:@"First build itinerary "];
 return ;
 }
 
 }
 else
 {
 [[CommonMethods sharedInstance]AlertAction:@"Server error"];
 return;
 }
 }];
 } else
 {
 [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
 return;
 
 
 
 }
 
 
 
 
 
 
 
 
 //            if (IsStudent) {
 //                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 //                AddStudentViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddStudentView"];
 //                Controller.IsEditStudent=YES;
 //                Controller.dictStudent=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:tableViewCell.tag];
 //                [self.navigationController pushViewController:Controller animated:YES ];
 //
 //            }else{
 //                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 //                AddChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AddChaperoneView"];
 //                Controller.IsEditChaperone=YES;
 //                Controller.dictChaperone=[[arrListOfChaperones valueForKey:@"result"]objectAtIndex:tableViewCell.tag];
 //                [self.navigationController pushViewController:Controller animated:YES ];
 //
 //            }
 }
 NSLog(@"Edit Button Got Clicked on Cell : %ld", (long)tableViewCell.tag);
 break;
 case 1:
 {
 UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to delete " delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"OK", nil];
 [Alert show];
 
 y=(int)tableViewCell.tag;
 
 break;
 
 }
 
 }
 }
 
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 return NO;
 }
 
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else {
 // NSLog(@"Unhandled editing style! %d", editingStyle);
 }
 }
 
 
 -(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
 
 
 //insert your editAction here
 }];
 //editAction.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Cross_mark"]];
 editAction.backgroundColor=[UIColor blueColor];
 
 UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
 //[arrListOfChaperones removeObjectAtIndex:indexPath.row];
 [tableView reloadData];
 
 }];
 deleteAction.backgroundColor = [UIColor redColor];
 return @[deleteAction,editAction];
 
 }
 */



@end
