//
//  ProfileViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 7/25/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "ProfileViewController.h"
#import "CommonMethods.h"
#import "Constants.h"
#import "WebService.h"

@interface ProfileViewController ()<UIScrollViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    UIScrollView *sclVw;
    UITextField *tfName,*tfSchoolName,*tfEmail,*tfPhn,*tfLastName;
    CGPoint svos;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"My Profile";
    [self CreateUi];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- BackButtonPressed
-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark-CreateUi
-(void)CreateUi{
    
    sclVw=[[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    sclVw.contentSize=CGSizeMake( [[UIScreen mainScreen]bounds].size.width, 550);
    [self.view addSubview:sclVw];
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate=self;
    [tapper setCancelsTouchesInView:NO];
    [sclVw addGestureRecognizer:tapper];
    
    
    int k,x;
    UIImageView *imgvwBoy;
    if (IS_IPAD) {
        k=100;
        x=320;
        imgvwBoy=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, 70.0, 240, 200)];
        
    }else{
        x=220;
        k=[[UIScreen mainScreen]bounds].size.width/3;
        imgvwBoy=[[UIImageView alloc]initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-120, 10.0, 240, 200)];
    }
    
    imgvwBoy.contentMode=UIViewContentModeScaleAspectFit;
    imgvwBoy.image=[UIImage imageNamed:@"boy1_icon"];
    [sclVw addSubview:imgvwBoy];
    
    
    
    tfName=[self creatertextfild:CGRectMake(20.0, x,[[UIScreen mainScreen]bounds].size.width/2-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"user_name"]];
    tfName.placeholder=@"First name";
    tfName.returnKeyType=UIReturnKeyNext;
    
    tfLastName=[self creatertextfild:CGRectMake([[UIScreen mainScreen]bounds].size.width/2+20, x,[[UIScreen mainScreen]bounds].size.width/2-40, 30) andView:sclVw andImage:[UIImage imageNamed:@""]];
    tfLastName.placeholder=@" Last name";
    tfLastName.returnKeyType=UIReturnKeyNext;
    
    
    tfSchoolName=[self creatertextfild:CGRectMake(20.0, tfName.frame.origin.y+45,[[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"school_icon"]];
    tfSchoolName.placeholder=@"School name";
    tfSchoolName.returnKeyType=UIReturnKeyNext;
    
    
    tfEmail=[self creatertextfild:CGRectMake(20.0, tfSchoolName.frame.origin.y+45, [[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"mail_icon"]];
    tfEmail.placeholder=@"Email address";
    tfEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    tfEmail.returnKeyType=UIReturnKeyNext;
    tfEmail.userInteractionEnabled=NO;
    
    
    tfPhn=[self creatertextfild:CGRectMake(20.0, tfEmail.frame.origin.y+45,[[UIScreen mainScreen]bounds].size.width-40, 30) andView:sclVw andImage:[UIImage imageNamed:@"phone_icon"]];
    tfPhn.placeholder=@" Contact no#";
    tfPhn.returnKeyType=UIReturnKeyDone;
    tfPhn.keyboardType=UIKeyboardTypePhonePad;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                                  target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    tfPhn.inputAccessoryView = toolBar;
    // tfFieldTrip.inputAccessoryView=toolBar;
    
    
    
    
    UIButton *BtnUpdate=[UIButton buttonWithType:UIButtonTypeCustom];
    [BtnUpdate setTitle:@"Update profile" forState:UIControlStateNormal];
    BtnUpdate.frame=CGRectMake(20.0,tfPhn.frame.origin.y+70,[[UIScreen mainScreen]bounds].size.width-40, 45);
    [BtnUpdate addTarget:self action:@selector(updateTeacher:) forControlEvents:UIControlEventTouchUpInside];
    BtnUpdate.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    
    [sclVw addSubview:BtnUpdate];
    [self getUserprofile];
    
    
}
#pragma mark- handleSingleTap
-(void)handleSingleTap:(id)sender{
    [sclVw setContentOffset:svos animated:YES];
    [self.view endEditing:YES];
    
}
#pragma mark-DoneButton
-(void)done:(id)sender{
  //  [tfPhn resignFirstResponder];
    
    NSString *strippedNumber = [tfPhn.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [tfPhn.text length])];
    if ([self validatePhoneNumberWithString1:strippedNumber]) {
        [sclVw setContentOffset:svos animated:YES];
        [tfPhn resignFirstResponder];
    }
    else
    {
       // [tfPhn resignFirstResponder];
        
        [[CommonMethods sharedInstance]AlertAction:@"Please enter valid phone number"];
        
    }
    
//    if (!(IS_IPAD)) {
//        [sclVw setContentOffset:svos animated:YES];
//    }
    
    
}
#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (!(IS_IPAD)) {
        if (textField!=tfName && textField!=tfSchoolName && textField!=tfLastName) {
            svos = sclVw.contentOffset;
            CGPoint pt;
            CGRect rc = [textField bounds];
            rc = [textField convertRect:rc toView:sclVw];
            pt = rc.origin;
            pt.x = 0;
            pt.y -= 270;
            [sclVw setContentOffset:pt animated:YES];
            
        }
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (!(IS_IPAD)) {
        [sclVw setContentOffset:svos animated:YES];
    }
    if (textField==tfName) {
        [tfEmail becomeFirstResponder];
        
        //    }else if (textField==tfSchoolName){
        //        [tfEmail becomeFirstResponder];
        
    }else if (textField==tfEmail){
        
        [tfPhn becomeFirstResponder];
    }else if (textField==tfPhn){
        
        
    }
    
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField==tfPhn) {
        NSInteger length = [self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if ([textField.text hasPrefix:@"1"]) {
            if(length == 11)
            {
                if(range.length == 0)
                    return NO;
            }
            if(length == 4)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"%@ (%@) ",[num substringToIndex:1],[num substringFromIndex:1]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:4]];
            }
            else if(length == 7)
            {
                NSString *num = [self formatNumber:textField.text];
                NSRange numRange = NSMakeRange(1, 3);
                textField.text = [NSString stringWithFormat:@"%@ (%@) %@-",[num substringToIndex:1] ,[num substringWithRange:numRange],[num substringFromIndex:4]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
            
        } else {
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:textField.text];
                
                textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        return YES;
   
    }
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if (textField==tfPhn ){
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return ([string isEqualToString:filtered] && newLength <= 15) || returnKey ;
    }else if (textField==tfName){
        return newLength <= 30;
        
    }
    return newLength <= 80;
}

#pragma mark- updateTeacher
-(void)updateTeacher:(id)sender{
    
     NSString *strippedNumber = [tfPhn.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [tfPhn.text length])];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    //  BOOL isEmailValid=[self NSStringIsValidEmail:tfEmail.text];
    
    
    if (tfName.text.length==0) {
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide input for name"];
        return;
    }else if (tfLastName.text.length==0) {
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide input for last name"];
        return;
    } else if (tfSchoolName.text.length==0) {
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide input for school name"];
        return;
        
    }else  if (tfPhn.text.length==0) {
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide input for contact number"];
        return;
    }else if (tfPhn.text.length<10){
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide valid contact number"];
        return;
    }
    
    else if (![self validatePhoneNumberWithString1:strippedNumber])
    {
        [[CommonMethods sharedInstance]AlertAction:@"Kindly provide valid contact number"];
        return;
    }
    else{
        NSString *strName=[[tfName.text stringByAppendingString:@"`"] stringByAppendingString:tfLastName.text];
        
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
        [dict setValue:strName forKey:@"name"];
        [dict setValue:tfSchoolName.text forKey:@"school_name"];
        [dict setValue:tfPhn.text forKey:@"contact_number"];
        [dict setValue:[NSNumber numberWithInt:2] forKey:@"platform"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/post_teacher_details",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"success"]){
                        
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        NSString *substrings = strName;
                        
                        
                        NSMutableString * string = [substrings mutableCopy];
                        [string replaceOccurrencesOfString:@"`"
                                                withString:@" "
                                                   options:0
                                                     range:NSMakeRange(0, string.length)];
                        
                        [prefs setObject:string forKey:@"name"];
                        
                        
                        
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                        
                        
                    }
                }
                else
                {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
            
        }else{
            
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
    }
}


//- (BOOL)validatePhone:(NSString *)phoneNumber
//{
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,11}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//    
//    return [phoneTest evaluateWithObject:phoneNumber];
//}

#pragma mark-createTextfield
-(UITextField *)creatertextfild:(CGRect )frame andView:(UIView *)vw andImage:(UIImage *)Img{
    
    UITextField *txtfld=[[UITextField alloc]initWithFrame:frame];
    txtfld.delegate=self;
    txtfld.spellCheckingType = UITextSpellCheckingTypeNo;
    txtfld.autocorrectionType = UITextAutocorrectionTypeNo;
    txtfld.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    if (Img!=nil) {
        UIImageView *imgVw = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,40, txtfld.frame.size.height-5)];
        txtfld.leftView = imgVw;
        imgVw.image=Img;
        imgVw.contentMode=UIViewContentModeScaleAspectFit;
        txtfld.leftViewMode = UITextFieldViewModeAlways;
    }
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, txtfld.frame.size.height - borderWidth, txtfld.frame.size.width, txtfld.frame.size.height);
    border.borderWidth = borderWidth;
    [txtfld.layer addSublayer:border];
    txtfld.layer.masksToBounds = YES;
    
    [vw addSubview:txtfld];
    return txtfld;
}

#pragma mark- EmailValidation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark- PhoneMasking
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
        
    }
    
    
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
    
    
}
-(void)getUserprofile{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_details?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                NSLog(@"returArray %@",returArray);
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[returArray valueForKey:@"Teacher"];
                if ([dict allKeys].count>0) {
                    tfEmail.text=[dict valueForKey:@"email"];
                    tfPhn.text=[dict valueForKey:@"contact"];
                    tfSchoolName.text=[dict valueForKey:@"school"];
                    NSArray *substrings = [[dict valueForKey:@"name"] componentsSeparatedByString:@"`"];
                    tfName.text=[substrings objectAtIndex:0];
                    tfLastName.text=[substrings objectAtIndex:1];
                    
                    
                }
                
            }
            
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }

}

- (BOOL)validatePhoneNumberWithString1:(NSString *)string {
    
    if (string.length>2) {
        string = [string substringWithRange:NSMakeRange(0, 3)];
        if ([string isEqualToString:@"000"]) {
            return NO;
        
        }
        else
        {
            return YES;
        }

    }
    return NO;
  
}


@end
