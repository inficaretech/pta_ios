//
//  ChaperoneDetailsViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/2/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "ChaperoneDetailsViewController.h"
#import "QuizPageViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "GroupDetailsTableViewCell.h"
#import "DatabaseManagement.h"
#import "DataBaseFetchValues.h"
#import "Constants.h"

@interface ChaperoneDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *arrStudentList;
    UITableView *tblVw;
    
}

@end

@implementation ChaperoneDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrStudentList=[[NSMutableArray alloc]init];
    [self CustomNavigationBar];
    [self createUi];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-CustomNavigationBar
-(void)CustomNavigationBar
{
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    self.navigationItem.title =[[NSUserDefaults standardUserDefaults] valueForKey:@"group_name"];
    
}

#pragma mark- BackButtonPressed

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-createUi
-(void)createUi{
    
    
    UIImageView *ImgVw=[[UIImageView alloc]initWithFrame:CGRectMake(20, 100.0, 160.0, 80)];
    ImgVw.image=[UIImage imageNamed:@"quiz_icon"];
    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
    // ImgVw.backgroundColor=[UIColor blueColor];
    [self.view addSubview:ImgVw];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(50, 180.0, 80.0, 40.0);
    [btn setTitle:@"QUIZ" forState:UIControlStateNormal];
    btn.backgroundColor=[UIColor blackColor];
    
    [btn addTarget:self action:@selector(GoToQuizPage:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:btn];
    
    
    
    
    UILabel *lblScore=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-130, 180.0, 80.0, 40.0)];
    lblScore.text=@"SCORE";
    lblScore.textAlignment=NSTextAlignmentCenter;
    lblScore.backgroundColor=[UIColor blackColor];
    lblScore.textColor=[UIColor whiteColor];
    
    [self.view addSubview:lblScore];
    
    UILabel *lblScoreCount=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-110, 140.0, 40.0, 30.0)];
    lblScoreCount.text=[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:@"MarksObtained"] intValue]];
    //lblScoreCount.text=@"100";
    lblScoreCount.font=[UIFont fontWithName:@"Roboto-regular" size:23];
    lblScoreCount.textAlignment=NSTextAlignmentCenter;
    // lblScoreCount.backgroundColor=[UIColor blackColor];
    lblScoreCount.textColor=[UIColor darkGrayColor];
    lblScoreCount.adjustsFontSizeToFitWidth=YES;
    
    [self.view addSubview:lblScoreCount];
    
    
    tblVw=[[UITableView alloc]initWithFrame:CGRectMake(0.0, 250.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-300)];
    [self.view addSubview:tblVw];
    tblVw.delegate=self;
    tblVw.dataSource=self;
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
    
    
    
    [self getStudentListForChaperone];
    //     [[CommonMethods sharedInstance] AddMenuBaronChaperone:self.view andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )];
    
    
}

#pragma mark-GoToQuizPage
-(void)GoToQuizPage:(id)sender{
    [self getQustionAndAnswerFromServer];
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    QuizPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"QuizPageView"];
//    [self.navigationController pushViewController:Controller animated:YES ];
    
    
}
#pragma mark-TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrStudentList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"GroupDetailsTableViewCell";
    
    GroupDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UILabel *lblline=[[UILabel alloc]initWithFrame:CGRectMake(0.0,49, [[UIScreen mainScreen]bounds].size.width, 1)];
        lblline.backgroundColor=[UIColor lightGrayColor];
        [cell.contentView addSubview:lblline];
        
    }
    cell.imgVwMoney.image=nil;
    cell.imgVwCamera.image=nil;
    cell.imgVwAllergies.image=nil;
    cell.imgVwMedicines.image=nil;
    cell.imgVwSpecialNeeds.image=nil;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(arrStudentList.count>0){
        cell.lblStudentName.numberOfLines=2;
        cell.lblStudentName.text=[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"name"];
        NSMutableArray *arrImgaes=[[NSMutableArray alloc]init];
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"photography_allowed"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"photography_allowed"];
            
        }else if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"photography_allowed"]isEqualToString:@"0"]){
            [arrImgaes addObject:@"camera_icon - cross"];
        }
        
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"special_need"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"special_need"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"medicine"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"medicine_icon"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"spending_money"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"dollar_icon"];
            
        }
        if ([[[arrStudentList objectAtIndex:indexPath.row]valueForKey:@"allergies"]isEqualToString:@"1"]) {
            [arrImgaes addObject:@"allergy_icon"];
            
        }
        
        if (arrImgaes.count==1) {
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            
        }else if (arrImgaes.count==2){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
        }else if (arrImgaes.count==3){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
        }else if (arrImgaes.count==4){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
            cell.imgVwMoney.image=[UIImage imageNamed:[arrImgaes objectAtIndex:3]];
        }else if (arrImgaes.count==5){
            cell.imgVwCamera.image=[UIImage imageNamed:[arrImgaes objectAtIndex:0]];
            cell.imgVwSpecialNeeds.image=[UIImage imageNamed:[arrImgaes objectAtIndex:1]];
            cell.imgVwMedicines.image=[UIImage imageNamed:[arrImgaes objectAtIndex:2]];
            cell.imgVwMoney.image=[UIImage imageNamed:[arrImgaes objectAtIndex:3]];
            cell.imgVwAllergies.image=[UIImage imageNamed:[arrImgaes objectAtIndex:4]];
            
            
        }
        
        arrImgaes=nil;
        
    }
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}

#pragma mark- getListFromApi
-(void)getStudentListForChaperone{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/students_by_group_id?group_id=%@&access_token=%@&teacher_id=%@&trip_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"group_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                if ([returArray valueForKey:@"success"]) {
                    arrStudentList=[returArray valueForKey:@"result"];
                    [tblVw reloadData];
                    
                    /////////////////SaveInDatabase
                    [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Chaperone_Student_List"];
                    
                    for (int k=0; k<arrStudentList.count; k++) {
                        [[DatabaseManagement sharedInstance] SavedataInDatabaseforChaperoneStudentList:[arrStudentList objectAtIndex:k]];
                    }
                    
                    
                    
                }
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
            }
        }];
    }
    else
    {
        
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        arrStudentList = [[DataBaseFetchValues sharedInstance] fetchAllDataWithTableName:@"Chaperone_Student_List"];
        [tblVw reloadData];
        
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self showViewAtIndex:(int)indexPath.row];
    
    
}
-(void)showViewAtIndex:(int) index{
    NSLog(@"Data to show on popup: %@", [arrStudentList objectAtIndex:index]);
    
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)];
    vw.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
    vw.tag=100;
    [self.view addSubview:vw];
    UIView *vwAlerrt=[[UIView alloc]init];
    
    vwAlerrt.frame=CGRectMake(10, ([[UIScreen mainScreen]bounds].size.height/2)-200, [[UIScreen mainScreen]bounds].size.width-20, 450);
    vwAlerrt.layer.cornerRadius=5;
    vwAlerrt.backgroundColor=[UIColor whiteColor];
    
    [vw addSubview:vwAlerrt];
    
    UILabel *lblLine1=[[UILabel alloc]initWithFrame:CGRectMake(0, 40,vwAlerrt.frame.size.width , 1)];
    lblLine1.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine1];
    
    UILabel *lblStudentDetails=[[UILabel alloc]initWithFrame:CGRectMake(0, 10,vwAlerrt.frame.size.width , 30)];
    lblStudentDetails.textColor=[UIColor darkGrayColor];
    lblStudentDetails.text=@"Student Details";
    lblStudentDetails.textAlignment=NSTextAlignmentCenter;
    lblStudentDetails.font=[UIFont fontWithName:@"Roboto-regular" size:18];
    [vwAlerrt addSubview:lblStudentDetails];
    // Text Label Start
    UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(5, 40, vwAlerrt.frame.size.width-10, vwAlerrt.frame.size.height-85)];
    [lblDetail setTextColor:[UIColor darkGrayColor]];
    [lblDetail setFont:[UIFont fontWithName:@"Roboto-regular" size:16]];
    [lblDetail setNumberOfLines:0];
    NSMutableArray *arrDetail = [[NSMutableArray alloc]init];
    [arrDetail addObject:[NSString stringWithFormat:@"  %@",[[arrStudentList objectAtIndex:index] valueForKey:@"name"]]];
    
    NSString *strClass = @"\n\n  Class : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"class"] length] > 0)
    {
        strClass = [strClass stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"class"]];
        [arrDetail addObject:strClass];
    }
    NSString *strContact1 = @"\n\n  ICE Contact #1 : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"contact1"] length] > 0)
    {
        strContact1 = [strContact1 stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"contact1"]];
        [arrDetail addObject:strContact1];
        
    }
    
    NSString *strContact2 = @"\n\n  ICE Contact #2 : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"contact2"] length] > 0)
    {
        strContact2 =[strContact2 stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"contact2"]];
        [arrDetail addObject:strContact2];
    }
    
    NSString *strPhoto = @"\n\n  Photograph Allowed : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"photography_allowed"] intValue] == 1)
    {
        strPhoto = [strPhoto stringByAppendingString:@"YES"];
        [arrDetail addObject:strPhoto];
    }
    else
    {
        strPhoto = [strPhoto stringByAppendingString:@"NO"];
        [arrDetail addObject:strPhoto];
    }
    NSString *strSpecialNeeds = @"\n\n  Special Needs : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"special_need"] intValue] > 0)
    {
        strSpecialNeeds = [strSpecialNeeds stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"special_need_details"]];
        [arrDetail addObject:strSpecialNeeds];
                           
        }
    
    NSString *strMedicin = @"\n\n  Medicines : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"medicine"] intValue] == 1)
    {
        strMedicin = [strMedicin stringByAppendingString:@"YES"];
        [arrDetail addObject:strMedicin];
    }
    else
    {
        strMedicin = [strMedicin stringByAppendingString:@"NO"];
        [arrDetail addObject:strMedicin];

    }
    NSString *strSpendingMoney = @"\n\n  Spending Money : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"spending_money"] intValue] > 0)
    {
        strSpendingMoney = [strSpendingMoney stringByAppendingString:[[arrStudentList objectAtIndex:index] valueForKey:@"spending_money_amount"]];
        [arrDetail addObject:strSpendingMoney];
    }
    
    NSString *strAllergies = @"\n\n  Allergies : ";
    if([[[arrStudentList objectAtIndex:index] valueForKey:@"allergies"] intValue] > 0)
    {
        strAllergies = [strAllergies stringByAppendingString: [[arrStudentList objectAtIndex:index] valueForKey:@"allergy_details"]];
        [arrDetail addObject:strAllergies];
    }
    
    
    NSString *strText ;//= [NSString stringWithFormat:@"%@\n\nClass : %@\n\nICE Contact #1 : %@\n\nICE Contact #2 : %@\n\nPhotograph Allowed : %@\n\nSpecial Needs : %@\n\nMedicines : %@\n\nSpending Money : %@\n\nAllergies : %@", [[arrStudentList objectAtIndex:index] valueForKey:@"name"], strClass, strContact1, strContact2, strPhoto, strSpecialNeeds, strMedicin, strSpendingMoney, strAllergies];
   
    NSMutableString *str = [[NSMutableString alloc]init];
    for (int i = 0; i< arrDetail.count; i++) {
        strText = [arrDetail objectAtIndex:i];
        [str appendString:strText];
    }
    [lblDetail setText:str];
    [lblDetail sizeToFit];
    [vwAlerrt addSubview:lblDetail];
    //Text Label End
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, vwAlerrt.frame.size.height-45,vwAlerrt.frame.size.width , 1)];
    lblLine.backgroundColor=[UIColor lightGrayColor];
    [vwAlerrt addSubview:lblLine];
    
    UIButton *btnOk=[UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.frame=CGRectMake((vwAlerrt.frame.size.width/2)-60, vwAlerrt.frame.size.height-40, 120, 35);
    [btnOk setTitle:@"OK" forState:UIControlStateNormal];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk setBackgroundColor:[UIColor redColor]];
    [btnOk addTarget:self  action:@selector(DismisstheView:) forControlEvents:UIControlEventTouchUpInside];
    [vwAlerrt addSubview:btnOk];
    
    
    
    
}
#pragma mark- DismisstheView
-(void)DismisstheView:(id)sender{
    UIView *removeView;
    while((removeView = [self.view viewWithTag:100]) != nil) {
        [removeView removeFromSuperview];
        
    }
    
    
}
#pragma mark- Addlabel
-(UILabel *)addlabel:(CGRect )frame andvw:(UIView *)vw andtitl:(NSString *)strTitl{
    UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    lbl.text=strTitl;
    lbl.textAlignment=NSTextAlignmentLeft;
    
    
    
    [vw addSubview:lbl];
    
    
    return lbl;
    
    
}

#pragma mark- GetQuestion
-(void)getQustionAndAnswerFromServer{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/get_question_detail?access_token=%@&teacher_id=%@&trip_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                if ([returArray valueForKey:@"current_date"] && [returArray valueForKey:@"itinerary_date"]) {
                    if([[[returArray valueForKey:@"current_date"] substringToIndex:10] isEqualToString:[[returArray valueForKey:@"itinerary_date"] substringToIndex:10]]){
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        QuizPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"QuizPageView"];
                        [self.navigationController pushViewController:Controller animated:YES ];
                    }
                    else{
                        [[CommonMethods sharedInstance] AlertAction:@"You can only play quiz on the field trip day."];
                    }
                }
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
            }
        }];
    }
    else{
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"currentDate"] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"itineraryDate"]]){
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            QuizPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"QuizPageView"];
            [self.navigationController pushViewController:Controller animated:YES ];
        }
        else{
            [[CommonMethods sharedInstance] AlertAction:@"You can only play quiz on the field trip day."];
        }

    }
}


@end
