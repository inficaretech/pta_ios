//
//  HomeViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/2/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "HomeViewController.h"
#import "Constants.h"
#import "ManageChaperonesViewController.h"
#import "UsefulLinkTableViewController.h"
#import "AppDelegate.h"
#import "AboutGroupViewController.h"
#import "MyItineraryViewController.h"
#import "StartingPageViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
//#import "AssignGroupViewController.h"
//#import "PanormaViewController.h"
#import "GroupScoresViewController.h"
#import "AssignGroupToStudentViewController.h"
#import "SelectTypeOfItineraryViewController.h"
#import "GalleryViewController.h"
#import "ProfileViewController.h"
#import "WallOfFameViewController.h"
#import "OutreachProgramsViewController.h"
#import "FieldTripPricingViewController.h"
#import "HomeTableViewCell.h"
#import "ChaperoneInstructionsViewController.h"


@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>{
    // UIButton *btnLogout;
    UIImageView *imgVwMenu;
    
}

@end

@implementation HomeViewController
@synthesize strName;
- (void)viewDidLoad {
    [super viewDidLoad];
  
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self CustomNavigationBar];
    [self createUi];
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isVerified"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.navigationItem.title =[prefs valueForKey:@"name"];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- CustomNavigation
-(void)CustomNavigationBar
{
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.navigationItem.hidesBackButton=YES;
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdel.navController=self.navigationController;
    
    //    btnLogout = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btnLogout.frame = CGRectMake(10.0, 10.0, 20.0, 20.0);
    //    [btnLogout setImage:[UIImage imageNamed:@"logout_button"] forState:UIControlStateNormal];
    //    [btnLogout addTarget:self action:@selector(LogOutButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *btnAdd =[[UIBarButtonItem alloc] initWithCustomView:btnLogout];
    //    self.navigationItem.rightBarButtonItem =btnAdd;
    
}
#pragma mark-CreateUi
-(void)createUi{
    
    imgVwMenu= [[CommonMethods sharedInstance] AddMenu:self.navigationController.navigationBar andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-50, 5,35 ,35)];
    
    UIImageView *imageLogo = [[UIImageView alloc]init];
     UITableView *tblVw=[[UITableView alloc]init];
    if (IS_IPAD) {
        imageLogo.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2-125, 64, 250, 150);
         tblVw.frame =CGRectMake(0.0, 224.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-164);
    }
    else
    {
        imageLogo.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2-70, 64, 140, 80);
         tblVw.frame =CGRectMake(0.0, 144.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-144);
    }
    
    [self.view addSubview:imageLogo];
    imageLogo.image = [UIImage imageNamed:@"Putnam_logo"];
    imageLogo.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.navigationController.navigationBar addSubview:imgVwMenu];
   
    UIButton *buttonGalleryView = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonGalleryView.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-50, 78, 40.0, 40.0);
    [self.view addSubview:buttonGalleryView];
    [buttonGalleryView setImage:[UIImage imageNamed:@"360_icon"] forState:UIControlStateNormal];
    [buttonGalleryView addTarget:self action:@selector(buttonGalleryViewPrssed) forControlEvents:UIControlEventTouchUpInside];
    
   
    tblVw.separatorStyle=UITableViewCellSeparatorStyleNone;
    tblVw.backgroundColor=[UIColor whiteColor];
    
    tblVw.delegate=self;
    tblVw.dataSource=self;
    [self.view addSubview:tblVw];
    if (IS_IPAD) {
        tblVw.scrollEnabled=NO;
    }
    
}


-(void)buttonGalleryViewPrssed
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            GalleryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GalleryViewController"];
            [self.navigationController pushViewController:assignGroupView animated:YES];
}

#pragma mark-LogOut
-(void)LogOutButtonPressed:(id)sender{
    
    
    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to logout" delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"OK", nil];
    [Alert show];
    /*
     UIButton *btn=(UIButton *)(id)sender;
     NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
     [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
     [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"id"];
     BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
     if (isinternet) {
     btn.userInteractionEnabled=NO;
     [[CommonMethods sharedInstance] addSpinner:self.view];
     [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/logout",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
     
     if (!error) {
     btn.userInteractionEnabled=YES;
     [[CommonMethods sharedInstance] removeSpinner:self.view];
     NSLog(@"returArray %@",returArray);
     //  NSString *errorstr = [returArray valueForKey:@"success"];
     
     if([returArray valueForKey:@"success"]){
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     [prefs setBool:NO forKey:@"isVerified"];
     [[NSUserDefaults standardUserDefaults] synchronize];
     //[self.navigationController popToRootViewControllerAnimated:YES];
     
     UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
     UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
     AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     appdel.window.rootViewController = obj;
     }
     
     }
     else
     { btn.userInteractionEnabled=YES;
     [[CommonMethods sharedInstance] removeSpinner:self.view];
     
     }
     }];
     
     
     }else
     {
     [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
     
     
     }
     
     */
    
}

#pragma mark- AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex==0) {
        
    }else if (buttonIndex==1){
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"id"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            // btnLogout.userInteractionEnabled=NO;
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/logout",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    // btnLogout.userInteractionEnabled=YES;
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    //  NSString *errorstr = [returArray valueForKey:@"success"];
                    
                    //if([returArray valueForKey:@"success"]){
                    {
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setBool:NO forKey:@"isVerified"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //[self.navigationController popToRootViewControllerAnimated:YES];
                        
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
                        UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appdel.window.rootViewController = obj;
                    }
                    
                }
                else
                {
                    //btnLogout.userInteractionEnabled=YES;
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
    }
}
#pragma mark- TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier=@"HomeTableViewCell";
    HomeTableViewCell *cell=(HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  // UIButton *btn;
   // UIView *topView;
   // UIImageView *imgv;
    if (cell==nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        
        
        
        
    }
    //btn=[UIButton buttonWithType:UIButtonTypeCustom];
  cell.btn.frame=CGRectMake(20.0, 10.0, [[UIScreen mainScreen]bounds].size.width-40, 50);
    [ cell.btn addTarget:self action:@selector(ButtonsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
     cell.btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
     cell.btn.contentEdgeInsets = UIEdgeInsetsMake(0, 90, 0, 0);
     cell.btn.layer.shadowColor = [UIColor grayColor].CGColor;
     cell.btn.layer.shadowOffset = CGSizeMake(0, 1.0);
     cell.btn.layer.shadowOpacity = 1.0;
     cell.btn.layer.shadowRadius = 3.0;
    
//    topView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0,5.0, cell.btn.frame.size.height)];
    cell.topView.opaque = YES;
//    [ cell.btn addSubview:topView];
//    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(20.0, 10, 30, 30)];
  cell.ImgVw.contentMode=UIViewContentModeScaleAspectFill;
//    [ cell.btn addSubview:imgv];
    //btn.backgroundColor=[UIColor redColor];
    //[cell.contentView addSubview:btn];
     cell.btn.tag=indexPath.row;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor whiteColor];
   
        if (indexPath.row==0) {
             cell.btn.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
            
             cell.topView.backgroundColor=[UIColor colorWithRed:164.0/255.0 green:0.0/255.0 blue:7.0/255.0 alpha:1];
            [ cell.btn setTitle:@"My Itinerary" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"itienrary_icon"];
           
            
        }else if (indexPath.row==1){
             cell.btn.backgroundColor=[UIColor colorWithRed:139.0/255.0 green:195.0/255.0 blue:74.0/255.0 alpha:1];
             cell.topView.backgroundColor=[UIColor colorWithRed:77.0/255.0 green:143.0/255.0 blue:0.0/255.0 alpha:1];
            [ cell.btn setTitle:@"Student & Groups" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"student_list_icon"];
           
            
        }else if (indexPath.row==2){
            
             cell.btn.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:239.0/255.0 alpha:1];
             cell.topView.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:162.0/255.0 alpha:1];
            [ cell.btn setTitle:@"Manage Chaperones" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"chaperon_icon"];
            
        }
//        else if (indexPath.row==3){
//             cell.btn.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:88.0/255.0 blue:37.0/255.0 alpha:1];
//             cell.topView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:58.0/255.0 blue:0.0/255.0 alpha:1];
//            [ cell.btn setTitle:@"Manage Groups" forState:UIControlStateNormal];
//            cell.ImgVw.image=[UIImage imageNamed:@"group_icon"];
//            
//        }
        else if (indexPath.row==3){
             cell.btn.backgroundColor=[UIColor colorWithRed:204.0/255.0 green:104.0/255.0 blue:246.0/255.0 alpha:1];
             cell.topView.backgroundColor=[UIColor colorWithRed:194.0/255.0 green:48.0/255.0 blue:253.0/255.0 alpha:1];
            [ cell.btn setTitle:@"Group Scores" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"score_icon"];
            
        }
//        else if (indexPath.row==5){
//           // imgv.frame=CGRectMake(20.0, 10, 30, 30);
//             cell.btn.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:188.0/255.0 blue:212.0/255.0 alpha:1];
//             cell.topView.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:138.0/255.0 alpha:1];
//            [ cell.btn setTitle:@"360° Views" forState:UIControlStateNormal];
//            cell.ImgVw.image=[UIImage imageNamed:@"white_gallery_icon"];
//            
//            
//        }
        else if (indexPath.row==7){
             cell.btn.backgroundColor=[UIColor colorWithRed:251.0/255.0 green:48.0/255.0 blue:117.0/255.0 alpha:1];
             cell.topView.backgroundColor=[UIColor colorWithRed:218.0/255.0 green:0.0/255.0 blue:74.0/255.0 alpha:1];
            [ cell.btn setTitle:@"Useful Links" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"useful_link_icon"];
            
        }
        else if (indexPath.row==4){
            cell.btn.backgroundColor=[UIColor colorWithRed:103.0/255.0 green:58.0/255.0 blue:183.0/255.0 alpha:1];
            cell.topView.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:9.0/255.0 blue:176.0/255.0 alpha:1];
          [ cell.btn setTitle:@"Wall Of Fame" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"fame_icon-1"];
           
            
            
        }
        else if (indexPath.row==5){
             cell.btn.backgroundColor=[UIColor colorWithRed:244.0/255.0 green:58.0/255.0 blue:54.0/255.0 alpha:1];
            cell.topView.backgroundColor=[UIColor colorWithRed:213.0/255.0 green:27.0/255.0 blue:23.0/255.0 alpha:1];
           
            [ cell.btn setTitle:@"Field Trip Pricing" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"Pricing_icon"];
            
        }
        else if (indexPath.row==6){
             cell.btn.backgroundColor=[UIColor colorWithRed:140.0/255.0 green:116.0/255.0 blue:64.0/255.0 alpha:1];
             cell.topView.backgroundColor=[UIColor colorWithRed:102.0/255.0 green:79.0/255.0 blue:28.0/255.0 alpha:1];
            [ cell.btn setTitle:@"Outreach Programs" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"outreach_icon"];
            
        }
        else if (indexPath.row==8){
            cell.btn.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:145.0/255.0 blue:0.0/255.0 alpha:1];
            cell.topView.backgroundColor= [UIColor colorWithRed:187.0/255.0 green:107.0/255.0 blue:1.0/255.0 alpha:1];
            [ cell.btn setTitle:@"My Profile" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"profile_icon"];
            
        } else if (indexPath.row==9){
            cell.btn.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:188.0/255.0 blue:212.0/255.0 alpha:1];
            cell.topView.backgroundColor= [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:193.0/255.0 alpha:1];
            [cell.btn setTitle:@"Chaperone Instructions" forState:UIControlStateNormal];
            cell.ImgVw.image=[UIImage imageNamed:@"chaperone_instruction_icon"];
            
        }

        
    
    
    
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
        
        return 60;
    
}

#pragma mark- ButtonsAction
-(void)ButtonsClicked:(id)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
    NSLog(@"%ld",(long)btn.tag);
    
    
    if (btn.tag == 0) {
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_grades?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    
                    
                    if ([[returArray valueForKey:@"itinerary"] isEqualToString:@"1"])
                    {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SelectTypeOfItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"SelectTypeOfItineraryViewController"];
                        [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
                    }
                    else
                    {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MyItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MyItineraryViewController"];
                        
                        [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
                        
                    }
                    // arrForCLassLevel = [returArray mutableCopy];
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
        
        
    }
    
    
    if (btn.tag==2) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ManageChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ManageChaperonesView"];
        Controller.IsStudent=NO;
        [self.navigationController pushViewController:Controller animated:YES ];
        
    }else if (btn.tag==1){
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ManageChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ManageChaperonesView"];
        Controller.IsStudent=YES;
        [self.navigationController pushViewController:Controller animated:YES ];
        
        
        
    }else if (btn.tag==7){
        
        UsefulLinkTableViewController *tableViewController = [[UsefulLinkTableViewController alloc] initWithStyle:UITableViewStylePlain];
        
        [self.navigationController pushViewController:tableViewController animated:YES ];
        
        //UsefulLink
    }
//    else if (btn.tag==3){
//        //AboutGroupViewController.h
//        
//        //        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        //        AboutGroupViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AboutGroupView"];
//        //        [self.navigationController pushViewController:Controller animated:YES ];
//        
//        
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        AssignGroupToStudentViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"AssignGroupToStudentViewController"];
//        [self.navigationController pushViewController:assignGroupView animated:YES];
//        
//        
//        
//    }
    
    else if (btn.tag==3)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GroupScoresViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GroupScoresViewController"];
        [self.navigationController pushViewController:assignGroupView animated:YES];
        
    }
    
//    else if (btn.tag==5)
//    {
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        GalleryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GalleryViewController"];
//        [self.navigationController pushViewController:assignGroupView animated:YES];
//        
//    }
    
    else if (btn.tag==8)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProfileViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"ProfileView"];
        [self.navigationController pushViewController:assignGroupView animated:YES];
        
    }
    
    
    else if (btn.tag==5)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FieldTripPricingViewController *WallOfFameViewController =[storyBoard instantiateViewControllerWithIdentifier:@"FieldTripPricingViewController"];
          WallOfFameViewController.IsEditStudent=YES;
        [self.navigationController pushViewController:WallOfFameViewController animated:YES];
        
    }
    
    else if (btn.tag==4)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        WallOfFameViewController *WallOfFameViewController =[storyBoard instantiateViewControllerWithIdentifier:@"WallOfFameViewController"];
          WallOfFameViewController.IsEditStudent=YES;
        [self.navigationController pushViewController:WallOfFameViewController animated:YES];
        
        
    }
    
    else if (btn.tag==6)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        OutreachProgramsViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"OutreachProgramsViewController"];
        WallOfFameViewController.IsEditStudent=YES;
        [self.navigationController pushViewController:WallOfFameViewController animated:YES];
        
    }else if (btn.tag==9) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ChaperoneInstructionsViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneInstructions"];
        Controller.isChaperone = NO;
        [self.navigationController pushViewController:Controller animated:YES ];
        
    }
    
    
    
    
}

@end
