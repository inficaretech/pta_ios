//
//  DataBaseFetchValues.m
//  Water Soldiers
//
//  Created by Inficare on 5/2/16.
//  Copyright © 2016 Inficare. All rights reserved.
//

#import "DataBaseFetchValues.h"

@implementation DataBaseFetchValues
@synthesize tagsValuesArr;

+(DataBaseFetchValues *)sharedInstance
{
    static DataBaseFetchValues *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataBaseFetchValues alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

-(NSString *)getDbPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *str=[path objectAtIndex:0];
    //dbPath=[str stringByAppendingPathComponent:@"Museum.sqlite"];
    return [str stringByAppendingPathComponent:@"Teacher.sqlite"];
}


- (NSMutableArray *)fetchAllDataWithTableName:(NSString *)tblName {
    
    //@"ChaperoneList"
    NSString *sqlQuerry;
    if (self.tagsValuesArr) {
        self.tagsValuesArr = nil;
    }
    self.tagsValuesArr = [[NSMutableArray alloc] init];
    
    NSString *path = [self getDbPath];
   
            sqlQuerry = [NSString stringWithFormat:@"SELECT * FROM %@ ",tblName];
    
    
    
    
    if(sqlite3_open([path UTF8String],&database) == SQLITE_OK )
    {
        NSString *querySQL = sqlQuerry;
        
        const char *query_stmt = [querySQL UTF8String];
        sqlite3_stmt *statement ;
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                currentRow = [[NSMutableDictionary alloc] init];
                int count = sqlite3_column_count(statement);
                for (int i=0; i < count; i++) {
                    char *name = (char*) sqlite3_column_name(statement, i);
                    char *data = (char*) sqlite3_column_text(statement, i);
                    columnData = [[NSString alloc]init];
                    columnName = [[NSString alloc]init];
                    columnName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
                    if(data != nil){
                        columnData = [NSString stringWithCString:data encoding:NSUTF8StringEncoding];
                    }
                    else {
                        columnData = @"";
                    }
                    [currentRow setObject:columnData forKey:columnName];
                }
                [self.tagsValuesArr addObject:currentRow];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
        
    }
    else{
        NSAssert1(0, @"Error: failed to close database on memwarning with message '%s'.", sqlite3_errmsg(database));
        
    }
    return self.tagsValuesArr;

   }
-(void)deleteQuerry:(NSString*)_query
{
       
    const char *sql = [_query cStringUsingEncoding:NSUTF8StringEncoding];
    sqlite3_stmt *statement = nil;
  NSString  *dbPath= [self getDbPath];
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK){
        
        if(sqlite3_prepare_v2(database,sql, -1, &statement, NULL)!= SQLITE_OK)
        {
            //NSLog(@"error in deletion %s",sqlite3_errmsg(db));
        }
        else
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    
}
-(void)UpdateWithTableName:(NSString *)Oldtkt andNewTkt:(NSString *)newTkt
{
    NSString *qsql = nil;
    sqlite3_stmt *statement =nil;
  
    char *err;
    NSString *path = [self getDbPath];
    if (sqlite3_open([path UTF8String], &database)==SQLITE_OK)
    {
        
        qsql = [NSString stringWithFormat:@"UPDATE Report SET TicketNumber = '%@'WHERE TicketNumber = '%@'",newTkt, Oldtkt];
        

       // qsql = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@'WHERE QuesID = '%@'",tableName,strColumnName, strColumnValue, strQusId];
        
        sqlite3_finalize(statement);
    }
    if(sqlite3_exec(database, [qsql UTF8String], NULL, NULL, &err) == SQLITE_OK)
    {
       
        //NSLog(@"Data Updated");
    }
    else
    {
        sqlite3_close(database);
        //NSLog(@"error in Updation");
    }
    sqlite3_finalize(statement);
   
    
}





@end
