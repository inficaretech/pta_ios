//
//  AssignGroupToStudentViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 7/8/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "AssignGroupToStudentViewController.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "AboutGroupViewController.h"
#import "MarqueeLabel.h"
#import "Constants.h"

@interface AssignGroupToStudentViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSMutableArray *arrNames;
    NSMutableArray *arrGroups;
    UITableView *tblVwStudent;
    MarqueeLabel *titlelbl ;
    // UITableView *tblVwFieldtrip;
    
    NSMutableArray *arrIndex;
    BOOL isAlpbhebetSort;
    
    // NSMutableArray *arrFieldtrip;
    NSMutableArray *arrGroupAssignment;
    
    // UIPickerView *myPickerView;
    // UIToolbar *toolBar;
    int fieldtripId;
    // UIButton *btnFieldName;
    
    
}

@end

@implementation AssignGroupToStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrNames=[[NSMutableArray alloc]init];
    arrGroups=[[NSMutableArray alloc]init];
    // arrFieldtrip=[[NSMutableArray alloc]init];
    
    
    isAlpbhebetSort=YES;
    
    arrGroupAssignment =[[NSMutableArray alloc]init];
    
    arrIndex=[[NSMutableArray alloc]init];
    [self UiForNavigationBar];
    
   
    
    //[self createUi];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Navigation Bar Creation
-(void)UiForNavigationBar
{
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    
    
    
}
#pragma mark- viewwillAppear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    titlelbl = [[MarqueeLabel alloc] init];
    [titlelbl setFrame:CGRectMake(60, 0,[[UIScreen mainScreen] bounds].size.width-170, 40)];
    //titlelbl.text = @"Appointment Details";
    [titlelbl setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    titlelbl.textColor = [UIColor whiteColor];
    [titlelbl setTextAlignment:NSTextAlignmentCenter];
    titlelbl.marqueeType = MLContinuous;
    titlelbl.scrollDuration = 15.0;
    titlelbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    titlelbl.fadeLength = 10.0f;
    titlelbl.leadingBuffer = 30.0f;
    titlelbl.trailingBuffer = 20.0f;
    
    
    
    [self.navigationController.navigationBar addSubview:titlelbl];

     [self getListOfStudent];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [titlelbl removeFromSuperview];
    
}

#pragma back button method
-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark- CreateUi
-(void)createUi{
    
    /*
     UILabel *lblFieldTrip=[[UILabel alloc]init];
     lblFieldTrip.frame=CGRectMake(0.0, x, [[UIScreen mainScreen]bounds].size.width, 30);
     lblFieldTrip.text=@"CHOOSE FIELD TRIP NAME";
     lblFieldTrip.textAlignment=NSTextAlignmentCenter;
     [self.view addSubview:lblFieldTrip];
     lblFieldTrip.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
     lblFieldTrip.textColor =[UIColor darkGrayColor];
     
     UILabel *lblFieldTripNAME=[[UILabel alloc]init];
     lblFieldTripNAME.frame=CGRectMake(0.0, x+40, ([[UIScreen mainScreen]bounds].size.width/2)-10, 30);
     lblFieldTripNAME.text=@"Field Trip Name";
     lblFieldTripNAME.textAlignment=NSTextAlignmentCenter;
     [self.view addSubview:lblFieldTripNAME];
     lblFieldTripNAME.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
     lblFieldTripNAME.textColor =[UIColor darkGrayColor];
     
     btnFieldName =[UIButton buttonWithType:UIButtonTypeCustom];
     btnFieldName.frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width/2)-20, x+40, ([[UIScreen mainScreen]bounds].size.width/2), 30);
     btnFieldName.layer.borderWidth=1;
     [btnFieldName setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
     btnFieldName.layer.borderColor=[UIColor darkGrayColor].CGColor;
     [btnFieldName addTarget:self action:@selector(ShowFieldtrip:) forControlEvents:UIControlEventTouchUpInside];
     UIImageView *imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"drop_dark_icon"]];
     imgv.frame=CGRectMake(btnFieldName.frame.size.width-25, 10, 15, 10);
     // imgv.contentMode=UIViewContentModeScaleAspectFit;
     [btnFieldName addSubview:imgv];
     
     
     
     
     CGFloat padding = 8;
     btnFieldName.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
     [btnFieldName setTitleEdgeInsets:UIEdgeInsetsMake(0, padding, 0, -padding)];
     [btnFieldName setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, padding)];
     [self.view addSubview:btnFieldName];
     
     //    tblVwFieldtrip = [[UITableView alloc]init];
     //    tblVwFieldtrip.frame = CGRectMake(btnFieldName.frame.origin.x, btnFieldName.frame.origin.y+30, btnFieldName.frame.size.width,200);
     //    tblVwFieldtrip.delegate=self;
     //    tblVwFieldtrip.hidden=YES;
     //    tblVwFieldtrip.dataSource=self;
     //    //tableViewStudentsName.backgroundColor=[UIColor redColor];
     //    tblVwFieldtrip.separatorStyle=UITableViewCellSeparatorStyleNone;
     //    [self.view addSubview:tblVwFieldtrip];
     //
     
     
     
     x=150;
     
     */
    
    //    UILabel *lbl = [[UILabel alloc]init];
    //    lbl.frame = CGRectMake( 20.0, x, self.view.frame.size.width-40, 40.0);
    //    lbl.text = @"ASSIGN STUDENT TO GROUP";
    //    lbl.textAlignment=NSTextAlignmentCenter;
    //    [self.view addSubview:lbl];
    //    lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    //    lbl.textColor =[UIColor darkGrayColor];
    
    int x=90;
    
    UILabel *lblName = [[UILabel alloc]init];
    lblName.frame = CGRectMake(20.0, x, 80, 20.0);
    lblName.text=@"STUDENTS";
    lblName.userInteractionEnabled=YES;
        UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(GetStudentListwithAlphbabetSort:)];
        singleTap.numberOfTapsRequired=1;
        [lblName addGestureRecognizer:singleTap];
    lblName.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:lblName];
    lblName.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    lblName.textColor = [UIColor darkGrayColor];;
    
    UIButton *btnAlphabeticSorting = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAlphabeticSorting.frame = CGRectMake(100.0,  x, 20.0, 20.0);
    [self.view addSubview:btnAlphabeticSorting];
    [btnAlphabeticSorting addTarget:self action:@selector(GetStudentListwithAlphbabetSort:) forControlEvents:UIControlEventTouchUpInside];
    [btnAlphabeticSorting setImage:[UIImage imageNamed:@"a-z sorting_icon"] forState:UIControlStateNormal];
    
    
    
    UILabel *lblGroup = [[UILabel alloc]init];
    lblGroup.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width-160.0,  x, 70.0, 20.0);
    lblGroup.text=@"GROUPS";
    lblGroup.userInteractionEnabled=YES;
    
        UITapGestureRecognizer *singleTap1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(GetStudentListwithNumericSort:)];
        singleTap1.numberOfTapsRequired=1;
        [lblGroup addGestureRecognizer:singleTap1];
    lblGroup.textAlignment=NSTextAlignmentLeft;
    lblGroup.textColor=[UIColor darkGrayColor];
    [self.view addSubview:lblGroup];
    lblGroup.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    
    UIButton *btnGroupSorting = [UIButton buttonWithType:UIButtonTypeCustom];
    btnGroupSorting.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width-90.0, x, 20.0, 20.0);
    [self.view addSubview:btnGroupSorting];
    [btnGroupSorting addTarget:self action:@selector(GetStudentListwithNumericSort:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnGroupSorting setImage:[UIImage imageNamed:@"numeric_sorting icon-1"] forState:UIControlStateNormal];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width/2-20,[[UIScreen mainScreen] bounds].size.height-60, 40, 40);
    
    [btnNext setImage: [UIImage imageNamed:@"next_button"] forState:UIControlStateNormal];
    
    //        [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0.0,10.0, 30.0, 20.0)];
    //        [btnNext setTitleEdgeInsets:UIEdgeInsetsMake(10.0,-45.0, 0.0,0.0)];
    //[btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(goToNextView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnNext];
    [btnNext setTitleColor:[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnNext.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    
    tblVwStudent = [[UITableView alloc]init];
    tblVwStudent.frame = CGRectMake(10.0, 150, self.view.frame.size.width-20,[[UIScreen mainScreen]bounds].size.height-150-90 );
    tblVwStudent.delegate=self;
    tblVwStudent.dataSource=self;
    //tblVwStudent.backgroundColor=[UIColor redColor];
    tblVwStudent.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tblVwStudent];
    
    
    
}
//#pragma mark- StudentTableView
//-(void)createTableView{
//
//
//
//
//
//
//}
/*
 -(void)done:(id)sender{
 myPickerView.hidden=!myPickerView.hidden;
 toolBar.hidden=!toolBar.hidden;
 //  [btnFieldName setTitle:[[arrFieldtrip objectAtIndex:fieldtripId]valueForKey:@"fieldtrip"] forState:UIControlStateNormal];
 [self getListOfStudent];
 
 
 
 }
 #pragma mark- ShowFieldtrip
 -(void)ShowFieldtrip:(id)sender{
 // tblVwFieldtrip.hidden=!tblVwFieldtrip.hidden;
 
 myPickerView.hidden=!myPickerView.hidden;
 toolBar.hidden=!toolBar.hidden;
 
 }
 */
#pragma mark - SortWithAlphabet
-(void)GetStudentListwithAlphbabetSort:(id)sender{
    //    if (!isAlpbhebetSort) {
    //        [self getListOfStudent];
    //
    //    }
    NSArray *arrSort=[[NSArray alloc]init];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"student" ascending:YES];
    arrSort=[arrNames sortedArrayUsingDescriptors:@[sort]];
    
    arrNames=nil;
    arrNames=[arrSort mutableCopy];
    [tblVwStudent reloadData];
    arrSort=nil;
    
    
    
}

/*
 #pragma mark- getFieldTripfromApi
 -(void)getFieldTripfromApi{
 BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
 if (isinternet) {
 [[CommonMethods sharedInstance] addSpinner:self.view];
 [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_fieldtrip?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
 if (!error) {
 [[CommonMethods sharedInstance] removeSpinner:self.view];
 
 NSLog(@"returArray %@",returArray);
 NSArray *arr=[[NSArray alloc]init];
 arr=[returArray valueForKey:@"result"];
 if (arr.count>0) {
 
 arrFieldtrip =[arr mutableCopy];
 for (int k=0; k<arrFieldtrip.count; k++) {
 NSDictionary *dict=[[NSDictionary alloc]init];
 dict=[arrFieldtrip objectAtIndex:k];
 if ([[dict valueForKey:@"selected"] isEqualToString:@"1"]) {
 [btnFieldName setTitle:[[arrFieldtrip objectAtIndex:k]valueForKey:@"fieldtrip"] forState:UIControlStateNormal];
 fieldtripId=k;
 }
 
 }
 
 
 
 // [btnFieldName setTitle:[[arr objectAtIndex:0]valueForKey:@"fieldtrip"] forState:UIControlStateNormal];
 // fieldtripId=0;
 
 [self getListOfStudent];
 
 myPickerView= [[UIPickerView alloc]init];
 myPickerView.frame = CGRectMake(0.0,self.view.frame.size.height-150 , self.view.frame.size.width, 150);
 myPickerView.dataSource = self;
 myPickerView.delegate = self;
 myPickerView.showsSelectionIndicator = YES;
 myPickerView.hidden=YES;
 myPickerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
 [self.view addSubview:myPickerView];
 
 
 
 
 UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
 target:self action:@selector(done:)];
 toolBar= [[UIToolbar alloc]initWithFrame:
 CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
 [toolBar setBarStyle:UIBarStyleBlackOpaque];
 NSArray *toolbarItems = [NSArray arrayWithObjects:
 doneButton, nil];
 [toolBar setItems:toolbarItems];
 toolBar.hidden=YES;
 [self.view addSubview:toolBar];
 
 // [myPickerView reloadData];
 }
 
 }
 else
 {
 [[CommonMethods sharedInstance]AlertAction:@"Server error"];
 }
 }];
 } else
 {
 [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
 
 
 
 }
 
 }
 
 */

#pragma mark- AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark- SortWithNumeric
-(void)GetStudentListwithNumericSort:(id)sender{
    
    
    //baseurl().index.php/Students/sort_group?ordertype=1&teacher_id=14&access_token=eHfkWoYKN7a0Qu4ji1pE
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    for (int k=0; k<7; k++) {
        if (k==0) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==0) {
                    [arr addObject:dict];
                }
            }
        }else if (k==1) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==1) {
                    [arr addObject:dict];
                }
            }
        }else if (k==2) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==2) {
                    [arr addObject:dict];
                }
            }
        }else if (k==3) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==3) {
                    [arr addObject:dict];
                }
            }
        }else if (k==4) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==4) {
                    [arr addObject:dict];
                }
            }
        }else if (k==5) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==5) {
                    [arr addObject:dict];
                }
            }
        }else if (k==6) {
            for(NSDictionary *dict in arrNames){
                if ([[dict valueForKey:@"group_id"]intValue]==6) {
                    [arr addObject:dict];
                }
            }
        }
    }
    
    if (arrNames.count>0) {
        
        
        arrNames=nil;
        arrNames=[arr mutableCopy];
        [tblVwStudent reloadData];
        
    }
    arr=nil;
    
    
    /*
     if (isAlpbhebetSort) {
     
     
     BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
     if (isinternet) {
     self.view.userInteractionEnabled= NO;
     
     // NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Grades/student_grades?grades=@%&access_token=fLDZ4FpiNIHljQeXkoxV";
     
     
     // [defaults setObject:strClass forKey:@"ClassId"];
     [[CommonMethods sharedInstance] addSpinner:self.view];
     
     [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/sort_group?ordertype=1&teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] valueForKey:@"id"],[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error) {
     if (!error) {
     NSLog(@"returArray %@",returArray);
     [[CommonMethods sharedInstance] removeSpinner:self.view];
     self.view.userInteractionEnabled = YES;
     //   NSMutableArray *arrResponse=[returArray mutableCopy];
     isAlpbhebetSort=NO;
     
     arrNames = [returArray valueForKey:@"result"];
     arrGroups = [returArray valueForKey:@"groups"];
     [tblVwStudent reloadData];
     
     
     }
     
     
     
     else
     {
     [[CommonMethods sharedInstance] removeSpinner:self.view];
     self.view.userInteractionEnabled = YES;
     
     }
     }];
     
     
     }else{
     
     [[CommonMethods sharedInstance]AlertAction:@"Please check your internet connection"];
     }
     
     }
     */
    
    
}


#pragma tableview delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (tableView==tblVwFieldtrip) {
    //        return arrFieldtrip.count;
    //    }else
    return arrNames.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (tableView==tblVwFieldtrip) {
    //        static NSString *cellIdentifier = @"Cell";
    //
    //        UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //        // AssignGroupTableViewCell *cell = [[AssignGroupTableViewCell alloc] init];
    //        if (cell == nil) {
    //            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //            UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 39, tblVwStudent.frame.size.width, 1)];
    //            lblLine.backgroundColor=[UIColor lightGrayColor];
    //            [cell.contentView addSubview:lblLine];
    //
    //        }
    //        cell.backgroundColor=[UIColor darkGrayColor];
    //        cell.textLabel.text=[[arrFieldtrip objectAtIndex:indexPath.row]valueForKey:@"fieldtrip"];
    //        return cell;
    //
    //    }else{
    static NSString *cellIdentifier = @"Cell";
    UILabel *lblName;
    UITableViewCell *cell;
    //= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    // AssignGroupTableViewCell *cell = [[AssignGroupTableViewCell alloc] init];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 39, tblVwStudent.frame.size.width, 1)];
        lblLine.backgroundColor=[UIColor lightGrayColor];
        [cell.contentView addSubview:lblLine];
        
        cell.tag=indexPath.row;
        lblName=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, tblVwStudent.frame.size.width-200, 20)];
        lblName.textColor=[UIColor darkGrayColor];
        [cell.contentView addSubview:lblName];
        
        for (int k=0; k<6; k++) {
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(tblVwStudent.frame.size.width-(180-30*k), 7, 25, 25);
            btn.layer.borderWidth=1;
            [btn setTitle:[NSString stringWithFormat:@"%i", k+1]  forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            btn.layer.cornerRadius=btn.frame.size.height/2;
            btn.tag=10*indexPath.row+k;
            btn.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:13];
            [btn addTarget:self action:@selector(SelectTheGroupForStudent:) forControlEvents:UIControlEventTouchUpInside];
            btn.layer.borderColor=[UIColor darkGrayColor].CGColor;
            [cell.contentView addSubview:btn];
        }
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (arrNames.count>0) {
        NSString *strStudent=[[arrNames objectAtIndex:indexPath.row] valueForKey:@"student"];
        if (strStudent !=(id)[NSNull null]) {
            lblName.text=[[arrNames objectAtIndex:indexPath.row] valueForKey:@"student"];
        }
        
    }
    
    NSString *str=[[arrNames objectAtIndex:indexPath.row]valueForKey:@"group_id"];
    if (str != (id)[NSNull null] && ![str isEqualToString:@""] ) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[NSNumber numberWithInt:[[[arrNames objectAtIndex:indexPath.row] valueForKey:@"group_id"]intValue]-1] forKey:@"remainder"];
        [dict setValue:[NSNumber numberWithInt:(int)indexPath.row] forKey:@"dividend"];
        [dict setValue:[[arrNames objectAtIndex:indexPath.row] valueForKey:@"id"]forKey:@"student_id"];
        [dict setValue:[NSNumber numberWithInt:[[[arrNames objectAtIndex:indexPath.row] valueForKey:@"group_id"]intValue]] forKey:@"group_id"];
        // dict
        
        [arrIndex addObject:dict];
        
        
        for(UIButton *Vw in cell.contentView.subviews){
            
            if ([Vw isKindOfClass:[UIButton class]] && Vw.tag==10*indexPath.row+[[[arrNames objectAtIndex:indexPath.row] valueForKey:@"group_id"]intValue]-1) {
                [Vw setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
                [Vw setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                Vw.layer.borderColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
                // [arrIndex removeObject:dict];
                
            }
            
        }
        
    }
    
    
    
    return cell;
    
    // }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}
/*
 #pragma mark - Picker View Data source
 -(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
 return 1;
 }
 -(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
 return [arrFieldtrip count];
 }
 #pragma mark- Picker View Delegate
 
 -(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
 //    [tfFieldtrip setText:[[arrGrades objectAtIndex:row] valueForKey:@"fieldtrip"]];
 fieldtripId=(int)row;
 
 }
 - (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
 return [[arrFieldtrip objectAtIndex:row] valueForKey:@"fieldtrip"];
 }
 
 */


#pragma mark-SelectTheGroupForStudent
-(void)SelectTheGroupForStudent:(UIButton *)sender{
    UIButton *btn=(UIButton *)(id)sender;
    //int x=btn.tag;
    NSLog(@"btn.tag %ld",(long)btn.tag);
    // UITableViewCell *ImgVw=(UITableViewCell *)sender.superview;
    [btn setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1]];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.layer.borderColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1].CGColor;
    
    
    // UITableViewCell *cell1=[cell viewWithTag:0];
    
    
    int y=btn.tag%10;
    NSLog(@"remainder %d",y);
    
    int z=(int)btn.tag/10;
    NSLog(@"dividend %d",z);
    
    UITableViewCell *cell1 =(UITableViewCell *) [[sender superview] superview];
    
    NSMutableDictionary *dictarrName=[[NSMutableDictionary alloc]init];
    dictarrName =[[arrNames objectAtIndex:z]mutableCopy];
    [dictarrName setValue:[NSString stringWithFormat:@"%d",y+1] forKey:@"group_id"];
    // [arrNames removeObjectAtIndex:z];
    [arrNames replaceObjectAtIndex:z withObject:dictarrName];
    
    
    
    
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
    [dict1 setValue:[NSNumber numberWithInt:y] forKey:@"remainder"];
    [dict1 setValue:[NSNumber numberWithInt:z] forKey:@"dividend"];
    [dict1 setValue:[[arrNames objectAtIndex:z] valueForKey:@"id"]forKey:@"student_id"];
    [dict1 setValue:[NSNumber numberWithInt:y+1] forKey:@"group_id"];
    // dict
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    arr=[arrIndex mutableCopy];
    if (arrIndex.count>0) {
        for(NSDictionary *dict in arr){
            
            if (  [[dict valueForKey:@"dividend"]intValue]==z) {
                
                for(UIButton *Vw in cell1.contentView.subviews){
                    
                    if ([Vw isKindOfClass:[UIButton class]] && Vw.tag==10*[[dict valueForKey:@"dividend"]intValue]+[[dict valueForKey:@"remainder"]intValue]) {
                        Vw.backgroundColor=[UIColor whiteColor];
                        // NSLog(@"btn1.tag %ld",(long)btn1.tag);
                        [Vw setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                        Vw.layer.borderColor=[UIColor darkGrayColor].CGColor;
                        [arrIndex removeObject:dict];
                        
                        //                        NSMutableDictionary *dictarrName=[[NSMutableDictionary alloc]init];
                        //                        dictarrName =[[arrNames objectAtIndex:z]mutableCopy];
                        //                        [dictarrName setValue:@"" forKey:@"group_id"];
                        //                        // [arrNames removeObjectAtIndex:z];
                        //                        [arrNames replaceObjectAtIndex:z withObject:dictarrName];
                        
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    
    
    [arrIndex addObject:dict1];
    
    for(NSDictionary *dict in arr){
        if ([dict isEqualToDictionary:dict1]) {
            [arrIndex removeObject:dict1];
            
        }
        
        
    }
    
    arr=nil;
    //
    
    
    
    
}



#pragma mark- getListOfStudent
-(void)getListOfStudent{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        self.view.userInteractionEnabled= NO;
        
        // NSString *str =@"http://14.141.136.170:8888/projects/Teacherapp/index.php/Grades/student_grades?grades=@%&access_token=fLDZ4FpiNIHljQeXkoxV";
        
        
        // [defaults setObject:strClass forKey:@"ClassId"];
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Students/student_and_group?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] valueForKey:@"id"],[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error) {
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
                //   NSMutableArray *arrResponse=[returArray mutableCopy];
                isAlpbhebetSort=YES;
                NSArray *arr=[[NSArray alloc]init];
                arr=[[returArray valueForKey:@"result"]mutableCopy];
                if (arr.count>0) {
                    
                    titlelbl.text=[NSString stringWithFormat:@"%@-%@", @"Assign Groups",[returArray valueForKey:@"fieldtrip_name"]]    ;

                    
                    fieldtripId=[[returArray valueForKey:@"trip_id"]intValue] ;
                    [self createUi];
                    arrNames=[arr mutableCopy];
                    // [self createTableView];
                    //[tblVwStudent reloadData];
                    arrGroups = [[returArray valueForKey:@"result"]mutableCopy];
                }else{
                    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add student first to manage group." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    
                    [Alert show];
                    
                    arrNames=nil;
                    arrIndex=nil;
                    arrIndex=[[NSMutableArray alloc]init];
                    [tblVwStudent reloadData];
                    
                }
                
                
            }
            
            
            
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                self.view.userInteractionEnabled = YES;
                
            }
        }];
        
        
    }else{
        
        [[CommonMethods sharedInstance]AlertAction:@"Please check your internet connection"];
    }
    
    
}

#pragma mark- button Next button Method
-(void)goToNextView
{
    if (arrIndex.count>0) {
        
        for (int k=0; k<arrIndex.count; k++) {
            NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
            [dictGroupAsignment setObject:[[arrIndex objectAtIndex:k ]valueForKey:@"group_id"] forKey:@"group_id"];
            [dictGroupAsignment setObject:[[arrIndex objectAtIndex:k] valueForKey:@"student_id"] forKey:@"student_id"];
            [arrGroupAssignment addObject:dictGroupAsignment];
            
            
            
        }
        
        
        
    }else{
        
        [[CommonMethods sharedInstance] AlertAction:@"Please assign groups to students"];
        return;
    }
    
    
    
    
    {
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            
            
            NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
            NSString *accesstoken = [defualts valueForKey:@"access_token"];
            NSString *teacherId = [defualts valueForKey:@"id"];
            [dict setValue:accesstoken forKey:@"access_token"];
            [dict setValue:teacherId forKey:@"teacher_id"];
            [dict setValue:arrGroupAssignment forKey:@"mapping_id"];
            [dict setValue:[NSNumber numberWithInt:fieldtripId] forKey:@"trip_id"];
            self.view.userInteractionEnabled=NO;
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            //[arrGroupAssignment setValue:[NSNumber numberWithInt:74] forKey:@"trip_id"];
            
            [[WebService sharedInstance] fetchDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/GroupsAPI/group_student", kBaseUrl] param:dict andcompletionhandler:^(NSArray *returArray, NSError *error ){
                if (!error) {
                    arrGroupAssignment =nil;
                    arrGroupAssignment=[[NSMutableArray alloc]init];
                    self.view.userInteractionEnabled=YES;
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    // NSString *errorstr = [returArray valueForKey:@"error"];
                    if([returArray valueForKey:@"success"]){
                        
                        // [self.navigationController popViewControllerAnimated:YES];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AboutGroupViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AboutGroupView"];
                        Controller.FieldTripId=fieldtripId;
                        [self.navigationController pushViewController:Controller animated:YES ];
                        
                        
                        
                    }else{
                        
                    }
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    self.view.userInteractionEnabled=YES;
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
        
    }
    
}





@end
