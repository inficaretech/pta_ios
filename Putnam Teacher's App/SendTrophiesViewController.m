//
//  SendTrophiesViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 7/8/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "SendTrophiesViewController.h"
#import "CloseFieldTripViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"

@interface SendTrophiesViewController ()

@end

@implementation SendTrophiesViewController
@synthesize arrGroup;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self UiForNavigationBar];
    [self CreateUI];
    // Do any additional setup after loading the view.
}


-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Group Scores";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)CreateUI
{
    UITextView *textView = [[UITextView alloc]init];
    textView.frame = CGRectMake(20.0, 20.0, [[UIScreen mainScreen] bounds].size.width-40, [[UIScreen mainScreen] bounds].size.height-180);
    [self.view addSubview:textView];
    // textView.backgroundColor = [UIColor redColor];
    textView.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in ornare augue. Cras et auctor urna. Mauris congue mi ut dui tempus viverra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec sollicitudin, urna nec tempor varius, dolor nunc sodales neque, sed eleifend tellus lectus eget nisl. Nam feugiat elit venenatis, ornare turpis a, dapibus dui. Sed volutpat ac tortor a posuere. Nulla interdum non quam et scelerisque. Aliquam non turpis maximus dolor egestas dignissim eget quis tellus.";
    textView.editable=NO;
    textView.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    UIImageView *imageTrophy = [[UIImageView alloc]init];
    imageTrophy.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 45, [[UIScreen mainScreen] bounds].size.height-180, 90.0, 90.0);
    [self.view addSubview:imageTrophy];
    imageTrophy.image = [UIImage imageNamed:@"trophy_icon"];
    imageTrophy.contentMode = UIViewContentModeScaleAspectFit;
    
    UIButton *buttonSendTrophies = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSendTrophies.frame = CGRectMake(20.0, [[UIScreen mainScreen ] bounds].size.height-70, [[UIScreen mainScreen] bounds].size.width-40, 40.0);
    [self.view addSubview:buttonSendTrophies];
    [buttonSendTrophies setTitle:@"Send Trophies" forState:UIControlStateNormal];
    [buttonSendTrophies setBackgroundColor:[UIColor  colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
    buttonSendTrophies.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    [buttonSendTrophies addTarget:self action:@selector(ButtonSendTrophiesClicked) forControlEvents:UIControlEventTouchUpInside];
    buttonSendTrophies.layer.shadowColor = [UIColor grayColor].CGColor;
    buttonSendTrophies.layer.shadowOffset = CGSizeMake(0, 1.0);
    buttonSendTrophies.layer.shadowOpacity = 1.0;
    buttonSendTrophies.layer.shadowRadius = 4.0;
    
    
}

#pragma mark - ButtonSendTrophiesClicked
-(void)ButtonSendTrophiesClicked
{
    
    if (arrGroup.count>0) {
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        for (NSMutableDictionary *dict in arrGroup) {
            [dict removeObjectForKey:@"declare"];
        }
        
        NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
        NSString *accesstoken = [defualts valueForKey:@"access_token"];
        NSString *teacherId = [defualts valueForKey:@"id"];
        [dict setValue:accesstoken forKey:@"access_token"];
        [dict setValue:teacherId forKey:@"teacher_id"];
        [dict setValue:arrGroup forKey:@"mapping_id"];
        // [dict setValue:[NSNumber numberWithInt:fieldtripId] forKey:@"trip_id"];
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            self.view.userInteractionEnabled=NO;
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            
            
            [[WebService sharedInstance] fetchDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/GroupsAPI/winner_mail", kBaseUrl] param:dict andcompletionhandler:^(NSArray *returArray, NSError *error ){
                  [[CommonMethods sharedInstance] removeSpinner:self.view];
                if (!error) {
                  
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                        self.view.userInteractionEnabled=YES;
                        return;
                        
                        
                    }else if([[returArray valueForKey:@"result"] isEqual:@"success"]){
                        
    
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        CloseFieldTripViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"CloseFieldTripViewController"];
                        [self.navigationController pushViewController:Controller animated:YES ];
                        self.view.userInteractionEnabled=YES;
                    }
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    self.view.userInteractionEnabled=YES;
                    
                }
            }];
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
        
        
//            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            SendTrophiesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"SendTrophiesViewController"];
//            [self.navigationController pushViewController:Controller animated:YES ];
//            self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        [[CommonMethods sharedInstance]AlertAction:@"Please select to declare the winner"];
    }
    
    
    
    
    
    
    
    

    
    
    
    
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        CloseFieldTripViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"CloseFieldTripViewController"];
//        [self.navigationController pushViewController:Controller animated:YES ];
//        self.view.userInteractionEnabled=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
