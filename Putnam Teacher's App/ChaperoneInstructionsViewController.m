//
//  ChaperoneInstructionsViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 3/1/17.
//  Copyright © 2017 inficare. All rights reserved.
//

#import "ChaperoneInstructionsViewController.h"
#import "CommonMethods.h"
#import "Reachability.h"
#import "WebService.h"
#import "Constants.h"

@interface ChaperoneInstructionsViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *txtVwForDirection;
@property (weak, nonatomic) IBOutlet UITextView *txtVwForQuestion;
@property (weak, nonatomic) IBOutlet UITextView *txtVwForWhattoInclude;
@property (weak, nonatomic) IBOutlet UITextView *txtVwForTargets;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *lblSave;

@end

@implementation ChaperoneInstructionsViewController
@synthesize isChaperone;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Chaperone Instructions";
    [self createUi];
    [self CustomNavigationBar];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- back
-(void)BackButtonPressed:(id)sender
{
    if (isChaperone) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        
    }else{
        [self.navigationController popViewControllerAnimated:YES ];
    }
}

#pragma mark : - cretaeUi
-(void)createUi{
    
    if (isChaperone) {
        _txtVwForWhattoInclude.editable = NO;
          _txtVwForQuestion.editable = NO;
          _txtVwForTargets.editable = NO;
          _txtVwForDirection.editable = NO;
        
        _btnSave.hidden = YES;
        _lblSave.hidden = YES;
    }
    
    _txtVwForTargets.layer.borderWidth = 1;
    _txtVwForTargets.layer.cornerRadius = 10;
    _txtVwForTargets.delegate = self;
    _txtVwForTargets.returnKeyType = UIReturnKeyDone;
    _txtVwForTargets.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _txtVwForQuestion.layer.borderWidth = 1;
    _txtVwForQuestion.layer.cornerRadius = 10;
    _txtVwForQuestion.delegate = self;
    _txtVwForQuestion.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _txtVwForWhattoInclude.layer.borderWidth = 1;
    _txtVwForWhattoInclude.layer.cornerRadius = 10;
    _txtVwForWhattoInclude.delegate = self;
    _txtVwForWhattoInclude.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _txtVwForDirection.layer.borderWidth = 1;
    _txtVwForDirection.layer.cornerRadius = 10;
    _txtVwForDirection.delegate = self;
    _txtVwForDirection.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked:)];
    singleTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTap];
    
    [self getInstructionsForChaperones];
    
    
}

-(void)CustomNavigationBar
{
    // [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    // self.navigationItem.title =@"Wall of Fame";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
}


#pragma mark - tapGstureClicked
-(void)tapGestureClicked:(id)sender{
    
    [self.view endEditing:YES];

    
}

#pragma mark - getInstructions
-(void)getInstructionsForChaperones{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        
        NSString *teacher_id = @"";
        //teacher_id
        if (isChaperone) {
            teacher_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"];
        }else{
            teacher_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"id"];
        }
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Chaperones/get_chep_instruction?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],teacher_id]   andcompletionhandler:^(NSArray *returArray, NSError *error){
            [[CommonMethods sharedInstance] removeSpinner:self.view];
            if (!error) {
                
                NSLog(@"returnArray = %@",returArray);
             //   NSString *success = [returArray valueForKey:@"success"];
                
                if ([[returArray valueForKey:@"result"] count]>0) {
                    
                    _txtVwForTargets.text = [[[returArray valueForKey:@"result"]objectAtIndex:0]valueForKey:@"learning_targets"];
                    _txtVwForWhattoInclude.text = [[[returArray valueForKey:@"result"]objectAtIndex:0]valueForKey:@"what_includes"];
                    _txtVwForDirection.text = [[[returArray valueForKey:@"result"]objectAtIndex:0]valueForKey:@"direction_for_adult"];
                    _txtVwForQuestion.text = [[[returArray valueForKey:@"result"]objectAtIndex:0]valueForKey:@"ques_to_ask"];
                    
                }
                

                
                
            }
            else
            {
                [[CommonMethods sharedInstance]AlertAction:@"Server error"];
                return;
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        return;
        
        
        
    }
    
    
}

#pragma mark - textViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        if (textView == _txtVwForDirection) {
            [_txtVwForQuestion becomeFirstResponder];
        }else if (textView == _txtVwForQuestion){
            [_txtVwForWhattoInclude becomeFirstResponder];
        }else if(textView == _txtVwForWhattoInclude){
            [_txtVwForTargets becomeFirstResponder];
        }else{
        [textView resignFirstResponder ] ;
        }
    }
    
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    
    if (IS_IPHONE_5) {
        
        if (textView == _txtVwForTargets) {
            
            [UIView animateWithDuration:0.3 animations:^{
                self.view.frame = CGRectMake(0, -200, SCREEN_WIDTH, SCREEN_HEIGHT);
                
            }];
            
        }else if (textView == _txtVwForWhattoInclude){
            [UIView animateWithDuration:0.3 animations:^{
                self.view.frame = CGRectMake(0, -100, SCREEN_WIDTH, SCREEN_HEIGHT);
                
            }];
            
            
        }

        
    }else{
        
        if (textView == _txtVwForTargets) {
            
            [UIView animateWithDuration:0.3 animations:^{
                self.view.frame = CGRectMake(0, -200, SCREEN_WIDTH, SCREEN_HEIGHT);
                
            }];
            
        }
        
        
    }
  
  
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
    }];

    
     return YES;
    
}





#pragma mark - btnSaveClicked
- (IBAction)btnSaveClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    
    if (isinternet) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:_txtVwForTargets.text forKey:@"learning_targets"];
        [dict setValue:_txtVwForQuestion.text forKey:@"ques_to_ask"];
        [dict setValue:_txtVwForDirection.text forKey:@"direction_for_adult"];
        [dict setValue:_txtVwForWhattoInclude.text forKey:@"what_includes"];

        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"teacher_id"];
        
        self.view.userInteractionEnabled=NO;
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Chaperones/chep_instruction",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
            self.view.userInteractionEnabled=YES;
            [[CommonMethods sharedInstance] removeSpinner:self.view];
            if (!error) {
                NSLog(@"returArray %@",returArray);
                [[CommonMethods sharedInstance] AlertAction:@"Successfully updated"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                 [[CommonMethods sharedInstance] AlertAction:@"Something went wrong"];
            }
        }];
    }else{
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
    }
    
    
    
    
}


@end
