//
//  DatabaseManagement.h
//  Water Soldiers
//
//  Created by Inficare on 5/2/16.
//  Copyright © 2016 Inficare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseManagement : NSObject{
    
    NSString *dbPath;
    sqlite3 *db;
    sqlite3_stmt *stmt;
    
}
+(DatabaseManagement *) sharedInstance;
-(void)SavedataInDatabase:(NSDictionary *)dict;
-(void)SaveStudentListInDatabase:(NSDictionary *)dict;
-(void)SavedataInDatabaseforChaperoneStudentList:(NSDictionary *)dict;
-(void)SavedataInDatabaseforQuiz:(NSDictionary *)dict;
-(NSInteger)UpdateDataInDatabaseforQuizForAnswered:(int) Answered andQuestion_id:(int) Question_id;
@end
