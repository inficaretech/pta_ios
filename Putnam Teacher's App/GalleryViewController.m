//
//  GalleryViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 7/19/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "GalleryViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "PanormaViewController.h"
#import "UIImageView+WebCache.h"


@interface GalleryViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    NSArray *arrImages;
    UICollectionView *myCollectionView;
    
    
}

@end

@implementation GalleryViewController
@synthesize IsChaperone;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrImages=[[NSArray alloc]init];
    self.navigationItem.title=@"360° Views";
    //    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    //    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    //    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    //    self.navigationItem.leftBarButtonItem =mailbutton;
    [self createUi];
    [self galleryApi];
    [self CustomNavigationBar];
    
    
}
-(void)CustomNavigationBar
{
    // [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    // self.navigationItem.title =@"Wall of Fame";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- back
-(void)BackButtonPressed:(id)sender
{
    if (IsChaperone) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        
    }else{
        [self.navigationController popViewControllerAnimated:YES ];
    }
}
#pragma mark - CreateUi
-(void)createUi{
    UICollectionViewFlowLayout *myLayout = [[UICollectionViewFlowLayout alloc]init];
    [myLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [myLayout setMinimumInteritemSpacing:0];
    [myLayout setMinimumLineSpacing:5];
    myCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height) collectionViewLayout:myLayout];
    myCollectionView.backgroundColor=[UIColor whiteColor];
    myCollectionView.delegate=self;
    myCollectionView.dataSource=self;
    [myCollectionView registerClass:[UICollectionViewCell class]  forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:myCollectionView];
    
}
#pragma mark- CollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrImages.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    int y=([[UIScreen mainScreen]bounds].size.width)/2;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
       UIImageView *imgVw=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, y-20, y-20)];
    
    [cell.contentView addSubview:imgVw];
      UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(5, y-5, y-20, 20)];
    lbl.textColor=[UIColor darkGrayColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    [cell.contentView addSubview:lbl];
    if (arrImages.count>0) {
        
        lbl.text=[[arrImages objectAtIndex:indexPath.row]valueForKey:@"title"];
        NSURL *url =[[arrImages objectAtIndex:indexPath.row]valueForKey:@"image"];
        
        [imgVw setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [imgVw setShowActivityIndicatorView:true];
        
        [imgVw sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
        [self downloadImage:[[arrImages objectAtIndex:indexPath.row]valueForKey:@"image"]];
        
    }
    
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int y=([[UIScreen mainScreen]bounds].size.width)/2;
    return CGSizeMake(y, y+20);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PanormaViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PanormaViewController"];
    MyItineraryViewController.workingImage= [NSString stringWithFormat:@"%@",[[[arrImages objectAtIndex:indexPath.row]valueForKey:@"image"] substringFromIndex:64]];
    MyItineraryViewController.strNavTitil=[[arrImages objectAtIndex:indexPath.row]valueForKey:@"title"];
    [self.navigationController pushViewController:MyItineraryViewController animated:YES ];
    
    
}
#pragma mark- saveImagefromurl
-(UIImage *)downloadImage:(NSString *)urlString{
    
    //    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://a3.twimg.com/profile_images/414797877/05052008321_bigger.jpg"]];
    //    [NSURLConnection connectionWithRequest:request delegate:self];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[urlString substringFromIndex:64]]];
    NSData *data=[NSData dataWithContentsOfFile:localFilePath];
    
    UIImage *img;
    if (data!=nil) {
        img= [[UIImage alloc] initWithData:data];
        
    }else{
        NSData* theData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        [theData writeToFile:localFilePath atomically:YES];
        img= [[UIImage alloc] initWithData:theData];
    }
    
    return img;
    
    
}

#pragma mark- GalleryApi
-(void)galleryApi{
    
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/GalleriesAPI/get_all",kBaseUrl] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                
                NSLog(@"returArray %@",returArray);
                arrImages=[returArray valueForKey:@"result"];
                [myCollectionView reloadData];
                //[self createUi];
                
                
            }
            else
            {
                 [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server error"];
            }
        }];
    } else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
        
    }
}

@end
