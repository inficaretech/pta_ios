//
//  AssignChaperoneTableViewCell.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/24/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignChaperoneTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimary;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondary;

@end
