//
//  ChaperoneStrtViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/2/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "ChaperoneStrtViewController.h"
#import "ChaperoneDetailsViewController.h"
#import "Constants.h"
#import "CommonMethods.h"

@interface ChaperoneStrtViewController ()
{
    NSString *strIsCompleted;
}
@end

@implementation ChaperoneStrtViewController
@synthesize Islogin;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self CustomNavigationBar];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
-(void)CustomNavigationBar
{
    //self.navigationItem.hidesBackButton=YES;
    // [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    // btnBack.backgroundColor = [UIColor redColor];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =[[NSUserDefaults standardUserDefaults] valueForKey:@"group_name"];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:20.0], NSFontAttributeName, nil]];
    
    [self createUi];
    
}

-(void)BackButtonPressed
{
    if (Islogin) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    }else{
    [self.navigationController popViewControllerAnimated:YES ];
    }
}
-(void)createUi{
//    UILabel *lbl;
//    if (IS_IPAD) {
//        
//         lbl=[[UILabel alloc]initWithFrame:CGRectMake(10.0,64.0, [[UIScreen mainScreen]bounds].size.width-20, 190)];
//    }else{
//         lbl=[[UILabel alloc]initWithFrame:CGRectMake(10.0,64.0, [[UIScreen mainScreen]bounds].size.width-20, 350)];
//        
//    }
    
  UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10.0,75.0, [[UIScreen mainScreen]bounds].size.width-20, 350)];
   
    lbl.numberOfLines=0;
    lbl.textColor=[UIColor darkGrayColor];
   
    lbl.text=@"Thank you for volunteering for the upcoming field trip. Teachers will assign you to a group of students. Student group information shall be accessible only on field trip day.\n\nAs a chaperone for the field trip, we request to have your assigned group answer multiple choice questions during the trip to enrich their field trip experience.\n\nAfter the field trip is completed, the teacher will close the trip and you will no longer have access to the group. This App will then send group e-trophies to the teacher for distribution.";
    lbl.font =[UIFont fontWithName:@"Roboto-regular" size:18];
    if (!IS_IPAD) {
         lbl.font = [UIFont fontWithName:@"Roboto-regular" size:14];
    }
   
    UIButton *btnNext=[UIButton buttonWithType:UIButtonTypeCustom];
     btnNext.backgroundColor=[UIColor colorWithRed:50.0/255.0 green:172.0/255.0 blue:55.0/255.0 alpha:1];
    btnNext.frame=CGRectMake(20.0, [[UIScreen mainScreen]bounds].size.height-100, [[UIScreen mainScreen]bounds].size.width-40, 45);
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [self.view addSubview:btnNext];
    [btnNext addTarget:self action:@selector(GoToChaperoneDetailspage:) forControlEvents:UIControlEventTouchUpInside];
     [lbl sizeToFit];
    [self.view addSubview:lbl];
    if (Islogin) {
          AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        appdel.navController=self.navigationController;
     UIImageView   *imgVwMenu= [[CommonMethods sharedInstance] AddMenu:self.navigationController.navigationBar andFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-50, 5,35 ,35)];
        
        
        [self.navigationController.navigationBar addSubview:imgVwMenu];
    }
    
}


#pragma mark-GoToChaperoneDetailspage
-(void)GoToChaperoneDetailspage:(id)sender{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChaperoneDetailsViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneDetailView"];
    [self.navigationController pushViewController:Controller animated:YES ];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
