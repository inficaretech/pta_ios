//
//  CommonMethods.m
//  Putnam Teacher's App
//
//  Created by Inficare on 5/31/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "CommonMethods.h"
#import "TJSpinner.h"
#import "Reachability.h"
#import "UsefulLinkTableViewController.h"
#import "ManageChaperonesViewController.h"
#import "Constants.h"
#import "MyItineraryViewController.h"
#import "AboutGroupViewController.h"
#import "Constants.h"
#import "AssignGroupToStudentViewController.h"
#import "WebService.h"
#import "ChaperoneStrtViewController.h"
#import "GalleryViewController.h"
#import "StartingPageViewController.h"
#import "GroupScoresViewController.h"
#import "ChaperoneStrtViewController.h"
#import "ChaperoneDetailsViewController.h"
#import "QuizPageViewController.h"
#import "SelectTypeOfItineraryViewController.h"
#import "DatabaseManagement.h"
#import "DataBaseFetchValues.h"
#import "ChaperoneInstructionsViewController.h"

NSString *const kTJCircularSpinner = @"TJCircularSpinner";

NSString *const kStartAnimation = @"Start";
NSString *const kStopAnimation = @"Stop";
@interface CommonMethods()<UITextFieldDelegate,UIAlertViewDelegate>
{
    TJSpinner *circularSpinner;
    UIView *vwMenu;
    UITextField *tfForEnterCode;
    UIButton *btnremeber;
    NSMutableArray *ArrQuestion;
    
}

@end


@implementation CommonMethods


+(CommonMethods *)sharedInstance
{
    static CommonMethods *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CommonMethods alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

#pragma mark- AlertAction
-(void)AlertAction:(NSString *)msg {
    //AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[appdel.navController popToRootViewControllerAnimated:NO];
    
    
    // if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
    // }
    //        else
    //        {
    //            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:msg preferredStyle:UIAlertControllerStyleAlert];
    //
    //            UIAlertAction *okAction = [UIAlertAction
    //                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
    //                                       style:UIAlertActionStyleDefault
    //                                       handler:^(UIAlertAction *action)
    //                                       {
    //
    //                                       }];
    //
    //            [alert addAction:okAction];
    //            [appdel.navController presentViewController:alert animated:YES completion:nil];
    //        }
    
}

#pragma mark-Spinner
-(void)addSpinner:(UIView *)view1{
    circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJCircularSpinner];
    circularSpinner.hidesWhenStopped = YES;
    
    //Generate second spinner
    
    circularSpinner.radius = 20;
    circularSpinner.pathColor = [UIColor whiteColor];
    circularSpinner.fillColor = [UIColor orangeColor];
    circularSpinner.thickness = 7;
    CGFloat spinnerViewWidth = view1.frame.size.width/3.0;
    [circularSpinner setBounds:CGRectMake(0,0 , [circularSpinner frame].size.width, [circularSpinner frame].size.height)];
    [circularSpinner setCenter:CGPointMake((spinnerViewWidth/2.00)+spinnerViewWidth, view1.center.y)];
    [view1 addSubview:circularSpinner];
    [circularSpinner startAnimating];
    
}
-(void)removeSpinner:(UIView *)view1{
    
    [circularSpinner stopAnimating];
    [circularSpinner removeFromSuperview];
    
}
#pragma mark- AddMenuBar
-(UIImageView *)AddMenu:(UIView *)Vw andFrame:(CGRect)frame{
    UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_white_button"]];
    ImgVw.frame=frame;
    [Vw addSubview:ImgVw];
    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
    ImgVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowMenu:)];
    tapper.cancelsTouchesInView = NO;
    [tapper setCancelsTouchesInView:NO];
    [ImgVw addGestureRecognizer:tapper];
    
    return ImgVw;
    
}

#pragma mark-checkInternetConnection
-(BOOL)checkInternetConnection
{
    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))/*14-10-14*/
    {
        internet = NO;
        
    } else
    {
        internet = YES;
    }
    return internet;
}
#pragma mark-ShowMenu
-(void)ShowMenu:(UITapGestureRecognizer *)sender{
    UIView *view = sender.view;
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *viewContrlls=[[appdel navController] viewControllers];
    
    UIViewController *lastController = [viewContrlls lastObject];
    
    if ([lastController isKindOfClass:[ChaperoneStrtViewController class]] || [lastController isKindOfClass:[ChaperoneDetailsViewController class]] || [lastController isKindOfClass:[QuizPageViewController class]] ) {
        
        vwMenu=nil;
        vwMenu=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width,  [[UIScreen mainScreen]bounds].size.height)];
        vwMenu.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
        
         [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-122, 18,122 ,40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_cross_icon"] andTitle:@"Close" andTag:11];
        
        [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-190,70 , 180, 50) andView:vwMenu andImge:[UIImage imageNamed:@"teacher_icon"] andTitle:@"Teacher login" andTag:10];
        
        [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-170, 130, 160, 50) andView:vwMenu andImge:[UIImage imageNamed:@"360_icon"] andTitle:@"360° Views" andTag:20];
        
         [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-270, 190, 260, 50) andView:vwMenu andImge:[UIImage imageNamed:@"chaperon_instruction"] andTitle:@"Chaperone Instructions" andTag:999];
        
       
        
        //        UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
        //        ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-60, 15,50 ,50);
        //        ImgVw.contentMode=UIViewContentModeScaleAspectFit;
        //        ImgVw.userInteractionEnabled=YES;
        //        UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
        //        tapper.cancelsTouchesInView = NO;
        //        [tapper setCancelsTouchesInView:NO];
        //        [ImgVw addGestureRecognizer:tapper];
        //        [vwMenu addSubview:ImgVw];
        
        
        
        
    }else{
        
        //AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appdel.btnAdd setHidden:YES];
        vwMenu=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width,  [[UIScreen mainScreen]bounds].size.height)];
        vwMenu.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
        
        
        if (IS_IPHONE_5) {
            // int x=[[UIScreen mainScreen]bounds].size.height+30;
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-207,70 , 207, 40) andView:vwMenu andImge:[UIImage imageNamed:@"chaperon_icon2"] andTitle:@"Chaperone login" andTag:7];
            
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-170,170 , 170, 40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Student List" andTag:1];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-170,120 , 170, 40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_red_icon"] andTitle:@"My Itinerary" andTag:2];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-240, 220, 240, 40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_blue_icon"] andTitle:@"Manage Chaperones" andTag:3];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-200, 270, 200, 40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_orange_icon"] andTitle:@"Manage Groups" andTag:4];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-180,320 , 180, 40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_purple_icon"] andTitle:@"Group Scores" andTag:5];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-160, 370, 160, 40) andView:vwMenu andImge:[UIImage imageNamed:@"360_icon"] andTitle:@"360° Views" andTag:6];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-115, 420 , 115, 40) andView:vwMenu andImge:[UIImage imageNamed:@"home_icon"] andTitle:@"Home" andTag:8];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-257, 470, 257, 40) andView:vwMenu andImge:[UIImage imageNamed:@"chaperon_instruction"] andTitle:@"Chaperone Instructions" andTag:1000];
            //chaperon_instruction
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-122, 520,122 ,40) andView:vwMenu andImge:[UIImage imageNamed:@"logout_button-1"] andTitle:@"Logout" andTag:9];
            
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-122, 18,122 ,40) andView:vwMenu andImge:[UIImage imageNamed:@"menu_cross_icon"] andTitle:@"Close" andTag:11];
            
            
            
            
            
            //            UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
            //            ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-50, 15,40 ,40);
            //            ImgVw.contentMode=UIViewContentModeScaleAspectFit;
            //            ImgVw.userInteractionEnabled=YES;
            //            UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
            //            tapper.cancelsTouchesInView = NO;
            //            [tapper setCancelsTouchesInView:NO];
            //            [ImgVw addGestureRecognizer:tapper];
            //            [vwMenu addSubview:ImgVw];
            
            
            
        }else{
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-215, 70, 205, 50) andView:vwMenu andImge:[UIImage imageNamed:@"chaperon_icon2"] andTitle:@"Chaperone login" andTag:7];
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-180, 190, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Student List" andTag:1];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-180, 130, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_red_icon"] andTitle:@"My Itinerary" andTag:2];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-250, 250, 240, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_blue_icon"] andTitle:@"Manage Chaperones" andTag:3];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-210, 310, 200, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_orange_icon"] andTitle:@"Manage Groups" andTag:4];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-190, 370, 180, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_purple_icon"] andTitle:@"Group Scores" andTag:5];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-175, 430, 165, 50) andView:vwMenu andImge:[UIImage imageNamed:@"360_icon"] andTitle:@"360° Views" andTag:6];
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-130, 490 , 120, 50) andView:vwMenu andImge:[UIImage imageNamed:@"home_icon"] andTitle:@"Home" andTag:8];
            
              [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-270, 550, 260, 50) andView:vwMenu andImge:[UIImage imageNamed:@"chaperon_instruction"] andTitle:@"Chaperone Instructions" andTag:1000];
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-135, 610, 125, 50) andView:vwMenu andImge:[UIImage imageNamed:@"logout_button-1"] andTitle:@"Logout" andTag:9];
            
           
            
            
            [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-125, 15,115 ,50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_cross_icon"] andTitle:@"Close" andTag:11];
            
            
                     
        }
        
        
    }
    
    [view.superview.superview addSubview:vwMenu];
    
}
#pragma removemenu
-(void)RemoveMenu:(id)sender{
    [vwMenu removeFromSuperview];
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.btnAdd setHidden:NO];
    
}
#pragma mark-createbuttton
-(UIButton *)CreateMenuButton:(CGRect)frame andView:(UIView *)Vw andImge:(UIImage *)img andTitle:(NSString *)strTitl andTag:(int)tag{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=frame;
    btn.layer.shadowColor = [UIColor blackColor].CGColor;
    btn.layer.shadowOffset = CGSizeMake(0, 1.0);
    btn.layer.shadowOpacity = 1.0;
    btn.layer.shadowRadius = 3.0;
    
    
    
    UIImageView *Imgv=[[UIImageView alloc]initWithImage:img];
    Imgv.frame=CGRectMake(btn.frame.size.width-50, 0, btn.frame.size.height, btn.frame.size.height);
    Imgv.clipsToBounds=YES;
    
    Imgv.layer.cornerRadius=btn.frame.size.height/2;
    
    [btn addTarget:self action:@selector(ButtonClickedPage:) forControlEvents:UIControlEventTouchUpInside];
    // btn.backgroundColor =[UIColor redColor];
    [btn addSubview:Imgv];
    //btn.backgroundColor=[UIColor whiteColor];
    [btn setTitle:strTitl forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    [btn setImageEdgeInsets:UIEdgeInsetsMake(0,100, 0, 0)];
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(10, 0, 10, 0)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.tag=tag;
    [Vw addSubview:btn];
    
    return btn;
    
}
#pragma mark-ButtonClickedofMenu
-(void)ButtonClickedPage:(id)sender{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    UIButton *btn=(UIButton *)(id)sender;
    NSLog(@"%ld",(long)btn.tag);
    // [vwMenu removeFromSuperview];
    [appdel.btnAdd setHidden:NO];
    
    if (btn.tag==3) {
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ManageChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ManageChaperonesView"];
        Controller.IsStudent=NO;
        [appdel.navController pushViewController:Controller animated:YES ];
        
        
    }else if (btn.tag==1){
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ManageChaperonesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ManageChaperonesView"];
        Controller.IsStudent=YES;
        [appdel.navController pushViewController:Controller animated:YES ];
        
    }else if (btn.tag==8){
        [vwMenu removeFromSuperview];
        //        [appdel.navController popToRootViewControllerAnimated:NO];
        //        UsefulLinkTableViewController *tableViewController = [[UsefulLinkTableViewController alloc] initWithStyle:UITableViewStylePlain];
        //        [appdel.navController pushViewController:tableViewController animated:YES ];
        [appdel.navController popToRootViewControllerAnimated:YES];
        
    }else if (btn.tag==2){
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            [[CommonMethods sharedInstance] addSpinner:vwMenu];
            [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Teachers/get_teacher_grades?teacher_id=%@&access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    NSLog(@"returArray %@",returArray);
                    
                    
                    if ([[returArray valueForKey:@"itinerary"] isEqualToString:@"1"])
                    {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SelectTypeOfItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"SelectTypeOfItineraryViewController"];
                        [appdel.navController pushViewController:MyItineraryViewController animated:YES ];
                    }
                    else
                    {
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MyItineraryViewController *MyItineraryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MyItineraryViewController"];
                        
                        [appdel.navController pushViewController:MyItineraryViewController animated:YES ];
                        
                    }
                    // arrForCLassLevel = [returArray mutableCopy];
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
        
        
        
    }else if (btn.tag==4){
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        //        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //        AboutGroupViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"AboutGroupView"];
        //        [appdel.navController pushViewController:Controller animated:YES ];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AssignGroupToStudentViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"AssignGroupToStudentViewController"];
        [appdel.navController pushViewController:assignGroupView animated:YES];
        
        
        
        
    }else if (btn.tag==7){
        [self createChaperonePage];
    }else if (btn.tag==10){
        NSString *str1=[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"];
        if (str1.length==0) {
            [self AlertAction:@"User not login"];
            
        }else{
            [vwMenu removeFromSuperview];
            AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appdel.navController popToRootViewControllerAnimated:NO];
        }
    } else if (btn.tag==6)
    {
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GalleryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GalleryViewController"];
        [appdel.navController pushViewController:assignGroupView animated:YES];
        
    }else if (btn.tag==9){
        UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to logout" delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"OK", nil];
        [Alert show];
        
    }else if (btn.tag==5){
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GroupScoresViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GroupScoresViewController"];
        [appdel.navController pushViewController:assignGroupView animated:YES];
        
        
    }else if (btn.tag==20){
        
        [vwMenu removeFromSuperview];
        // [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GalleryViewController *assignGroupView= [story instantiateViewControllerWithIdentifier:@"GalleryViewController"];
        assignGroupView.IsChaperone=YES;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:assignGroupView];
        [appdel.navController presentViewController:navController animated:YES completion:nil];
        
    }else if (btn.tag==11){
        
        [self RemoveMenu:self];
    }else if(btn.tag == 1000){
        [vwMenu removeFromSuperview];
        [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ChaperoneInstructionsViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneInstructions"];
          Controller.isChaperone = NO;
        [appdel.navController pushViewController:Controller animated:YES ];
        
        
    }else if (btn.tag == 999){
        
        [vwMenu removeFromSuperview];
        // [appdel.navController popToRootViewControllerAnimated:NO];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ChaperoneInstructionsViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneInstructions"];
        Controller.isChaperone = YES;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:Controller];
        [appdel.navController presentViewController:navController animated:YES completion:nil];

//        [vwMenu removeFromSuperview];
//        [appdel.navController popToRootViewControllerAnimated:NO];
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//       
//      
//        [appdel.navController pushViewController:Controller animated:YES ];
        
    }
    
    
    
}

- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    CGRect rect = [text boundingRectWithSize:(CGSize){width, MAXFLOAT}
                                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                     context:nil];//you need to specify the some width, height will be calculated
    NSLog(@"REct Height ===  %f",rect.size.height);
    
    
    if(rect.size.height>400){
        
        return rect.size.height + 160 ;
        
    }
    if(rect.size.height>350){
        
        return rect.size.height + 130 ;
        
    }
    
    
    if(rect.size.height>300){
        
        return rect.size.height + 100 ;
        
    }
    
    
    if(rect.size.height>250){
        
        return rect.size.height + 70 ;
        
    }
    
    if(rect.size.height>200){
        
        return rect.size.height + 50 ;
        
    }
    
    
    return rect.size.height ;
    
    
    
}

#pragma mark- ChaperoneButtonAction
-(void)createChaperonePage {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //create a new view with the same size
    UIView* vw = [[UIView alloc] initWithFrame:screenRect];
    vw.tag=110;
    // change the background color to black and the opacity to 0.7
    vw.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.9];
    
    //    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    //    vw.backgroundColor=[UIColor lightGrayColor];
    
    // UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(240.0, 330.0, 300.0, 300.0)];
    
    UIView *vwForAccessCode=[[UIView alloc]initWithFrame:CGRectMake(30,150, [[UIScreen mainScreen]bounds].size.width-60, 250.0)];
    vwForAccessCode.backgroundColor=[UIColor whiteColor];
    vwForAccessCode.layer.borderWidth=2;
    vwForAccessCode.layer.borderColor=[UIColor blueColor].CGColor;
    
    UILabel *lblEnterAccessCode=[[UILabel alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-100, 70.0, 200, 20.0)];
    lblEnterAccessCode.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    lblEnterAccessCode.text=@"Please enter access code";
    lblEnterAccessCode.adjustsFontSizeToFitWidth = YES;
    lblEnterAccessCode.font =[UIFont fontWithName:@"Roboto-Regular" size:22];
    [vwForAccessCode addSubview:lblEnterAccessCode];
    
    tfForEnterCode=[[UITextField alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)-70, 120.0, 140, 30.0)];
    tfForEnterCode.textAlignment=NSTextAlignmentCenter;
    tfForEnterCode.delegate=self;
    tfForEnterCode.returnKeyType=UIReturnKeyDone;
    tfForEnterCode.spellCheckingType = UITextSpellCheckingTypeNo;
    tfForEnterCode.autocapitalizationType=UITextAutocapitalizationTypeNone;
    tfForEnterCode.autocorrectionType = UITextAutocorrectionTypeNo;
    [vwForAccessCode addSubview:tfForEnterCode];
    tfForEnterCode.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"Chaperone_Access_code"];
    
    
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1].CGColor;
    border.frame = CGRectMake(0, tfForEnterCode.frame.size.height - borderWidth, tfForEnterCode.frame.size.width, tfForEnterCode.frame.size.height);
    border.borderWidth = borderWidth;
    [tfForEnterCode.layer addSublayer:border];
    tfForEnterCode.layer.masksToBounds = YES;
    
    btnremeber=[UIButton buttonWithType:UIButtonTypeCustom];
    btnremeber.frame=CGRectMake(10, 180, 120, 40);
    [btnremeber setSelected:YES];
    [btnremeber setTitle:@"Remember" forState:UIControlStateNormal];
    [btnremeber setImage:[UIImage imageNamed:@"box_icon"] forState:UIControlStateNormal];
    [btnremeber setImage:[UIImage imageNamed:@"tick_icon"] forState:UIControlStateSelected];
    btnremeber.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
    [btnremeber setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnremeber addTarget:self action:@selector(RememberPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    [vwForAccessCode addSubview:btnremeber];
    
    
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake((vwForAccessCode.frame.size.width/2)+20, 180, 80, 40)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:22];
    
    [btnDone addTarget:self action:@selector(goToChaperonepage:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDone.backgroundColor=[UIColor colorWithRed:228.0/255.0 green:27.0/255.0 blue:35.0/255.0 alpha:1];
    [vwForAccessCode addSubview:btnDone];
    
    UIButton *btnCross=[[UIButton alloc]initWithFrame:CGRectMake(vwForAccessCode.frame.size.width-30, 10.0, 20, 20)];
    //[btnCross setTitle:@"*" forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(RemoveAccessView:) forControlEvents:UIControlEventTouchUpInside];
    [btnCross setImage:[UIImage imageNamed:@"Cross_mark"] forState:UIControlStateNormal];
    //btnCross.backgroundColor=[UIColor redColor];
    [vwForAccessCode addSubview:btnCross];
    
    
    
    [vw addSubview:vwForAccessCode];
    [vwMenu addSubview:vw];
    
    
}

#pragma mark- RememberPassword
-(void)RememberPassword:(id)sender{
    
    UIButton *btn=(UIButton *)(id)sender;
    btn.selected=!btn.selected;
    
    
    
    
}


#pragma mark- RemoveAccessView
-(void)RemoveAccessView:(id)sender{
    
    UIView *vw=[vwMenu viewWithTag:110];
    vw.alpha     = 0.0f;
    vw.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
         usingSpringWithDamping:2.0f
          initialSpringVelocity:1
                        options:0
                     animations:^{
                         vw.alpha     = 1.0f;
                         vw.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                    }
                     completion:^(BOOL finished){
                         [vw removeFromSuperview];

                     }];
    

  
    
    
    
}

#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.layer.borderColor=[UIColor blackColor].CGColor;
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    
    return newLength <= 5;
    
    
    
}
/*
#pragma mark-goToChaperonepage
-(void)goToChaperonepage:(id)sender{
    
    if (tfForEnterCode.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for access code"];
        return;
        
    }else if (tfForEnterCode.text.length<5){
        [[CommonMethods sharedInstance] AlertAction:@"Please enter correct 5-digit chaperone access code"];
        return;
        
    }else{
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [ dict setValue:tfForEnterCode.text forKey:@"access_code"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            [[CommonMethods sharedInstance] addSpinner:vwMenu];
            //[self addSpinner:vwMenu];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/chaperon_login",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([returArray valueForKey:@"error"]) {
                        [[CommonMethods sharedInstance] AlertAction:errorstr];
                        return;
                    }else if([returArray valueForKey:@"access_token"]){
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        // saving an NSString
                        if (btnremeber.isSelected) {
                            [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Access_code"];
                            
                        }else{
                            [prefs removeObjectForKey:@"Chaperone_Access_code"];
                            
                        }
                        
                        
                        //  NSString *strFullgroupName=[@"Group-" stringByAppendingString:[returArray valueForKey:@"group_name"] ];
                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"Chaperone_id"];
                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"chaperone_access_token"];
                        [prefs setObject:[returArray valueForKey:@"teacher_id"] forKey:@"teacher_id"];
                        [prefs setObject:[returArray valueForKey:@"group_id"] forKey:@"group_id"];
                        [prefs setObject:[returArray valueForKey:@"trip_id"] forKey:@"trip_id"];
                        [prefs setObject: [@"Group -" stringByAppendingString:[returArray valueForKey:@"group_name"] ]   forKey:@"group_name"];
                        [prefs setObject:[returArray valueForKey:@"score"] forKey:@"group_score"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [vwMenu removeFromSuperview];
                        
                        //                        UIView *vw=[vwMenu viewWithTag:110];
                        //                        [vw removeFromSuperview];
                        
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
 
                        [appdel.navController pushViewController:WallOfFameViewController animated:YES];
                        
                        
                    }
                    
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    
                }
            }];
            
            
            
            
        }else{
            
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
        }
        
        
        
        
        
        
    }
    
}
 */
#pragma mark-goToChaperonepage
-(void)goToChaperonepage:(id)sender{
    
    if (tfForEnterCode.text.length==0) {
        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide input for access code"];
        return;
        
    }else if (tfForEnterCode.text.length<5){
        [[CommonMethods sharedInstance] AlertAction:@"Please enter correct 5-digit chaperone access code"];
        return;
        
    }else{
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [ dict setValue:tfForEnterCode.text forKey:@"access_code"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
//            if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text]){
//                [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Chaperone_Student_List"];
//                [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
//            }
            
            
            [[CommonMethods sharedInstance] addSpinner:vwMenu];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/chaperon_login",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([returArray valueForKey:@"error"]) {
                        [[CommonMethods sharedInstance] AlertAction:errorstr];
                        return;
                    }else if([returArray valueForKey:@"access_token"]){
                        
                        if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text] || ![[returArray valueForKey:@"group_id"]isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"group_id"]] ){
                            [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Chaperone_Student_List"];
                            [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
                        }

                        
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        // saving an NSString
                        
                        if (btnremeber.isSelected) {
                            [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Access_code"];
                            
                        }else{
                            [prefs removeObjectForKey:@"Chaperone_Access_code"];
                            
                        }
                        
                        //  NSString *strFullgroupName=[@"Group-" stringByAppendingString:[returArray valueForKey:@"group_name"] ];
                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"Chaperone_id"];
                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"chaperone_access_token"];
                        [prefs setObject:[returArray valueForKey:@"teacher_id"] forKey:@"teacher_id"];
                        [prefs setObject:[returArray valueForKey:@"group_id"] forKey:@"group_id"];
                        [prefs setObject:[returArray valueForKey:@"trip_id"] forKey:@"trip_id"];
                        [prefs setObject: [@"Group -" stringByAppendingString:[returArray valueForKey:@"group_name"] ]   forKey:@"group_name"];
                        [prefs setObject: [returArray valueForKey:@"group_id"]    forKey:@"group_id"];
                        [prefs setObject:[returArray valueForKey:@"score"] forKey:@"MarksObtained"];
                        [prefs setObject:tfForEnterCode.text forKey:@"Chaperone_Code"];
                       // [prefs setObject:@"1" forKey:@"completed"];
                        [prefs setObject:[returArray valueForKey:@"completed"] forKey:@"completed"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        
                        
                        
                        
                        [self getQustionAndAnswerFromServer];
                        
           //             UIView *vw=[self.view viewWithTag:1];
                   //     [vw removeFromSuperview];
                        
                        
                    }
                    
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    
                }
            }];
            
            
            
            
        }else{
            
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chaperone_Code"] isEqualToString:tfForEnterCode.text]){
                [vwMenu removeFromSuperview];
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                WallOfFameViewController.Islogin=NO;
                AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appdel.navController pushViewController:WallOfFameViewController animated:YES];
            }
        }
    }
}




-(void)getQustionAndAnswerFromServer{
    ArrQuestion = [[NSMutableArray alloc]init];
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        
        [[CommonMethods sharedInstance] addSpinner:vwMenu];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/QuestionsAPI/get_question_detail?access_token=%@&teacher_id=%@&trip_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"chaperone_access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"teacher_id"],[[NSUserDefaults standardUserDefaults] stringForKey:@"trip_id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                NSLog(@"returArray %@",returArray);
                //New By Anand to check current and itinerary date
                if ([returArray valueForKey:@"current_date"] && [returArray valueForKey:@"itinerary_date"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:[[returArray valueForKey:@"current_date"] substringToIndex:10] forKey:@"currentDate"];
                    [[NSUserDefaults standardUserDefaults] setObject:[[returArray valueForKey:@"itinerary_date"] substringToIndex:10] forKey:@"itineraryDate"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
              //  [[CommonMethods sharedInstance] removeSpinner:self.view];
                ArrQuestion=[returArray valueForKey:@"result"];
                
                
                if ([returArray valueForKey:@"success"]) {
                    
                    if (ArrQuestion.count==0) {
                        
//                        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(20.0,( self.view.frame.size.height/2)-8, self.view.frame.size.width-40, 16)];
//                        lbl.text=@"No questions available";
//                        lbl.textAlignment=NSTextAlignmentCenter;
//                        lbl.textColor=[UIColor darkGrayColor];
                      //  [self.view addSubview:lbl];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                        WallOfFameViewController.Islogin=NO;
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        
                        [appdel.navController pushViewController:WallOfFameViewController animated:YES];

                        
                    }else{
                        //[self createUi];
                        
                        [[DataBaseFetchValues sharedInstance] deleteQuerry:@"DELETE FROM Quiz_Game"];
                        for (int k=0; k<ArrQuestion.count; k++) {
                            NSDictionary *dict=[[NSDictionary alloc]init];
                            dict=[ArrQuestion objectAtIndex:k];
                            NSArray *arr=[[NSArray alloc]init];
                            arr=[dict valueForKey:@"Questions"];
                            for (int m=0; m<arr.count; m++) {
                                NSMutableDictionary *dictQuestionsave=[[NSMutableDictionary alloc]init];
                                [ dictQuestionsave setValue:[dict valueForKey:@"QuestionCategory"] forKey:@"QuestionCategory"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Question"] forKey:@"Question"];
                                
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer1"] forKey:@"Answer1"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer2"] forKey:@"Answer2"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer3"] forKey:@"Answer3"];
                                [ dictQuestionsave setValue:[[arr objectAtIndex:m] valueForKey:@"Answer4"] forKey:@"Answer4"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"Correct_answer"] intValue]] forKey:@"Correct_answer"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"Marks"] intValue]] forKey:@"Marks"];
                                [ dictQuestionsave setValue:[NSNumber numberWithInt:[[[arr objectAtIndex:m] valueForKey:@"id"] intValue]] forKey:@"Question_id"];
                                [[DatabaseManagement sharedInstance]SavedataInDatabaseforQuiz:dictQuestionsave];
                                
                                
                                
                            }
                            
                        }
                        
                        
                        [vwMenu removeFromSuperview];

                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        ChaperoneStrtViewController *WallOfFameViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ChaperoneView"];
                        WallOfFameViewController.Islogin=NO;
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        
                        [appdel.navController pushViewController:WallOfFameViewController animated:YES];
                        //[DatabaseManagement sharedInstance]SavedataInDatabaseforQuiz:<#(NSDictionary *)#>
                        
                    }
                }
//                if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"completed"]intValue] == 1) {
//                    [self fetchFromDBAndPrepareQuizIfCompleted];
//                }
//                else
//                    [self fetchFromDBAndPrepareQuiz];
           }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:vwMenu];
               

            }
        }];
    }
    else
    {
//        
//        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
//        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"completed"]intValue] == 1) {
//            [self fetchFromDBAndPrepareQuizIfCompleted];
//        }
//        else
//            [self fetchFromDBAndPrepareQuiz];
        
    }
    
    
}

#pragma mark- AddMenuBaronChaperone
-(UIImageView *)AddMenuBaronChaperone:(UIView *)Vw andFrame:(CGRect)frame{
    UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_button"]];
    ImgVw.frame=frame;
    [Vw addSubview:ImgVw];
    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
    ImgVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowMenuChaperone:)];
    tapper.cancelsTouchesInView = NO;
    [tapper setCancelsTouchesInView:NO];
    [ImgVw addGestureRecognizer:tapper];
    
    return ImgVw;
    
}

#pragma mark-ShowMenu
-(void)ShowMenuChaperone:(UITapGestureRecognizer *)sender{
    UIView *view = sender.view;
    //    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [appdel.btnAdd setHidden:YES];
    vwMenu=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width,  [[UIScreen mainScreen]bounds].size.height)];
    vwMenu.backgroundColor=[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:0.8];
    
    [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-200,[[UIScreen mainScreen]bounds].size.height-170 , 160, 50) andView:vwMenu andImge:[UIImage imageNamed:@"teacher_icon"] andTitle:@"Teacher login" andTag:10];
    
    
    UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
    ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 );
    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
    ImgVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
    tapper.cancelsTouchesInView = NO;
    [tapper setCancelsTouchesInView:NO];
    [ImgVw addGestureRecognizer:tapper];
    [vwMenu addSubview:ImgVw];
    
    [view.superview.superview addSubview:vwMenu];
    
    /*
     if (IS_IPHONE_5) {
     int x=[[UIScreen mainScreen]bounds].size.height+30;
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-240, x-530, 200, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Chaperone login" andTag:7];
     
     
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-210, x-470, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Student List" andTag:1];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-210, x-410, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_red_icon"] andTitle:@"My Itinerary" andTag:2];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-270, x-350, 230, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_blue_icon"] andTitle:@"Manage Chaperone" andTag:3];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-240, x-290, 200, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_orange_icon"] andTitle:@"Manage Groups" andTag:4];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-220, x-230, 180, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_purple_icon"] andTitle:@"Group Scores" andTag:5];
     
     
     UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
     ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-80,70 ,70 );
     ImgVw.contentMode=UIViewContentModeScaleAspectFit;
     ImgVw.userInteractionEnabled=YES;
     UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
     tapper.cancelsTouchesInView = NO;
     [tapper setCancelsTouchesInView:NO];
     [ImgVw addGestureRecognizer:tapper];
     [vwMenu addSubview:ImgVw];
     
     
     
     //        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-200, x-500, 170, 50)];
     //        lbl.text=@"Chaperone";
     //        lbl.textColor=[UIColor whiteColor];
     //        lbl.textAlignment=NSTextAlignmentLeft;
     //        UISwitch *switchbtn  =[[UISwitch alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, x-500, 170, 50)];
     //        [switchbtn setOn:NO animated:YES];
     //
     //        //[switchbtn addTarget:self action:@selector(SelectRequiredMode:) forControlEvents:UIControlEventValueChanged];
     //        [vwMenu addSubview:switchbtn];
     //
     //        [vwMenu addSubview:lbl];
     
     
     }else{
     int x=[[UIScreen mainScreen]bounds].size.height;
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-230, x-590, 200, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Chaperone login" andTag:7];
     
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-210, x-520, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_icon"] andTitle:@"Student List" andTag:1];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-210, x-450, 170, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_red_icon"] andTitle:@"My Itinerary" andTag:2];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-270, x-380, 230, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_blue_icon"] andTitle:@"Manage Chaperone" andTag:3];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-240, x-310, 200, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_orange_icon"] andTitle:@"Manage Groups" andTag:4];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-220, x-240, 180, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_purple_icon"] andTitle:@"Group Scores" andTag:5];
     [self CreateMenuButton:CGRectMake([[UIScreen mainScreen]bounds].size.width-200,x-170 , 160, 50) andView:vwMenu andImge:[UIImage imageNamed:@"menu_pink_icon"] andTitle:@"Useful Link" andTag:6];
     
     
     UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
     ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 );
     ImgVw.contentMode=UIViewContentModeScaleAspectFit;
     ImgVw.userInteractionEnabled=YES;
     UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
     tapper.cancelsTouchesInView = NO;
     [tapper setCancelsTouchesInView:NO];
     [ImgVw addGestureRecognizer:tapper];
     [vwMenu addSubview:ImgVw];
     
     
     } //CGRectMake([[UIScreen mainScreen]bounds].size.width-100, [[UIScreen mainScreen]bounds].size.height-100,70 ,70 )
     
     
     
     
     //  UIImageView *ImgVw=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_cross_icon"]];
     //    if (IS_IPAD) {
     //         ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-95,70 ,70 );
     //    }
     //    else
     //    {
     //    ImgVw.frame=CGRectMake([[UIScreen mainScreen]bounds].size.width-90, [[UIScreen mainScreen]bounds].size.height-95,60 ,60 );
     //    }
     //    ImgVw.contentMode=UIViewContentModeScaleAspectFit;
     //    ImgVw.userInteractionEnabled=YES;
     //    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(RemoveMenu:)];
     //    tapper.cancelsTouchesInView = NO;
     //    [tapper setCancelsTouchesInView:NO];
     //    [ImgVw addGestureRecognizer:tapper];
     //    [vwMenu addSubview:ImgVw];
     
     [view.superview.superview addSubview:vwMenu];
     
     */
    
}

#pragma mark- AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex==0) {
        
    }else if (buttonIndex==1){
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"] forKey:@"access_token"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"id"] forKey:@"id"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            [vwMenu removeFromSuperview];
            // btnLogout.userInteractionEnabled=NO;
            [[CommonMethods sharedInstance] addSpinner:vwMenu];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/logout",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    // btnLogout.userInteractionEnabled=YES;
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    NSLog(@"returArray %@",returArray);
                    //  NSString *errorstr = [returArray valueForKey:@"success"];
                    
                    //if([returArray valueForKey:@"success"]){
                    {
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setBool:NO forKey:@"isVerified"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //[self.navigationController popToRootViewControllerAnimated:YES];
                        
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        StartingPageViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"StartingPageView"];
                        UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appdel.window.rootViewController = obj;
                    }
                    
                }
                else
                {
                    //btnLogout.userInteractionEnabled=YES;
                    [[CommonMethods sharedInstance] removeSpinner:vwMenu];
                    
                }
            }];
            
            
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
            
            
        }
        
    }
}









@end
