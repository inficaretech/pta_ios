//
//  AddStudentViewController.h
//  Putnam Teacher's App
//
//  Created by Inficare on 6/6/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddStudentViewController : UIViewController
@property (nonatomic, assign) BOOL IsEditStudent;
@property (nonatomic, copy) NSDictionary *dictStudent;
@property(nonatomic,strong) NSMutableArray *arrGrades;

@end
