//
//  GroupScoresViewController.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/28/16.
//  Copyright Â© 2016 inficare. All rights reserved.
//

#import "GroupScoresViewController.h"
#import "GroupScoreTableViewCell.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "SendTrophiesViewController.h"
#import "Constants.h"

@interface GroupScoresViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tableViewGroupScores;
    NSMutableArray *arrGroupScores;
    NSMutableArray *arrGroupAssignment;
    BOOL isButtonPressed;
    NSMutableString *stringButton;
    NSMutableArray *arrGroup;
}
@end

@implementation GroupScoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self UiForNavigationBar];
    stringButton = [[NSMutableString alloc]init];
    arrGroup = [[NSMutableArray alloc]init];
    
    //[self CreateUi];
    
    // Do any additional setup after loading the view.
}



-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Group Scores";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self GetApiForScores];
}



-(void)CreateUi
{
    if (arrGroupScores.count==0) {
        
        
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        label.text = @"No Group Scores found ";
        label.font = [UIFont fontWithName:@"Roboto-regular" size:17];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.textColor = [UIColor darkGrayColor];
        [self.view addSubview:label];
        
    }else{
        UILabel *labelTripName  = [[UILabel alloc]init];
        labelTripName.frame = CGRectMake(30.0, 64, [[UIScreen mainScreen]bounds].size.width/2-30, 40);
        [self.view addSubview:labelTripName];
        labelTripName.font = [UIFont fontWithName:@"Roboto-regular" size:15];
        labelTripName.text = [[arrGroupScores valueForKey:@"trip_name"]objectAtIndex:0];
        labelTripName.textColor = [UIColor blackColor];
        
        NSString *strDate = [[arrGroupScores valueForKey:@"trip_date"]objectAtIndex:0];
        if ([strDate isKindOfClass:[NSNull class]]) {
            strDate = @"N/A";
        }
        else
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM-dd-yyyy HH:mm"];
            NSDate *date = [formatter dateFromString:strDate];
            [formatter setDateFormat:@"MM/dd/yyyy"];
            strDate = [formatter stringFromDate:date];
        }
        UILabel *labelDate = [[UILabel alloc]init];
        labelDate.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2, 64, [[UIScreen mainScreen] bounds].size.width/2-30, 40);
        [self.view addSubview:labelDate];
        labelDate.font = [UIFont fontWithName:@"Roboto-regular" size:15];
        labelDate.text = [@"Date : " stringByAppendingString:strDate];
        labelDate.textColor = [UIColor blackColor];
        labelDate.textAlignment = NSTextAlignmentRight;
        
        
        tableViewGroupScores = [[UITableView alloc]init];
        tableViewGroupScores.frame = CGRectMake(0.0, 104.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-150);
        [self.view addSubview:tableViewGroupScores];
        tableViewGroupScores.delegate = self;
        tableViewGroupScores.dataSource=self;
        tableViewGroupScores.separatorColor = [UIColor clearColor];
        
        UIButton *buttonDeclareWinner = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonDeclareWinner.frame = CGRectMake(20.0, [[UIScreen mainScreen] bounds].size.height-100, [[UIScreen mainScreen] bounds].size.width-40, 40.0);
        [buttonDeclareWinner setTitle:@"Declare Winner" forState:UIControlStateNormal];
        [self.view addSubview:buttonDeclareWinner];
        buttonDeclareWinner.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        [buttonDeclareWinner setBackgroundColor:[UIColor  colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1]];
        [buttonDeclareWinner addTarget:self action:@selector(buttonDeclareWinnerClicked) forControlEvents:UIControlEventTouchUpInside];
        buttonDeclareWinner.layer.shadowColor = [UIColor grayColor].CGColor;
        buttonDeclareWinner.layer.shadowOffset = CGSizeMake(0, 1.0);
        buttonDeclareWinner.layer.shadowOpacity = 2.0;
        buttonDeclareWinner.layer.shadowRadius = 3.0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrGroupScores.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    GroupScoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GroupScoreTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    NSString *strDetail = @"Group ";
    strDetail =[ strDetail stringByAppendingString:[[arrGroupScores objectAtIndex:indexPath.row]valueForKey:@"group_name"]];
    strDetail = [strDetail stringByAppendingString:@" "];
    NSRange range = NSMakeRange(0, strDetail.length);
    NSString *strChaperoneName =[[arrGroupScores objectAtIndex:indexPath.row]valueForKey:@"chaperon_name"];
    if (![strChaperoneName isKindOfClass:[NSNull class]]) {
        
        
        NSString *substrings = [strDetail stringByAppendingString:strChaperoneName];
        NSMutableString * string = [substrings mutableCopy];
        [string replaceOccurrencesOfString:@"`"
                                withString:@" "
                                   options:0
                                     range:NSMakeRange(0, string.length)];
        
        strDetail = string;
    }
    else
    {
        strDetail = [strDetail stringByAppendingString:@"N/A"];
    }
    NSRange rangeOfdetail = NSMakeRange(0, strDetail.length);
    NSString *strName = [[arrGroupScores objectAtIndex:indexPath.row]valueForKey:@"chaperon_name"];
    //   NSRange range = NSMakeRange(0, strName.length);
    if ([strName isKindOfClass:[NSNull class]]) {
        strName = @"N/A";
    }
    else
    {
        strName = [strName substringWithRange:NSMakeRange(0, 1)];
    }
    NSMutableAttributedString *text =[[NSMutableAttributedString alloc] initWithString:strDetail];
    
    for (int k=0; k<=arrGroupScores.count; k+=4) {
        if (indexPath.row==k)
        {
            [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:139.0/255.0 green:195.0/255.0 blue:74.0/255.0 alpha:1] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Regular" size:17] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            cell.labelAlphabet.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:195.0/255.0 blue:74.0/255.0 alpha:1];
        }
        else if (indexPath.row==k+1)
        {
            [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Regular" size:17] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            cell.labelAlphabet.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:27.0/255.0  blue:35.0/255.0 alpha:1];
        }
        else if (indexPath.row==k+2)
        {
            [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:174.0/255.0  blue:239.0/255.0 alpha:1] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Regular" size:17] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            cell.labelAlphabet.backgroundColor =[UIColor colorWithRed:0 green:174.0/255.0  blue:239.0/255.0 alpha:1];
        }
        else if (indexPath.row==k+3)
        {
            [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:248.0/255.0 green:88.0/255.0 blue:37.0/255.0 alpha:1] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Regular" size:17] range:NSMakeRange(range.length, rangeOfdetail.length-range.length)];
            cell.labelAlphabet.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:88.0/255.0 blue:37.0/255.0 alpha:1];
            
        }
    }
    cell.labelAlphabet.text = strName;
    cell.labelDetail.attributedText = text;
    
    [cell.buttonTick setImage:[UIImage imageNamed:@"Blue_tick"] forState:UIControlStateNormal];
   // [cell.buttonTick addTarget:self action:@selector(ButtonClickPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonTick.tag = indexPath.row;
    cell.labelMarks.text= [[arrGroupScores objectAtIndex:indexPath.row]valueForKey:@"score"];
    
    return  cell;
    
}
-(void)ButtonClickPressed:(id)sender
{
    UIButton *btn = (id)sender;
    NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
    if ([stringButton containsString:[NSString stringWithFormat:@"%ld",(long)btn.tag]]) {
        NSMutableArray *arr = [[stringButton componentsSeparatedByString:@","] mutableCopy];
        [arr removeObject:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
        stringButton = [[arr componentsJoinedByString:@","] mutableCopy];
        
        btn.layer.borderColor = [UIColor clearColor].CGColor;
        [btn setImage:nil forState:UIControlStateNormal];
        btn.layer.borderColor = [UIColor blackColor].CGColor;
        btn.layer.borderWidth = 1.0;
        NSString *strId = [[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"];
        
        for (NSDictionary *dict in arrGroupAssignment) {
            if ([[dict valueForKey:@"chaperon_id"] isEqualToString:strId]) {
                [arrGroupAssignment removeObject:dict];
                [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"] forKey:@"chaperon_id"];
                [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"group_id"] forKey:@"group_id"];
                [dictGroupAsignment setObject:@"0" forKey:@"declare"];
                [arrGroupAssignment addObject:dictGroupAsignment];
                break;
            }
        }
        
        for (NSDictionary *dict in arrGroup) {
            if ([[dict valueForKey:@"chaperon_id"] isEqualToString:strId]) {
                [arrGroup removeObject:dict];
                break;
            }
        }
        
        
    }
    else{
        
        btn.layer.borderColor = [UIColor clearColor].CGColor;
        [btn setImage:[UIImage imageNamed:@"Blue_tick"] forState:UIControlStateNormal];
        
        if (stringButton.length>0) {
            [stringButton appendString:@","];
        }
        [stringButton appendString:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
        
        NSString *strId = [[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"];
        
        for (NSDictionary *dict in arrGroupAssignment) {
            if ([[dict valueForKey:@"chaperon_id"] isEqualToString:strId]) {
                [arrGroupAssignment removeObject:dict];
                [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"] forKey:@"chaperon_id"];
                [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"group_id"] forKey:@"group_id"];
                [dictGroupAsignment setObject:@"1" forKey:@"declare"];
                [arrGroupAssignment addObject:dictGroupAsignment];
                break;
            }
        }
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        [dict setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"] forKey:@"chaperon_id"];
        [dict setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"group_id"] forKey:@"group_id"];
        [arrGroup addObject:dict];
        
    }
    
    /*if ([stringButton containsString:[NSString stringWithFormat:@"%ld",(long)button.tag]]) {
     
     
     if (!isButtonPressed) {
     UIButton *btn = (id)sender;
     btn.layer.borderColor = [UIColor clearColor].CGColor;
     [btn setImage:[UIImage imageNamed:@"Blue_tick"] forState:UIControlStateNormal];
     NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
     [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"] forKey:@"cheperon_id"];
     [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"group_id"] forKey:@"group_id"];
     [arrGroupAssignment addObject:dictGroupAsignment];
     isButtonPressed = YES;
     
     }
     else
     {
     UIButton *btn = (id)sender;
     btn.layer.borderColor = [UIColor clearColor].CGColor;
     [btn setImage:nil forState:UIControlStateNormal];
     btn.layer.borderColor = [UIColor blackColor].CGColor;
     btn.layer.borderWidth = 1.0;
     NSString *strId = [[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperon_id"];
     
     for (NSDictionary *dict in arrGroupAssignment) {
     if ([[dict valueForKey:@"cheperon_id"] isEqualToString:strId]) {
     [arrGroupAssignment removeObject:dict];
     break;
     }
     }
     isButtonPressed = NO;
     }
     }
     else
     {
     UIButton *btn = (id)sender;
     btn.layer.borderColor = [UIColor clearColor].CGColor;
     [btn setImage:[UIImage imageNamed:@"Blue_tick"] forState:UIControlStateNormal];
     NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
     [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"cheperone_id"] forKey:@"cheperon_id"];
     [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:btn.tag]valueForKey:@"group_id"] forKey:@"group_id"];
     [arrGroupAssignment addObject:dictGroupAsignment];
     isButtonPressed = NO;
     
     }
     if (stringButton.length>0) {
     [stringButton appendString:@","];
     }
     [stringButton appendString:[NSString stringWithFormat:@"%ld",(long)button.tag]];*/
}

-(void)buttonDeclareWinnerClicked
{
    if (arrGroupAssignment.count>0) {
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        
        
        NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
        NSString *accesstoken = [defualts valueForKey:@"access_token"];
        NSString *teacherId = [defualts valueForKey:@"id"];
        [dict setValue:accesstoken forKey:@"access_token"];
        [dict setValue:teacherId forKey:@"teacher_id"];
        [dict setValue:arrGroupAssignment forKey:@"mapping_id"];
        // [dict setValue:[NSNumber numberWithInt:fieldtripId] forKey:@"trip_id"];
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        if (isinternet) {
            self.view.userInteractionEnabled=NO;
            [[CommonMethods sharedInstance] addSpinner:self.view];
            
            
            
            [[WebService sharedInstance] fetchDataWithCompletionBlock:[NSString stringWithFormat:@"%@index.php/GroupsAPI/winner", kBaseUrl] param:dict andcompletionhandler:^(NSArray *returArray, NSError *error ){
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User is already exist"];
                        self.view.userInteractionEnabled=YES;
                        return;
                        
                        
                    }else if([[returArray valueForKey:@"result"] isEqual:@"success"]){
                        
                        
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SendTrophiesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"SendTrophiesViewController"];
                        Controller.arrGroup = arrGroupAssignment;
                        [self.navigationController pushViewController:Controller animated:YES ];
                        self.view.userInteractionEnabled=YES;
                        
                    }
                    
                }
                else
                {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    self.view.userInteractionEnabled=YES;
                    
                }
            }];
        }else
        {
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
        
        
        //
        //    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //    SendTrophiesViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"SendTrophiesViewController"];
        //    [self.navigationController pushViewController:Controller animated:YES ];
        //    self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        [[CommonMethods sharedInstance]AlertAction:@"Please select to declare the winner"];
    }
}
-(void)GetApiForScores
{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/GroupsAPI/group_score?access_token=%@&teacher_id=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"],[[NSUserDefaults standardUserDefaults] stringForKey:@"id"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                arrGroupScores = [[NSMutableArray alloc]init];
                //                NSArray *arr=[[NSArray alloc]init];
                //                arr=[[returArray valueForKey:@"result"]mutableCopy];
                if ([returArray valueForKey:@"result"]!=(id)[NSNull null]) {
                    arrGroupScores = [returArray valueForKey:@"result"];
                    if (arrGroupScores.count>0) {
                        [[NSUserDefaults standardUserDefaults]setValue:[[[returArray valueForKey:@"result"]objectAtIndex:0]valueForKey:@"trip_id"] forKey:@"tripFieldIdname"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        arrGroupAssignment = [[NSMutableArray alloc]init];
                        
                        for (int i = 0; i<arrGroupScores.count; i++) {
                            NSMutableDictionary *dictGroupAsignment = [[NSMutableDictionary alloc]init];
                            [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:i]valueForKey:@"cheperone_id"] forKey:@"chaperon_id"];
                            [dictGroupAsignment setObject:[[arrGroupScores objectAtIndex:i]valueForKey:@"group_id"] forKey:@"group_id"];
                            [dictGroupAsignment setObject:@"1" forKey:@"declare"];
                            [arrGroupAssignment addObject:dictGroupAsignment];
                        }
                        
                        
                        
                    }
                    
                    [self CreateUi];
                }else{
                    return;
                }
                
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end