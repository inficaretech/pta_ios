//
//  SignUpViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 5/27/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "SignUpViewController.h"
#import "WebService.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "HomeViewController.h"

@interface SignUpViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    
    
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfPassword;
    IBOutlet UITextField *tfName;
    IBOutlet UITextField *tfSchool;
    IBOutlet UITextField *tfNumber;
    IBOutlet UIButton *btnSignUp;
    IBOutlet UIImageView *ImgVw;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lblTeacher;
    IBOutlet  UILabel *lblCreate;
    IBOutlet UITextField *tfLastName;
    
    
}
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    // self.navigationItem.hidesBackButton=NO
    
    
    [self crateUi];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)crateUi{
    scrollView.scrollEnabled=NO;
    tfEmail.delegate=self;
    tfEmail.keyboardType = UIKeyboardTypeEmailAddress;
    tfName.delegate=self;
    tfSchool.delegate=self;
    tfNumber.delegate=self;
    tfPassword.delegate=self;
    tfNumber.keyboardType=UIKeyboardTypePhonePad;
    tfLastName.delegate=self;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                                                  target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    tfNumber.inputAccessoryView = toolBar;
    
    
    
    UIView *Vw=[[UIView alloc]initWithFrame:CGRectMake(tfPassword.frame.size.width-50, 5, 50, tfPassword.frame.size.height-10)];
    tfPassword.rightView=Vw;
    tfPassword.rightViewMode = UITextFieldViewModeAlways;// Set rightview mode
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 5, 60, tfPassword.frame.size.height-20);
    [Vw addSubview:btn];
    [btn setImage:[UIImage imageNamed:@"eye_icon"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(ShowPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnSignUp.titleLabel.numberOfLines = 2;
    [btnSignUp setTitle:@"       SIGN UP\n(For teacher only)" forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapper=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    tapper.delegate=self;
    [tapper setCancelsTouchesInView:NO];
    [scrollView addGestureRecognizer:tapper];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(20.0, 40.0, 20.0, 20.0);
    [btnBack setImage:[UIImage imageNamed:@"back_button2"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
}
#pragma mark-DoneButton
-(void)done:(id)sender{
    
    
    NSString *strippedNumber = [tfNumber.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [tfNumber.text length])];
    if ([self validatePhoneNumberWithString1:strippedNumber]) {
        [tfNumber resignFirstResponder];
    }
    else
    {
        [[CommonMethods sharedInstance]AlertAction:@"Please enter valid phone number"];
    }
    

    
    
    [tfNumber resignFirstResponder];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
    
    
}
#pragma mark- ShowPassword
-(void)ShowPassword:(id)sender{
    
    [tfPassword setSecureTextEntry:!tfPassword.secureTextEntry];
    
    
    
}

#pragma mark- back
-(void)BackButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
}
-(void)handleSingleTap:(id)sender{
    
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
    
}


#pragma mark-SignUP
- (IBAction)SignUp:(id)sender {
    
    //    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    HomeViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"HomeView"];
    //    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:Controller];
    //    [self presentViewController:navController animated:YES completion:nil];
    //
    
  //  NSString *strippedNumber = [tfNumber.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [tfNumber.text length])];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    BOOL isEmailValid=[self NSStringIsValidEmail:tfEmail.text];
    
    if (tfEmail.text.length==0) {
        [self AlertAction:@"Kindly provide input for email"];
        return;
        
    }else if (!isEmailValid){
        [self AlertAction:@"Kindly provide valid input for email"];
        return;
        
    }else if (tfPassword.text.length==0){
        [self AlertAction:@"Kindly provide input for password"];
        return;
        
    }else if (tfPassword.text.length<8){
        [self AlertAction:@"Kindly provide password between 8-15 characters"];
        return;
        
    }else if (tfName.text.length==0) {
        [self AlertAction:@"Kindly provide input for name"];
        return;
    }else if (tfLastName.text.length==0) {
        [self AlertAction:@"Kindly provide input for last name"];
        return;
    }
    else if (tfSchool.text.length==0) {
        [self AlertAction:@"Kindly provide input for school name"];
        return;
    }else  if (tfNumber.text.length==0) {
        [self AlertAction:@"Kindly provide input for contact number"];
        return;
    }else if (tfNumber.text.length<10){
        [self AlertAction:@"Kindly provide valid contact number"];
        return;
    }
//    else if (![self validatePhoneNumberWithString1:strippedNumber])
//    {
//        [[CommonMethods sharedInstance] AlertAction:@"Kindly provide valid contact number"];
//         return;
//    }
    
    else{
        //Anvesh
        NSString *strName=[[tfName.text stringByAppendingString:@"`"] stringByAppendingString:tfLastName.text];
        
        [dict setValue:tfEmail.text forKey:@"email"];
        [dict setValue:tfPassword.text forKey:@"password"];
        [dict setValue:strName forKey:@"name"];
        [dict setValue:tfSchool.text forKey:@"school_name"];
        [dict setValue:tfNumber.text forKey:@"contact_number"];
        [dict setValue:[NSNumber numberWithInt:2] forKey:@"platform"];
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Teachers/teacher_signup",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    NSLog(@"returArray %@",returArray);
                    NSString *errorstr = [returArray valueForKey:@"error"];
                    
                    if ([errorstr isEqualToString:@"already exist"]) {
                        [[CommonMethods sharedInstance] AlertAction:@"User already exist"];
                        return;
                        
                        
                    }else if([returArray valueForKey:@"access_token"]){
                        
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        // saving an NSString
                        [prefs setObject:[returArray valueForKey:@"id"] forKey:@"id"];
                        [prefs setObject:[returArray valueForKey:@"access_token"] forKey:@"access_token"];
                        [prefs removeObjectForKey:@"Class"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //[self.navigationController pushViewController:Controller animated:YES];
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        HomeViewController *Controller = [storyBoard instantiateViewControllerWithIdentifier:@"HomeView"];
                        //Controller.strName=[returArray valueForKey:@"name"];
                        
                        
                        // Anvesh
                        NSArray *substrings = [returArray valueForKey:@"name"];
                        
                        
                        NSMutableString * string = [substrings mutableCopy];
                        [string replaceOccurrencesOfString:@"`"
                                                withString:@" "
                                                   options:0
                                                     range:NSMakeRange(0, string.length)];
                        // if (substrings.count>=2) {
                        //    NSString * strLastName =[substrings objectAtIndex:1];
                        [prefs setObject:string forKey:@"name"];
                        [prefs setObject:tfEmail.text forKey:@"EmailId"];
                        //      }
                        UINavigationController *obj  = [[UINavigationController alloc]initWithRootViewController:Controller];
                        AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appdel.window.rootViewController = obj;
                        
                    }
                }
                else
                {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    [[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
            
        }else{
            
            [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        }
    }
    
    
}
#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField==tfName  && IS_IPHONE_5) {
        
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
        }];
        
    }else if (textField==tfLastName  && IS_IPHONE_5) {
        
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
        }];
    }
    
    else if (textField==tfSchool && IS_IPHONE_5){
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, -100, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-100);
        }];
        
        
        
    }else if ( !IS_IPAD  && textField==tfNumber ){
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, -200, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        }];
        
    }
    
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if (textField==tfEmail) {
        
        if (tfEmail.text.length==0) {
            [self AlertAction:@"Kindly provide input for email"];
            
            return YES;
            
        }else if (![self NSStringIsValidEmail:tfEmail.text]){
            [self AlertAction:@"Kindly provide valid input for email"];
            
            return YES;
        }else{
            [tfPassword becomeFirstResponder];
        }
        
    }else if (textField==tfPassword){
        
        if (tfPassword.text.length==0){
            [self AlertAction:@"Kindly provide input for password"];
            
            return YES;
            
        }else if (tfPassword.text.length<8){
            [self AlertAction:@"Kindly provide password between 8-15 characters"];
            return YES;
            
        }else{
            [tfName becomeFirstResponder];
            
        }
        
    }else if (textField==tfName){
        
        if (tfName.text.length==0) {
            [self AlertAction:@"Kindly provide input for name"];
            return YES;
        }
        [tfLastName becomeFirstResponder];
        
    }else if (textField==tfLastName){
        
        if (tfLastName.text.length==0) {
            [self AlertAction:@"Kindly provide input for last name"];
            return YES;
        }else
            [tfSchool becomeFirstResponder];
    }
    else if (textField==tfSchool){
        if (tfSchool.text.length==0) {
            [self AlertAction:@"Kindly provide input for school name"];
            return YES;
        }
        [tfNumber becomeFirstResponder];
    }
    
    else if (textField==tfNumber){
        if (tfNumber.text.length==0) {
            [self AlertAction:@"Kindly provide input for contact number"];
            return YES;
        }else if (tfNumber.text.length<10){
            [self AlertAction:@"Kindly provide valid contact number"];
            return YES;
        }
        [tfNumber resignFirstResponder];
        
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==tfNumber) {
        int length = [self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-",num];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        
        else if(length == 7)
        {
            NSString *num = [self formatNumber:textField.text];
            
            textField.text = [NSString stringWithFormat:@"%@-%@",[num  substringToIndex:3],[num substringFromIndex:3]];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
        
        else if(length == 8)
        {
            
            NSString *num = [self formatNumber:textField.text];
            
            
            NSString *subString = [num substringWithRange: NSMakeRange(3,3)];
            
            
            
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num  substringToIndex:3],subString,[num substringFromIndex:6]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num substringToIndex:3],subString,[num substringFromIndex:6]];
        }
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    if (textField==tfName) {
        return newLength <= 15;
    }else if (textField==tfLastName){
        return newLength <= 15;
    }else if (textField==tfEmail){
        return newLength <= 100;
    }else if (textField==tfPassword){
        return newLength <= 15;
        
    }else if (textField==tfNumber){
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return ([string isEqualToString:filtered] && newLength <= 14) || returnKey ;
    }
    return newLength <= 50;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark- EmailValidation
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark- AlertAction
-(void)AlertAction:(NSString *)msg{
    
    [[CommonMethods sharedInstance] AlertAction:msg];
}

#pragma mark- PhoneMasking
-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
        
    }
    
    
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
    
    
}



- (BOOL)validatePhoneNumberWithString1:(NSString *)string {
    
    
    string = [string substringWithRange:NSMakeRange(0, 3)];
    if ([string isEqualToString:@"000"]) {
        return NO;
    }
    else
    {
        return YES;
    }
    
    
}


@end
