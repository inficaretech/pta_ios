//
//  FeedBackViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 8/5/16.
//  Copyright © 2016 inficare. All rights reserved.
//





//
//  FeedBackViewController.m
//  Putnam Teacher's App
//
//  Created by inficare on 8/5/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "FeedBackViewController.h"
#import "CommonMethods.h"
#import "WebService.h"
#import "Constants.h"
#import "FeedbackTableViewCell.h"

@interface FeedBackViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrQuestions;
    UITableView *tableViewFeedBack;
    NSMutableString *stringButton;
    NSMutableArray *arrFeedBack;
    //    BOOL isFirst;
    //    BOOL isSecond;
    //    BOOL isThird;
    //    BOOL isFourth;
    //    BOOL isFifth;
    NSMutableArray *arrAnswer;
    NSMutableArray *arrIsSelected;
}
@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrAnswer=[[NSMutableArray alloc]init];
    arrIsSelected = [[NSMutableArray alloc]init];
    NSArray *arr=[[NSArray alloc]initWithObjects:@"Strongly Disagree",@"Disagree",@"Neutral",@"Agree",@"Strongly Agree", nil];
    for (int k=0; k<5; k++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:[arr objectAtIndex:k] forKey:@"answer"];
        [dict setValue:[NSString stringWithFormat:@"%d",0] forKey:@"IsSelected"];
        [arrAnswer addObject:dict];
        
    }
    arr=nil;
    
    
    [self UiForNavigationBar];
    [self ApiForGettingQuestions];
    stringButton = [[NSMutableString alloc]init];
    arrFeedBack = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
}

-(void)UiForNavigationBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem =mailbutton;
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.title =@"Feedback";
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 2);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
    
}

-(void)BackButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)CreateUiForFeedBack
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(20.0, 72.0, [[UIScreen mainScreen] bounds].size.width, 50);
    [self.view addSubview:label];
    label.text = @"Please answer this quick, three answer survey";
    label.textColor= [UIColor darkGrayColor];
    label.font = [UIFont fontWithName:@"Roboto-regular" size:13];
    label.textAlignment = NSTextAlignmentLeft;
    
    tableViewFeedBack = [[UITableView alloc]init];
    tableViewFeedBack.frame = CGRectMake(0.0, 122, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-182);
    [self.view addSubview:tableViewFeedBack];
    tableViewFeedBack.separatorColor = [UIColor clearColor];
    tableViewFeedBack.delegate = self;
    tableViewFeedBack.dataSource = self;
    [tableViewFeedBack setContentInset:UIEdgeInsetsMake(0,0,50,0)];
    //  tableViewFeedBack.backgroundColor = [UIColor grayColor];
    
    UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSubmit.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 80.0,[[UIScreen mainScreen] bounds].size.height-50 , 160.0, 40.0);
    [self.view addSubview:buttonSubmit];
    [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    buttonSubmit.titleLabel.font = [UIFont fontWithName:@"Roboto-regular" size:15];
    [buttonSubmit setBackgroundColor:[UIColor grayColor]];
    buttonSubmit.userInteractionEnabled = YES;
    [buttonSubmit addTarget:self action:@selector(ButtonSubmitPressed) forControlEvents:UIControlEventTouchUpInside];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!(section == arrQuestions.count)) {
        return 5;
    }
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    FeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FeedbackTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    
    
    
    if (indexPath.section == 3) {
        
        UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonSubmit.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 80.0, 10.0 , 160.0, 40.0);
        [cell.contentView addSubview:buttonSubmit];
        [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        buttonSubmit.titleLabel.font = [UIFont fontWithName:@"Roboto-regular" size:15];
        [buttonSubmit setBackgroundColor:[UIColor grayColor]];
        buttonSubmit.userInteractionEnabled = YES;
        [buttonSubmit addTarget:self action:@selector(ButtonSubmitPressed) forControlEvents:UIControlEventAllEvents];
    }
    
    
    else
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(20.0, 0.0, 15.0, 15.0);
        [cell.contentView addSubview:button];
        
        button.layer.cornerRadius = 7.5;
        button.layer.borderColor=[UIColor grayColor].CGColor;
        button.layer.borderWidth = 1.0;
        
        
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(40.0, 0.0, [[UIScreen mainScreen] bounds].size.width-80, 15.0);
        [cell.contentView addSubview:label];
        label.text = [[arrAnswer valueForKey:@"answer"]objectAtIndex:indexPath.row];
        label.font = [UIFont fontWithName:@"Roboto-regular" size:13];
        label.textColor = [UIColor darkGrayColor];
        if (arrIsSelected.count>0) {
            
            for (NSDictionary *dict in arrIsSelected) {
                NSString *section = [dict valueForKey:@"section"];
                NSString *row = [dict valueForKey:@"row"];
                
                
                
                if (indexPath.section== [section integerValue] && indexPath.row == [row integerValue]) {
                    [button setBackgroundImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
                    button.layer.borderColor = [UIColor clearColor].CGColor;
                    break;
                }
            }
            
        }
        
        [button addTarget:self action:@selector(ButtonAnswerPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = indexPath.row+(indexPath.section*10);
        
    }
    return cell;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc]init];
    header.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 60);
    header.backgroundColor = [UIColor whiteColor];
    UILabel *lblBackground = [[UILabel alloc]init];
    lblBackground.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 60);
    lblBackground.backgroundColor = [UIColor clearColor];
    [header addSubview:lblBackground];
    UILabel *labelMySavedPlans = [[UILabel alloc]init];
    labelMySavedPlans.frame = CGRectMake( 20.0, 5.0, [[UIScreen mainScreen] bounds].size.width - 40, 60.0);
    [lblBackground addSubview:labelMySavedPlans];
    [labelMySavedPlans setTextColor:[UIColor darkGrayColor]];
    labelMySavedPlans.font = [UIFont fontWithName:@"Roboto-regular" size:13];
    labelMySavedPlans.numberOfLines=0;
    labelMySavedPlans.textAlignment = NSTextAlignmentLeft;
    
    
    NSString *strQuestion = [NSString stringWithFormat:@"%ld. %@",section +1, [[arrQuestions valueForKey:@"questions"]objectAtIndex:section]];
    if ([[[arrQuestions valueForKey:@"isRequired"]objectAtIndex:section] isEqualToString:@"1" ]) {
        
        
        strQuestion = [strQuestion stringByReplacingOccurrencesOfString:@"\u2019" withString:@"'"];
        strQuestion = [strQuestion stringByAppendingString:@" (required) "];
    }
    else
    {
        
        strQuestion = [strQuestion stringByReplacingOccurrencesOfString:@"\u2019" withString:@"'"];
        strQuestion = [strQuestion stringByAppendingString:@" (optional) "];
    }
    
    
    labelMySavedPlans.text =strQuestion; //[[arrQuestions valueForKey:@"questions"]objectAtIndex:section];
    
    return header;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrQuestions.count;
}

-(void)ButtonAnswerPressed:(id)sender
{
    UIButton *btn = (UIButton *)(id)sender;
    [btn setBackgroundImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
    NSLog(@"%ld",btn.tag);
    NSInteger section = btn.tag/10;
    NSInteger row = btn.tag%10;
    BOOL isFound = NO;
    if (arrIsSelected.count >0) {
        for (NSDictionary *dict in arrIsSelected) {
            if ([[dict valueForKey:@"section"] isEqualToString:[NSString stringWithFormat:@"%ld",section]]) {
                [arrIsSelected removeObject:dict];
                isFound = YES;
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%ld", (long)section] forKey:@"section"];
                [dict setObject:[NSString stringWithFormat:@"%ld", (long)row] forKey:@"row"];
                [dict setObject:[NSString stringWithFormat:@"%ld",(long)row+1] forKey:@"answer"];
                [dict setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:section]forKey:@"question"];
                [arrIsSelected addObject:dict];
                break;
            }
            
            
        }
        
        if (!isFound) {
            isFound = YES;
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSString stringWithFormat:@"%ld", (long)section] forKey:@"section"];
            [dict setObject:[NSString stringWithFormat:@"%ld", (long)row] forKey:@"row"];
            [dict setObject:[NSString stringWithFormat:@"%ld",(long)row+1] forKey:@"answer"];
            [dict setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:section]forKey:@"question"];
            [arrIsSelected addObject:dict];
        }
        
        
        
        
        
    }
    else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[NSString stringWithFormat:@"%ld", (long)section] forKey:@"section"];
        [dict setObject:[NSString stringWithFormat:@"%ld", (long)row] forKey:@"row"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)row+1] forKey:@"answer"];
        [dict setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:section]forKey:@"question"];
        [arrIsSelected addObject:dict];
    }
    [tableViewFeedBack reloadData];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 25;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!(section == arrQuestions.count)) {
        return 70;
    }
    else
    {
        return 0;
    }
    
}
-(void)ButtonPressed:(id)sender
{
    UIButton *btn = (UIButton *)(id)sender;
    NSInteger answerId = btn.tag;
    //    NSInteger questionId = answerId;
    answerId = answerId%10;
    //    questionId = questionId/10;
    //    btn.layer.borderColor = [UIColor clearColor].CGColor;
    //    [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
    
    
    
    NSMutableDictionary *dictarrName=[[NSMutableDictionary alloc]init];
    dictarrName =[[arrAnswer objectAtIndex:answerId]mutableCopy];
    [dictarrName setValue:[NSString stringWithFormat:@"%d",1] forKey:@"IsSelected"];
    // [arrNames removeObjectAtIndex:z];
    [arrAnswer replaceObjectAtIndex:answerId withObject:dictarrName];
    
    for(NSDictionary *dict in arrAnswer){
        if ([[dict valueForKey:@"IsSelected"] isEqualToString:@"1"]) {
            btn.layer.borderColor = [UIColor clearColor].CGColor;
            [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
            
        }
        
        
    }
    
    
    
    
    
    
    
    /*if (arrFeedBack.count>0) {
     
     
     
     for (NSDictionary *dict in arrFeedBack) {
     if (![[dict valueForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)answerId]]) {
     NSMutableDictionary *dictionary =[[NSMutableDictionary alloc]init];
     [arrFeedBack removeObject:dict];
     [dictionary setObject:[NSString stringWithFormat:@"%ld",(long)answerId] forKey:@"answer"];
     [dictionary setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:questionId] forKey:@"question"];
     [arrFeedBack addObject:dictionary];
     }
     
     }
     }
     else
     {
     
     NSMutableDictionary *dictionary =[[NSMutableDictionary alloc]init];
     //   [arrFeedBack removeObject:dict];
     [dictionary setObject:[NSString stringWithFormat:@"%ld",(long)answerId] forKey:@"answer"];
     [dictionary setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:questionId] forKey:@"question"];
     [arrFeedBack addObject:dictionary];
     
     
     }
     
     
     
     */
    
    
}

-(void)ButtonSubmitPressed
{
    
    if (arrIsSelected.count == 0) {
        [[CommonMethods sharedInstance]AlertAction:@"Please fill the feedback to close the trip"];
    }
    else
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        for (NSMutableDictionary *dict in arrIsSelected) {
            
            [dict removeObjectForKey:@"section"];
            [dict removeObjectForKey:@"row"];
            //        [dict removeObjectForKey:@"section"];
            
            //        [dict removeObjectForKey:@"row"];
        }
        
        NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
        NSString *accesstoken = [defualts valueForKey:@"access_token"];
        NSString *teacherId = [defualts valueForKey:@"id"];
        [dict setValue:accesstoken forKey:@"access_token"];
        [dict setValue:teacherId forKey:@"teacher_id"];
        [dict setValue:arrIsSelected forKey:@"mapping_id"];
        
        
        BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
        
        if (isinternet) {
            
            
            //http://14.141.136.170:8888/projects/Teacherapp/index.php/Feedbacks/feedbacks
            // [[CommonMethods sharedInstance] addSpinner:self.view];
            [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Feedbacks/feedbacks",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
                
                if (!error) {
                    
                    if ([[returArray valueForKey:@"success"] isEqualToString:@"success"]) {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                    
                }
                else
                {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
                    //[[CommonMethods sharedInstance] removeSpinner:self.view];
                    
                }
            }];
            
            
            
        }
        
        
    }
    
    
    //   [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}

-(void)ApiForGettingQuestions
{
    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
    if (isinternet) {
        [[CommonMethods sharedInstance] addSpinner:self.view];
        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Feedbacks/get_feedbacks?access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
            if (!error) {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                NSLog(@"returArray %@",returArray);
                arrQuestions = [[NSMutableArray alloc]init];
                if (!([[returArray valueForKey:@"result"] count] == 0)) {
                    for (int i =0; i<[[returArray valueForKey:@"result"] count]; i++) {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"is_required"] objectAtIndex:i]  forKey:@"isRequired"];
                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"id"] objectAtIndex:i] forKey:@"id"];
                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"questions"] objectAtIndex:i ] forKey:@"questions"];
                        //   [dict setObject:@"Strongly Disagree" forKey:@"1"];
                        
                        [arrQuestions addObject:dict];
                    }
                    
                }
                
                [self CreateUiForFeedBack];
                // [tableViewFeedBack reloadData];
                
            }
            else
            {
                [[CommonMethods sharedInstance] removeSpinner:self.view];
                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
                
            }
        }];
        
    }else
    {
        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
        
        
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end













//
//#import "FeedBackViewController.h"
//#import "CommonMethods.h"
//#import "WebService.h"
//#import "FeedbackTableViewCell.h"
//
//@interface FeedBackViewController ()<UITableViewDelegate,UITableViewDataSource>
//{
//    NSMutableArray *arrQuestions;
//    UITableView *tableViewFeedBack;
//    NSMutableString *stringButton;
//    NSMutableArray *arrFeedBack;
//    BOOL isFirst;
//    BOOL isSecond;
//    BOOL isThird;
//    BOOL isFourth;
//    BOOL isFifth;
//    NSMutableArray *arrAnswer;
//}
//@end
//
//@implementation FeedBackViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    arrAnswer=[[NSMutableArray alloc]init];
//
//   NSArray *arr=[[NSArray alloc]initWithObjects:@"Strongly Disagree",@"Disagree",@"Neutral",@"Agree",@"Strongly Agree", nil];
//    for (int k=0; k<5; k++) {
//         NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//        [dict setValue:[arr objectAtIndex:k] forKey:@"answer"];
//         [dict setValue:[NSString stringWithFormat:@"%d",0] forKey:@"IsSelected"];
//        [arrAnswer addObject:dict];
//
//    }
//    arr=nil;
//
//
//    [self UiForNavigationBar];
//    [self ApiForGettingQuestions];
//    stringButton = [[NSMutableString alloc]init];
//    arrFeedBack = [[NSMutableArray alloc]init];
//
//    // Do any additional setup after loading the view.
//}
//
//-(void)UiForNavigationBar
//{
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112.0/255.0 green:100.0/255.0 blue:202.0/255.0 alpha:1.0];
//    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnBack.frame = CGRectMake(10.0, 10.0, 30.0, 25.0);
//    [btnBack setImage:[UIImage imageNamed:@"Back_button"] forState:UIControlStateNormal];
//    [btnBack addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
//    self.navigationItem.leftBarButtonItem =mailbutton;
//    [self setNeedsStatusBarAppearanceUpdate];
//    self.navigationItem.title =@"Feedback";
//    NSShadow *shadow = [[NSShadow alloc] init];
//    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
//    shadow.shadowOffset = CGSizeMake(0, 2);
//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
//                                                           [UIFont fontWithName:@"Roboto-regular" size:18.0], NSFontAttributeName, nil]];
//
//}
//
//-(void)BackButtonPressed
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//
//-(void)CreateUiForFeedBack
//{
//    UILabel *label = [[UILabel alloc]init];
//    label.frame = CGRectMake(0.0, 72.0, [[UIScreen mainScreen] bounds].size.width, 50);
//    [self.view addSubview:label];
//    label.text = @"Please answer this quick, three answer survey";
//    label.textColor= [UIColor redColor];
//    label.font = [UIFont fontWithName:@"Roboto-regular" size:13];
//    label.textAlignment = NSTextAlignmentCenter;
//
//    tableViewFeedBack = [[UITableView alloc]init];
//    tableViewFeedBack.frame = CGRectMake(0.0, 122, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-122);
//    [self.view addSubview:tableViewFeedBack];
//    tableViewFeedBack.separatorColor = [UIColor clearColor];
//    tableViewFeedBack.delegate = self;
//    tableViewFeedBack.dataSource = self;
//  //  tableViewFeedBack.backgroundColor = [UIColor grayColor];
//
//
//}
//
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return arrQuestions.count+1;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *cellIdentifier = @"cellIdentifier";
//    FeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FeedbackTableViewCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//
//   }
//
//        if (indexPath.row == arrQuestions.count) {
//
//            cell.buttonAnswer1.hidden = YES;
//            cell.buttonAnswer2.hidden = YES;
//            cell.buttonAnswer3.hidden = YES;
//            cell.buttonAnswer4.hidden = YES;
//            cell.buttonAnswer5.hidden = YES;
//            cell.labelQuestion.hidden = YES;
//        UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
//        buttonSubmit.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2-50, 20.0, 100, 40);
//        [cell.contentView addSubview:buttonSubmit];
//        [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
//        [buttonSubmit setBackgroundColor:[UIColor grayColor]];
//        [buttonSubmit addTarget:self action:@selector(ButtonSubmitPressed) forControlEvents:UIControlEventTouchUpInside];
//
//    }
//
//    else
//    {
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        NSString *strQuestion = [NSString stringWithFormat:@"%d. %@",indexPath.row +1, [[arrQuestions valueForKey:@"questions"]objectAtIndex:indexPath.row]];
//        if ([[[arrQuestions valueForKey:@"isRequired"]objectAtIndex:indexPath.row] isEqualToString:@"1" ]) {
//
//
//            strQuestion = [strQuestion stringByReplacingOccurrencesOfString:@"\u2019" withString:@"'"];
//            strQuestion = [strQuestion stringByAppendingString:@" (required) "];
//        }
//        else
//        {
//
//            strQuestion = [strQuestion stringByReplacingOccurrencesOfString:@"\u2019" withString:@"'"];
//            strQuestion = [strQuestion stringByAppendingString:@" (optional) "];
//        }
//
//        cell.labelQuestion.text = strQuestion;
//        cell.labelQuestion.numberOfLines = 0;
//        cell.labelQuestion.font = [UIFont fontWithName:@"Roboto-regular" size:13];
//
//
//        cell.labelanswer1.text = [[arrAnswer objectAtIndex:0] valueForKey:@"answer"];
//        cell.labelanswer2.text = [[arrAnswer objectAtIndex:1] valueForKey:@"answer"];
//        cell.labelanswer3.text = [[arrAnswer objectAtIndex:2] valueForKey:@"answer"];
//        cell.labelanswer4.text = [[arrAnswer objectAtIndex:3] valueForKey:@"answer"];
//        cell.labelanswer5.text = [[arrAnswer objectAtIndex:4] valueForKey:@"answer"];
//        cell.labelanswer1.font = [UIFont fontWithName:@"Roboto-regular" size:13];
//
//
//    }
//
//    return cell;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == arrQuestions.count) {
//        return 80;
//    }
//    else
//    return 200;
//}
//
//
//-(void)ButtonPressed:(id)sender
//{
//    UIButton *btn = (UIButton *)(id)sender;
//    NSInteger answerId = btn.tag;
////    NSInteger questionId = answerId;
//    answerId = answerId%10;
////    questionId = questionId/10;
////    btn.layer.borderColor = [UIColor clearColor].CGColor;
////    [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
//
//
//
//    NSMutableDictionary *dictarrName=[[NSMutableDictionary alloc]init];
//    dictarrName =[[arrAnswer objectAtIndex:answerId]mutableCopy];
//    [dictarrName setValue:[NSString stringWithFormat:@"%d",1] forKey:@"IsSelected"];
//    // [arrNames removeObjectAtIndex:z];
//    [arrAnswer replaceObjectAtIndex:answerId withObject:dictarrName];
//
//    for(NSDictionary *dict in arrAnswer){
//        if ([[dict valueForKey:@"IsSelected"] isEqualToString:@"1"]) {
//            btn.layer.borderColor = [UIColor clearColor].CGColor;
//        [btn setImage:[UIImage imageNamed:@"green_radio"] forState:UIControlStateNormal];
//
//        }
//
//
//    }
//
//
//
//
//
//
//
//    /*if (arrFeedBack.count>0) {
//
//
//
//    for (NSDictionary *dict in arrFeedBack) {
//        if (![[dict valueForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)answerId]]) {
//            NSMutableDictionary *dictionary =[[NSMutableDictionary alloc]init];
//            [arrFeedBack removeObject:dict];
//            [dictionary setObject:[NSString stringWithFormat:@"%ld",(long)answerId] forKey:@"answer"];
//            [dictionary setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:questionId] forKey:@"question"];
//            [arrFeedBack addObject:dictionary];
//        }
//
//    }
//    }
//    else
//    {
//
//            NSMutableDictionary *dictionary =[[NSMutableDictionary alloc]init];
//            //   [arrFeedBack removeObject:dict];
//            [dictionary setObject:[NSString stringWithFormat:@"%ld",(long)answerId] forKey:@"answer"];
//            [dictionary setObject:[[arrQuestions valueForKey:@"id"] objectAtIndex:questionId] forKey:@"question"];
//            [arrFeedBack addObject:dictionary];
//
//
//    }
//
//
//
//
//
//
//}
//
//-(void)ButtonSubmitPressed
//{
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//
//
//
//    NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
//    NSString *accesstoken = [defualts valueForKey:@"access_token"];
//    NSString *teacherId = [defualts valueForKey:@"id"];
//    [dict setValue:accesstoken forKey:@"access_token"];
//    [dict setValue:teacherId forKey:@"teacher_id"];
//    [dict setValue:arrFeedBack forKey:@"mapping_id"];
//
//
//    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
//
//    if (isinternet) {
//
//
//        //http://14.141.136.170:8888/projects/Teacherapp/index.php/Feedbacks/feedbacks
//        // [[CommonMethods sharedInstance] addSpinner:self.view];
//        [[WebService sharedInstance] SignUpApi:[NSString stringWithFormat:@"%@index.php/Feedbacks/feedbacks",kBaseUrl] andParam:dict andcompletionhandler:^(NSArray *returArray, NSError *error) {
//
//            if (!error) {
//
//                if ([[returArray valueForKey:@"success"] isEqualToString:@"success"]) {
//                [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//
//
//            }
//            else
//            {  [[CommonMethods sharedInstance] AlertAction:@"Server error"];
//                //[[CommonMethods sharedInstance] removeSpinner:self.view];
//
//            }
//        }];
//
//
//
//    }
//
//
//
//
//
//}
//
//-(void)ApiForGettingQuestions
//{
//    BOOL isinternet =[[CommonMethods sharedInstance] checkInternetConnection];
//    if (isinternet) {
//        [[CommonMethods sharedInstance] addSpinner:self.view];
//        [[WebService sharedInstance]ApiOfgetMethod:[NSString stringWithFormat:@"%@index.php/Feedbacks/get_feedbacks?access_token=%@",kBaseUrl,[[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"]] andcompletionhandler:^(NSArray *returArray, NSError *error){
//            if (!error) {
//                [[CommonMethods sharedInstance] removeSpinner:self.view];
//                NSLog(@"returArray %@",returArray);
//                arrQuestions = [[NSMutableArray alloc]init];
//                if (!([[returArray valueForKey:@"result"] count] == 0)) {
//                    for (int i =0; i<[[returArray valueForKey:@"result"] count]; i++) {
//                        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"is_required"] objectAtIndex:i]  forKey:@"isRequired"];
//                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"id"] objectAtIndex:i] forKey:@"id"];
//                        [dict setObject:[[[returArray valueForKey:@"result" ] valueForKey:@"questions"] objectAtIndex:i ] forKey:@"questions"];
//                     //   [dict setObject:@"Strongly Disagree" forKey:@"1"];
//
//                        [arrQuestions addObject:dict];
//                    }
//
//                 }
//
//                 [self CreateUiForFeedBack];
//               // [tableViewFeedBack reloadData];
//
//            }
//            else
//            {
//                [[CommonMethods sharedInstance] removeSpinner:self.view];
//                [[CommonMethods sharedInstance] AlertAction:@"Server Error"];
//
//            }
//        }];
//
//    }else
//    {
//        [[CommonMethods sharedInstance] AlertAction:@"Please check your internet connection"];
//
//
//    }
//
//
//}
//
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//
//
//@end
