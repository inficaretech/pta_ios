//
//  StudentListTableViewCell.m
//  Putnam Teacher's App
//
//  Created by Inficare on 6/29/16.
//  Copyright © 2016 inficare. All rights reserved.
//

#import "StudentListTableViewCell.h"

@implementation StudentListTableViewCell
@synthesize lblName,lbClass,lblColour,lblContact1,lblContact2;
@synthesize btnDelete,btnEdit;
@synthesize imageCamera,imageAllergy,imageMedicine,imageSpecialNeeds,imageSpendingMoney;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    CALayer *border = [CALayer layer];
//    CGFloat borderWidth = 2;
//    border.borderColor = [UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1].CGColor;
//    border.frame = CGRectMake(0, lblName.frame.size.height - borderWidth, lblName.frame.size.width, lblName.frame.size.height);
//    border.borderWidth = borderWidth;
//    [lblName.layer addSublayer:border];
//    lblName.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
